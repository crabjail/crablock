#!/bin/bash

set -e

export LLVM_PROFILE_FILE="/tmp/crablock_%m_%p.profraw"
export RUSTFLAGS="-Cinstrument-coverage"

rm -rf /tmp/crablock_*.profraw crablock.profdata crablock-coverage-report
cargo clean
cargo build -p crabreaper
cargo build -p cl-test
cargo test

grcov /tmp/crablock_*.profraw --source-dir . --binary-path ./target/debug/ --output-types html --branch --ignore-not-existing --output-path=crablock-coverage-report

# llvm-profdata merge -sparse /tmp/crablock_*.profraw -o crablock.profdata
# llvm-cov show --ignore-filename-regex='/.cargo' --format=html --output-dir=crablock-coverage-report --show-line-counts-or-regions --coverage-watermark=90,60 --instr-profile=crablock.profdata --object=target/debug/crablock --object=target/debug/crabreaper
