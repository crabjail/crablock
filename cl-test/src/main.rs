/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::env::current_dir;
use std::ffi::{CString, c_char};
use std::fs::read_to_string;
use std::io::{Error as IoError, ErrorKind as IoErrorKind, Result as IoResult};
use std::net::{TcpListener, TcpStream};
use std::num::ParseIntError;
use std::os::unix::prelude::*;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::str::FromStr;
use std::{env, fs, process};

use capctl::{
    Cap, CapSet, CapState, MDWEFlags, SpecFlags, SpecVariant, ambient, bounding, get_mdwe,
    get_no_new_privs, get_speculation_ctrl,
};
use clap::Parser;
use linux_raw_sys::general::{
    CGROUP2_SUPER_MAGIC, DEVPTS_SUPER_MAGIC, PROC_SUPER_MAGIC, SYSFS_MAGIC,
};
use procfs::process::Process;
use rustix::fs::{
    AtFlags, CWD, Mode, OFlags, StatxFlags, mkdir, open, readlink, rmdir, statfs, statx, symlink,
    unlink,
};
use rustix::io::Errno;
use rustix::process::{
    Pid, getegid, geteuid, getgid, getpid, getpriority_process, getsid, getuid, umask,
};
use rustix::system::uname;
use syscalls::Sysno;

// While linux uses this SUPER_MAGIC in its uapi, it is missing in the uapi headers.
const MQUEUE_MAGIC: u32 = 0x19800202;

// assert_matches! in std is unstable, see
// https://github.com/rust-lang/rust/issues/82775
macro_rules! assert_matches {
    ($left:expr, $pattern:pat $(if $guard:expr)?, $($arg:tt)+) => {
        assert!(matches!($left, $pattern $(if $guard)?), $($arg)*)
    };
}

fn truncate<P: AsRef<Path>>(path: P, length: i64) -> IoResult<()> {
    let _path = CString::new(path.as_ref().as_os_str().as_bytes())?;
    let path: *const c_char = _path.as_ptr();

    if unsafe { libc::truncate(path, length) } != 0 {
        return Err(IoError::last_os_error());
    }

    Ok(())
}

#[derive(Parser, Debug)]
struct Args {
    /* TODO
     * --help
     * --version
     * --args-lf / --args-nul
     */
    #[arg(long)]
    assert_unshare_cgroup: Option<String>,

    #[arg(long)]
    assert_share_cgroup: Option<String>,

    #[arg(long)]
    assert_unshare_ipc: Option<String>,

    #[arg(long)]
    assert_share_ipc: Option<String>,

    #[arg(long)]
    assert_unshare_mnt: Option<String>,

    #[arg(long)]
    assert_share_mnt: Option<String>,

    #[arg(long)]
    assert_unshare_net: Option<String>,

    #[arg(long)]
    assert_share_net: Option<String>,

    #[arg(long)]
    assert_unshare_pid: Option<String>,

    #[arg(long)]
    assert_share_pid: Option<String>,

    #[arg(long)]
    assert_unshare_user: Option<String>,

    #[arg(long)]
    assert_share_user: Option<String>,

    #[arg(long)]
    assert_unshare_uts: Option<String>,

    #[arg(long)]
    assert_share_uts: Option<String>,

    #[arg(long)]
    assert_uid: Option<u32>,

    #[arg(long)]
    assert_euid: Option<u32>,

    #[arg(long)]
    assert_gid: Option<u32>,

    #[arg(long)]
    assert_egid: Option<u32>,

    #[arg(long)]
    assert_uid_map: Option<String>,

    #[arg(long)]
    assert_gid_map: Option<String>,

    #[arg(long, value_delimiter = ',')]
    assert_capabilities_eff: Option<Vec<Cap>>,

    #[arg(long)]
    assert_capabilities_eff_empty: bool,

    #[arg(long, value_delimiter = ',')]
    assert_capabilities_bnd: Option<Vec<Cap>>,

    #[arg(long)]
    assert_capabilities_bnd_empty: bool,

    #[arg(long, value_delimiter = ',')]
    assert_capabilities_amb: Option<Vec<Cap>>,

    #[arg(long)]
    assert_capabilities_amb_empty: bool,

    #[arg(long)]
    assert_securebits: Option<String>,

    #[arg(long)]
    assert_no_new_privs: bool,

    #[arg(long)]
    assert_mount_cgroup2: Option<u64>,

    #[arg(long)]
    assert_mount_devpts: Option<u32>,

    #[arg(long)]
    assert_mount_mqueue: Option<u32>,

    #[arg(long)]
    assert_mount_proc: Option<u32>,

    #[arg(long)]
    assert_procfs_subset: Option<String>,

    #[arg(long)]
    assert_procfs_no_subset: bool,

    #[arg(long)]
    assert_procfs_ro: bool,

    #[arg(long)]
    assert_procfs_rw: bool,

    #[arg(long)]
    assert_mount_sysfs: Option<u32>,

    // TODO: --fsm
    #[arg(long)]
    assert_mountpoint: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_tmpfs: Option<Vec<PathBuf>>,

    // TODO: --assert-[no-]{file,dir}(-exists)?=PATH,mode
    #[arg(long)]
    assert_mount_option_ro: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_rw: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_nosuid: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_suid: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_nodev: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_dev: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_noexec: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mount_option_exec: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_cwd: Option<PathBuf>,

    #[arg(long)]
    assert_open_rdonly: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_open_rdonly: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_open_wronly: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_open_wronly: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_open_wronly_trunc: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_open_wronly_trunc: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_truncate: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_truncate: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_open_rdwr: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_open_rdwr: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_execve: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_execve: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_open_directory: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_open_directory: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_rmdir: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_rmdir: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_unlink: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_unlink: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_mkdir: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_mkdir: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_open_creat_excl: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_open_creat_excl: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_symlink: Option<Vec<PathBuf>>,

    #[arg(long)]
    assert_not_symlink: Option<Vec<PathBuf>>,

    // socket FIFO char block

    // #[arg(long)]
    // assert_rename: Option<Vec<(PathBuf, PathBuf)>>,

    // #[arg(long)]
    // assert_not_rename: Option<Vec<(PathBuf, PathBuf)>>,
    #[arg(long)]
    assert_bind_tcp: Option<Vec<String>>,

    #[arg(long)]
    assert_not_bind_tcp: Option<Vec<String>>,

    #[arg(long)]
    assert_connect_tcp: Option<Vec<String>>,

    #[arg(long)]
    assert_not_connect_tcp: Option<Vec<String>>,

    #[arg(long)]
    assert_hostname: Option<String>,

    // TODO: pasta
    #[arg(long, value_delimiter = ',')]
    assert_seccomp_action_kill: Option<Vec<String>>,

    #[arg(long, value_delimiter = ',')]
    assert_seccomp_action_eperm: Option<Vec<String>>,

    #[arg(long, value_delimiter = ',')]
    assert_seccomp_action_enosys: Option<Vec<String>>,

    #[arg(long, value_delimiter = ',')]
    assert_seccomp_action_log_allow: Option<Vec<String>>,

    #[arg(long)]
    assert_seccomp_socket: Option<String>,

    #[arg(long)]
    assert_seccomp_memfd_noexec: bool,

    #[arg(long)]
    assert_seccomp_deny_clone_newuser: bool,

    #[arg(long)]
    assert_seccomp_deny_memory_write_execute: bool,

    #[arg(long)]
    assert_sysctl: Option<Vec<String>>,

    #[arg(long)]
    assert_oom_score_adj: Option<u32>,

    #[arg(long)]
    assert_nice: Option<i32>,

    #[arg(long)]
    assert_env_set: Option<Vec<String>>,

    #[arg(long)]
    assert_env_not_set: Option<Vec<String>>,

    #[arg(long)]
    assert_mdwe_refuse_exec_gain: bool,

    #[arg(long)]
    assert_new_session: Option<i32>,

    #[arg(long)]
    assert_strict_mitigations: bool,

    #[arg(long, value_parser = octal_u32)]
    assert_umask: Option<u32>,

    #[arg(long)]
    assert_is_pid1: bool,

    #[arg(long)]
    assert_is_not_pid1: bool,
    // TODO: dbus_proxy
}

fn octal_u32(arg: &str) -> Result<u32, ParseIntError> {
    u32::from_str_radix(arg, 8)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();

    if let Some(id) = args.assert_unshare_cgroup {
        assert_ne!(
            readlink("/proc/self/ns/cgroup", [])?.into_string()?,
            id,
            "assert-unshare-cgroup"
        );
    }

    if let Some(id) = args.assert_share_cgroup {
        assert_eq!(
            readlink("/proc/self/ns/cgroup", [])?.into_string()?,
            id,
            "assert-share-cgroup"
        );
    }

    if let Some(id) = args.assert_unshare_ipc {
        assert_ne!(
            readlink("/proc/self/ns/ipc", [])?.into_string()?,
            id,
            "assert-unshare-ipc"
        );
    }

    if let Some(id) = args.assert_share_ipc {
        assert_eq!(
            readlink("/proc/self/ns/ipc", [])?.into_string()?,
            id,
            "assert-share-ipc"
        );
    }

    if let Some(id) = args.assert_unshare_mnt {
        assert_ne!(
            readlink("/proc/self/ns/mnt", [])?.into_string()?,
            id,
            "assert-unshare-mnt"
        );
    }

    if let Some(id) = args.assert_share_mnt {
        assert_eq!(
            readlink("/proc/self/ns/mnt", [])?.into_string()?,
            id,
            "assert-share-mnt"
        );
    }

    if let Some(id) = args.assert_unshare_net {
        assert_ne!(
            readlink("/proc/self/ns/net", [])?.into_string()?,
            id,
            "assert-unshare-net"
        );
    }

    if let Some(id) = args.assert_share_net {
        assert_eq!(
            readlink("/proc/self/ns/net", [])?.into_string()?,
            id,
            "assert-share-net"
        );
    }

    if let Some(id) = args.assert_unshare_pid {
        assert_ne!(
            readlink("/proc/self/ns/pid", [])?.into_string()?,
            id,
            "assert-unshare-pid"
        );
    }

    if let Some(id) = args.assert_share_pid {
        assert_eq!(
            readlink("/proc/self/ns/pid", [])?.into_string()?,
            id,
            "assert-share-pid"
        );
    }

    if let Some(id) = args.assert_unshare_user {
        assert_ne!(
            readlink("/proc/self/ns/user", [])?.into_string()?,
            id,
            "assert-unshare-user"
        );
    }

    if let Some(id) = args.assert_share_user {
        assert_eq!(
            readlink("/proc/self/ns/user", [])?.into_string()?,
            id,
            "assert-share-user"
        );
    }

    if let Some(id) = args.assert_unshare_uts {
        assert_ne!(
            readlink("/proc/self/ns/uts", [])?.into_string()?,
            id,
            "assert-unshare-uts"
        );
    }

    if let Some(id) = args.assert_share_uts {
        assert_eq!(
            readlink("/proc/self/ns/uts", [])?.into_string()?,
            id,
            "assert-share-uts"
        );
    }

    if let Some(uid) = args.assert_uid {
        assert_eq!(uid, getuid().as_raw(), "assert-uid");
    }

    if let Some(uid) = args.assert_euid {
        assert_eq!(uid, geteuid().as_raw(), "assert-euid");
    }

    if let Some(gid) = args.assert_gid {
        assert_eq!(gid, getgid().as_raw(), "assert-gid");
    }

    if let Some(gid) = args.assert_egid {
        assert_eq!(gid, getegid().as_raw(), "assert-egid");
    }

    if let Some(uid_map) = args.assert_uid_map {
        assert_eq!(
            uid_map,
            read_to_string("/proc/self/uid_map")?,
            "assert-uid-map"
        );
    }

    if let Some(gid_map) = args.assert_gid_map {
        assert_eq!(
            gid_map,
            read_to_string("/proc/self/gid_map")?,
            "assert-gid-map"
        );
    }

    if let Some(caps) = args.assert_capabilities_eff {
        assert_eq!(
            CapSet::from_iter(caps),
            CapState::get_current()?.effective,
            "assert-capabilities-eff"
        );
    }

    if args.assert_capabilities_eff_empty {
        assert!(
            CapState::get_current()?.effective.is_empty(),
            "assert-capabilities-eff-empty"
        );
    }

    if let Some(caps) = args.assert_capabilities_bnd {
        assert_eq!(
            CapSet::from_iter(caps),
            bounding::probe(),
            "assert-capabilities-bnd"
        );
    }

    if args.assert_capabilities_bnd_empty {
        assert!(
            bounding::probe().is_empty(),
            "assert-capabilities-bnd-empty"
        );
    }

    if let Some(caps) = args.assert_capabilities_amb {
        assert_eq!(
            CapSet::from_iter(caps),
            ambient::probe().unwrap(),
            "assert-capabilities-amb"
        );
    }

    if args.assert_capabilities_amb_empty {
        assert!(
            ambient::probe().unwrap().is_empty(),
            "assert-capabilities-amb-empty"
        );
    }

    if let Some(_secbit) = args.assert_securebits {
        unimplemented!();
    }

    if args.assert_no_new_privs {
        assert!(get_no_new_privs()?, "assert-no-new-privs");
    }

    let mountinfo = Process::myself()?.mountinfo()?;

    if let Some(mnt_id) = args.assert_mount_cgroup2 {
        let f_type = statfs("/sys/fs/cgroup")?.f_type;
        // FIXME: STATX_MNT_ID_UNIQUE
        let stx = statx(CWD, "/sys/fs/cgroup", AtFlags::empty(), StatxFlags::MNT_ID)?;
        assert!(
            f_type == CGROUP2_SUPER_MAGIC as i64 && stx.stx_mnt_id != mnt_id,
            "assert-mount-cgroup2"
        );
    }

    if let Some(dev_minor) = args.assert_mount_devpts {
        let f_type = statfs("/dev/pts")?.f_type;
        let stx = statx(CWD, "/dev/pts", AtFlags::empty(), StatxFlags::empty())?;
        assert!(
            f_type == DEVPTS_SUPER_MAGIC as i64 && stx.stx_dev_major != dev_minor,
            "assert-mount-devpts"
        );
    }

    if let Some(dev_minor) = args.assert_mount_mqueue {
        let f_type = statfs("/dev/mqueue")?.f_type;
        let stx = statx(CWD, "/dev/mqueue", AtFlags::empty(), StatxFlags::empty())?;
        assert!(
            f_type == MQUEUE_MAGIC as i64 && stx.stx_dev_minor != dev_minor,
            "assert-mount-mqueue"
        );
    }

    if let Some(dev_minor) = args.assert_mount_proc {
        let f_type = statfs("/proc")?.f_type;
        let stx = statx(CWD, "/proc", AtFlags::empty(), StatxFlags::empty())?;
        assert!(
            f_type == PROC_SUPER_MAGIC as i64 && stx.stx_dev_minor != dev_minor,
            "assert-mount-proc"
        );
    }

    if let Some(subset) = args.assert_procfs_subset {
        assert!(
            mountinfo
                .0
                .iter()
                .rfind(|m| m.mount_point == Path::new("/proc"))
                .is_some_and(|m| {
                    m.fs_type == "proc"
                        && m.super_options
                            .get("subset")
                            .and_then(|opt| opt.as_deref())
                            .is_some_and(|ss| ss == subset)
                }),
            "assert-procfs-subset"
        );
    }

    if args.assert_procfs_no_subset {
        assert!(
            mountinfo
                .0
                .iter()
                .rfind(|m| m.mount_point == Path::new("/proc"))
                .is_some_and(|m| m.fs_type == "proc" && !m.super_options.contains_key("subset")),
            "assert-procfs-no-subset"
        );
    }

    if args.assert_procfs_ro {
        assert!(
            mountinfo
                .0
                .iter()
                .rfind(|m| m.mount_point == Path::new("/proc"))
                .is_some_and(|m| m.fs_type == "proc" && m.super_options.contains_key("ro")),
            "assert-procfs-ro"
        );
    }

    if args.assert_procfs_rw {
        assert!(
            mountinfo
                .0
                .iter()
                .rfind(|m| m.mount_point == Path::new("/proc"))
                .is_some_and(|m| m.fs_type == "proc" && m.super_options.contains_key("rw")),
            "assert-procfs-rw"
        );
    }

    if let Some(dev_minor) = args.assert_mount_sysfs {
        let f_type = statfs("/sys")?.f_type;
        let stx = statx(CWD, "/sys", AtFlags::empty(), StatxFlags::empty())?;
        assert!(
            f_type == SYSFS_MAGIC as i64 && stx.stx_dev_minor != dev_minor,
            "assert-mount-sysfs"
        );
    }

    if let Some(paths) = args.assert_mountpoint {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some(),
                "assert-mountpoint"
            );
        }
    }

    if let Some(paths) = args.assert_tmpfs {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| m.fs_type == "tmpfs" && m.root == "/"),
                "assert-tmpfs"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_ro {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| m.mount_options.contains_key("ro")),
                "assert-mount-option-ro"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_rw {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| m.mount_options.contains_key("rw")),
                "assert-mount-option-rw"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_nosuid {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| m.mount_options.contains_key("nosuid")),
                "assert-mount-option-nosuid"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_suid {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| !m.mount_options.contains_key("nosuid")),
                "assert-mount-option-suid"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_nodev {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| m.mount_options.contains_key("nodev")),
                "assert-mount-option-nodev"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_dev {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| !m.mount_options.contains_key("nodev")),
                "assert-mount-option-dev"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_noexec {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| m.mount_options.contains_key("noexec")),
                "assert-mount-option-noexec"
            );
        }
    }

    if let Some(paths) = args.assert_mount_option_exec {
        for path in paths {
            assert!(
                mountinfo
                    .0
                    .iter()
                    .rfind(|m| m.mount_point == path)
                    .is_some_and(|m| !m.mount_options.contains_key("noexec")),
                "assert-mount-option-exec"
            );
        }
    }

    if let Some(path) = args.assert_cwd {
        assert_eq!(path, current_dir()?, "assert-cwd");
    }

    if let Some(hostname) = args.assert_hostname {
        assert_eq!(hostname, uname().nodename().to_str()?, "assert-hostname");
    }

    if let Some(paths) = args.assert_open_rdonly {
        for path in paths {
            assert!(
                open(&path, OFlags::CLOEXEC | OFlags::RDONLY, Mode::empty()).is_ok(),
                "assert-open-rdonly"
            );
        }
    }

    if let Some(paths) = args.assert_not_open_rdonly {
        for path in paths {
            assert_matches!(
                open(&path, OFlags::CLOEXEC | OFlags::RDONLY, Mode::empty()),
                Err(Errno::ACCESS),
                "assert-not-open-rdonly"
            );
        }
    }

    if let Some(paths) = args.assert_open_wronly {
        for path in paths {
            assert!(
                open(&path, OFlags::CLOEXEC | OFlags::WRONLY, Mode::empty()).is_ok(),
                "assert-open-wronly"
            );
        }
    }

    if let Some(paths) = args.assert_not_open_wronly {
        for path in paths {
            assert_matches!(
                open(&path, OFlags::CLOEXEC | OFlags::WRONLY, Mode::empty()),
                Err(Errno::ACCESS),
                "assert-not-open-wronly"
            );
        }
    }

    if let Some(paths) = args.assert_open_wronly_trunc {
        for path in paths {
            assert!(
                open(
                    &path,
                    OFlags::CLOEXEC | OFlags::WRONLY | OFlags::TRUNC,
                    Mode::empty()
                )
                .is_ok(),
                "assert-open-wronly-trunc"
            );
        }
    }

    if let Some(paths) = args.assert_not_open_wronly_trunc {
        for path in paths {
            assert_matches!(
                open(
                    &path,
                    OFlags::CLOEXEC | OFlags::WRONLY | OFlags::TRUNC,
                    Mode::empty()
                ),
                Err(Errno::ACCESS),
                "assert-not-open-wronly-trunc"
            );
        }
    }

    if let Some(paths) = args.assert_truncate {
        for path in paths {
            assert!(truncate(&path, 0).is_ok(), "assert-truncate");
        }
    }

    if let Some(paths) = args.assert_not_truncate {
        for path in paths {
            assert_eq!(
                truncate(&path, 0).expect_err("assert-not-truncate").kind(),
                IoErrorKind::PermissionDenied,
                "assert-not-truncate"
            );
        }
    }

    if let Some(paths) = args.assert_open_rdwr {
        for path in paths {
            assert!(
                open(&path, OFlags::CLOEXEC | OFlags::RDWR, Mode::empty()).is_ok(),
                "assert-open-rdwr"
            );
        }
    }

    if let Some(paths) = args.assert_not_open_rdwr {
        for path in paths {
            assert_matches!(
                open(&path, OFlags::CLOEXEC | OFlags::RDWR, Mode::empty()),
                Err(Errno::ACCESS),
                "assert-not-open-rdwr"
            );
        }
    }

    if let Some(paths) = args.assert_execve {
        for path in paths {
            assert!(Command::new(path).spawn().is_ok(), "assert-execve");
        }
    }

    if let Some(paths) = args.assert_not_execve {
        for path in paths {
            assert!(
                fs::metadata(&path)?.permissions().mode() & 0o111 == 0o111,
                "assert-not-execve (non-executable file)"
            );
            assert!(Command::new(path).spawn().is_err(), "assert-not-execve");
        }
    }

    if let Some(paths) = args.assert_open_directory {
        for path in paths {
            assert!(
                open(
                    &path,
                    OFlags::CLOEXEC | OFlags::DIRECTORY | OFlags::RDONLY,
                    Mode::empty()
                )
                .is_ok(),
                "assert-open-directory"
            );
        }
    }

    if let Some(paths) = args.assert_not_open_directory {
        for path in paths {
            assert_matches!(
                open(
                    &path,
                    OFlags::CLOEXEC | OFlags::DIRECTORY | OFlags::RDONLY,
                    Mode::empty()
                ),
                Err(Errno::ACCESS),
                "assert-not-open-directory"
            );
        }
    }

    if let Some(paths) = args.assert_rmdir {
        for path in paths {
            assert!(rmdir(path).is_ok(), "assert-rmdir");
        }
    }

    if let Some(paths) = args.assert_not_rmdir {
        for path in paths {
            assert_matches!(rmdir(path), Err(Errno::ACCESS), "assert-not-rmdir");
        }
    }

    if let Some(paths) = args.assert_unlink {
        for path in paths {
            assert!(unlink(path).is_ok(), "assert-unlink");
        }
    }

    if let Some(paths) = args.assert_not_unlink {
        for path in paths {
            assert_matches!(unlink(path), Err(Errno::ACCESS), "assert-not-unlink");
        }
    }

    if let Some(paths) = args.assert_mkdir {
        for path in paths {
            assert!(mkdir(path, Mode::empty()).is_ok(), "assert-mkdir");
        }
    }

    if let Some(paths) = args.assert_not_mkdir {
        for path in paths {
            assert_matches!(
                mkdir(path, Mode::empty()),
                Err(Errno::ACCESS),
                "assert-not-mkdir"
            );
        }
    }

    if let Some(paths) = args.assert_open_creat_excl {
        for path in paths {
            assert!(
                open(
                    path,
                    OFlags::CLOEXEC | OFlags::CREATE | OFlags::EXCL,
                    Mode::empty()
                )
                .is_ok(),
                "assert-open-creat-excl"
            );
        }
    }

    if let Some(paths) = args.assert_not_open_creat_excl {
        for path in paths {
            assert_matches!(
                open(
                    path,
                    OFlags::CLOEXEC | OFlags::CREATE | OFlags::EXCL,
                    Mode::empty()
                ),
                Err(Errno::ACCESS),
                "assert-not-open-creat-excl"
            );
        }
    }

    if let Some(paths) = args.assert_symlink {
        for path in paths {
            assert!(symlink("ferris", path).is_ok(), "assert-symlink");
        }
    }

    if let Some(paths) = args.assert_not_symlink {
        for path in paths {
            assert_matches!(
                symlink("ferris", path),
                Err(Errno::ACCESS),
                "assert-not-symlink"
            );
        }
    }

    /*
    If let Some(paths) = args.assert_rename {
        for (oldpath, newpath) in paths {
            assert!(rename(oldpath, newpath).is_ok(), "assert-rename");
        }
    }

    if let Some(paths) = args.assert_not_rename {
        for (oldpath, newpath) in paths {
            assert!(rename(oldpath, newpath).is_err(), "assert-not-rename");
        }
    }
    */

    if let Some(socket_addrs) = args.assert_bind_tcp {
        for addr in socket_addrs {
            assert!(TcpListener::bind(addr).is_ok(), "assert-bind-tcp");
        }
    }

    if let Some(socket_addrs) = args.assert_not_bind_tcp {
        for addr in socket_addrs {
            assert_eq!(
                TcpListener::bind(addr)
                    .expect_err("assert-not-bind-tcp")
                    .kind(),
                IoErrorKind::PermissionDenied,
                "assert-not-bind-tcp"
            );
        }
    }

    if let Some(socket_addrs) = args.assert_connect_tcp {
        for addr in socket_addrs {
            assert!(
                match TcpStream::connect(addr) {
                    Ok(_) => true,
                    Err(err) if err.kind() == IoErrorKind::NetworkUnreachable => true,
                    Err(_) => false,
                },
                "assert-connect-tcp"
            );
        }
    }

    if let Some(socket_addrs) = args.assert_not_connect_tcp {
        for addr in socket_addrs {
            assert_eq!(
                TcpStream::connect(addr)
                    .expect_err("assert-not-connect-tcp")
                    .kind(),
                IoErrorKind::PermissionDenied,
                "assert-not-connect-tcp"
            );
        }
    }

    if let Some(_syscalls) = args.assert_seccomp_action_kill {
        unimplemented!();
    }

    if let Some(syscalls) = args.assert_seccomp_action_eperm {
        for syscall in syscalls {
            let sysno = Sysno::from_str(&syscall).expect("<Sysno as FromStr>::Err is ()");
            assert_eq!(
                -libc::EPERM as usize,
                unsafe { syscalls::raw_syscall!(sysno, 0, 0, 0, 0, 0, 0) },
                "seccomp-action-eperm"
            );
        }
    }

    if let Some(syscalls) = args.assert_seccomp_action_enosys {
        for syscall in syscalls {
            let sysno = Sysno::from_str(&syscall).expect("<Sysno as FromStr>::Err is ()");
            assert_eq!(
                -libc::ENOSYS as usize,
                unsafe { syscalls::raw_syscall!(sysno, 0, 0, 0, 0, 0, 0) },
                "seccomp-action-enosys"
            );
        }
    }

    if let Some(syscalls) = args.assert_seccomp_action_log_allow {
        const EBADF: usize = -libc::EBADF as usize;
        const EINVAL: usize = -libc::EINVAL as usize;
        const EFAULT: usize = -libc::EFAULT as usize;

        for syscall in syscalls {
            let sysno = Sysno::from_str(&syscall).expect("<Sysno as FromStr>::Err is ()");
            match unsafe { syscalls::raw_syscall!(sysno, 0, 0, 0, 0, 0, 0) } {
                /* Unknown syscall was executed successfully,
                 * consider us to be in an unknown state.
                 * Aborting is the only safe action we can do. */
                r if r < -4096_i32 as usize => process::abort(),
                /* Syscall returned an error that is expected to be
                 * caused by a allowed call with invalid arguments.
                 * Assume syscall was allowed by seccomp filters. */
                EBADF | EINVAL | EFAULT => (),
                /* Assume all other errors to be a seccomp filter denial. */
                _ => panic!("assert-seccomp-action-log-allow"),
            }
        }
    }

    if args.assert_seccomp_socket.is_some() {
        unimplemented!();
    }

    if args.assert_seccomp_memfd_noexec {
        unimplemented!();
    }

    if args.assert_seccomp_deny_clone_newuser {
        unimplemented!();
    }

    if args.assert_seccomp_deny_memory_write_execute {
        unimplemented!();
    }

    if let Some(sysctls) = args.assert_sysctl {
        for sysctl in sysctls {
            let (key, value) = sysctl
                .split_once('=')
                .expect("Expected --assert-sysctl=key=value");
            assert_eq!(
                value,
                read_to_string(Path::new("/proc/sys").join(key.replace('.', "/")))?.trim(),
                "assert-sysctl"
            );
        }
    }

    if let Some(score) = args.assert_oom_score_adj {
        assert_eq!(
            score,
            read_to_string("/proc/self/oom_score_adj")?
                .trim_end()
                .parse()?,
            "assert-oom-score-adj"
        );
    }

    if let Some(val) = args.assert_nice {
        assert_eq!(val, getpriority_process(None)?, "assert-nice");
    }

    if let Some(envvars) = args.assert_env_set {
        for envvar in envvars {
            let (key, value) = envvar
                .split_once('=')
                .expect("Expected --assert-env-set=key=value");
            assert_eq!(value, env::var(key)?, "assert-env-set");
        }
    }

    if let Some(names) = args.assert_env_not_set {
        for name in names {
            assert!(env::var_os(name).is_none(), "assert-env-not-set");
        }
    }

    if args.assert_mdwe_refuse_exec_gain {
        assert!(
            get_mdwe()?.contains(MDWEFlags::REFUSE_EXEC_GAIN),
            "assert-mdwe-refuse-exec-gain"
        );
    }

    if let Some(sid) = args.assert_new_session {
        assert_ne!(
            sid,
            getsid(None)?.as_raw_nonzero().get(),
            "assert-new-session"
        );
    }

    if args.assert_strict_mitigations {
        assert_eq!(
            SpecFlags::FORCE_DISABLE,
            get_speculation_ctrl(SpecVariant::StoreBypass)?,
            "assert-strict-mitiagtions (StoreBypass)"
        );
        assert_eq!(
            SpecFlags::FORCE_DISABLE,
            get_speculation_ctrl(SpecVariant::IndirectBranch)?,
            "assert-strict-mitiagtions (IndirectBranch)"
        );
    }

    if let Some(mask) = args.assert_umask {
        assert_eq!(
            mask,
            umask(Mode::from_raw_mode(0o022)).as_raw_mode(),
            "assert-umask"
        );
    }

    if args.assert_is_pid1 {
        assert_eq!(Pid::from_raw(1).unwrap(), getpid(), "assert-is-pid1");
    }

    if args.assert_is_not_pid1 {
        assert_ne!(Pid::from_raw(1).unwrap(), getpid(), "assert-is-not-pid1");
    }

    Ok(())
}
