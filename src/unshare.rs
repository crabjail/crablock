/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use bitflags::bitflags;

use crate::os::CloneFlags;

bitflags! {
    #[derive(Debug, Default, Clone, Copy)]
    pub struct Unshare: u32 {
        const USER = linux_raw_sys::general::CLONE_NEWUSER;
        const PID = linux_raw_sys::general::CLONE_NEWPID;
        const IPC = linux_raw_sys::general::CLONE_NEWIPC;
        const MNT = linux_raw_sys::general::CLONE_NEWNS;
        const NET = linux_raw_sys::general::CLONE_NEWNET;
        const UTS = linux_raw_sys::general::CLONE_NEWUTS;
        const CGROUP = linux_raw_sys::general::CLONE_NEWCGROUP;
    }
}
impl Unshare {
    pub fn user(self) -> bool {
        self.contains(Self::USER)
    }

    pub fn into_clone_flags(self) -> CloneFlags {
        CloneFlags::from_bits(self.bits() as i32).unwrap()
    }
}
