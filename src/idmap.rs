/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General public License for more details.
 *
 * You should have received a copy of the GNU General public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::os::unix::net::UnixDatagram;
use std::process::Command;
use std::str::FromStr;
use std::{fs, iter};

use lnix::ugid::{Gid, Uid};

use crate::utils::{CommandExt, ResultExt};

/*
trait IdMap {}
*/

#[derive(Debug)]
pub struct IdMap {
    pub our: IdMapping,
    pub others: Vec<IdMapping>,
    kind: IdMapKind,
}
impl IdMap {
    pub fn uid_map() -> Self {
        Self {
            our: IdMapping {
                id: Uid::effective().as_raw(),
                lowerid: Uid::effective().as_raw(),
                count: 1,
            },
            others: Vec::new(),
            kind: IdMapKind::UidMap,
        }
    }

    pub fn gid_map() -> Self {
        Self {
            our: IdMapping {
                id: Gid::effective().as_raw(),
                lowerid: Gid::effective().as_raw(),
                count: 1,
            },
            others: Vec::new(),
            kind: IdMapKind::GidMap,
        }
    }

    pub fn should_use_newidmap(&self) -> bool {
        !self.others.is_empty()
    }

    pub fn write(&self, host_op_sock: &UnixDatagram) -> Result<(), String> {
        if self.should_use_newidmap() {
            self.write_parent(host_op_sock)
        } else {
            self.write_direct()
        }
    }

    fn write_direct(&self) -> Result<(), String> {
        let file_path = match self.kind {
            IdMapKind::UidMap => "/proc/self/uid_map",
            IdMapKind::GidMap => "/proc/self/gid_map",
        };
        let uid_map = format!("{} {} {}", self.our.id, self.our.lowerid, self.our.count);
        debug!("Writing '{uid_map}' to {file_path}.");
        fs::write(file_path, uid_map)
            .map_err(|err| format!("Failed to write {file_path}: {err}"))?;
        Ok(())
    }

    fn write_parent(&self, host_op_sock: &UnixDatagram) -> Result<(), String> {
        match self.kind {
            IdMapKind::UidMap => crate::request_host_op(host_op_sock, b"start newuidmap;")?,
            IdMapKind::GidMap => crate::request_host_op(host_op_sock, b"start newgidmap;")?,
        }

        Ok(())
    }

    pub fn write_newidmap(&self, pid: u32) -> Result<(), String> {
        let program = match self.kind {
            IdMapKind::UidMap => "newuidmap",
            IdMapKind::GidMap => "newgidmap",
        };

        let status = Command::new(program)
            .arg(pid.to_string())
            .args(
                self.others
                    .iter()
                    .chain(iter::once(&self.our))
                    .flat_map(|mapping| mapping.to_string_array()),
            )
            .status_log()
            .with_err_msg(|| format!("Failed to start {program}"))?;

        if !status.success() {
            return Err(if let Some(code) = status.code() {
                format!("{program} exited with status {code}.")
            } else {
                format!("{program} exited with status <signal>.")
            });
        }

        Ok(())
    }
}

#[derive(Debug)]
pub enum IdMapKind {
    UidMap,
    GidMap,
}

#[derive(Debug, Clone, Copy)]
pub struct IdMapping {
    pub id: u32,
    pub lowerid: u32,
    pub count: u32,
}
impl IdMapping {
    fn to_string_array(self) -> [String; 3] {
        [
            self.id.to_string(),
            self.lowerid.to_string(),
            self.count.to_string(),
        ]
    }

    /*
    fn contains(&self, uid: u32) -> bool {
        self.id <= uid && uid <= (self.id + self.count - 1)
    }
    */
}
impl FromStr for IdMapping {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut numbers = s
            .splitn(3, ':')
            .map(|n| n.parse::<u32>().map_err(|e| e.to_string()));
        Ok(Self {
            id: numbers.next().ok_or_else(|| "Expected id".to_string())??,
            lowerid: numbers
                .next()
                .ok_or_else(|| "Expected lowerid".to_string())??,
            count: numbers
                .next()
                .ok_or_else(|| "Expected count".to_string())??,
        })
    }
}
