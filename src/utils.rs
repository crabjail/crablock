/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! Module for various programming helpers not directly related to crablock.

use std::borrow::Cow;
use std::os::unix::prelude::{CommandExt as _, *};
use std::path::{Path, PathBuf};
use std::process::{Child, Command, ExitStatus};
use std::str::FromStr;
use std::{env, fmt, io};

use lnix::io::ioctl_fionclex;
use lnix::signal::{SigSet, SigmaskHow, Signal, sigprocmask};
use lnix::std_suppl::prelude::*;
use rustix::fs::{AtFlags, CWD, FileType, Mode, makedev, mkdirat, mknodat, statat};
use rustix::io::Errno;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum TriBool {
    True = 1,
    False = 0,
    #[default]
    Unknown = -1,
}
impl TriBool {
    pub fn is_true(self) -> bool {
        matches!(self, Self::True)
    }

    pub fn is_false(self) -> bool {
        matches!(self, Self::False)
    }

    pub fn is_unknown(self) -> bool {
        matches!(self, Self::Unknown)
    }
}
impl From<bool> for TriBool {
    fn from(value: bool) -> Self {
        match value {
            true => Self::True,
            false => Self::False,
        }
    }
}
impl TryFrom<TriBool> for bool {
    type Error = TriBool;

    fn try_from(value: TriBool) -> Result<Self, Self::Error> {
        match value {
            TriBool::True => Ok(true),
            TriBool::False => Ok(false),
            TriBool::Unknown => Err(value),
        }
    }
}
impl From<i32> for TriBool {
    fn from(value: i32) -> Self {
        match value {
            1..=i32::MAX => Self::True,
            0 => Self::False,
            i32::MIN..=-1 => Self::Unknown,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum LogLevel {
    Fatal,
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}
impl LogLevel {
    /// Gets the log level from environment or a fallback level.
    pub fn get(env_name: &str, default: Self) -> Self {
        // Get log level from environment or return default.
        let Ok(level) = env::var(env_name) else {
            return default;
        };

        // Parse log level from environment or return default.
        let Ok(level) = Self::from_str(&level) else {
            eprintln!("Invalid log level: {level}");
            return default;
        };

        level
    }
}
impl fmt::Display for LogLevel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::Fatal => "FATAL",
            Self::Error => "ERROR",
            Self::Warn => "WARN",
            Self::Info => "INFO",
            Self::Debug => "DEBUG",
            Self::Trace => "TRACE",
        };
        write!(f, "{s}")
    }
}
impl FromStr for LogLevel {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            s if s.eq_ignore_ascii_case("FATAL") => Ok(Self::Fatal),
            s if s.eq_ignore_ascii_case("ERROR") => Ok(Self::Error),
            s if s.eq_ignore_ascii_case("WARN") => Ok(Self::Warn),
            s if s.eq_ignore_ascii_case("INFO") => Ok(Self::Info),
            s if s.eq_ignore_ascii_case("DEBUG") => Ok(Self::Debug),
            s if s.eq_ignore_ascii_case("TRACE") => Ok(Self::Trace),
            _ => Err(()),
        }
    }
}

macro_rules! _log {
    ($lvl:expr, $($args:tt)*) => {
        let log_level = *crate::LOG_LEVEL.get()
            .expect("Called logging macro before LOG_LEVEL got initialized.");
        if log_level >= $lvl {
            let log_msg = format!(
                "crablock({}) {}[{}:{}:{}]: {}\n",
                std::process::id(),
                $lvl,
                file!(), line!(), column!(),
                format_args!($($args)*),
            );
            eprint!("{log_msg}");
        }
    };
}

#[allow(unused_macros)]
macro_rules! fatal {
    ($($args:tt)*) => {
        _log!(crate::utils::LogLevel::Fatal, $($args)*)
    };
}
#[allow(unused_macros)]
macro_rules! error {
    ($($args:tt)*) => {
        _log!(crate::utils::LogLevel::Error, $($args)*)
    };
}
#[allow(unused_macros)]
macro_rules! warn {
    ($($args:tt)*) => {
        _log!(crate::utils::LogLevel::Warn, $($args)*)
    };
}
#[allow(unused_macros)]
macro_rules! info {
    ($($args:tt)*) => {
        _log!(crate::utils::LogLevel::Info, $($args)*)
    };
}
#[allow(unused_macros)]
macro_rules! debug {
    ($($args:tt)*) => {
        _log!(crate::utils::LogLevel::Debug, $($args)*)
    };
}
#[allow(unused_macros)]
macro_rules! trace {
    ($($args:tt)*) => {
        _log!(crate::utils::LogLevel::Trace, $($args)*)
    };
}

/// Asserts that a boolean expression is `true` at compile-time.
///
/// # Examples
///
/// ```
/// const FOO: i32 = 5;
///
/// const_assert!(FOO == 5);
///
/// // This will fail to compile.
/// const_assert!(FOO == 6, "FOO must be 6")
/// ```
macro_rules! const_assert {
    ( $cond:expr $(,)? ) => {
        const _: () = assert!($cond);
    };
    ( $cond:expr, $( $arg:tt )+ ) => {
        const _: () = assert!($cond, $( $arg )+);
    }
}

/// Bail out of a function with an error.
macro_rules! bail {
    ( $arg:literal ) => {
        return Err($arg.into())
    };
    ( $( $arg:tt )* ) => {
        return Err(format!( $( $arg )* ).into())
    };
}

pub trait ResultExt<T> {
    fn err_msg(self, msg: &str) -> Result<T, Cow<'static, str>>;
    fn with_err_msg<F, M>(self, msg: F) -> Result<T, Cow<'static, str>>
    where
        F: FnOnce() -> M,
        M: fmt::Display;
}
impl<T, E: fmt::Display> ResultExt<T> for Result<T, E> {
    fn err_msg(self, msg: &str) -> Result<T, Cow<'static, str>> {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) => Err(format!("{msg}: {err}").into()),
        }
    }

    fn with_err_msg<F, M>(self, msg: F) -> Result<T, Cow<'static, str>>
    where
        F: FnOnce() -> M,
        M: fmt::Display,
    {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) => Err(format!("{}: {err}", msg()).into()),
        }
    }
}

/*
pub trait ResultExtFor<T, U> {
    fn err_msg_for(self, for_: U, msg: &str) -> Result<T, Cow<'static, str>>;
    fn with_err_msg_for<F, M>(self, for_: U, msg: F) -> Result<T, Cow<'static, str>>
    where
        F: FnOnce() -> M,
        M: fmt::Display;
}
impl<T, U: fmt::Display, E: fmt::Display> ResultExtFor<T, U> for Result<T, E> {
    fn err_msg_for(self, for_: U, msg: &str) -> Result<T, Cow<'static, str>> {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) => Err(format!("{msg} for '{for_}': {err}").into()),
        }
    }

    fn with_err_msg_for<F, M>(self, for_: U, msg: F) -> Result<T, Cow<'static, str>>
    where
        F: FnOnce() -> M,
        M: fmt::Display,
    {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) => Err(format!("{} for '{for_}': {err}", msg()).into()),
        }
    }
}
impl<T, E: fmt::Display> ResultExtFor<T, &Path> for Result<T, E> {
    fn err_msg_for(self, for_: &Path, msg: &str) -> Result<T, Cow<'static, str>> {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) => Err(format!("{msg} for '{}': {err}", for_.display()).into()),
        }
    }

    fn with_err_msg_for<F, M>(self, for_: &Path, msg: F) -> Result<T, Cow<'static, str>>
    where
        F: FnOnce() -> M,
        M: fmt::Display,
    {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) => Err(format!("{} for '{}': {err}", msg(), for_.display()).into()),
        }
    }
}
*/

pub trait CommandExt {
    /// Pass `fd` to the spawned process
    // fn steal_fd(&mut self, fd: OwnedFd) -> &mut Self;
    /// Pass `fds` to the spawned process
    fn steal_fds(&mut self, fds: Vec<OwnedFd>) -> &mut Self;
    /// Unblock signal `signum`.
    fn unblock_signal(&mut self, signum: Signal) -> &mut Self;
    /// Like [`spawn`](Command::spawn) but [`debug!`] log the command-line
    fn spawn_log(&mut self) -> io::Result<Child>;
    /// Like [`status`](Command::status) but [`debug!`] log the command-line
    fn status_log(&mut self) -> io::Result<ExitStatus>;
}

impl CommandExt for Command {
    // fn steal_fd(&mut self, fd: OwnedFd) -> &mut Self {
    //     unsafe {
    //         self.pre_exec(move || ioctl_fionclex(fd.as_fd()));
    //     }
    //     self
    // }

    fn steal_fds(&mut self, fds: Vec<OwnedFd>) -> &mut Self {
        unsafe {
            self.pre_exec(move || {
                for fd in &fds {
                    ioctl_fionclex(fd.as_fd())?;
                }
                Ok(())
            });
        }
        self
    }

    fn unblock_signal(&mut self, signum: Signal) -> &mut Self {
        unsafe {
            self.pre_exec(move || {
                let mask = {
                    let mut set = SigSet::empty();
                    set.add(signum);
                    set
                };
                sigprocmask(SigmaskHow::Unblock, Some(mask))?;

                Ok(())
            });
        }
        self
    }

    fn spawn_log(&mut self) -> io::Result<Child> {
        debug!(
            "Spawn: {} {}",
            self.get_program().to_string_lossy(),
            self.get_args()
                .flat_map(|a| [a.to_string_lossy(), Cow::Borrowed(" ")])
                .collect::<String>(),
        );
        self.spawn()
    }

    fn status_log(&mut self) -> io::Result<ExitStatus> {
        debug!(
            "Spawn: {} {}",
            self.get_program().to_string_lossy(),
            self.get_args()
                .flat_map(|a| [a.to_string_lossy(), Cow::Borrowed(" ")])
                .collect::<String>(),
        );
        self.status()
    }
}

pub trait FilterErrno<E> {
    fn filter_errno(self, errno: E) -> Self;
}
impl FilterErrno<i32> for Result<(), capctl::Error> {
    fn filter_errno(self, errno: i32) -> Self {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) if err.code() == errno => Ok(()),
            Err(err) => Err(err),
        }
    }
}
impl FilterErrno<i32> for Result<(), io::Error> {
    fn filter_errno(self, errno: i32) -> Self {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) if err.raw_os_error() == Some(errno) => Ok(()),
            Err(err) => Err(err),
        }
    }
}
impl FilterErrno<io::ErrorKind> for Result<(), io::Error> {
    fn filter_errno(self, kind: io::ErrorKind) -> Self {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) if err.kind() == kind => Ok(()),
            Err(err) => Err(err),
        }
    }
}
impl FilterErrno<i32> for Result<(), rustix::io::Errno> {
    fn filter_errno(self, errno: i32) -> Self {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) if err.raw_os_error() == errno => Ok(()),
            Err(err) => Err(err),
        }
    }
}
impl FilterErrno<rustix::io::Errno> for Result<(), rustix::io::Errno> {
    fn filter_errno(self, errno: rustix::io::Errno) -> Self {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) if err == errno => Ok(()),
            Err(err) => Err(err),
        }
    }
}
impl<P> FilterErrno<P> for Result<(), rustix::io::Errno>
where
    P: FnOnce(rustix::io::Errno) -> bool,
{
    fn filter_errno(self, predicate: P) -> Self {
        match self {
            Ok(ok) => Ok(ok),
            Err(err) if predicate(err) => Ok(()),
            Err(err) => Err(err),
        }
    }
}

#[derive(Debug, Clone)]
pub struct EntryBuilder {
    file_type: FileType,
    mode: Option<Mode>,
    recursive: bool,
    parent_mode: Option<Mode>,
}
impl EntryBuilder {
    fn _new(file_type: FileType) -> Self {
        Self {
            file_type,
            mode: None,
            recursive: false,
            parent_mode: None,
        }
    }

    pub fn regular() -> Self {
        Self::_new(FileType::RegularFile)
    }

    pub fn directory() -> Self {
        Self::_new(FileType::Directory)
    }

    pub fn symlink() -> Self {
        Self::_new(FileType::Symlink)
    }

    pub fn fifo() -> Self {
        Self::_new(FileType::Fifo)
    }

    pub fn socket() -> Self {
        Self::_new(FileType::Socket)
    }

    pub fn char_device() -> Self {
        Self::_new(FileType::CharacterDevice)
    }

    pub fn block_device() -> Self {
        Self::_new(FileType::BlockDevice)
    }

    pub fn mode(&mut self, mode: Mode) -> &mut Self {
        self.mode = Some(mode);
        self
    }

    pub fn recursive(&mut self, recursive: bool) -> &mut Self {
        self.recursive = recursive;
        self
    }

    pub fn parent_mode(&mut self, parent_mode: Mode) -> &mut Self {
        self.parent_mode = Some(parent_mode);
        self
    }

    fn _create(&self, dirfd: BorrowedFd<'_>, path: &Path) -> io::Result<()> {
        if self.recursive && path.parent().is_some_and(|p| !p.exists()) {
            let path_parent = path.parent().unwrap();
            let mut buf = PathBuf::with_capacity(path_parent.len());
            for component in path_parent.components() {
                buf.push(component);
                mkdirat(dirfd, &buf, self.parent_mode.unwrap_or(Mode::empty()))
                    .filter_errno(Errno::EXIST)?;
            }
        }

        // Returns `true` if e is EEXIST _and_ the existing entry matches the
        // requested file_type and mode.
        let already_exists = |e: Errno| {
            e == Errno::EXIST
                && statat(dirfd, path, AtFlags::SYMLINK_NOFOLLOW).is_ok_and(|s| {
                    FileType::from_raw_mode(s.st_mode) == self.file_type
                        && if let Some(mode) = self.mode {
                            Mode::from_raw_mode(s.st_mode) == mode
                        } else {
                            true
                        }
                })
        };

        if self.file_type.is_dir() {
            mkdirat(dirfd, path, self.mode.unwrap_or(Mode::empty()))
                .filter_errno(already_exists)?;
        } else {
            mknodat(
                dirfd,
                path,
                self.file_type,
                self.mode.unwrap_or(Mode::empty()),
                makedev(0, 0),
            )
            .filter_errno(already_exists)?;
        }

        Ok(())
    }

    /// Creates an entry with the options configured in this builder.
    ///
    /// Relative paths are relative to the current working directory.
    pub fn create<P: AsRef<Path>>(&self, path: P) -> io::Result<()> {
        self._create(CWD, path.as_ref())
    }

    /*
    /// Creates an entry with the options configured in this builder.
    ///
    /// Relative paths are relative to `dirfd`.
    pub fn create_at<FD: AsFd, P: AsRef<Path>>(&self, dirfd: FD, path: P) -> io::Result<()> {
        self._create(dirfd.as_fd(), path.as_ref())
    }
    */

    /// Creates an entry with the options configured in this builder.
    ///
    /// Paths are relative to `dirfd`. Absolute paths are made relative
    /// by stripping the leading `/`.
    /// **Note:** There is no check or assurance to make sure that `path` is beneath `dirfd`.
    pub fn create_at_root<FD: AsFd, P: AsRef<Path>>(&self, dirfd: FD, path: P) -> io::Result<()> {
        let path = path.as_ref();
        self._create(dirfd.as_fd(), path.strip_prefix("/").unwrap_or(path))
    }
}

pub fn atoi(buf: &[u8]) -> i32 {
    (0..buf.len())
        .zip((0..buf.len()).rev().map(|i| 10_i32.pow(i as u32)))
        .map(|(i, j)| j * (buf[i] - b'0') as i32)
        .sum()
}

/*
pub fn unescape_octal(s: &str) -> Result<Cow<'_, str>, String> {
    if !s.contains('\\') {
        return Ok(s.into());
    }

    let mut s = s.as_bytes();
    let mut buf = Vec::with_capacity(s.len());

    while let Some(idx) = s.iter().copied().enumerate().find_map(|(i, b)| (b == b'\\').then_some(i)) {
        buf.extend_from_slice(&s[..idx]);
        if s[idx + 1..].len() < 3 {
            return Err("To short escape sequence".to_string());
        }
        if s[idx + 1..idx + 4].iter().copied().any(|b| b < b'0' || b > b'7') {
            return Err("Escape sequence is not a octal number".to_string());
        }
        buf.push(s[idx + 1] - b'0' << 6 | s[idx + 2] - b'0' << 3 | s[idx + 3] - b'0' << 0);
        s = &s[idx + 4..];
    }
    buf.extend_from_slice(s);

    Ok(String::from_utf8(buf).map_err(|e| e.to_string())?.into())
}
*/

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_log_level_ord() {
        assert!(LogLevel::Trace > LogLevel::Debug);
        assert!(LogLevel::Debug > LogLevel::Info);
        assert!(LogLevel::Info > LogLevel::Warn);
        assert!(LogLevel::Warn > LogLevel::Error);
        assert!(LogLevel::Error > LogLevel::Fatal);
    }
}
