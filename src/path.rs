/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

// FIXME: What to do with this file? Merge copy of it in crabjail into a new crate.
// crabdirs or crabutils::dirs? Same story for dirs.rs.

use std::borrow::Cow;
use std::path::Path;

use lnix::std_suppl::prelude::*;

use crate::dirs::{
    UserDirs, home_dir, tmp_dir, xdg_cache_home, xdg_config_home, xdg_data_home, xdg_runtime_dir,
    xdg_state_home,
};

pub(crate) fn expand_path<'a, P: Into<Cow<'a, Path>>>(path: P) -> Cow<'a, Path> {
    let path = path.into();

    if !path.as_os_str().starts_with("@") {
        return path;
    }

    if let Ok(subpath) = path.strip_prefix("@{HOME}") {
        home_dir().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{TMPDIR}") {
        tmp_dir().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_CACHE_HOME}") {
        xdg_cache_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_CONFIG_HOME}") {
        xdg_config_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DATA_HOME}") {
        xdg_data_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_STAT_HOME}") {
        xdg_state_home().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_RUNTIME_DIR}") {
        xdg_runtime_dir().join(subpath).into()
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DESKTOP_DIR}") {
        if let Some(desktop_dir) = &UserDirs::get().desktop {
            desktop_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DOCUMENTS_DIR}") {
        if let Some(documents_dir) = &UserDirs::get().documents {
            documents_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_DOWNLOAD_DIR}") {
        if let Some(download_dir) = &UserDirs::get().download {
            download_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_MUSIC_DIR}") {
        if let Some(music_dir) = &UserDirs::get().music {
            music_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_PICTURES_DIR}") {
        if let Some(pictures_dir) = &UserDirs::get().pictures {
            pictures_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_PUBLICSHARE_DIR}") {
        if let Some(publicshare_dir) = &UserDirs::get().publicshare {
            publicshare_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_TEMPLATES_DIR}") {
        if let Some(templates_dir) = &UserDirs::get().templates {
            templates_dir.join(subpath).into()
        } else {
            path
        }
    } else if let Ok(subpath) = path.strip_prefix("@{XDG_VIDEOS_DIR}") {
        if let Some(videos_dir) = &UserDirs::get().videos {
            videos_dir.join(subpath).into()
        } else {
            path
        }
    } else {
        path
    }
}

// pub fn expand_path_many(path: &Path) -> Box<dyn Iterator<Item = PathBuf> + '_> {
//     if !path.as_os_str().starts_with("@") {
//         Box::new(iter::once(path.to_path_buf()))
//     } else if let Ok(subpath) = path.strip_prefix("@{PATH}") {
//         Box::new(
//             executable_dirs()
//                 .iter()
//                 .map(move |executable_dir| executable_dir.join(subpath)),
//         )
//     } else if let Ok(subpath) = path.strip_prefix("@{XDG_CONFIG_DIRS}") {
//         Box::new(
//             xdg_config_dirs()
//                 .iter()
//                 .map(move |config_dir| config_dir.join(subpath)),
//         )
//     } else if let Ok(subpath) = path.strip_prefix("@{XDG_DATA_DIRS}") {
//         Box::new(
//             xdg_data_dirs()
//                 .iter()
//                 .map(move |data_dir| data_dir.join(subpath)),
//         )
//     } else {
//         Box::new(iter::once(expand_path(path).into_owned()))
//     }
// }
