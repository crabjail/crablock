/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashMap;
use std::ffi::CString;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::os::unix::prelude::*;
use std::process::{Command, Stdio};

use lnix::signal::Signal;
use rustix::pipe::{PipeFlags, pipe_with};

use crate::utils::CommandExt;

#[derive(Debug, Default)]
pub struct DBusProxy {
    pub buses: HashMap<String, Vec<CString>>,
    pub capture_output: Option<String>,
}
impl DBusProxy {
    pub fn spawn(&self) -> io::Result<File> {
        let stdout = get_stdout(self.capture_output.as_deref());
        let fd = pipe_with(PipeFlags::CLOEXEC)?;
        let args = pipe_with(PipeFlags::CLOEXEC)?;
        Command::new("xdg-dbus-proxy")
            .args(&[
                format!("--fd={}", fd.1.as_raw_fd()),
                format!("--args={}", args.0.as_raw_fd()),
            ])
            .steal_fds(vec![fd.1, args.0])
            .stdout(stdout)
            .unblock_signal(Signal::Term)
            .spawn_log()?;

        let mut args_pipe = File::from(args.1);
        for arg in self.buses.values().flatten() {
            debug!("xdg-dbus-proxy args: {:?}", arg);

            args_pipe.write_all(arg.as_bytes_with_nul())?;
        }

        Ok(File::from(fd.0))
    }
}

fn get_stdout(dbus_proxy_capture_output: Option<&str>) -> Stdio {
    match dbus_proxy_capture_output {
        None => Stdio::inherit(),
        Some("null" | "-") => Stdio::null(),
        Some(filename) => match File::create(filename) {
            Ok(file) => Stdio::from(file),
            Err(err) => {
                error!("Failed to create {filename}: {err}");
                Stdio::null()
            }
        },
    }
}
