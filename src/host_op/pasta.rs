/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::fs::File;
use std::path::PathBuf;
use std::process::{Command, Stdio};
use std::time::SystemTime;
use std::{env, process};

use lnix::fs::mkfifoat;
use lnix::signal::Signal;
use rustix::fs::CWD;
use rustix_0_38::fs::Mode;

use crate::dirs::tmp_dir;
use crate::utils::{CommandExt, ResultExt};

#[derive(Debug, Default)]
enum PortForwardingSpec {
    #[default]
    None,
    Auto,
    Ports(Vec<String>),
}
impl PortForwardingSpec {
    fn add_ports(&mut self, ports: &str) {
        if let Self::Ports(ps) = self {
            ps.push(String::from(ports));
        } else {
            *self = Self::Ports(vec![String::from(ports)]);
        }
    }
}

#[derive(Debug, Default)]
pub struct Pasta {
    pcap: Option<String>,

    mtu: Option<String>,
    address: (Option<String>, Option<String>),
    netmask: Option<String>,
    mac_addr: Option<String>,
    gateway: (Option<String>, Option<String>),
    interface: Option<String>,
    outbound: (Option<String>, Option<String>),
    outbound_if4: Option<String>,
    outbound_if6: Option<String>,
    dns: Vec<String>,
    dns_forward: Option<String>,
    dns_host: (Option<String>, Option<String>),
    search: Option<String>,
    dhcp_dns: Option<String>,
    dhcp_search: Option<String>,
    no_tcp: bool,
    no_udp: bool,
    no_icmp: bool,
    no_dhcp: bool,
    no_ndp: bool,
    no_dhcpv6: bool,
    no_ra: bool,
    freebind: bool,
    map_host_loopback: Vec<String>,
    /// Actually `no_map_gw` but we want `--no-map-gw` to be the default.
    map_gw: bool,
    map_guest_addr: Vec<String>,
    ipv4_only: bool,
    ipv6_only: bool,

    ns_ifname: Option<String>,
    tcp_ports: PortForwardingSpec,
    udp_ports: PortForwardingSpec,
    tcp_ns: PortForwardingSpec,
    udp_ns: PortForwardingSpec,
    /// Actually `config_net` but we want `--config-net` to be the default.
    no_config_net: bool,
    ns_mac_addr: Option<String>,
    no_splice: bool,
}
impl Pasta {
    pub fn set_flag(&mut self, flag: &str) -> Result<(), Cow<'static, str>> {
        match flag {
            "--pasta-no-tcp" => self.no_tcp = true,
            "--pasta-no-udp" => self.no_udp = true,
            "--pasta-no-icmp" => self.no_icmp = true,
            "--pasta-no-dhcp" => self.no_dhcp = true,
            "--pasta-no-ndp" => self.no_ndp = true,
            "--pasta-no-dhcpv6" => self.no_dhcpv6 = true,
            "--pasta-no-ra" => self.no_ra = true,
            "--pasta-freebind" => self.freebind = true,
            "--pasta-no-map-gw" => self.map_gw = false,
            "--pasta-ipv4-only" => self.ipv4_only = true,
            "--pasta-ipv6-only" => self.ipv6_only = true,
            "--pasta-config-net" => self.no_config_net = false,
            "--pasta-no-splice" => self.no_splice = true,
            _ => return Err("Unknown pasta flag".into()),
        }

        Ok(())
    }

    pub fn set_option(&mut self, arg: &str, val: &str) -> Result<(), Cow<'static, str>> {
        match arg {
            "--pasta-pcap" => self.pcap = Some(String::from(val)),
            "--pasta-mtu" => self.mtu = Some(String::from(val)),
            "--pasta-address" => {
                if let Some((addr1, addr2)) = val.split_once(',') {
                    self.address.0 = Some(String::from(addr1));
                    self.address.1 = Some(String::from(addr2));
                } else {
                    self.address.0 = Some(String::from(val));
                    self.address.1 = None;
                }
            }
            "--pasta-netmask" => self.netmask = Some(String::from(val)),
            "--pasta-mac-addr" => self.mac_addr = Some(String::from(val)),
            "--pasta-gateway" => {
                if let Some((addr1, addr2)) = val.split_once(',') {
                    self.gateway.0 = Some(String::from(addr1));
                    self.gateway.1 = Some(String::from(addr2));
                } else {
                    self.gateway.0 = Some(String::from(val));
                    self.gateway.1 = None;
                }
            }
            "--pasta-interface" => self.interface = Some(String::from(val)),
            "--pasta-outbound" => {
                if let Some((addr1, addr2)) = val.split_once(',') {
                    self.outbound.0 = Some(String::from(addr1));
                    self.outbound.1 = Some(String::from(addr2));
                } else {
                    self.outbound.0 = Some(String::from(val));
                    self.outbound.1 = None;
                }
            }
            "--pasta-outbound-if4" => self.outbound_if4 = Some(String::from(val)),
            "--pasta-outbound-if6" => self.outbound_if6 = Some(String::from(val)),
            "--pasta-dns" => self.dns.push(String::from(val)),
            "--pasta-dns-forward" => self.dns_forward = Some(String::from(val)),
            "--pasta-dns-host" => {
                if let Some((addr1, addr2)) = val.split_once(',') {
                    self.dns_host.0 = Some(String::from(addr1));
                    self.dns_host.1 = Some(String::from(addr2));
                } else {
                    self.dns_host.0 = Some(String::from(val));
                    self.dns_host.1 = None;
                }
            }
            "--pasta-search" => self.search = Some(String::from(val)),
            "--pasta-dhcp-dns" => self.dhcp_dns = Some(String::from(val)),
            "--pasta-dhcp-search" => self.dhcp_search = Some(String::from(val)),
            "--pasta-no-tcp" => self.no_tcp = str2bool(val)?,
            "--pasta-no-udp" => self.no_udp = str2bool(val)?,
            "--pasta-no-icmp" => self.no_icmp = str2bool(val)?,
            "--pasta-no-dhcp" => self.no_dhcp = str2bool(val)?,
            "--pasta-no-ndp" => self.no_ndp = str2bool(val)?,
            "--pasta-no-dhcpv6" => self.no_dhcpv6 = str2bool(val)?,
            "--pasta-no-ra" => self.no_ra = str2bool(val)?,
            "--pasta-freebind" => self.freebind = str2bool(val)?,
            "--pasta-map-host-loopback" => self.map_host_loopback.push(String::from(val)),
            "--pasta-no-map-gw" => self.map_gw = !str2bool(val)?,
            "--pasta-map-guest-addr" => self.map_guest_addr.push(String::from(val)),
            "--pasta-ipv4-only" => self.ipv4_only = str2bool(val)?,
            "--pasta-ipv6-only" => self.ipv6_only = str2bool(val)?,
            "--pasta-ns-ifname" => self.ns_ifname = Some(String::from(val)),
            "--pasta-tcp-ports" | "--pasta-t" => match val {
                "none" => self.tcp_ports = PortForwardingSpec::None,
                "auto" => self.tcp_ports = PortForwardingSpec::Auto,
                ports => self.tcp_ports.add_ports(ports),
            },
            "--pasta-udp-ports" | "--pasta-u" => match val {
                "none" => self.udp_ports = PortForwardingSpec::None,
                "auto" => self.udp_ports = PortForwardingSpec::Auto,
                ports => self.udp_ports.add_ports(ports),
            },
            "--pasta-tcp-ns" | "--pasta-T" => match val {
                "none" => self.tcp_ns = PortForwardingSpec::None,
                "auto" => self.tcp_ns = PortForwardingSpec::Auto,
                ports => self.tcp_ns.add_ports(ports),
            },
            "--pasta-udp-ns" | "--pasta-U" => match val {
                "none" => self.udp_ns = PortForwardingSpec::None,
                "auto" => self.udp_ns = PortForwardingSpec::Auto,
                ports => self.udp_ns.add_ports(ports),
            },
            "--pasta-config-net" => self.no_config_net = !str2bool(val)?,
            "--pasta-ns-mac-addr" => self.ns_mac_addr = Some(String::from(val)),
            "--pasta-no-splice" => self.no_splice = str2bool(val)?,
            _ => return Err("Unknown pasta option".into()),
        }

        Ok(())
    }

    pub fn spawn(&self, child_pid: u32) -> Result<(File, PathBuf), String> {
        let pid_file = tmp_dir().join(format!(
            "{}{}_pasta.pid",
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs(),
            process::id()
        ));
        mkfifoat(CWD, &pid_file, Mode::RUSR | Mode::WUSR).err_msg("mkfifoat")?;

        #[expect(
            clippy::needless_borrows_for_generic_args,
            reason = "https://github.com/rust-lang/rust-clippy/issues/13162"
        )]
        Command::new("pasta")
            .args(&[
                "--quiet",
                &format!("--pid={}", pid_file.as_os_str().to_str().expect("utf-8")),
            ])
            .args(self.to_args())
            .arg(child_pid.to_string())
            .env_remove("LD_LIBRARY_PATH")
            .current_dir("/")
            .stdin(Stdio::null())
            .stdout(if env::var_os("CRABLOCK_DEBUG_PASTA").is_some() {
                Stdio::inherit()
            } else {
                Stdio::null()
            })
            .stderr(if env::var_os("CRABLOCK_DEBUG_PASTA").is_some() {
                Stdio::inherit()
            } else {
                Stdio::null()
            })
            .unblock_signal(Signal::Term)
            .spawn_log()
            .err_msg("spawn")?;

        Ok((File::open(&pid_file).err_msg("open fifo")?, pid_file))
    }

    fn to_args(&self) -> Vec<&str> {
        let mut args = Vec::new();

        if let Some(path) = &self.pcap {
            args.extend_from_slice(&["--pcap", path]);
        }

        if let Some(mtu) = &self.mtu {
            args.extend_from_slice(&["--mtu", mtu]);
        }
        if let Some(address) = &self.address.0 {
            args.extend_from_slice(&["--address", address]);
        }
        if let Some(address) = &self.address.1 {
            args.extend_from_slice(&["--address", address]);
        }
        if let Some(netmask) = &self.netmask {
            args.extend_from_slice(&["--netmask", netmask]);
        }
        if let Some(mac_addr) = &self.mac_addr {
            args.extend_from_slice(&["--mac-addr", mac_addr]);
        }
        if let Some(gateway) = &self.gateway.0 {
            args.extend_from_slice(&["--gateway", gateway]);
        }
        if let Some(gateway) = &self.gateway.1 {
            args.extend_from_slice(&["--gateway", gateway]);
        }
        if let Some(interface) = &self.interface {
            args.extend_from_slice(&["--interface", interface]);
        }
        if let Some(outbound) = &self.outbound.0 {
            args.extend_from_slice(&["--outbound", outbound]);
        }
        if let Some(outbound) = &self.outbound.1 {
            args.extend_from_slice(&["--outbound", outbound]);
        }
        if let Some(outbound_if4) = &self.outbound_if4 {
            args.extend_from_slice(&["--outbound-if4", outbound_if4]);
        }
        if let Some(outbound_if6) = &self.outbound_if6 {
            args.extend_from_slice(&["--outbound-if6", outbound_if6]);
        }
        for dns in &self.dns {
            args.extend_from_slice(&["--dns", dns]);
        }
        if let Some(dns_forward) = &self.dns_forward {
            args.extend_from_slice(&["--dns-forward", dns_forward]);
        }
        if let Some(addr) = &self.dns_host.0 {
            args.extend_from_slice(&["--dns-host", addr]);
        }
        if let Some(addr) = &self.dns_host.1 {
            args.extend_from_slice(&["--dns-host", addr]);
        }
        if let Some(search) = &self.search {
            args.extend_from_slice(&["--search", search]);
        }
        if let Some(dhcp_dns) = &self.dhcp_dns {
            args.extend_from_slice(&["--dhcp-dns", dhcp_dns]);
        }
        if let Some(dhcp_search) = &self.dhcp_search {
            args.extend_from_slice(&["--dhcp-search", dhcp_search]);
        }
        if self.no_tcp {
            args.push("--no-tcp");
        }
        if self.no_udp {
            args.push("--no-udp");
        }
        if self.no_icmp {
            args.push("--no-icmp");
        }
        if self.no_dhcp {
            args.push("--no-dhcp");
        }
        if self.no_ndp {
            args.push("--no-ndp");
        }
        if self.no_dhcpv6 {
            args.push("--no-dhcpv6");
        }
        if self.no_ra {
            args.push("--no-ra");
        }
        if self.freebind {
            args.push("--freebind");
        }
        for addr in &self.map_host_loopback {
            args.extend_from_slice(&["--map-host-loopback", addr]);
        }
        if !self.map_gw {
            args.push("--no-map-gw");
        }
        for addr in &self.map_guest_addr {
            args.extend_from_slice(&["--map-guest-addr", addr]);
        }
        if self.ipv4_only {
            args.push("--ipv4-only");
        }
        if self.ipv6_only {
            args.push("--ipv6-only");
        }

        if let Some(ns_ifname) = &self.ns_ifname {
            args.extend_from_slice(&["--ns-ifname", ns_ifname]);
        }
        match &self.tcp_ports {
            PortForwardingSpec::None => args.extend_from_slice(&["-t", "none"]),
            PortForwardingSpec::Auto => args.extend_from_slice(&["-t", "auto"]),
            PortForwardingSpec::Ports(ports) => {
                for port in ports {
                    args.extend_from_slice(&["-t", port]);
                }
            }
        }
        match &self.udp_ports {
            PortForwardingSpec::None => args.extend_from_slice(&["-u", "none"]),
            PortForwardingSpec::Auto => args.extend_from_slice(&["-u", "auto"]),
            PortForwardingSpec::Ports(ports) => {
                for port in ports {
                    args.extend_from_slice(&["-u", port]);
                }
            }
        }
        match &self.tcp_ns {
            PortForwardingSpec::None => args.extend_from_slice(&["-T", "none"]),
            PortForwardingSpec::Auto => args.extend_from_slice(&["-T", "auto"]),
            PortForwardingSpec::Ports(ports) => {
                for port in ports {
                    args.extend_from_slice(&["-T", port]);
                }
            }
        }
        match &self.udp_ns {
            PortForwardingSpec::None => args.extend_from_slice(&["-U", "none"]),
            PortForwardingSpec::Auto => args.extend_from_slice(&["-U", "auto"]),
            PortForwardingSpec::Ports(ports) => {
                for port in ports {
                    args.extend_from_slice(&["-U", port]);
                }
            }
        }
        if !self.no_config_net {
            args.push("--config-net");
        }
        if let Some(ns_mac_addr) = &self.ns_mac_addr {
            args.extend_from_slice(&["--ns-mac-addr", ns_mac_addr]);
        }
        if self.no_splice {
            args.push("--no-splice");
        }

        args
    }
}

fn str2bool(s: &str) -> Result<bool, Cow<'static, str>> {
    match s {
        "true" => Ok(true),
        "false" => Ok(false),
        _ => Err("Expected 'true' or 'false'.".into()),
    }
}
