/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::io::prelude::*;
use std::os::unix::net::UnixDatagram;
use std::os::unix::prelude::*;
use std::process::{Command, ExitCode};

use lnix::signal::{SigSet, SigmaskHow, SignalFd, sigprocmask};
use rustix::event::{PollFd, PollFlags, poll};
use rustix::fs::{Timespec, unlink};
use rustix::io::ioctl_fionbio;
use rustix::process::{Pid, Signal, kill_process};

use crate::ClConfig;
use crate::utils::{ResultExt, atoi};

pub mod dbus_proxy;
pub mod pasta;

const POLL_TIMEOUT_INFINITY: Option<&Timespec> = None;

pub fn main(
    config: &ClConfig,
    host_op_sock: UnixDatagram,
    child_pid: u32,
) -> Result<ExitCode, String> {
    let sigterm_mask = {
        let mut set = SigSet::empty();
        set.add(rustix_0_38::process::Signal::Term);
        set
    };

    unsafe { sigprocmask(SigmaskHow::Block, Some(sigterm_mask)) }.err_msg("sigprocmask")?;

    let mut pasta_pid = None;
    let mut pasta_pipe = if let Some(pasta) = &config.pasta {
        Some(pasta.spawn(child_pid).err_msg("pasta.spawn")?)
    } else {
        None
    };

    let mut dbus_proxy_pipe = if let Some(dbus_proxy) = &config.dbus_proxy {
        Some(dbus_proxy.spawn().err_msg("dbus_proxy.spawn")?)
    } else {
        None
    };

    let signalfd = SignalFd::new(sigterm_mask).err_msg("signalfd")?;

    let mut fds = [
        PollFd::new(&signalfd, PollFlags::IN),
        PollFd::new(&host_op_sock, PollFlags::IN),
    ];

    loop {
        poll(&mut fds, POLL_TIMEOUT_INFINITY).err_msg("poll")?;

        if !fds[0].revents().is_empty() {
            break;
        }

        let mut buf: [u8; 256] = [0; 256];
        let len = host_op_sock.recv(&mut buf).err_msg("recv")?;
        match &buf[..len] {
            b"start newuidmap;" => {
                config.uid_map.as_ref().unwrap().write_newidmap(child_pid)?;
            }
            b"start newgidmap;" => {
                config.gid_map.as_ref().unwrap().write_newidmap(child_pid)?;
            }
            b"set_memfd_noexec;" => {
                if !Command::new(crate::SET_MEMFD_NOEXEC)
                    .arg(child_pid.to_string())
                    .status()
                    .is_ok_and(|es| es.success())
                {
                    host_op_sock.send(b"failed;").unwrap();
                    continue;
                }
            }
            b"block on xdg-dbus-proxy;" => {
                if let Some(dbus_proxy_pipe) = &mut dbus_proxy_pipe {
                    let mut buf: [u8; 1] = Default::default();
                    dbus_proxy_pipe
                        .read(&mut buf)
                        .err_msg("block on xdg-dbus-proxy")?;
                }
            }
            b"block on pasta;" => {
                if let Some(mut pasta_pipe) = pasta_pipe.take() {
                    let mut buf: [u8; 8] = Default::default();
                    let n = pasta_pipe.0.read(&mut buf).err_msg("block on pasta")?;
                    assert!(n > 0);
                    let pid = atoi(&buf[..n - 1]);
                    pasta_pid = Some(Pid::from_raw(pid).unwrap());
                    let _ = unlink(pasta_pipe.1);
                }
            }
            _ => panic!(
                "Received unexpected message b\"{}\"",
                String::from_utf8_lossy(&buf[..len]),
            ),
        }
        host_op_sock.send(b"success;").unwrap();
    }

    if let Some(mut pasta_pipe) = pasta_pipe {
        ioctl_fionbio(pasta_pipe.0.as_fd(), true).err_msg("ioctl_fionbio")?;
        let mut buf: [u8; 8] = Default::default();
        let n = pasta_pipe.0.read(&mut buf).err_msg("block on pasta")?;
        assert!(n > 0);
        let pid = atoi(&buf[..n - 1]);
        pasta_pid = Some(Pid::from_raw(pid).unwrap());
        let _ = unlink(pasta_pipe.1);
    }

    if let Some(pasta_pid) = pasta_pid {
        kill_process(pasta_pid, Signal::TERM).err_msg("kill_process")?;
    }

    Ok(ExitCode::SUCCESS)
}
