/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#[derive(Debug, Default)]
pub struct Capabilities {
    pub capabilities: Option<capctl::caps::CapSet>,
    pub securebits: Option<capctl::prctl::Secbits>,
}
impl Capabilities {
    pub fn needs_unshare_user(&self) -> bool {
        self.capabilities.is_some() || self.securebits.is_some()
    }

    pub fn apply(&self) -> Result<(), capctl::Error> {
        if let Some(mut caps) = self.capabilities {
            let mut cap_state = capctl::caps::CapState::get_current()?;
            cap_state.inheritable = caps;
            cap_state.set_current()?;

            capctl::caps::ambient::clear()?;
            for cap in caps {
                capctl::caps::ambient::raise(cap)?;
            }

            capctl::caps::bounding::clear_unknown()?;
            for cap in capctl::caps::bounding::probe() & !caps {
                capctl::caps::bounding::drop(cap)?;
            }

            if let Some(secbits) = self.securebits {
                capctl::prctl::set_securebits(secbits)?;
            }

            /* Keep CAP_SYS_ADMIN for the case where we want to install seccomp filters
             * without using NO_NEW_PRIVS. It will be cleared on execve. */
            caps.add(capctl::caps::Cap::SYS_ADMIN);
            cap_state.permitted = caps;
            cap_state.effective = caps;
            cap_state.set_current()?;
        } else if let Some(secbits) = self.securebits {
            capctl::prctl::set_securebits(secbits)?;
        }

        Ok(())
    }
}
