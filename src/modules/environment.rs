/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::{HashMap, HashSet};
use std::env;
use std::ffi::OsString;

#[derive(Debug, Default)]
pub struct Environment {
    pub clear: bool,
    pub keep: HashSet<OsString>,
    pub unset: HashSet<OsString>,
    pub set: HashMap<OsString, OsString>,
}
impl Environment {
    pub fn apply(&self) {
        if self.clear {
            for (key, _) in env::vars_os() {
                if !self.keep.contains(&key) {
                    // FIXME: #68
                    unsafe { env::remove_var(key) };
                }
            }
        }

        for key in &self.unset {
            // FIXME: #68
            unsafe { env::remove_var(key) };
        }

        for (key, val) in &self.set {
            // FIXME: #68
            unsafe { env::set_var(key, val) };
        }
    }
}
