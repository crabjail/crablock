/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use lnix::process::{Gid, Uid, setegid, seteuid, setgid, setuid};

use crate::utils::ResultExt;

#[derive(Debug, Default)]
pub struct Setid {
    pub uid: Option<Uid>,
    pub euid: Option<Uid>,
    pub gid: Option<Gid>,
    pub egid: Option<Gid>,
}
impl Setid {
    pub fn apply(&self) -> Result<(), String> {
        if let Some(uid) = self.uid {
            setuid(uid).err_msg("setuid")?;
        }
        if let Some(euid) = self.euid {
            seteuid(euid).err_msg("seteuid")?;
        }
        if let Some(gid) = self.gid {
            setgid(gid).err_msg("setgid")?;
        }
        if let Some(egid) = self.egid {
            setegid(egid).err_msg("setegid")?;
        }

        Ok(())
    }
}
