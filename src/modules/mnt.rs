/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::collections::BTreeMap;
use std::collections::btree_map::Entry;
use std::ffi::OsString;
use std::fmt::Write;
use std::fs::File;
use std::io::{ErrorKind as IoErrorKind, IsTerminal, stdout};
use std::os::unix::prelude::*;
use std::path::{Path, PathBuf};
use std::sync::OnceLock;
use std::{fmt, fs, path};

use bitflags::bitflags;
use lnix::mount::mountfd::{
    FilesystemContext, FsMountFlags, FsOpenFlags, MountAttr, MountAttrFlags, MountFd,
    MountSetattrFlags, MoveMountFlags, OpenTreeFlags, mount_setattr,
};
use lnix::mount::{UmountFlags, umount2};
use lnix::std_suppl::prelude::*;
use rustix::fs::{
    CWD, FileType, Mode, OFlags, ResolveFlags, fstat, mkdirat, openat2, readlinkat, rmdir, symlink,
};
use rustix::process::pivot_root;

use crate::settings::Settings;
use crate::utils::{EntryBuilder, FilterErrno, ResultExt};

const NOSUID_NODEV_NOEXEC: MountAttrFlags = MountAttrFlags::empty()
    .union(MountAttrFlags::NOSUID)
    .union(MountAttrFlags::NODEV)
    .union(MountAttrFlags::NOEXEC);

#[derive(Debug, Default)]
pub struct Mnt {
    paths: BTreeMap<PathBuf, MntPolicy>,
}
impl Mnt {
    fn add_path<F>(&mut self, path: &Path, flags: MntFlags, mut f: F) -> Result<(), String>
    where
        F: FnMut(Entry<'_, PathBuf, MntPolicy>),
    {
        if flags.contains(MntFlags::CREATE_DIR) {
            let _ = fs::create_dir(path);
        }
        if flags.contains(MntFlags::CREATE_REG) {
            let _ = File::create(path);
        }

        let res = if flags.contains(MntFlags::FOLLOW_SYMLINKS) {
            path.canonicalize()
        } else if flags.contains(MntFlags::NORMALIZE) {
            path::absolute(path)
        } else {
            path.canonicalize_nofollow()
        };

        match res {
            Ok(cpath) => f(self.paths.entry(cpath)),
            Err(e) => {
                if e.kind() != IoErrorKind::NotFound || !flags.contains(MntFlags::EXISTING_ONLY) {
                    bail!("canonicalize* {}: {}", path.display(), e);
                }
            }
        }

        Ok(())
    }

    fn add_access_set(
        &mut self,
        path: &Path,
        flag: MntAccess,
        set_flag: MntAccess,
        flags: MntFlags,
    ) -> Result<(), String> {
        self.add_path(path, flags, |entry| {
            let policy = entry.or_default();
            policy.access.insert(flag | set_flag);
        })
    }

    fn add_access_clear(
        &mut self,
        path: &Path,
        flag: MntAccess,
        set_flag: MntAccess,
        flags: MntFlags,
    ) -> Result<(), String> {
        self.add_path(path, flags, |entry| {
            let policy = entry.or_default();
            policy.access.remove(flag);
            policy.access.insert(set_flag);
        })
    }

    fn add_action(
        &mut self,
        path: &Path,
        action: MntAction,
        flags: MntFlags,
    ) -> Result<(), String> {
        self.add_path(path, flags, |entry| {
            entry.or_default().action = action.clone();
        })
    }

    pub fn add_read_only_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_set(path, MntAccess::RDONLY, MntAccess::SET_RDONLY, flags)
    }

    pub fn add_read_write_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_clear(path, MntAccess::RDONLY, MntAccess::SET_RDONLY, flags)
    }

    pub fn add_nosuid_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_set(path, MntAccess::NOSUID, MntAccess::SET_NOSUID, flags)
    }

    pub fn add_suid_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_clear(path, MntAccess::NOSUID, MntAccess::SET_NOSUID, flags)
    }

    pub fn add_nodev_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_set(path, MntAccess::NODEV, MntAccess::SET_NODEV, flags)
    }

    pub fn add_dev_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_clear(path, MntAccess::NODEV, MntAccess::SET_NODEV, flags)
    }

    pub fn add_noexec_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_set(path, MntAccess::NOEXEC, MntAccess::SET_NOEXEC, flags)
    }

    pub fn add_exec_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_access_clear(path, MntAccess::NOEXEC, MntAccess::SET_NOEXEC, flags)
    }

    pub fn add_mask_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::Mask, flags)
    }

    pub fn add_mask_dev_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::MaskDev, flags)
    }

    pub fn add_private_volatile_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::PrivateVolatile, flags)
    }

    pub fn add_private_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::Private, flags)
    }

    pub fn add_allow_volatile_from_path<P>(
        &mut self,
        path: P,
        from: PathBuf,
        flags: MntFlags,
    ) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::AllowVolatileFrom(from), flags)
    }

    pub fn add_allow_volatile_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::AllowVolatile, flags)
    }

    pub fn add_allow_from_path<P>(
        &mut self,
        path: P,
        from: PathBuf,
        flags: MntFlags,
    ) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::AllowFrom(from), flags)
    }

    pub fn add_allow_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::Allow, flags)
    }

    pub fn add_deny_path<P>(&mut self, path: P, flags: MntFlags) -> Result<(), String>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        self.add_action(path, MntAction::Deny, flags)
    }

    pub fn needs_unshare_mnt(&self) -> bool {
        !self.paths.is_empty()
    }

    pub fn apply(&self, settings: &Settings) -> Result<(), Cow<'static, str>> {
        if self.paths.is_empty() {
            return Ok(());
        }

        /* We need access to the original root tree in order to allow
         * access to subtress of it later. For that we mount a dedicated
         * tree of the original root above our root because cloning
         * subtrees is only possible for trees attached to our
         * mount-namespace (they do not have to be reachable by path,
         * an open fd is enough). This means our "/" is distinct from
         * the "/" of our mount-namespace (which is our "/..").
         * This original root tree is either a clone of the
         * initial root tree or the initial root tree if we replaced
         * our root with pivot_root.
         */
        let oroot = apply_root(&self.paths)?;

        let mut deny_paths = Vec::new();
        for (path, policy) in &self.paths {
            if path.is_root() {
                continue;
            }

            match policy.action {
                MntAction::None => {
                    apply_none_path(path)
                        .with_err_msg(|| format!("apply_none_path {}", path.display()))?;
                }
                MntAction::Mask => {
                    apply_mask_path(path)
                        .with_err_msg(|| format!("apply_mask_path {}", path.display()))?;
                }
                MntAction::MaskDev => {
                    apply_mask_dev_path(path, &oroot)
                        .with_err_msg(|| format!("apply_mask_dev_path {}", path.display()))?;
                }
                MntAction::PrivateVolatile => {
                    apply_private_volatile_path(path).with_err_msg(|| {
                        format!("apply_private_volatile_path {}", path.display())
                    })?;
                }
                MntAction::Private => {
                    apply_private_path(path, &oroot, settings)
                        .with_err_msg(|| format!("apply_private_path {}", path.display()))?;
                }
                MntAction::AllowVolatileFrom(ref from) => {
                    apply_allow_volatile_from_path(path, &oroot, from).with_err_msg(|| {
                        format!("apply_allow_volatile_from_path {}", path.display())
                    })?;
                }
                MntAction::AllowVolatile => {
                    apply_allow_volatile_path(path, &oroot)
                        .with_err_msg(|| format!("apply_allow_volatile_path {}", path.display()))?;
                }
                MntAction::AllowFrom(ref from) => {
                    apply_allow_from_path(path, &oroot, from)
                        .with_err_msg(|| format!("apply_allow_from_path {}", path.display()))?;
                }
                MntAction::Allow => {
                    apply_allow_path(path, &oroot)
                        .with_err_msg(|| format!("apply_allow_path {}", path.display()))?;
                }
                MntAction::Deny => {
                    apply_deny_path(path, &oroot, settings)
                        .with_err_msg(|| format!("apply_deny_path {}", path.display()))?;
                    deny_paths.push(path);
                }
            }
        }

        /* Umount the original root at top most "/" mountpoint. */
        umount2("/", UmountFlags::DETACH).err_msg("umount2")?;

        for (path, policy) in &self.paths {
            if !policy.access.is_empty() {
                debug!("{}: {}", policy.access, path.display());

                let mut mount_attr = policy.access.to_mount_attr();
                mount_setattr(
                    CWD,
                    path,
                    MountSetattrFlags::RECURSIVE | MountSetattrFlags::SYMLINK_NOFOLLOW,
                    &mut mount_attr,
                )
                .err_msg("mount_setattr")?;
            }
        }

        for path in deny_paths {
            debug!("deny: {}", path.display());

            mount_setattr(
                CWD,
                path,
                MountSetattrFlags::SYMLINK_NOFOLLOW,
                MountAttr::new().set_flags(MountAttrFlags::RDONLY | NOSUID_NODEV_NOEXEC),
            )
            .err_msg("mount_setattr")?;
        }

        Ok(())
    }
}

fn apply_root(paths: &BTreeMap<PathBuf, MntPolicy>) -> Result<OwnedFd, Cow<'static, str>> {
    match paths.first_key_value().unwrap() {
        /* No-op, we assume / to be a mountpoint:
         * (path, policy) if path.is_root() && policy.action.is_none() */
        (path, policy) if path.is_root() && policy.action.is_mask() => {
            mask_root().err_msg("mask_root")
        }
        (path, policy) if path.is_root() && policy.action.is_mask_dev() => {
            Err("Requested mnt-mask-dev for / which is not implemented.".into())
        }
        (path, policy) if path.is_root() && policy.action.is_private_volatile() => {
            unimplemented!()
        }
        (path, policy) if path.is_root() && policy.action.is_private() => {
            unimplemented!()
        }
        (path, policy) if path.is_root() && policy.action.is_allow_volatile_from() => {
            unimplemented!()
        }
        (path, policy) if path.is_root() && policy.action.is_allow_volatile() => {
            unimplemented!()
        }
        (path, policy) if path.is_root() && policy.action.is_allow_from() => {
            unimplemented!()
        }
        /* No-op, there is nothing above it:
         * (path, policy) if path.is_root() && policy.action.is_allow() */
        (path, policy) if path.is_root() && policy.action.is_deny() => {
            Err("Requested mnt-deny for / which is impossible.".into())
        }
        _ => clone_root().err_msg("clone_root"),
    }
}

fn clone_root() -> Result<OwnedFd, String> {
    debug!("clone: /");

    let oroot = MountFd::open_tree(CWD, "/", OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE)
        .err_msg("open_tree")?;
    oroot
        .move_to(CWD, "/", MoveMountFlags::default())
        .err_msg("move_mount")?;
    Ok(oroot.into())
}

fn mask_root() -> Result<OwnedFd, String> {
    debug!("mask: /");

    let mfd = FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "750")
        .err_msg("fsconfig_set_string mode=750")?
        .fsconfig_set_string("size", "50m")
        .err_msg("fsconfig_set_string size=50m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?;

    mkdirat(&mfd, "put_old", Mode::empty()).unwrap();
    mfd.move_to(CWD, "/", MoveMountFlags::default())
        .err_msg("move_mount")?;
    // pivot_root("/..", "/../put_old")
    pivot_root(
        mfd.as_fd().to_procfs_path(),
        mfd.as_fd().to_procfs_path().join("put_old"),
    )
    .err_msg("pivot_root")?;

    let oroot =
        MountFd::open_tree(CWD, "/put_old", OpenTreeFlags::default()).err_msg("open_tree")?;
    oroot
        .move_to(CWD, "/", MoveMountFlags::default())
        .err_msg("move_mount")?;
    rmdir("/put_old").err_msg("rmdir")?;
    Ok(oroot.into())
}

fn apply_none_path(path: &Path) -> Result<(), String> {
    debug!("none: {}", path.display());

    /* We bind path onto itself so that mount_setattr can assume every path is a mountpoint. */
    MountFd::open_tree(
        CWD,
        path,
        OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE | OpenTreeFlags::SYMLINK_NOFOLLOW,
    )
    .err_msg("open_tree")?
    .move_to(CWD, path, MoveMountFlags::default())
    .err_msg("move_to")?;

    Ok(())
}

fn apply_mask_path(path: &Path) -> Result<(), String> {
    debug!("mask: {}", path.display());

    FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "750")
        .err_msg("fsconfig_set_string mode=750")?
        .fsconfig_set_string("size", "10m")
        .err_msg("fsconfig_set_string size=10m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn apply_mask_dev_path<FD: AsFd>(path: &Path, oroot: FD) -> Result<(), String> {
    debug!("mask-dev: {}", path.display());

    EntryBuilder::directory()
        .recursive(true)
        .parent_mode(Mode::RWXU)
        .create(path)
        .err_msg("create")?;

    FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "750")
        .err_msg("fsconfig_set_string mode=750")?
        .fsconfig_set_string("size", "10m")
        .err_msg("fsconfig_set_string size=10m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;

    let mut source_path = PathBuf::from("dev/_placeholder");
    let mut target_path = path.join("_placeholder");

    const COMMON_DEVNODES: &[&str] = &["null", "zero", "full", "random", "urandom", "tty"];
    for devnode in COMMON_DEVNODES {
        source_path.set_file_name(devnode);
        target_path.set_file_name(devnode);

        EntryBuilder::regular()
            .create(&target_path)
            .err_msg("create")?;

        MountFd::open_tree(&oroot, &source_path, OpenTreeFlags::CLONE)
            .err_msg("open_tree")?
            .setattr(
                MountSetattrFlags::default(),
                MountAttr::new().set_flags(MountAttrFlags::NOSUID | MountAttrFlags::NOEXEC),
            )
            .err_msg("setattr")?
            .move_to(CWD, &target_path, MoveMountFlags::default())
            .err_msg("move_mount")?;
    }

    if stdout().is_terminal() {
        // Why is getting a Path(Buf) from readlinkat (which returns a CString) so complicated?
        let ttyname = PathBuf::from(OsString::from_vec(
            readlinkat(&oroot, "proc/self/fd/1", [])
                .err_msg("readlinkat")?
                .into_bytes(),
        ));
        target_path.set_file_name("console");

        EntryBuilder::regular()
            .create(&target_path)
            .err_msg("create")?;

        MountFd::open_tree(&oroot, ttyname.strip_root(), OpenTreeFlags::CLONE)
            .err_msg("open_tree")?
            .setattr(
                MountSetattrFlags::default(),
                MountAttr::new().set_flags(MountAttrFlags::NOSUID | MountAttrFlags::NOEXEC),
            )
            .err_msg("setattr")?
            .move_to(CWD, &target_path, MoveMountFlags::default())
            .err_msg("move_mount")?;
    }

    let mut link_path = target_path;
    const COMMON_SYMLINKS: &[(&str, &str)] = &[
        ("/proc/kcore", "core"),
        ("/proc/self/fd", "fd"),
        ("/proc/self/fd/0", "stdin"),
        ("/proc/self/fd/1", "stdout"),
        ("/proc/self/fd/2", "stderr"),
    ];
    for &(target, name) in COMMON_SYMLINKS {
        link_path.set_file_name(name);

        symlink(target, &link_path).err_msg("symlink")?;
    }

    // devpts

    let path_pts = path.join("pts");

    EntryBuilder::directory()
        .create(&path_pts)
        .err_msg("create")?;

    FilesystemContext::fsopen("devpts", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_flag("newinstance")
        .err_msg("fsconfig_set newinstance")?
        .fsconfig_set_string("ptmxmode", "0666")
        .err_msg("fsconfig_set ptmxmode=0666")?
        .fsconfig_set_string("mode", "620")
        .err_msg("fsconfig_set mode=620")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(
            FsMountFlags::default(),
            MountAttrFlags::NOSUID | MountAttrFlags::NOEXEC,
        )
        .err_msg("fsmount")?
        .move_to(CWD, &path_pts, MoveMountFlags::default())
        .err_msg("move_mount")?;

    symlink("pts/ptmx", path.join("ptmx")).err_msg("symlink")?;

    // shm

    let path_shm = path.join("shm");

    EntryBuilder::directory()
        .create(&path_shm)
        .err_msg("create")?;

    FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "1777")
        .err_msg("fsconfig_set mode=1777")?
        .fsconfig_set_string("size", "100m")
        .err_msg("fsconfig_set_string size=100m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, &path_shm, MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn apply_private_volatile_path(path: &Path) -> Result<(), String> {
    debug!("private-volatile: {}", path.display());

    EntryBuilder::directory()
        .recursive(true)
        .parent_mode(Mode::RWXU)
        .create(path)
        .err_msg("create")?;

    FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "750")
        .err_msg("fsconfig_set_string mode=750")?
        .fsconfig_set_string("size", "500m")
        .err_msg("fsconfig_set_string size=500m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn apply_private_path<FD: AsFd>(path: &Path, oroot: FD, settings: &Settings) -> Result<(), String> {
    debug!("private: {}", path.display());

    let ppath = settings.get_mnt_private_path().join(path.strip_root());
    EntryBuilder::directory()
        .mode(Mode::RWXU)
        .recursive(true)
        .parent_mode(Mode::RWXU)
        .create_at_root(&oroot, &ppath)
        .err_msg("create_at_root")?;

    EntryBuilder::directory()
        .recursive(true)
        .parent_mode(Mode::RWXU)
        .create(path)
        .err_msg("create")?;

    MountFd::open_tree(
        oroot.as_fd(),
        ppath.strip_root(),
        OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE,
    )
    .err_msg("open_tree")?
    .move_to(CWD, path, MoveMountFlags::default())
    .err_msg("move_mount")?;

    Ok(())
}

fn apply_allow_volatile_from_path<FD: AsFd>(
    path: &Path,
    oroot: FD,
    from: &Path,
) -> Result<(), String> {
    debug!("allow-volatile-from: {} {}", path.display(), from.display());

    // BEGIN private-volatile
    EntryBuilder::directory()
        .recursive(true)
        .parent_mode(Mode::RWXU)
        .create(path)
        .err_msg("create")?;

    FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "750")
        .err_msg("fsconfig_set_string mode=750")?
        .fsconfig_set_string("size", "500m")
        .err_msg("fsconfig_set_string size=500m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;
    // END private-volatile

    let fd = openat2(
        &oroot,
        from,
        OFlags::CLOEXEC | OFlags::RDONLY | OFlags::NOFOLLOW,
        Mode::empty(),
        ResolveFlags::IN_ROOT,
    )
    .err_msg("openat2")?;

    let upperdir = path.join("diff");
    EntryBuilder::directory()
        .mode(Mode::RWXU)
        .create(&upperdir)
        .err_msg("create")?;

    let workdir = path.join("work");
    EntryBuilder::directory()
        .mode(Mode::RWXU)
        .create(&workdir)
        .err_msg("create")?;

    FilesystemContext::fsopen("overlay", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_fd("lowerdir+", fd.as_fd())
        .err_msg("fsconfig_set_fd lowerdir+")?
        .fsconfig_set_string("upperdir", upperdir)
        .err_msg("fsconfig_set_string upperdir")?
        .fsconfig_set_string("workdir", workdir)
        .err_msg("fsconfig_set_string workdir")?
        .fsconfig_set_flag("userxattr")
        .err_msg("fsconfig_set_flag userxattr")?
        .fsconfig_set_flag("volatile")
        .err_msg("fsconfig_set_flag volatile")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn apply_allow_volatile_path<FD: AsFd>(path: &Path, oroot: FD) -> Result<(), String> {
    debug!("allow-volatile: {}", path.display());

    // BEGIN private-volatile
    EntryBuilder::directory()
        .recursive(true)
        .parent_mode(Mode::RWXU)
        .create(path)
        .err_msg("create")?;

    FilesystemContext::fsopen("tmpfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_string("mode", "750")
        .err_msg("fsconfig_set_string mode=750")?
        .fsconfig_set_string("size", "500m")
        .err_msg("fsconfig_set_string size=500m")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;
    // END private-volatile

    let fd = openat2(
        &oroot,
        path,
        OFlags::CLOEXEC | OFlags::RDONLY | OFlags::NOFOLLOW,
        Mode::empty(),
        ResolveFlags::IN_ROOT,
    )
    .err_msg("openat2")?;

    let upperdir = path.join("diff");
    EntryBuilder::directory()
        .mode(Mode::RWXU)
        .create(&upperdir)
        .err_msg("create")?;

    let workdir = path.join("work");
    EntryBuilder::directory()
        .mode(Mode::RWXU)
        .create(&workdir)
        .err_msg("create")?;

    FilesystemContext::fsopen("overlay", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_fd("lowerdir+", fd.as_fd())
        .err_msg("fsconfig_set_fd lowerdir+")?
        .fsconfig_set_string("upperdir", upperdir)
        .err_msg("fsconfig_set_string upperdir")?
        .fsconfig_set_string("workdir", workdir)
        .err_msg("fsconfig_set_string workdir")?
        .fsconfig_set_flag("userxattr")
        .err_msg("fsconfig_set_flag userxattr")?
        .fsconfig_set_flag("volatile")
        .err_msg("fsconfig_set_flag volatile")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn apply_allow_from_path<FD: AsFd>(path: &Path, oroot: FD, from: &Path) -> Result<(), String> {
    debug!("allow-from: {} {}", path.display(), from.display());

    let fd = openat2(
        &oroot,
        from,
        OFlags::CLOEXEC | OFlags::PATH | OFlags::NOFOLLOW,
        Mode::empty(),
        ResolveFlags::IN_ROOT,
    )
    .err_msg("openat2")?;

    if FileType::from_raw_mode(fstat(&fd).err_msg("fstat")?.st_mode) == FileType::Directory {
        EntryBuilder::directory()
            .recursive(true)
            .parent_mode(Mode::RWXU)
            .create(path)
            .err_msg("create")?;
    } else {
        EntryBuilder::regular()
            .recursive(true)
            .parent_mode(Mode::RWXU)
            .create(path)
            .filter_errno(IoErrorKind::AlreadyExists)
            .err_msg("create")?;
    }

    MountFd::open_tree(
        fd,
        "",
        OpenTreeFlags::EMPTY_PATH | OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE,
    )
    .err_msg("open_tree")?
    .move_to(CWD, path, MoveMountFlags::default())
    .err_msg("move_mount")?;

    Ok(())
}

fn apply_allow_path<FD: AsFd>(path: &Path, oroot: FD) -> Result<(), String> {
    debug!("allow: {}", path.display());

    let fd = openat2(
        &oroot,
        path,
        OFlags::CLOEXEC | OFlags::PATH | OFlags::NOFOLLOW,
        Mode::empty(),
        ResolveFlags::IN_ROOT,
    )
    .err_msg("openat2")?;

    if FileType::from_raw_mode(fstat(&fd).err_msg("fstat")?.st_mode) == FileType::Directory {
        EntryBuilder::directory()
            .recursive(true)
            .parent_mode(Mode::RWXU)
            .create(path)
            .err_msg("create")?;
    } else {
        EntryBuilder::regular()
            .recursive(true)
            .parent_mode(Mode::RWXU)
            .create(path)
            .filter_errno(IoErrorKind::AlreadyExists)
            .err_msg("create")?;
    }

    MountFd::open_tree(
        fd,
        "",
        OpenTreeFlags::EMPTY_PATH | OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE,
    )
    .err_msg("open_tree")?
    .move_to(CWD, path, MoveMountFlags::default())
    .err_msg("move_mount")?;

    Ok(())
}

fn apply_deny_path<FD: AsFd>(path: &Path, oroot: FD, settings: &Settings) -> Result<(), String> {
    debug!("deny: {}", path.display());

    static DENY_BLOCK: OnceLock<PathBuf> = OnceLock::new();
    static DENY_CHAR: OnceLock<PathBuf> = OnceLock::new();
    static DENY_DIR: OnceLock<PathBuf> = OnceLock::new();
    static DENY_FIFO: OnceLock<PathBuf> = OnceLock::new();
    static DENY_REG: OnceLock<PathBuf> = OnceLock::new();
    static DENY_SOCK: OnceLock<PathBuf> = OnceLock::new();
    static DENY_SYM: OnceLock<PathBuf> = OnceLock::new();

    let deny_path = match fs::symlink_metadata(path) {
        Ok(m) if m.file_type().is_dir() => DENY_DIR.get_or_init(|| {
            let deny_dir = settings.get_mnt_deny_path().join("dir");
            EntryBuilder::directory()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_dir)
                .expect("create deny_dir");
            deny_dir
        }),
        Ok(m) if m.file_type().is_file() => DENY_REG.get_or_init(|| {
            let deny_reg = settings.get_mnt_deny_path().join("reg");
            EntryBuilder::regular()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_reg)
                .expect("create deny_reg");
            deny_reg
        }),
        Ok(m) if m.file_type().is_symlink() => DENY_SYM.get_or_init(|| {
            let deny_sym = settings.get_mnt_deny_path().join("sym");
            EntryBuilder::symlink()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_sym)
                .expect("create deny_sym");
            deny_sym
        }),
        Ok(m) if m.file_type().is_fifo() => DENY_FIFO.get_or_init(|| {
            let deny_fifo = settings.get_mnt_deny_path().join("fifo");
            EntryBuilder::fifo()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_fifo)
                .expect("create deny_fifo");
            deny_fifo
        }),
        Ok(m) if m.file_type().is_socket() => DENY_SOCK.get_or_init(|| {
            let deny_sock = settings.get_mnt_deny_path().join("sock");
            EntryBuilder::socket()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_sock)
                .expect("create deny_sock");
            deny_sock
        }),
        Ok(m) if m.file_type().is_char_device() => DENY_CHAR.get_or_init(|| {
            let deny_char = settings.get_mnt_deny_path().join("char");
            EntryBuilder::char_device()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_char)
                .expect("create deny_char");
            deny_char
        }),
        Ok(m) if m.file_type().is_block_device() => DENY_BLOCK.get_or_init(|| {
            let deny_block = settings.get_mnt_deny_path().join("block");
            EntryBuilder::block_device()
                .mode(Mode::empty())
                .recursive(true)
                .parent_mode(Mode::RWXU)
                .create_at_root(&oroot, &deny_block)
                .expect("create deny_block");
            deny_block
        }),
        Err(err) => return Err(format!("symlink_metadata returned: {err}")),
        _ => unreachable!(),
    };

    MountFd::open_tree(&oroot, deny_path, OpenTreeFlags::CLONE)
        .err_msg("open_tree")?
        .setattr(
            MountSetattrFlags::default(),
            MountAttr::new().set_flags(MountAttrFlags::RDONLY | NOSUID_NODEV_NOEXEC),
        )
        .err_msg("setattr")?
        .move_to(CWD, path, MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

#[derive(Debug, Default, Clone)]
struct MntPolicy {
    action: MntAction,
    access: MntAccess,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum MntAction {
    #[default]
    None,
    Mask,
    MaskDev,
    PrivateVolatile,
    Private,
    AllowVolatileFrom(PathBuf),
    AllowVolatile,
    AllowFrom(PathBuf),
    Allow,
    Deny,
}
impl MntAction {
    #[expect(dead_code, reason = "Possible future use")]
    fn is_none(&self) -> bool {
        *self == Self::None
    }

    fn is_mask(&self) -> bool {
        *self == Self::Mask
    }

    fn is_mask_dev(&self) -> bool {
        *self == Self::MaskDev
    }

    fn is_private_volatile(&self) -> bool {
        *self == Self::PrivateVolatile
    }

    fn is_private(&self) -> bool {
        *self == Self::Private
    }

    fn is_allow_volatile_from(&self) -> bool {
        matches!(self, Self::AllowVolatileFrom(_))
    }

    fn is_allow_volatile(&self) -> bool {
        *self == Self::AllowVolatile
    }

    fn is_allow_from(&self) -> bool {
        matches!(self, Self::AllowFrom(_))
    }

    #[expect(dead_code, reason = "Possible future use")]
    fn is_allow(&self) -> bool {
        *self == Self::Allow
    }

    fn is_deny(&self) -> bool {
        *self == Self::Deny
    }
}

const_assert!(MntAccess::RDONLY.bits() == MountAttrFlags::RDONLY.bits());
const_assert!(MntAccess::NOSUID.bits() == MountAttrFlags::NOSUID.bits());
const_assert!(MntAccess::NODEV.bits() == MountAttrFlags::NODEV.bits());
const_assert!(MntAccess::NOEXEC.bits() == MountAttrFlags::NOEXEC.bits());

bitflags! {
    #[derive(Debug, Default, Clone, Copy)]
    struct MntAccess: u32 {
        const RDONLY = 0x01;
        const SET_RDONLY = 0x10;
        const NOSUID = 0x02;
        const SET_NOSUID = 0x20;
        const NODEV = 0x04;
        const SET_NODEV = 0x40;
        const NOEXEC = 0x08;
        const SET_NOEXEC = 0x80;

        const _SET_MASK = 0xf0;
    }
}
impl MntAccess {
    fn to_mount_attr(self) -> MountAttr {
        let mut mount_attr = MountAttr::new();
        mount_attr.set_flags(MountAttrFlags::from_bits(self.bits() & (self.bits() >> 4)).unwrap());
        mount_attr
            .clear_flags(MountAttrFlags::from_bits(!self.bits() & (self.bits() >> 4)).unwrap());
        mount_attr
    }
}
impl fmt::Display for MntAccess {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut is_first = true;

        if self.contains(Self::SET_RDONLY) {
            is_first = false;
            if self.contains(Self::RDONLY) {
                f.write_str("ro")?;
            } else {
                f.write_str("rw")?;
            }
        }

        if self.contains(Self::SET_NOSUID) {
            if !is_first {
                f.write_char(',')?;
            }
            if self.contains(Self::NOSUID) {
                f.write_str("nosuid")?;
            } else {
                f.write_str("suid")?;
            }
            is_first = false;
        }

        if self.contains(Self::SET_NODEV) {
            if !is_first {
                f.write_char(',')?;
            }
            if self.contains(Self::NODEV) {
                f.write_str("nodev")?;
            } else {
                f.write_str("dev")?;
            }
            is_first = false;
        }

        if self.contains(Self::SET_NOEXEC) {
            if !is_first {
                f.write_char(',')?;
            }
            if self.contains(Self::NOEXEC) {
                f.write_str("noexec")?;
            } else {
                f.write_str("exec")?;
            }
        }

        Ok(())
    }
}

bitflags! {
    #[derive(Debug, Default, Clone, Copy)]
    pub struct MntFlags: u8 {
        const FOLLOW_SYMLINKS = 1 << 0;
        const NORMALIZE = 1 << 1;
        const EXISTING_ONLY = 1 << 2;
        const CREATE_DIR = 1 << 3;
        const CREATE_REG = 1 << 4;
    }
}
impl fmt::Display for MntFlags {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if !self.is_empty() {
            f.write_str("+")?;
            if self.contains(Self::FOLLOW_SYMLINKS) {
                f.write_str("f")?;
            }
            if self.contains(Self::NORMALIZE) {
                f.write_str("n")?;
            }
            if self.contains(Self::EXISTING_ONLY) {
                f.write_str("e")?;
            }
            if self.contains(Self::CREATE_DIR) {
                f.write_str("c")?;
            }
            if self.contains(Self::CREATE_REG) {
                f.write_str("r")?;
            }
        }

        Ok(())
    }
}
