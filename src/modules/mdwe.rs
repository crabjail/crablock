/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use crate::utils::ResultExt;

#[derive(Debug, Default, PartialEq, Eq)]
pub struct Mdwe {
    pub refuse_exec_gain: bool,
}
impl Mdwe {
    pub fn apply(&self) -> Result<(), String> {
        if self.refuse_exec_gain {
            debug!("MDWE: refuse exec gain");
            capctl::prctl::set_mdwe(capctl::prctl::MDWEFlags::REFUSE_EXEC_GAIN)
                .err_msg("MDWE_REFUSE_EXEC_GAIN")?;
        }

        Ok(())
    }
}
