/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use crate::utils::{FilterErrno, ResultExt};

#[derive(Debug, Default, PartialEq, Eq)]
pub enum Mitigations {
    #[default]
    Inherit,
    Strict,
}
impl Mitigations {
    pub fn apply(&self) -> Result<(), String> {
        if *self != Self::Strict {
            return Ok(());
        }

        capctl::prctl::set_speculation_ctrl(
            capctl::prctl::SpecVariant::StoreBypass,
            capctl::prctl::SpecFlags::FORCE_DISABLE,
        )
        .filter_errno(libc::ENXIO)
        .err_msg("Disable speculative store bypass misfeature")?;

        capctl::prctl::set_speculation_ctrl(
            capctl::prctl::SpecVariant::IndirectBranch,
            capctl::prctl::SpecFlags::FORCE_DISABLE,
        )
        .filter_errno(libc::ENXIO)
        .err_msg("Disable indirect branch speculation misfeature")?;

        Ok(())
    }
}
