/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use rustix::system::sethostname;

use crate::utils::ResultExt;

#[derive(Debug, Default, PartialEq, Eq)]
pub enum Hostname {
    #[default]
    Unchanged,
    //Random,
    Custom(String),
}
impl Hostname {
    pub fn needs_unshare_uts(&self) -> bool {
        *self != Self::Unchanged
    }

    pub fn apply(&self) -> Result<(), String> {
        let name = match self {
            Self::Unchanged => return Ok(()),
            //Self::Random => unimplemented!(),
            Self::Custom(name) => name,
        };
        sethostname(name.as_bytes()).err_msg("sethostname")?;

        Ok(())
    }
}
