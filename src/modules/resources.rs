/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs;

use rustix::process::nice;

use crate::utils::ResultExt;

#[derive(Debug, Default)]
pub struct Resources {
    pub choom: Option<i16>,
    pub nice: Option<i32>,
}
impl Resources {
    pub fn apply(&self) -> Result<(), String> {
        if let Some(score) = self.choom {
            fs::write("/proc/self/oom_score_adj", score.to_string())
                .err_msg("write /proc/self/oom_score_adj")?;
        }

        if let Some(inc) = self.nice {
            nice(inc).err_msg("nice")?;
        }

        Ok(())
    }
}
