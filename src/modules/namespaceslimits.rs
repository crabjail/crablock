/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::cell::RefCell;
use std::fs::File;
use std::io::prelude::Write;
use std::os::unix::prelude::OwnedFd;

use lnix::std_suppl::prelude::FileExt;
use rustix::fs::{CWD, Mode, OFlags, openat};

use crate::utils::ResultExt;

#[derive(Debug, Default)]
pub struct NamespacesLimits {
    pub cgroup: Option<i32>,
    pub ipc: Option<i32>,
    pub mnt: Option<i32>,
    pub net: Option<i32>,
    pub pid: Option<i32>,
    pub time: Option<i32>,
    pub user: Option<i32>,
    pub uts: Option<i32>,

    proc_sys_user: RefCell<Option<OwnedFd>>,
}
impl NamespacesLimits {
    pub fn needs_unshare_user(&self) -> bool {
        self.cgroup.is_some()
            || self.ipc.is_some()
            || self.mnt.is_some()
            || self.net.is_some()
            || self.pid.is_some()
            || self.time.is_some()
            || self.user.is_some()
            || self.uts.is_some()
    }

    /* We need to open a file-descriptor for /proc/sys before mount-proc
     * possible hides it with subset=pid and we can no longer access it.
     * At the same time, we can limit max_mnt_namespaces only after all
     * fsmount/open_tree calls are done, otherwise they would fail with
     * ENOSPC.
     */
    pub fn open_proc_sys_user(&self) -> Result<(), String> {
        *self.proc_sys_user.borrow_mut() = Some(
            openat(
                CWD,
                "/proc/sys/user",
                OFlags::CLOEXEC | OFlags::PATH | OFlags::DIRECTORY,
                Mode::empty(),
            )
            .err_msg("open /proc/sys/user")?,
        );

        Ok(())
    }

    pub fn apply(&self) -> Result<(), String> {
        let proc_sys_user_fd = self.proc_sys_user.take().unwrap();
        let limits = [
            ("max_cgroup_namespaces", self.cgroup),
            ("max_ipc_namespaces", self.ipc),
            ("max_mnt_namespaces", self.mnt),
            ("max_net_namespaces", self.net),
            ("max_pid_namespaces", self.pid),
            ("max_time_namespaces", self.time),
            ("max_user_namespaces", self.user),
            ("max_uts_namespaces", self.uts),
        ];

        for (file, count) in limits {
            if let Some(count) = count {
                File::create_at(&proc_sys_user_fd, file)
                    .with_err_msg(|| format!("open_at {file}"))?
                    .write_all(count.to_string().as_bytes())
                    .with_err_msg(|| format!("write_all {file}"))?;
            }
        }

        Ok(())
    }
}
