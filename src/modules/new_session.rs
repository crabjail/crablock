/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use rustix::process::setsid;

use crate::utils::ResultExt;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum NewSession {
    #[default]
    No,
    Yes,
}
impl NewSession {
    pub fn apply(self) -> Result<(), String> {
        if self == Self::Yes {
            setsid().err_msg("setsid")?;
        }

        Ok(())
    }
}
