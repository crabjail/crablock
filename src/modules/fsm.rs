/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::env::set_current_dir;
use std::fmt;
use std::os::unix::fs::chroot;
use std::path::PathBuf;
use std::str::FromStr;

use lnix::error::Errno as LnixErrno;
use lnix::mount::mountfd::{
    FilesystemContext, FsMountFlags, FsOpenFlags, MountAttr, MountAttrFlags, MountFd,
    MountSetattrFlags, MoveMountFlags, OpenTreeFlags, mount_setattr, move_mount,
};
use lnix::mount::{UmountFlags, umount2};
use rustix::fs::CWD;
use rustix::process::pivot_root;

use crate::utils::{ResultExt, TriBool};

#[derive(Debug, Default)]
pub struct Fsm {
    manipulations: Vec<Manipulation>,
    needs_unshare_pid: bool,
    needs_unshare_ipc: bool,
    needs_unshare_net: bool,
}
impl Fsm {
    pub fn add_mount<T>(
        &mut self,
        target: T,
        follow_target: TriBool,
        filesystem: &str,
        super_options: &str,
        mount_options: &str,
    ) -> Result<(), String>
    where
        T: Into<PathBuf>,
    {
        match filesystem {
            "proc" => self.needs_unshare_pid = true,
            "mqueue" => self.needs_unshare_ipc = true,
            "sysfs" => self.needs_unshare_net = true,
            _ => (),
        }

        let mut ro = TriBool::default();
        let mut nosuid = TriBool::default();
        let mut nodev = TriBool::default();
        let mut noexec = TriBool::default();
        for mount_option in mount_options.split(',') {
            match mount_option {
                "none" => (),
                "ro" => ro = TriBool::True,
                "rw" => ro = TriBool::False,
                "nosuid" => nosuid = TriBool::True,
                "suid" => nosuid = TriBool::False,
                "nodev" => nodev = TriBool::True,
                "dev" => nodev = TriBool::False,
                "noexec" => noexec = TriBool::True,
                "exec" => noexec = TriBool::False,
                _ => bail!("Unknown mount-option '{}'", mount_option),
            }
        }

        self.manipulations.push(Manipulation::Mount {
            target: target.into(),
            follow_target,
            filesystem: Filesystem::new(filesystem, super_options)?,
            ro,
            nosuid,
            nodev,
            noexec,
        });

        Ok(())
    }

    pub fn add_umount<P: Into<PathBuf>>(&mut self, target: P) {
        self.manipulations.push(Manipulation::Umount {
            target: target.into(),
            detach: TriBool::default(),
            nofollow: TriBool::default(),
        });
    }

    pub fn add_bind<S, T>(
        &mut self,
        source: S,
        follow_source: TriBool,
        target: T,
        follow_target: TriBool,
        recursive: TriBool,
    ) where
        S: Into<PathBuf>,
        T: Into<PathBuf>,
    {
        self.manipulations.push(Manipulation::Bind {
            source: source.into(),
            follow_source,
            target: target.into(),
            follow_target,
            recursive,
        });
    }

    pub fn add_move_mount<F, T>(&mut self, from: F, follow_from: TriBool, to: T, follow_to: TriBool)
    where
        F: Into<PathBuf>,
        T: Into<PathBuf>,
    {
        self.manipulations.push(Manipulation::MoveMount {
            from: from.into(),
            follow_from,
            to: to.into(),
            follow_to,
        });
    }

    pub fn add_pivot_root<P1, P2>(&mut self, new_root: P1, put_old: P2)
    where
        P1: Into<PathBuf>,
        P2: Into<PathBuf>,
    {
        self.manipulations.push(Manipulation::PivotRoot {
            new_root: new_root.into(),
            put_old: put_old.into(),
        });
    }

    pub fn add_mount_setattr<P: Into<PathBuf>>(
        &mut self,
        path: P,
        follow_path: TriBool,
        recursive: TriBool,
        attrs: &str,
    ) -> Result<(), String> {
        let mut attr_rdonly = TriBool::default();
        let mut attr_nosuid = TriBool::default();
        let mut attr_nodev = TriBool::default();
        let mut attr_noexec = TriBool::default();
        let mut attr_nosymfollow = TriBool::default();
        for attr in attrs.split(',') {
            match attr {
                "rdonly" => attr_rdonly = TriBool::True,
                "nordonly" => attr_rdonly = TriBool::False,
                "nosuid" => attr_nosuid = TriBool::True,
                "nonosuid" => attr_nosuid = TriBool::False,
                "nodev" => attr_nodev = TriBool::True,
                "nonodev" => attr_nodev = TriBool::False,
                "noexec" => attr_noexec = TriBool::True,
                "nonoexec" => attr_noexec = TriBool::False,
                "nosymfollow" => attr_nosymfollow = TriBool::True,
                "nonosymfollow" => attr_nosymfollow = TriBool::False,
                _ => bail!("Unknown mount-attr '{}'", attr),
            }
        }

        self.manipulations.push(Manipulation::MountSetattr {
            path: path.into(),
            follow_path,
            recursive,
            attr_rdonly,
            attr_nosuid,
            attr_nodev,
            attr_noexec,
            attr_nosymfollow,
        });

        Ok(())
    }

    pub fn add_chdir<P: Into<PathBuf>>(&mut self, path: P) {
        self.manipulations
            .push(Manipulation::Chdir { path: path.into() });
    }

    pub fn add_chroot<P: Into<PathBuf>>(&mut self, path: P) {
        self.manipulations
            .push(Manipulation::Chroot { path: path.into() });
    }

    pub fn needs_unshare_pid(&self) -> bool {
        self.needs_unshare_pid
    }

    pub fn needs_unshare_mnt(&self) -> bool {
        !self.manipulations.is_empty()
    }

    pub fn needs_unshare_ipc(&self) -> bool {
        self.needs_unshare_ipc
    }

    pub fn needs_unshare_net(&self) -> bool {
        self.needs_unshare_net
    }

    pub fn apply(&self) -> Result<(), String> {
        for manipulation in &self.manipulations {
            manipulation.apply()?;
        }

        Ok(())
    }
}

#[derive(Debug)]
enum Manipulation {
    Mount {
        target: PathBuf,
        follow_target: TriBool,
        filesystem: Filesystem,
        ro: TriBool,
        nosuid: TriBool,
        nodev: TriBool,
        noexec: TriBool,
    },
    Umount {
        target: PathBuf,
        // force: TriBool,
        detach: TriBool,
        // expire: TriBool,
        nofollow: TriBool,
    },
    Bind {
        source: PathBuf,
        follow_source: TriBool,
        target: PathBuf,
        follow_target: TriBool,
        recursive: TriBool,
    },
    MoveMount {
        from: PathBuf,
        follow_from: TriBool,
        to: PathBuf,
        follow_to: TriBool,
    },
    PivotRoot {
        new_root: PathBuf,
        put_old: PathBuf,
    },
    MountSetattr {
        path: PathBuf,
        follow_path: TriBool,
        recursive: TriBool,
        attr_rdonly: TriBool,
        attr_nosuid: TriBool,
        attr_nodev: TriBool,
        attr_noexec: TriBool,
        attr_nosymfollow: TriBool,
    },
    // _____Propagation {
    //     path: PathBuf,
    // },
    Chdir {
        path: PathBuf,
    },
    Chroot {
        path: PathBuf,
    },
    // Mkdir {
    //     path: PathBuf,
    //     mode: Mode,
    // },
    // MkdirP {
    //     path: PathBuf,
    //     mode: Mode,
    // },
    // Mknod {
    //     path: PathBuf,
    //     // type: rustix::fs::FileType, {reg, sock, fifo}?
    //     mode: Mode,
    // },
    // Symlink {
    //     target: PathBuf,
    //     path: PathBuf,
    // },
    // Chmod {
    //     path: PathBuf,
    //     mode: Mode,
    // },
    // Chown {
    //     path: PathBuf,
    //     owner: Uid,
    //     group: Gid,
    // },
}
impl Manipulation {
    fn apply(&self) -> Result<(), String> {
        match self {
            Self::Mount {
                target,
                follow_target,
                filesystem,
                ro,
                nosuid,
                nodev,
                noexec,
            } => {
                let fs_fd = filesystem.create_and_configure().with_err_msg(|| {
                    format!(
                        "Failed to create_and_configure the filesystem for {}",
                        target.display()
                    )
                })?;
                let mut attr_flags = MountAttrFlags::default();
                attr_flags.set(MountAttrFlags::RDONLY, ro.is_true());
                attr_flags.set(MountAttrFlags::NOSUID, !nosuid.is_false());
                attr_flags.set(MountAttrFlags::NODEV, !nodev.is_false());
                attr_flags.set(MountAttrFlags::NOEXEC, !noexec.is_false());
                let mut move_mount_flags = MoveMountFlags::default();
                move_mount_flags.set(MoveMountFlags::T_SYMLINKS, follow_target.is_true());
                fs_fd
                    .fsmount(FsMountFlags::default(), attr_flags)
                    .with_err_msg(|| {
                        format!("Failed to mount the filesystem for {}", target.display())
                    })?
                    .move_to(CWD, target, move_mount_flags)
                    .with_err_msg(|| {
                        format!("Failed to attach the mount to {}", target.display())
                    })?;
            }
            Self::Umount {
                target,
                detach,
                nofollow,
            } => {
                let mut umount_flags = UmountFlags::empty();
                umount_flags.set(UmountFlags::DETACH, !detach.is_false());
                umount_flags.set(UmountFlags::NOFOLLOW, !nofollow.is_false());
                umount2(target, umount_flags)
                    .with_err_msg(|| format!("Failed to umount2 {}", target.display()))?;
            }
            Self::Bind {
                source,
                follow_source,
                target,
                follow_target,
                recursive,
            } => {
                let mut open_tree_flags = OpenTreeFlags::CLONE;
                open_tree_flags.set(OpenTreeFlags::SYMLINK_NOFOLLOW, !follow_source.is_true());
                open_tree_flags.set(OpenTreeFlags::RECURSIVE, !recursive.is_false());
                let mut move_mount_flags = MoveMountFlags::default();
                move_mount_flags.set(MoveMountFlags::T_SYMLINKS, follow_target.is_true());
                MountFd::open_tree(CWD, source, open_tree_flags)
                    .with_err_msg(|| format!("Failed to clone {}", source.display()))?
                    .move_to(CWD, target, move_mount_flags)
                    .with_err_msg(|| format!("Failed to attach subtree to {}", target.display()))?;
            }
            Self::MoveMount {
                from,
                follow_from,
                to,
                follow_to,
            } => {
                let mut move_mount_flags = MoveMountFlags::default();
                move_mount_flags.set(MoveMountFlags::F_SYMLINKS, follow_from.is_true());
                move_mount_flags.set(MoveMountFlags::T_SYMLINKS, follow_to.is_true());
                move_mount(CWD, from, CWD, to, move_mount_flags).with_err_msg(|| {
                    format!(
                        "Failed to move_mount {} to {}",
                        from.display(),
                        to.display()
                    )
                })?;
            }
            Self::PivotRoot { new_root, put_old } => {
                pivot_root(new_root, put_old)
                    .with_err_msg(|| format!("Failed to pivot_root to {}", new_root.display()))?;
            }
            Self::MountSetattr {
                path,
                follow_path,
                recursive,
                attr_rdonly,
                attr_nosuid,
                attr_nodev,
                attr_noexec,
                attr_nosymfollow,
            } => {
                let mut flags = MountSetattrFlags::default();
                flags.set(MountSetattrFlags::SYMLINK_NOFOLLOW, !follow_path.is_true());
                flags.set(MountSetattrFlags::RECURSIVE, !recursive.is_false());
                let mut attr = MountAttr::new();
                if attr_rdonly.is_true() {
                    attr.set_flags(MountAttrFlags::RDONLY);
                } else if attr_rdonly.is_false() {
                    attr.clear_flags(MountAttrFlags::RDONLY);
                }
                if attr_nosuid.is_true() {
                    attr.set_flags(MountAttrFlags::NOSUID);
                } else if attr_nosuid.is_false() {
                    attr.clear_flags(MountAttrFlags::NOSUID);
                }
                if attr_nodev.is_true() {
                    attr.set_flags(MountAttrFlags::NODEV);
                } else if attr_nodev.is_false() {
                    attr.clear_flags(MountAttrFlags::NODEV);
                }
                if attr_noexec.is_true() {
                    attr.set_flags(MountAttrFlags::NOEXEC);
                } else if attr_noexec.is_false() {
                    attr.clear_flags(MountAttrFlags::NOEXEC);
                }
                if attr_nosymfollow.is_true() {
                    attr.set_flags(MountAttrFlags::NOSYMFOLLOW);
                } else if attr_nosymfollow.is_false() {
                    attr.clear_flags(MountAttrFlags::NOSYMFOLLOW);
                }
                mount_setattr(CWD, path, flags, &mut attr)
                    .with_err_msg(|| format!("Failed to mount_setattr on {}", path.display()))?;
            }
            Self::Chdir { path } => {
                set_current_dir(path)
                    .with_err_msg(|| format!("Failed to chdir to {}", path.display()))?;
            }
            Self::Chroot { path } => {
                chroot(path).with_err_msg(|| format!("Failed to chroot to {}", path.display()))?;
            }
        }

        Ok(())
    }
}

#[derive(Debug)]
enum Filesystem {
    Devpts {
        // uid: (),
        // gid: (),
        // mode: (),
        // ptmxmode: (),
        // newinstance: TriBool,
        // max: (),
    },
    MQueue,
    Overlay {
        lowerdirs: Vec<PathBuf>,
        datadirs: Vec<PathBuf>,
        workdir: Option<PathBuf>,
        upperdir: Option<PathBuf>,
        default_permissions: TriBool,
        redirect_dir: Option<FilesystemOverlayRedirectDir>,
        index: TriBool,
        uuid: Option<FilesystemOverlayUuid>,
        nfs_export: TriBool,
        userxattr: TriBool,
        xino: Option<FilesystemOverlayXino>,
        metacopy: TriBool,
        verity: Option<FilesystemOverlayVerity>,
        volatile: TriBool,
    },
    Proc {
        /// Actually `Option<u32>`, but we would do nothing more that with it than
        /// `u32::from_str(s).to_string()` and passing it to the kernel.
        gid: Option<String>,
        hidepid: Option<FilesystemProcHidepid>,
        /// `subset=pid`
        pidonly: TriBool,
    },
    Sysfs,
    Tmpfs {
        // gid: Gid,
        // huge: ???, // never, always, within_size, advise
        // mode: ???,
        // mpol: ???,
        nr_blocks: Option<String>,
        nr_inodes: Option<String>,
        size: Option<String>,
        // uid: ???,
        // inode32: bool,
        // inode64: bool,
        noswap: TriBool,
        // casefold: ???,
        // casefold: bool,
        // string_encoding: ???,
    },
}
impl Filesystem {
    fn new(filesystem: &str, super_options: &str) -> Result<Self, String> {
        match filesystem {
            "devpts" => Ok(Self::Devpts {}),
            "mqueue" => Ok(Self::MQueue),
            "overlay" => {
                let mut lowerdirs = Vec::default();
                let mut datadirs = Vec::default();
                let mut workdir = Option::default();
                let mut upperdir = Option::default();
                let mut default_permissions = TriBool::default();
                let mut redirect_dir = Option::default();
                let mut index = TriBool::default();
                let mut uuid = Option::default();
                let mut nfs_export = TriBool::default();
                let mut userxattr = TriBool::default();
                let mut xino = Option::default();
                let mut metacopy = TriBool::default();
                let mut verity = Option::default();
                let mut volatile = TriBool::default();
                for super_option in super_options.split(',') {
                    if super_option == "none" {
                    } else if let Some(lowerdir_spec) = super_option.strip_prefix("lowerdir=") {
                        lowerdirs.clear();
                        datadirs.clear();

                        if let Some((lowerdir_spec, datadir_spec)) = lowerdir_spec.split_once("::")
                        {
                            for path in lowerdir_spec.split(':') {
                                lowerdirs.push(PathBuf::from(path));
                            }
                            for path in datadir_spec.split(':') {
                                datadirs.push(PathBuf::from(path));
                            }
                        } else {
                            for path in lowerdir_spec.split(':') {
                                lowerdirs.push(PathBuf::from(path));
                            }
                        }
                    } else if let Some(path) = super_option.strip_prefix("lowerdir+=") {
                        lowerdirs.push(PathBuf::from(path));
                    } else if let Some(path) = super_option.strip_prefix("datadir+=") {
                        datadirs.push(PathBuf::from(path));
                    } else if let Some(path) = super_option.strip_prefix("workdir=") {
                        workdir = Some(PathBuf::from(path));
                    } else if let Some(path) = super_option.strip_prefix("upperdir=") {
                        upperdir = Some(PathBuf::from(path));
                    } else if super_option == "default_permissions" {
                        default_permissions = TriBool::True;
                    } else if let Some(mode) = super_option.strip_prefix("redirect_dir=") {
                        redirect_dir = Some(FilesystemOverlayRedirectDir::from_str(mode)?);
                    } else if super_option == "index=on" {
                        index = TriBool::True;
                    } else if super_option == "index=off" {
                        index = TriBool::False;
                    } else if let Some(mode) = super_option.strip_prefix("uuid=") {
                        uuid = Some(FilesystemOverlayUuid::from_str(mode)?);
                    } else if super_option == "nfs_export=on" {
                        nfs_export = TriBool::True;
                    } else if super_option == "nfs_export=off" {
                        nfs_export = TriBool::False;
                    } else if super_option == "userxattr" {
                        userxattr = TriBool::True;
                    } else if let Some(mode) = super_option.strip_prefix("xino=") {
                        xino = Some(FilesystemOverlayXino::from_str(mode)?);
                    } else if super_option == "metacopy=on" {
                        metacopy = TriBool::True;
                    } else if super_option == "metacopy=off" {
                        metacopy = TriBool::False;
                    } else if let Some(mode) = super_option.strip_prefix("verity=") {
                        verity = Some(FilesystemOverlayVerity::from_str(mode)?);
                    } else if super_option == "volatile" {
                        volatile = TriBool::True;
                    } else {
                        bail!("Unknown super-option '{}''", super_option);
                    }
                }

                Ok(Self::Overlay {
                    lowerdirs,
                    datadirs,
                    workdir,
                    upperdir,
                    default_permissions,
                    redirect_dir,
                    index,
                    uuid,
                    nfs_export,
                    userxattr,
                    xino,
                    metacopy,
                    verity,
                    volatile,
                })
            }
            "proc" => {
                let mut gid = Option::default();
                let mut hidepid = Option::default();
                let mut pidonly = TriBool::default();
                for super_option in super_options.split(',') {
                    if super_option == "none" {
                    } else if let Some(value) = super_option.strip_prefix("gid=") {
                        gid = Some(value.to_string());
                    } else if let Some(value) = super_option.strip_prefix("hidepid=") {
                        hidepid = Some(FilesystemProcHidepid::from_str(value)?);
                    } else if super_option == "subset=pid" {
                        pidonly = TriBool::True;
                    } else if super_option == "subset=" {
                        pidonly = TriBool::False;
                    } else {
                        bail!("Unknown super-option '{}''", super_option);
                    }
                }

                Ok(Self::Proc {
                    gid,
                    hidepid,
                    pidonly,
                })
            }
            "sysfs" => Ok(Self::Sysfs),
            "tmpfs" => {
                let mut nr_blocks = Option::default();
                let mut nr_inodes = Option::default();
                let mut size = Option::default();
                let mut noswap = TriBool::default();
                for super_option in super_options.split(',') {
                    if super_option == "none" {
                    } else if let Some(value) = super_option.strip_prefix("nr_blocks=") {
                        nr_blocks = Some(String::from(value));
                    } else if let Some(value) = super_option.strip_prefix("nr_inodes=") {
                        nr_inodes = Some(String::from(value));
                    } else if let Some(value) = super_option.strip_prefix("size=") {
                        size = Some(String::from(value));
                    } else if super_option == "noswap" {
                        noswap = TriBool::True;
                    } else {
                        bail!("Unknown super-option '{}''", super_option);
                    }
                }

                Ok(Self::Tmpfs {
                    nr_blocks,
                    nr_inodes,
                    size,
                    noswap,
                })
            }
            _i => Err(format!("Unknown filesystem '{filesystem}'")),
        }
    }

    fn create_and_configure(&self) -> Result<FilesystemContext, LnixErrno> {
        let fs_name = match self {
            Self::Devpts { .. } => "devpts",
            Self::MQueue => "mqueue",
            Self::Overlay { .. } => "overlay",
            Self::Proc { .. } => "proc",
            Self::Sysfs => "sysfs",
            Self::Tmpfs { .. } => "tmpfs",
        };

        let fs_fd = FilesystemContext::fsopen(fs_name, FsOpenFlags::default())?;

        match self {
            Self::Devpts {} => {
                fs_fd.fsconfig_set_flag("newinstance")?;
                fs_fd.fsconfig_set_string("ptmxmode", "0666")?;
                fs_fd.fsconfig_set_string("mode", "620")?;
            }
            Self::MQueue => (),
            Self::Overlay {
                lowerdirs,
                datadirs,
                workdir,
                upperdir,
                default_permissions,
                redirect_dir,
                index,
                uuid,
                nfs_export,
                userxattr,
                xino,
                metacopy,
                verity,
                volatile,
            } => {
                for lowerdir in lowerdirs {
                    fs_fd.fsconfig_set_string("lowerdir+", lowerdir)?;
                }
                for datadir in datadirs {
                    fs_fd.fsconfig_set_string("datadir+", datadir)?;
                }
                if let Some(workdir) = workdir {
                    fs_fd.fsconfig_set_string("workdir", workdir)?;
                }
                if let Some(upperdir) = upperdir {
                    fs_fd.fsconfig_set_string("upperdir", upperdir)?;
                }
                if default_permissions.is_true() {
                    fs_fd.fsconfig_set_flag("default_permissions")?;
                }
                if let Some(redirect_dir) = redirect_dir {
                    fs_fd.fsconfig_set_string("redirect_dir", redirect_dir.as_str())?;
                }
                if index.is_true() {
                    fs_fd.fsconfig_set_string("index", "on")?;
                } else if index.is_false() {
                    fs_fd.fsconfig_set_string("index", "off")?;
                }
                if let Some(uuid) = uuid {
                    fs_fd.fsconfig_set_string("uuid", uuid.as_str())?;
                }
                if nfs_export.is_true() {
                    fs_fd.fsconfig_set_string("nfs_export", "on")?;
                } else if nfs_export.is_false() {
                    fs_fd.fsconfig_set_string("nfs_export", "off")?;
                }
                if userxattr.is_true() {
                    fs_fd.fsconfig_set_flag("userxattr")?;
                }
                if let Some(xino) = xino {
                    fs_fd.fsconfig_set_string("xino", xino.as_str())?;
                }
                if metacopy.is_true() {
                    fs_fd.fsconfig_set_string("metacopy", "on")?;
                } else if metacopy.is_false() {
                    fs_fd.fsconfig_set_string("metacopy", "off")?;
                }
                if let Some(verity) = verity {
                    fs_fd.fsconfig_set_string("verity", verity.as_str())?;
                }
                if volatile.is_true() {
                    fs_fd.fsconfig_set_flag("volatile")?;
                }
            }
            Self::Proc {
                gid,
                hidepid,
                pidonly,
            } => {
                if let Some(gid) = gid {
                    fs_fd.fsconfig_set_string("gid", gid)?;
                }
                if let Some(hidepid) = hidepid {
                    fs_fd.fsconfig_set_string("hidepid", hidepid.as_str())?;
                }
                if !pidonly.is_false() {
                    fs_fd.fsconfig_set_string("subset", "pid")?;
                }
            }
            Self::Sysfs => (),
            Self::Tmpfs {
                nr_blocks,
                nr_inodes,
                size,
                noswap,
            } => {
                fs_fd.fsconfig_set_string("mode", "750")?;
                if let Some(nr_blocks) = nr_blocks {
                    fs_fd.fsconfig_set_string("nr_blocks", nr_blocks)?;
                }
                if let Some(nr_inodes) = nr_inodes {
                    fs_fd.fsconfig_set_string("nr_inodes", nr_inodes)?;
                }
                if let Some(size) = size {
                    fs_fd.fsconfig_set_string("size", size)?;
                }
                if noswap.is_true() {
                    fs_fd.fsconfig_set_flag("noswap")?;
                }
            }
        }

        fs_fd.fsconfig_cmd_create()?;

        Ok(fs_fd)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum FilesystemOverlayRedirectDir {
    Off,
    Follow,
    Nofollow,
    On,
}
impl FilesystemOverlayRedirectDir {
    fn as_str(self) -> &'static str {
        match self {
            Self::Off => "off",
            Self::Follow => "follow",
            Self::Nofollow => "nofollow",
            Self::On => "on",
        }
    }
}
impl fmt::Display for FilesystemOverlayRedirectDir {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Off => write!(f, "off"),
            Self::Follow => write!(f, "follow"),
            Self::Nofollow => write!(f, "nofollow"),
            Self::On => write!(f, "on"),
        }
    }
}
impl FromStr for FilesystemOverlayRedirectDir {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Off),
            "follow" => Ok(Self::Follow),
            "nofollow" => Ok(Self::Nofollow),
            "on" => Ok(Self::On),
            _ => Err("Unknown redirect_dir value"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum FilesystemOverlayUuid {
    Off,
    Null,
    Auto,
    On,
}
impl FilesystemOverlayUuid {
    fn as_str(self) -> &'static str {
        match self {
            Self::Off => "off",
            Self::Null => "null",
            Self::Auto => "auto",
            Self::On => "on",
        }
    }
}
impl fmt::Display for FilesystemOverlayUuid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Off => write!(f, "off"),
            Self::Null => write!(f, "null"),
            Self::Auto => write!(f, "auto"),
            Self::On => write!(f, "on"),
        }
    }
}
impl FromStr for FilesystemOverlayUuid {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Off),
            "null" => Ok(Self::Null),
            "auto" => Ok(Self::Auto),
            "on" => Ok(Self::On),
            _ => Err("Unknown uuid value"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum FilesystemOverlayXino {
    Off,
    Auto,
    On,
}
impl FilesystemOverlayXino {
    fn as_str(self) -> &'static str {
        match self {
            Self::Off => "off",
            Self::Auto => "auto",
            Self::On => "on",
        }
    }
}
impl fmt::Display for FilesystemOverlayXino {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Off => write!(f, "off"),
            Self::Auto => write!(f, "auto"),
            Self::On => write!(f, "on"),
        }
    }
}
impl FromStr for FilesystemOverlayXino {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Off),
            "auto" => Ok(Self::Auto),
            "on" => Ok(Self::On),
            _ => Err("Unknown xino value"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum FilesystemOverlayVerity {
    Off,
    On,
    Require,
}
impl FilesystemOverlayVerity {
    fn as_str(self) -> &'static str {
        match self {
            Self::Off => "off",
            Self::On => "on",
            Self::Require => "require",
        }
    }
}
impl fmt::Display for FilesystemOverlayVerity {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Off => write!(f, "off"),
            Self::On => write!(f, "on"),
            Self::Require => write!(f, "require"),
        }
    }
}
impl FromStr for FilesystemOverlayVerity {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Off),
            "on" => Ok(Self::On),
            "require" => Ok(Self::Require),
            _ => Err("Unknown verity value"),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum FilesystemProcHidepid {
    Off = 0,
    Noaccess = 1,
    Invisible = 2,
    Ptracable = 4,
}
impl FilesystemProcHidepid {
    fn as_str(self) -> &'static str {
        match self {
            Self::Off => "off",
            Self::Noaccess => "noaccess",
            Self::Invisible => "invisible",
            Self::Ptracable => "ptraceable",
        }
    }
}
impl fmt::Display for FilesystemProcHidepid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Off => write!(f, "off"),
            Self::Noaccess => write!(f, "noaccess"),
            Self::Invisible => write!(f, "invisible"),
            Self::Ptracable => write!(f, "ptraceable"),
        }
    }
}
impl FromStr for FilesystemProcHidepid {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "off" => Ok(Self::Off),
            "noaccess" => Ok(Self::Noaccess),
            "invisible" => Ok(Self::Invisible),
            "ptraceable" => Ok(Self::Ptracable),
            _ => Err("Unknown hidepid value"),
        }
    }
}
