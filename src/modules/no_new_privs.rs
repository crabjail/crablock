/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use crate::utils::ResultExt;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub enum NoNewPrivs {
    #[default]
    Unchanged,
    Yes,
}
impl NoNewPrivs {
    pub fn apply(self) -> Result<(), String> {
        if self == Self::Yes {
            debug!("Set no_new_privs");
            capctl::prctl::set_no_new_privs().err_msg("set_no_new_privs")?;
        }

        Ok(())
    }
}
