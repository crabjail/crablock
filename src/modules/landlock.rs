/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::{HashMap, HashSet};
use std::io::ErrorKind as IoErrorKind;
use std::os::unix::prelude::*;
use std::path::PathBuf;

use landlock::{AccessFs, AccessNet, ParentFd, RulesetAttr, Scoped};

use crate::utils::ResultExt;

#[derive(Debug, Default)]
pub struct Landlock {
    path_beneath: HashMap<PathBuf, AccessFs>,
    pub restrict_bind_tcp: Option<HashSet<u16>>,
    pub restrict_connect_tcp: Option<HashSet<u16>>,
    pub scope_abstract_unix_socket: bool,
    pub scope_signal: bool,
}
impl Landlock {
    pub fn add_custom_path(&mut self, allowed_access: AccessFs, path: PathBuf) {
        self.path_beneath
            .entry(path)
            .or_insert(AccessFs::empty())
            .insert(allowed_access);
    }

    pub fn add_executable_path(&mut self, path: PathBuf) {
        self.add_custom_path(AccessFs::EXECUTE, path);
    }

    pub fn add_readable_path(&mut self, path: PathBuf) {
        self.add_custom_path(AccessFs::READ_FILE, path);
    }

    pub fn add_writable_path(&mut self, path: PathBuf) {
        self.add_custom_path(
            AccessFs::WRITE_FILE | AccessFs::TRUNCATE.remove_unavailable(),
            path,
        );
    }

    pub fn add_listable_path(&mut self, path: PathBuf) {
        self.add_custom_path(AccessFs::READ_DIR, path);
    }

    pub fn add_manageable_path(&mut self, path: PathBuf) {
        self.add_custom_path(
            AccessFs::remove_unavailable(
                AccessFs::READ_FILE
                    | AccessFs::WRITE_FILE
                    | AccessFs::TRUNCATE
                    | AccessFs::REMOVE_DIR
                    | AccessFs::REMOVE_FILE
                    | AccessFs::MAKE_DIR
                    | AccessFs::MAKE_REG
                    | AccessFs::REFER,
            ),
            path,
        );
    }

    pub fn add_device_usable_path(&mut self, path: PathBuf) {
        if !AccessFs::IOCTL_DEV.is_available() {
            return;
        }
        self.add_custom_path(AccessFs::IOCTL_DEV, path);
    }

    pub fn add_unrestricted_path(&mut self, path: PathBuf) {
        self.add_custom_path(AccessFs::all_available(), path);
    }

    pub fn wants_no_new_privs(&self) -> bool {
        !self.path_beneath.is_empty()
            || self.restrict_bind_tcp.is_some()
            || self.restrict_connect_tcp.is_some()
            || self.scope_abstract_unix_socket
            || self.scope_signal
    }

    pub fn apply<FD: AsFd>(&self, crabreaper_fd: &Option<FD>) -> Result<(), String> {
        if self.path_beneath.is_empty()
            && self.restrict_bind_tcp.is_none()
            && self.restrict_connect_tcp.is_none()
            && !self.scope_abstract_unix_socket
            && !self.scope_signal
        {
            return Ok(());
        }

        /* Create a new Landlock Ruleset FD */
        let mut ruleset = RulesetAttr::new()
            .set_handled_access_fs(if !self.path_beneath.is_empty() {
                AccessFs::all_available()
            } else {
                AccessFs::empty()
            })
            .err_msg("set_handled_access_fs")?
            .set_handled_access_net(
                match (
                    self.restrict_bind_tcp.is_some(),
                    self.restrict_connect_tcp.is_some(),
                ) {
                    (true, true) => AccessNet::BIND_TCP | AccessNet::CONNECT_TCP,
                    (true, false) => AccessNet::BIND_TCP,
                    (false, true) => AccessNet::CONNECT_TCP,
                    (false, false) => AccessNet::empty(),
                },
            )
            .err_msg("set_handled_access_net")?
            .set_scoped(match (self.scope_abstract_unix_socket, self.scope_signal) {
                (true, true) => Scoped::ABSTRACT_UNIX_SOCKET | Scoped::SIGNAL,
                (true, false) => Scoped::ABSTRACT_UNIX_SOCKET,
                (false, true) => Scoped::SIGNAL,
                (false, false) => Scoped::empty(),
            })
            .err_msg("set_scoped")?
            .create_ruleset()
            .err_msg("create_ruleset")?;

        /* Add path_beneath rules */
        for (path, &access_fs) in &self.path_beneath {
            debug!("Adding path_beneath rule for {}", path.display());

            let parent_fd = match ParentFd::open(path) {
                Ok(pfd) => pfd,
                Err(err) if err.kind() == IoErrorKind::NotFound => {
                    debug!("... Not Found");
                    continue;
                }
                Err(err) => bail!("open {}: {}", path.display(), err),
            };

            ruleset
                .add_path_beneath_rule(access_fs, parent_fd)
                .with_err_msg(|| format!("add_path_beneath_rule for '{}'", path.display()))?;
        }

        /* Allow crabreaper */
        if !self.path_beneath.is_empty() {
            if let Some(crabreaper_fd) = crabreaper_fd {
                ruleset
                    .add_path_beneath_rule(
                        AccessFs::EXECUTE | AccessFs::READ_FILE,
                        ParentFd::from(crabreaper_fd.as_fd()),
                    )
                    .err_msg("Landlock allow crabreaper")?;
            }
        }

        /* Add net_port rules */
        let empty = HashSet::new();
        let bind_tcp_ports = self.restrict_bind_tcp.as_ref().unwrap_or(&empty);
        let connect_tcp_ports = self.restrict_connect_tcp.as_ref().unwrap_or(&empty);
        for port in bind_tcp_ports & connect_tcp_ports {
            ruleset
                .add_net_port_rule(AccessNet::BIND_TCP | AccessNet::CONNECT_TCP, port)
                .err_msg("add_net_port_rule")?;
        }
        for port in bind_tcp_ports - connect_tcp_ports {
            ruleset
                .add_net_port_rule(AccessNet::BIND_TCP, port)
                .err_msg("add_net_port_rule")?;
        }
        for port in connect_tcp_ports - bind_tcp_ports {
            ruleset
                .add_net_port_rule(AccessNet::CONNECT_TCP, port)
                .err_msg("add_net_port_rule")?;
        }

        /* enforce it */
        ruleset.enforce().err_msg("Landlock enforce")?;

        Ok(())
    }
}
