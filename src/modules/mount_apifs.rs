/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use lnix::error::Errno as LnixErrno;
use lnix::mount::mountfd::{
    FilesystemContext, FsMountFlags, FsOpenFlags, MountAttrFlags, MountFd, MoveMountFlags,
    OpenTreeFlags,
};
use rustix::fs::CWD;

use crate::utils::{ResultExt, TriBool};

const NOSUID_NODEV_NOEXEC: MountAttrFlags = MountAttrFlags::empty()
    .union(MountAttrFlags::NOSUID)
    .union(MountAttrFlags::NODEV)
    .union(MountAttrFlags::NOEXEC);

#[derive(Debug, Default)]
pub struct MountApifs {
    pub cgroup2: bool,

    pub devpts: bool,

    pub mqueue: bool,

    pub proc: bool,
    pub proc_ro: TriBool,
    pub proc_subset_pid: TriBool,

    pub sysfs: bool,
}
impl MountApifs {
    pub fn needs_unshare_mnt(&self) -> bool {
        self.cgroup2 || self.devpts || self.mqueue || self.proc || self.sysfs
    }

    pub fn needs_unshare_cgroup(&self) -> bool {
        self.cgroup2
    }

    pub fn needs_unshare_ipc(&self) -> bool {
        self.mqueue
    }

    pub fn needs_unshare_pid(&self) -> bool {
        self.proc
    }

    pub fn needs_unshare_net(&self) -> bool {
        self.sysfs
    }

    pub fn apply(&self) -> Result<(), String> {
        if !self.cgroup2 && !self.devpts && !self.mqueue && !self.proc && !self.sysfs {
            return Ok(());
        }

        if self.devpts {
            mount_devpts()?;
        }

        if self.mqueue {
            mount_mqueue()?;
        }

        if self.proc {
            debug_assert_ne!(self.proc_subset_pid, TriBool::Unknown);
            debug_assert_ne!(self.proc_ro, TriBool::Unknown);
            mount_proc(self.proc_subset_pid.is_true(), self.proc_ro.is_true())?;
        }

        if self.sysfs {
            mount_sysfs()?;
        }

        if self.cgroup2 {
            mount_cgroup2()?;
        }

        Ok(())
    }
}

fn mount_devpts() -> Result<(), String> {
    debug!("Mounting devpts at /dev/pts");

    FilesystemContext::fsopen("devpts", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_set_flag("newinstance")
        .err_msg("fsconfig_set newinstance")?
        .fsconfig_set_string("ptmxmode", "0666")
        .err_msg("fsconfig_set ptmxmode=0666")?
        .fsconfig_set_string("mode", "620")
        .err_msg("fsconfig_set mode=620")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(
            FsMountFlags::default(),
            MountAttrFlags::NOSUID | MountAttrFlags::NOEXEC,
        )
        .err_msg("fsmount")?
        .move_to(CWD, "/dev/pts", MoveMountFlags::default())
        .err_msg("move_mount")?;

    MountFd::open_tree(
        CWD,
        "/dev/pts/ptmx",
        OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE,
    )
    .err_msg("open_tree")?
    .move_to(CWD, "/dev/ptmx", MoveMountFlags::default())
    .err_msg("move_mount")?;

    Ok(())
}

fn mount_mqueue() -> Result<(), String> {
    debug!("Mounting mqueue at /dev/mqueue");

    FilesystemContext::fsopen("mqueue", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, "/dev/mqueue", MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn mount_proc(pidonly: bool, read_only_sb: bool) -> Result<(), String> {
    debug!("Mounting proc at /proc");

    let fs_fd = FilesystemContext::fsopen("proc", FsOpenFlags::default()).err_msg("fsopen")?;
    if pidonly {
        fs_fd
            .fsconfig_set_string("subset", "pid")
            .err_msg("fsconfig_set_string")?;
    }
    if read_only_sb {
        fs_fd.fsconfig_set_flag("ro").err_msg("fsconfig_set_flag")?;
    }
    fs_fd
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, "/proc", MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}

fn mount_sysfs() -> Result<(), String> {
    debug!("Mounting sysfs at /sys");

    let selinuxfs: Option<MountFd> = MountFd::open_tree(
        CWD,
        "/sys/fs/selinux",
        OpenTreeFlags::CLONE | OpenTreeFlags::RECURSIVE,
    )
    .map_or_else(
        |err| {
            if err == LnixErrno::ENOENT {
                Ok(None)
            } else {
                Err(err)
            }
        },
        |mfd| Ok(Some(mfd)),
    )
    .err_msg("open_tree")?;

    FilesystemContext::fsopen("sysfs", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, "/sys", MoveMountFlags::default())
        .err_msg("move_mount")?;

    if let Some(selinuxfs) = selinuxfs {
        selinuxfs
            .move_to(CWD, "/sys/fs/selinux", MoveMountFlags::default())
            .err_msg("move_mount")?;
    }

    Ok(())
}

fn mount_cgroup2() -> Result<(), String> {
    debug!("Mounting cgroup2 at /sys/fs/cgroup");

    FilesystemContext::fsopen("cgroup2", FsOpenFlags::default())
        .err_msg("fsopen")?
        .fsconfig_cmd_create()
        .err_msg("fsconfig_cmd_create")?
        .fsmount(FsMountFlags::default(), NOSUID_NODEV_NOEXEC)
        .err_msg("fsmount")?
        .move_to(CWD, "/sys/fs/cgroup", MoveMountFlags::default())
        .err_msg("move_mount")?;

    Ok(())
}
