/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::env::{current_dir, set_current_dir};
use std::path::PathBuf;

use crate::dirs::try_home_dir;

#[derive(Debug)]
pub struct Cwd {
    pub path: PathBuf,
}
impl Default for Cwd {
    fn default() -> Self {
        Self {
            path: current_dir().expect("current_dir"),
        }
    }
}
impl Cwd {
    pub fn apply(&self) {
        if set_current_dir(&self.path).is_ok() {
            return;
        }
        if let Some(home) = try_home_dir() {
            if set_current_dir(home).is_ok() {
                return;
            }
        }
        set_current_dir("/").expect("set_current_dir(/)");
    }
}
