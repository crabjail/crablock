/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General public License for more details.
 *
 * You should have received a copy of the GNU General public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#[cfg(not(any(target_arch = "x86_64", target_arch = "aarch64")))]
compile_error!("seccomp is only implemented for x86_64 and aarch64.");

use std::fs::File;
use std::io::prelude::*;
use std::os::unix::net::UnixDatagram;
use std::os::unix::prelude::*;
use std::str::FromStr;
use std::{env, fmt, fs, io, slice};

use libseccomp::error::SeccompError;
use libseccomp::{ScmpAction, ScmpArgCompare, ScmpFilterContext, ScmpSyscall, scmp_cmp};
use lnix::seccomp::{SockFProg, set_mode_filter as seccomp_set_mode_filter};
use seccomp::{
    AuditArch, SeccompFilterFlags, SysnoX32, SysnoX86_64, and, bpf, jump, jump_eq, jump_lt,
    jump_ne, load, or, ret, ret_errno,
};

use crate::utils::ResultExt;

const CRABLOCK_DEBUG_SECCOMP: &str = "CRABLOCK_DEBUG_SECCOMP";

type SocketCall = (Option<i32>, Option<i32>, Option<i32>);

#[derive(Default)]
pub struct Seccomp {
    pub fds: Vec<RawFd>,
    pub syscall_filter: Option<SeccompSyscallFilter>,
    pub restrict_ioctl: Option<Vec<Ioctl>>,
    pub restrict_prctl: Option<Vec<Prctl>>,
    pub restrict_socket: Option<Vec<SocketCall>>,
    //pub filter_clone_unshare_namespaces: Option<Vec<String|enum Namespace>>,
    pub flatpak: bool,
    pub memfd_noexec: bool,
    pub deny_clone_newuser: bool,
    pub deny_execve_null: bool,
    pub deny_memory_write_execute: bool,
}
impl Seccomp {
    pub fn wants_no_new_privs(&self) -> bool {
        !self.fds.is_empty()
            || self.syscall_filter.is_some()
            || self.restrict_ioctl.is_some()
            || self.restrict_prctl.is_some()
            || self.restrict_socket.is_some()
            || self.flatpak
            || self.memfd_noexec
            || self.deny_clone_newuser
            || self.deny_execve_null
            || self.deny_memory_write_execute
    }

    pub fn apply(&self, host_op_sock: &UnixDatagram) -> Result<(), String> {
        self.apply_deny_execve_null()
            .err_msg("apply_deny_execve_null")?;
        self.apply_deny_clone_newuser()
            .err_msg("apply_deny_clone_newuser")?;
        self.apply_deny_memory_write_execute()
            .err_msg("apply_deny_memory_write_execute")?;
        self.apply_memfd_noexec(host_op_sock)
            .err_msg("memfd_noexec")?;
        self.apply_flatpak().err_msg("apply_flatpak")?;
        self.apply_restrict_socket()
            .err_msg("apply_restrict_socket")?;
        self.apply_restrict_ioctl()
            .err_msg("apply_restrict_ioctls")?;
        self.apply_restrict_prctl()
            .err_msg("apply_restrict_prctl")?;
        self.apply_fds().err_msg("apply_fds")?;
        self.apply_syscall_filter()
            .err_msg("apply_syscall_filter")?;

        Ok(())
    }

    fn apply_fds(&self) -> Result<(), String> {
        for &fd in &self.fds {
            let mut buf: Vec<u8> = Vec::new();
            unsafe { File::from_raw_fd(fd) }
                .read_to_end(&mut buf)
                .map_err(|err| format!("Failed to read seccomp filter from fd: {err}"))?;

            assert!(buf.as_ptr() as usize % align_of::<libc::sock_filter>() == 0);
            assert!(buf.len() % size_of::<libc::sock_filter>() == 0);

            #[expect(clippy::cast_ptr_alignment, reason = "Alignment was just asserted")]
            let ptr: *mut libc::sock_filter = buf.as_mut_ptr().cast::<libc::sock_filter>();
            let len: usize = buf.len() / size_of::<libc::sock_filter>();

            let filter: &mut [libc::sock_filter] = unsafe { slice::from_raw_parts_mut(ptr, len) };

            let fprog = SockFProg::new(filter);

            seccomp_set_mode_filter(lnix::seccomp::SeccompFilterFlags::LOG, &fprog)
                .map_err(|err| format!("Failed to load seccomp: {err}"))?;
        }

        Ok(())
    }

    fn apply_syscall_filter(&self) -> Result<(), SeccompError> {
        let Some(syscall_filter) = &self.syscall_filter else {
            return Ok(());
        };

        let mut ctx = new_scmp_filter_context(syscall_filter.default_action)?;

        for &(syscall, action) in &syscall_filter.rules {
            ctx.add_rule(action, syscall)?;
        }

        load_scmp_filter_context(ctx, "syscall-filter")?;

        Ok(())
    }

    #[cfg(target_arch = "aarch64")]
    fn apply_restrict_ioctl(&self) -> Result<(), seccomp::Errno> {
        unimplemented!("--seccomp-restrict-ioctl is not yet implemented for aarch64.");
    }

    #[cfg(target_arch = "x86_64")]
    fn apply_restrict_ioctl(&self) -> Result<(), seccomp::Errno> {
        let Some(ioctls) = &self.restrict_ioctl else {
            return Ok(());
        };

        use seccomp::dsl::*;

        let ioctl_arg_rules = ioctls
            .iter()
            .map(|ioctl| match ioctl {
                Ioctl::ByName(_ioctl_name) => unimplemented!(),
                Ioctl::ByNumber(ioctl_nr) => ioctl_nr,
            })
            .map(|&ioctl| Arg1Lo.eq(ioctl))
            .collect::<Vec<_>>();

        let rules = vec![
            Rule::new(
                SyscallNr::NotEqual(Syscall::IOCTL),
                ArgRules::none(),
                Action::RetAllow,
            ),
            Rule::new(
                SyscallNr::Equal(Syscall::IOCTL),
                [Arg1Hi.ne(0u64)],
                Action::RetErrno(Errno::EPERM),
            ),
            Rule::new(
                SyscallNr::Equal(Syscall::IOCTL),
                ioctl_arg_rules,
                Action::RetAllow,
            ),
        ];

        let mut filter = compile(&rules, Action::RetErrno(Errno::EPERM));

        if env::var_os(CRABLOCK_DEBUG_SECCOMP).is_some() {
            println!("seccomp-restrict-ioctl:\n{filter}");
        }

        filter.load_seccomp_filter(SeccompFilterFlags::LOG)?;

        Ok(())
    }

    fn apply_restrict_prctl(&self) -> Result<(), seccomp::Errno> {
        let Some(prctls) = &self.restrict_prctl else {
            return Ok(());
        };

        let mut filter = bpf::Program::from_iter([
            /* If the architecture is != x86-64 or the abi is == x32,
             * we immediately kill the process. */
            load!("seccomp_data.arch"),
            jump_ne!(AuditArch::X86_64.as_(), +2),
            load!("seccomp_data.nr"),
            jump_lt!(SysnoX32::X32_SYSCALL_BIT, +1),
            ret!(KILL_PROCESS),
            /* If the syscall is not prctl, allow it. */
            // Already loaded: load!("seccomp_data.nr"),
            jump_eq!(SysnoX86_64::NR_prctl, +1),
            ret!(ALLOW),
            /* args[0].hi must be 0, otherwise return EPERM.  */
            load!("seccomp_data.args[0].hi"),
            jump_eq!(0, +1),
            ret_errno!(EPERM),
            /* Load args[0].lo into the accumulator. */
            load!("seccomp_data.args[0].lo"),
        ]);

        for (i, prctl) in prctls.iter().copied().enumerate() {
            let jump_offset = u8::try_from(prctls.len() - i).expect("jump limit");
            filter.instructions_mut().extend([
                // Loaded above: load!("seccomp_data.args[0].lo"),
                jump_eq!(prctl.as_(), +jump_offset),
            ]);
        }

        #[rustfmt::skip]
        filter.instructions_mut().extend([
            ret_errno!(EPERM),
            ret!(ALLOW),
        ]);

        if env::var_os(CRABLOCK_DEBUG_SECCOMP).is_some() {
            println!("seccomp-restrict-prctl:\n{filter}");
        }

        filter.load_seccomp_filter(SeccompFilterFlags::LOG)?;

        Ok(())
    }

    #[cfg(target_arch = "aarch64")]
    fn apply_restrict_socket(&self) -> Result<(), seccomp::Errno> {
        unimplemented!("--seccomp-restrict-socket is not yet implemented for aarch64.");
    }

    #[cfg(target_arch = "x86_64")]
    fn apply_restrict_socket(&self) -> Result<(), seccomp::Errno> {
        let Some(allowed_socket_calls) = &self.restrict_socket else {
            return Ok(());
        };

        const SOCK_NONBLOCK: u32 = 0x800;
        const SOCK_CLOEXEC: u32 = 0x80000;

        let mut filter = bpf::Program::from_iter([
            /* If the architecture is != x86-64 or the abi is == x32,
             * we immediately kill the process. */
            load!("seccomp_data.arch"),
            jump_ne!(AuditArch::X86_64.as_(), +2),
            load!("seccomp_data.nr"),
            jump_lt!(SysnoX32::X32_SYSCALL_BIT, +1),
            ret!(KILL_PROCESS),
            /* If the syscall is not socket, allow it. */
            // Already loaded: load!("seccomp_data.nr"),
            jump_eq!(SysnoX86_64::NR_socket, +1),
            ret!(ALLOW),
        ]);

        for &(domain, type_, protocol) in allowed_socket_calls {
            filter
                .instructions_mut()
                .extend(if let Some(domain) = domain {
                    [
                        load!("seccomp_data.args[0].hi"),
                        jump_ne!(0, +12),
                        load!("seccomp_data.args[0].lo"),
                        jump_ne!(domain as u32, +10),
                    ]
                } else {
                    [jump!(+4), or!(0), or!(0), or!(0)]
                });
            filter
                .instructions_mut()
                .extend(if let Some(type_) = type_ {
                    [
                        load!("seccomp_data.args[1].hi"),
                        jump_ne!(0, +8),
                        load!("seccomp_data.args[1].lo"),
                        and!(!(SOCK_NONBLOCK | SOCK_CLOEXEC)),
                        jump_ne!(type_ as u32, +5),
                    ]
                } else {
                    [jump!(+4), or!(0), or!(0), or!(0), or!(0)]
                });
            filter
                .instructions_mut()
                .extend(if let Some(protocol) = protocol {
                    [
                        load!("seccomp_data.args[2].hi"),
                        jump_ne!(0, +3),
                        load!("seccomp_data.args[2].lo"),
                        jump_ne!(protocol as u32, +1),
                        ret!(ALLOW),
                    ]
                } else {
                    [jump!(+3), or!(0), or!(0), or!(0), ret!(ALLOW)]
                });
        }

        filter.instructions_mut().push(ret_errno!(EACCES));

        if env::var_os(CRABLOCK_DEBUG_SECCOMP).is_some() {
            println!("seccomp-restrict-socket:\n{filter}");
        }

        filter.load_seccomp_filter(SeccompFilterFlags::LOG)?;

        Ok(())
    }

    /// Loads a seccomp filter similar to the seccomp filter used by flatpak.
    ///
    /// <https://github.com/flatpak/flatpak/blob/002e4455d80fdf692a8feb88b563e5ab37340220/common/flatpak-run.c#L3074>
    fn apply_flatpak(&self) -> Result<(), SeccompError> {
        if !self.flatpak {
            return Ok(());
        }

        type SyscallBlocklist = &'static [(&'static str, i32, &'static [ScmpArgCompare])];

        const EPERM: i32 = libc::EPERM;
        const ENOSYS: i32 = libc::ENOSYS;
        const EAFNOSUPPORT: i32 = libc::EAFNOSUPPORT;
        const CLONE_NEWUSER: u64 = libc::CLONE_NEWUSER as u64;
        const AF_UNSPEC: u64 = libc::AF_UNSPEC as u64;
        const AF_UNIX: u64 = libc::AF_UNIX as u64;
        const AF_INET: u64 = libc::AF_INET as u64;
        const AF_INET6: u64 = libc::AF_INET6 as u64;
        const AF_NETLINK: u64 = libc::AF_NETLINK as u64;
        #[allow(
            clippy::unnecessary_cast,
            reason = "libc uses different types for musl and gnu"
        )]
        const TIOCSTI: u64 = libc::TIOCSTI as u64;
        #[allow(
            clippy::unnecessary_cast,
            reason = "libc uses different types for musl and gnu"
        )]
        const TIOCLINUX: u64 = libc::TIOCLINUX as u64;

        #[rustfmt::skip]
        const SYSCALL_BLOCKLIST: SyscallBlocklist = &[
            /* Block dmesg */
            ("syslog", EPERM, &[]),
            /* Useless old syscall */
            ("uselib", EPERM, &[]),
            /* Don't allow disabling accounting */
            ("acct", EPERM, &[]),
            /* 16-bit code is unnecessary in the sandbox, and modify_ldt is a
               historic source of interesting information leaks. */
            ("modify_ldt", EPERM, &[]),
            /* Don't allow reading current quota use */
            ("quotactl", EPERM, &[]),

            /* Don't allow access to the kernel keyring */
            ("add_key", EPERM, &[]),
            ("keyctl", EPERM, &[]),
            ("request_key", EPERM, &[]),

            /* Scary VM/NUMA ops */
            ("move_pages", EPERM, &[]),
            ("mbind", EPERM, &[]),
            ("get_mempolicy", EPERM, &[]),
            ("set_mempolicy", EPERM, &[]),
            ("migrate_pages", EPERM, &[]),

            /* Don't allow subnamespace setups: */
            ("unshare", EPERM, &[]),
            ("setns", EPERM, &[]),
            ("mount", EPERM, &[]),
            ("umount", EPERM, &[]),
            ("umount2", EPERM, &[]),
            ("pivot_root", EPERM, &[]),
            ("chroot", EPERM, &[]),
            if cfg!(target_arch = "s390x") {
                /* Architectures with CONFIG_CLONE_BACKWARDS2: the child stack
                 * and flags arguments are reversed so the flags come second */
                ("clone", EPERM, &[scmp_cmp!($arg1 & CLONE_NEWUSER == CLONE_NEWUSER)])
            } else {
                /* Normally the flags come first */
                ("clone", EPERM, &[scmp_cmp!($arg0 & CLONE_NEWUSER == CLONE_NEWUSER)])
            },

            /* Don't allow faking input to the controlling tty (CVE-2017-5226) */
            // Why MaskedEqual? See <https://www.exploit-db.com/exploits/46594> and <https://github.com/madaidan/sandbox-app-launcher/blob/master/tests/tiocsti.c>
            ("ioctl", EPERM, &[scmp_cmp!($arg1 & (u32::MAX as u64) == TIOCSTI)]),
            /* In the unlikely event that the controlling tty is a Linux virtual
             * console (/dev/tty2 or similar), copy/paste operations have an effect
             * similar to TIOCSTI (CVE-2023-28100) */
            ("ioctl", EPERM, &[scmp_cmp!($arg1 & (u32::MAX as u64) == TIOCLINUX)]),

            /* seccomp can't look into clone3()'s struct clone_args to check whether
             * the flags are OK, so we have no choice but to block clone3().
             * Return ENOSYS so user-space will fall back to clone().
             * (GHSA-67h7-w3jq-vh4q; see also https://github.com/moby/moby/commit/9f6b562d) */
            ("clone3", ENOSYS, &[]),

            /* New mount manipulation APIs can also change our VFS. There's no
             * legitimate reason to do these in the sandbox, so block all of them
             * rather than thinking about which ones might be dangerous.
             * (GHSA-67h7-w3jq-vh4q) */
            ("open_tree", ENOSYS, &[]),
            ("move_mount", ENOSYS, &[]),
            ("fsopen", ENOSYS, &[]),
            ("fsconfig", ENOSYS, &[]),
            ("fsmount", ENOSYS, &[]),
            ("fspick", ENOSYS, &[]),
            ("mount_setattr", ENOSYS, &[]),
        ];

        const SYSCALL_NONDEVEL_BLOCKLIST: SyscallBlocklist = &[
            /* Profiling operations; we expect these to be done by tools from outside
             * the sandbox.  In particular perf has been the source of many CVEs.
             */
            ("perf_event_open", EPERM, &[]),
            /* Don't allow you to switch to bsd emulation or whatnot */
            ("personality", EPERM, &[]),
            ("ptrace", EPERM, &[]),
        ];

        /* Blocklist all but unix, inet, inet6 and netlink */
        const SOCKET_FAMILY_ALLOWLIST: [u64; 5] = [
            /* NOTE: Keep in numerical order */
            AF_UNSPEC, AF_UNIX, AF_INET, AF_INET6, AF_NETLINK,
        ];

        let mut ctx = new_scmp_filter_context(ScmpAction::Allow)?;

        for &(syscall_name, errno, comparators) in SYSCALL_BLOCKLIST {
            let syscall = ScmpSyscall::from_name(syscall_name)?;
            let action = ScmpAction::Errno(errno);

            ctx.add_rule_conditional(action, syscall, comparators)?;
        }

        for &(syscall_name, errno, comparators) in SYSCALL_NONDEVEL_BLOCKLIST {
            let syscall = ScmpSyscall::from_name(syscall_name)?;
            let action = ScmpAction::Errno(errno);

            ctx.add_rule_conditional(action, syscall, comparators)?;
        }

        let socket_syscall = ScmpSyscall::from_name("socket")?;

        /* Socket filtering doesn't work on e.g. i386, so ignore failures here
         * However, we need to use seccomp_rule_add_exact to avoid libseccomp doing
         * something else: https://github.com/seccomp/libseccomp/issues/8 */
        SOCKET_FAMILY_ALLOWLIST
            .iter()
            .zip(SOCKET_FAMILY_ALLOWLIST.iter().skip(1))
            .flat_map(|(start, end)| *start + 1..*end)
            .try_for_each(|disallowed_family| {
                /* Blocklist the in-between valid families */
                ctx.add_rule_conditional_exact(
                    ScmpAction::Errno(EAFNOSUPPORT),
                    socket_syscall,
                    &[scmp_cmp!($arg0 == disallowed_family)],
                )?;
                Ok::<(), SeccompError>(())
            })?;
        /* Blocklist the rest */
        ctx.add_rule_conditional_exact(
            ScmpAction::Errno(EAFNOSUPPORT),
            socket_syscall,
            &[scmp_cmp!($arg0 > *SOCKET_FAMILY_ALLOWLIST.last().unwrap())],
        )?;

        load_scmp_filter_context(ctx, "seccomp-flatpak")?;

        Ok(())
    }

    fn apply_memfd_noexec(&self, host_op_sock: &UnixDatagram) -> Result<(), SeccompError> {
        if !self.memfd_noexec {
            return Ok(());
        }

        /* Try to set vm.memfd_noexec = 1 in our pid namespace with a
         * privileged helper. */
        let vm_memfd_noexec_is_1 =
            crate::request_host_op(host_op_sock, b"set_memfd_noexec;").is_ok();

        let mut ctx = new_scmp_filter_context(ScmpAction::Allow)?;

        const MFD_NOEXEC_SEAL: u64 = linux_raw_sys::general::MFD_NOEXEC_SEAL as u64;
        const MFD_EXEC: u64 = linux_raw_sys::general::MFD_EXEC as u64;

        if vm_memfd_noexec_is_1 {
            /* vm.memfd_noexec == 1; memfd_create defaults to noexec.
             * We only need to deny calls with MFD_EXEC. */
            ctx.add_rule_conditional(
                ScmpAction::Errno(libc::EPERM),
                ScmpSyscall::from_name("memfd_create")?,
                &[scmp_cmp!($arg1 & MFD_EXEC == MFD_EXEC)],
            )?;

            debug_assert_eq!(
                fs::read_to_string("/proc/sys/vm/memfd_noexec")
                    .as_deref()
                    .unwrap_or("1"),
                "1"
            );
        } else {
            /* vm.memfd_noexec unknown; memfd_create likely defaults to exec.
             * We block all calls without MFD_NOEXEC_SEAL. */
            ctx.add_rule_conditional(
                ScmpAction::Errno(libc::EPERM),
                ScmpSyscall::from_name("memfd_create")?,
                &[scmp_cmp!($arg1 & MFD_NOEXEC_SEAL == 0)],
            )?;
        }

        load_scmp_filter_context(ctx, "seccomp-memfd-noexec")?;

        Ok(())
    }

    #[cfg(any(target_arch = "x86_64", target_arch = "aarch64"))]
    fn apply_deny_clone_newuser(&self) -> Result<(), SeccompError> {
        if !self.deny_clone_newuser {
            return Ok(());
        }

        let mut ctx = new_scmp_filter_context(ScmpAction::Allow)?;

        const CLONE_NEWUSER: u64 = libc::CLONE_NEWUSER as u64;

        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("setns")?,
            &[scmp_cmp!($arg1 == 0)],
        )?;

        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("setns")?,
            &[scmp_cmp!($arg1 & CLONE_NEWUSER == CLONE_NEWUSER)],
        )?;

        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("unshare")?,
            &[scmp_cmp!($arg0 & CLONE_NEWUSER == CLONE_NEWUSER)],
        )?;

        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("clone")?,
            &[scmp_cmp!($arg0 & CLONE_NEWUSER == CLONE_NEWUSER)],
        )?;

        ctx.add_rule(
            ScmpAction::Errno(libc::ENOSYS),
            ScmpSyscall::from_name("clone3")?,
        )?;

        load_scmp_filter_context(ctx, "seccomp-deny-clone-newuser")?;

        Ok(())
    }

    #[cfg(any(target_arch = "x86_64", target_arch = "aarch64"))]
    fn apply_deny_execve_null(&self) -> Result<(), SeccompError> {
        if !self.deny_execve_null {
            return Ok(());
        }

        let mut ctx = new_scmp_filter_context(ScmpAction::Allow)?;

        ctx.add_rule_conditional(
            ScmpAction::KillProcess,
            ScmpSyscall::from_name("execve")?,
            &[scmp_cmp!($arg1 == 0)],
        )?;

        ctx.add_rule_conditional(
            ScmpAction::KillProcess,
            ScmpSyscall::from_name("execve")?,
            &[scmp_cmp!($arg2 == 0)],
        )?;

        ctx.add_rule_conditional(
            ScmpAction::KillProcess,
            ScmpSyscall::from_name("execveat")?,
            &[scmp_cmp!($arg2 == 0)],
        )?;

        ctx.add_rule_conditional(
            ScmpAction::KillProcess,
            ScmpSyscall::from_name("execveat")?,
            &[scmp_cmp!($arg3 == 0)],
        )?;

        load_scmp_filter_context(ctx, "seccomp-deny-execve-null")?;

        Ok(())
    }

    #[cfg(any(target_arch = "x86_64", target_arch = "aarch64"))]
    fn apply_deny_memory_write_execute(&self) -> Result<(), SeccompError> {
        if !self.deny_memory_write_execute {
            return Ok(());
        }

        const_assert!(libc::PROT_NONE == 0);
        const_assert!(libc::PROT_READ == 1);
        const_assert!(libc::PROT_WRITE == 2);
        const_assert!(libc::PROT_EXEC == 4);

        const SHM_EXEC: u64 = libc::SHM_EXEC as u64;
        const READ_IMPLIES_EXEC: u64 = libc::READ_IMPLIES_EXEC as u64;

        let mut ctx = new_scmp_filter_context(ScmpAction::Allow)?;

        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("mmap")?,
            &[scmp_cmp!($arg2 >= 6)],
        )?;
        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("mprotect")?,
            &[scmp_cmp!($arg2 >= 4)],
        )?;
        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("pkey_mprotect")?,
            &[scmp_cmp!($arg2 >= 4)],
        )?;
        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("shmat")?,
            &[scmp_cmp!($arg2 & SHM_EXEC == SHM_EXEC)],
        )?;
        ctx.add_rule_conditional(
            ScmpAction::Errno(libc::EPERM),
            ScmpSyscall::from_name("personality")?,
            &[scmp_cmp!($arg0 & READ_IMPLIES_EXEC == READ_IMPLIES_EXEC)],
        )?;

        load_scmp_filter_context(ctx, "seccomp-deny-memory-write-execute")?;

        Ok(())
    }
}
impl fmt::Debug for Seccomp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // field_with and from_fn are unstable
        f.debug_struct("Seccomp")
            .field("fds", &self.fds)
            .field("syscall_filter", &format_args!("{:?}", self.syscall_filter))
            .field("restrict_ioctl", &self.restrict_ioctl)
            .field("restrict_prctl", &self.restrict_prctl)
            .field(
                "restrict_socket",
                &format_args!("{:?}", self.restrict_socket),
            )
            .field("flatpak", &self.flatpak)
            .field("memfd_noexec", &self.memfd_noexec)
            .field("deny_clone_newuser", &self.deny_clone_newuser)
            .field("deny_execve_null", &self.deny_execve_null)
            .field("deny_memory_write_execute", &self.deny_memory_write_execute)
            .finish()?;

        Ok(())
    }
}

fn new_scmp_filter_context(default_action: ScmpAction) -> Result<ScmpFilterContext, SeccompError> {
    let mut ctx = ScmpFilterContext::new_filter(default_action)?;

    // We handle CAP_SYS_ADMIN/NO_NEW_PRIVS ourself, therefore we
    // disable handling of it in libseccomp.
    ctx.set_ctl_nnp(false)?;

    // Log blocked syscalls in the audit log.
    ctx.set_ctl_log(true)?;

    Ok(ctx)
}

fn load_scmp_filter_context(ctx: ScmpFilterContext, name: &str) -> Result<(), SeccompError> {
    if env::var_os(CRABLOCK_DEBUG_SECCOMP).is_some() {
        println!("{name} pseudo filter code:");
        ctx.export_pfc(&mut io::stdout())?;

        match File::create(format!("{name}.bpf")) {
            Ok(mut file) => ctx.export_bpf(&mut file)?,
            Err(err) => eprintln!("export_bpf failed: {err}"),
        }
    }

    ctx.load()?;

    Ok(())
}

#[derive(Debug)]
pub struct SeccompSyscallFilter {
    pub default_action: ScmpAction,
    pub rules: Vec<(ScmpSyscall, ScmpAction)>,
}

#[derive(Debug)]
pub enum Ioctl {
    ByName(String),
    ByNumber(u32),
}
impl FromStr for Ioctl {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(if let Some(hex) = s.strip_prefix("0x") {
            Self::ByNumber(u32::from_str_radix(hex, 16).map_err(|e| e.to_string())?)
        } else {
            Self::ByName(s.to_string())
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(u32)]
pub enum Prctl {
    CapAmbient = linux_raw_sys::prctl::PR_CAP_AMBIENT,
    CapbsetDrop = linux_raw_sys::prctl::PR_CAPBSET_DROP,
    CapbsetRead = linux_raw_sys::prctl::PR_CAPBSET_READ,
    GetAuxv = linux_raw_sys::prctl::PR_GET_AUXV,
    GetChildSubreaper = linux_raw_sys::prctl::PR_GET_CHILD_SUBREAPER,
    GetDumpable = linux_raw_sys::prctl::PR_GET_DUMPABLE,
    GetEndian = linux_raw_sys::prctl::PR_GET_ENDIAN,
    GetFpMode = linux_raw_sys::prctl::PR_GET_FP_MODE,
    GetFpemu = linux_raw_sys::prctl::PR_GET_FPEMU,
    GetFpexc = linux_raw_sys::prctl::PR_GET_FPEXC,
    GetIoFlusher = linux_raw_sys::prctl::PR_GET_IO_FLUSHER,
    GetKeepcaps = linux_raw_sys::prctl::PR_GET_KEEPCAPS,
    GetMdwe = linux_raw_sys::prctl::PR_GET_MDWE,
    GetName = linux_raw_sys::prctl::PR_GET_NAME,
    GetNoNewPrivs = linux_raw_sys::prctl::PR_GET_NO_NEW_PRIVS,
    GetPdeathsig = linux_raw_sys::prctl::PR_GET_PDEATHSIG,
    GetSeccomp = linux_raw_sys::prctl::PR_GET_SECCOMP,
    GetSecurebits = linux_raw_sys::prctl::PR_GET_SECUREBITS,
    GetSpeculationCtrl = linux_raw_sys::prctl::PR_GET_SPECULATION_CTRL,
    GetTaggedAddrCtrl = linux_raw_sys::prctl::PR_GET_TAGGED_ADDR_CTRL,
    GetThpDisable = linux_raw_sys::prctl::PR_GET_THP_DISABLE,
    GetTidAddress = linux_raw_sys::prctl::PR_GET_TID_ADDRESS,
    GetTimerslack = linux_raw_sys::prctl::PR_GET_TIMERSLACK,
    GetTiming = linux_raw_sys::prctl::PR_GET_TIMING,
    GetTsc = linux_raw_sys::prctl::PR_GET_TSC,
    GetUnalign = linux_raw_sys::prctl::PR_GET_UNALIGN,
    MceKill = linux_raw_sys::prctl::PR_MCE_KILL,
    MceKillGet = linux_raw_sys::prctl::PR_MCE_KILL_GET,
    MpxDisableManagement = linux_raw_sys::prctl::PR_MPX_DISABLE_MANAGEMENT,
    MpxEnableManagement = linux_raw_sys::prctl::PR_MPX_ENABLE_MANAGEMENT,
    PacResetKeys = linux_raw_sys::prctl::PR_PAC_RESET_KEYS,
    SetChildSubreaper = linux_raw_sys::prctl::PR_SET_CHILD_SUBREAPER,
    SetDumpable = linux_raw_sys::prctl::PR_SET_DUMPABLE,
    SetEndian = linux_raw_sys::prctl::PR_SET_ENDIAN,
    SetFpMode = linux_raw_sys::prctl::PR_SET_FP_MODE,
    SetFpemu = linux_raw_sys::prctl::PR_SET_FPEMU,
    SetFpexc = linux_raw_sys::prctl::PR_SET_FPEXC,
    SetIoFlusher = linux_raw_sys::prctl::PR_SET_IO_FLUSHER,
    SetKeepcaps = linux_raw_sys::prctl::PR_SET_KEEPCAPS,
    SetMdwe = linux_raw_sys::prctl::PR_SET_MDWE,
    SetMm = linux_raw_sys::prctl::PR_SET_MM,
    SetName = linux_raw_sys::prctl::PR_SET_NAME,
    SetNoNewPrivs = linux_raw_sys::prctl::PR_SET_NO_NEW_PRIVS,
    SetPdeathsig = linux_raw_sys::prctl::PR_SET_PDEATHSIG,
    SetPtracer = linux_raw_sys::prctl::PR_SET_PTRACER,
    SetSeccomp = linux_raw_sys::prctl::PR_SET_SECCOMP,
    SetSecurebits = linux_raw_sys::prctl::PR_SET_SECUREBITS,
    SetSpeculationCtrl = linux_raw_sys::prctl::PR_SET_SPECULATION_CTRL,
    SetSyscallUserDispatch = linux_raw_sys::prctl::PR_SET_SYSCALL_USER_DISPATCH,
    SetTaggedAddrCtrl = linux_raw_sys::prctl::PR_SET_TAGGED_ADDR_CTRL,
    SetThpDisable = linux_raw_sys::prctl::PR_SET_THP_DISABLE,
    SetTimerslack = linux_raw_sys::prctl::PR_SET_TIMERSLACK,
    SetTiming = linux_raw_sys::prctl::PR_SET_TIMING,
    SetTsc = linux_raw_sys::prctl::PR_SET_TSC,
    SetUnalign = linux_raw_sys::prctl::PR_SET_UNALIGN,
    SetVma = linux_raw_sys::prctl::PR_SET_VMA,
    SveGetVl = linux_raw_sys::prctl::PR_SVE_GET_VL,
    SveSetVl = linux_raw_sys::prctl::PR_SVE_SET_VL,
    TaskPerfEventsDisable = linux_raw_sys::prctl::PR_TASK_PERF_EVENTS_DISABLE,
    TaskPerfEventsEnable = linux_raw_sys::prctl::PR_TASK_PERF_EVENTS_ENABLE,
}
impl Prctl {
    fn as_(self) -> u32 {
        self as _
    }
}
impl FromStr for Prctl {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "PR_CAP_AMBIENT" => Ok(Self::CapAmbient),
            "PR_CAPBSET_DROP" => Ok(Self::CapbsetDrop),
            "PR_CAPBSET_READ" => Ok(Self::CapbsetRead),
            "PR_GET_AUXV" => Ok(Self::GetAuxv),
            "PR_GET_CHILD_SUBREAPER" => Ok(Self::GetChildSubreaper),
            "PR_GET_DUMPABLE" => Ok(Self::GetDumpable),
            "PR_GET_ENDIAN" => Ok(Self::GetEndian),
            "PR_GET_FPEMU" => Ok(Self::GetFpemu),
            "PR_GET_FPEXC" => Ok(Self::GetFpexc),
            "PR_GET_FP_MODE" => Ok(Self::GetFpMode),
            "PR_GET_IO_FLUSHER" => Ok(Self::GetIoFlusher),
            "PR_GET_KEEPCAPS" => Ok(Self::GetKeepcaps),
            "PR_GET_MDWE" => Ok(Self::GetMdwe),
            "PR_GET_NAME" => Ok(Self::GetName),
            "PR_GET_NO_NEW_PRIVS" => Ok(Self::GetNoNewPrivs),
            "PR_GET_PDEATHSIG" => Ok(Self::GetPdeathsig),
            "PR_GET_SECCOMP" => Ok(Self::GetSeccomp),
            "PR_GET_SECUREBITS" => Ok(Self::GetSecurebits),
            "PR_GET_SPECULATION_CTRL" => Ok(Self::GetSpeculationCtrl),
            "PR_GET_TAGGED_ADDR_CTRL" => Ok(Self::GetTaggedAddrCtrl),
            "PR_GET_THP_DISABLE" => Ok(Self::GetThpDisable),
            "PR_GET_TID_ADDRESS" => Ok(Self::GetTidAddress),
            "PR_GET_TIMERSLACK" => Ok(Self::GetTimerslack),
            "PR_GET_TIMING" => Ok(Self::GetTiming),
            "PR_GET_TSC" => Ok(Self::GetTsc),
            "PR_GET_UNALIGN" => Ok(Self::GetUnalign),
            "PR_MCE_KILL_GET" => Ok(Self::MceKillGet),
            "PR_MCE_KILL" => Ok(Self::MceKill),
            "PR_MPX_DISABLE_MANAGEMENT" => Ok(Self::MpxDisableManagement),
            "PR_MPX_ENABLE_MANAGEMENT" => Ok(Self::MpxEnableManagement),
            "PR_PAC_RESET_KEYS" => Ok(Self::PacResetKeys),
            "PR_SET_CHILD_SUBREAPER" => Ok(Self::SetChildSubreaper),
            "PR_SET_DUMPABLE" => Ok(Self::SetDumpable),
            "PR_SET_ENDIAN" => Ok(Self::SetEndian),
            "PR_SET_FPEMU" => Ok(Self::SetFpemu),
            "PR_SET_FPEXC" => Ok(Self::SetFpexc),
            "PR_SET_FP_MODE" => Ok(Self::SetFpMode),
            "PR_SET_IO_FLUSHER" => Ok(Self::SetIoFlusher),
            "PR_SET_KEEPCAPS" => Ok(Self::SetKeepcaps),
            "PR_SET_MDWE" => Ok(Self::SetMdwe),
            "PR_SET_MM" => Ok(Self::SetMm),
            "PR_SET_NAME" => Ok(Self::SetName),
            "PR_SET_NO_NEW_PRIVS" => Ok(Self::SetNoNewPrivs),
            "PR_SET_PDEATHSIG" => Ok(Self::SetPdeathsig),
            "PR_SET_PTRACER" => Ok(Self::SetPtracer),
            "PR_SET_SECCOMP" => Ok(Self::SetSeccomp),
            "PR_SET_SECUREBITS" => Ok(Self::SetSecurebits),
            "PR_SET_SPECULATION_CTRL" => Ok(Self::SetSpeculationCtrl),
            "PR_SET_SYSCALL_USER_DISPATCH" => Ok(Self::SetSyscallUserDispatch),
            "PR_SET_TAGGED_ADDR_CTRL" => Ok(Self::SetTaggedAddrCtrl),
            "PR_SET_THP_DISABLE" => Ok(Self::SetThpDisable),
            "PR_SET_TIMERSLACK" => Ok(Self::SetTimerslack),
            "PR_SET_TIMING" => Ok(Self::SetTiming),
            "PR_SET_TSC" => Ok(Self::SetTsc),
            "PR_SET_UNALIGN" => Ok(Self::SetUnalign),
            "PR_SET_VMA" => Ok(Self::SetVma),
            "PR_SVE_GET_VL" => Ok(Self::SveGetVl),
            "PR_SVE_SET_VL" => Ok(Self::SveSetVl),
            "PR_TASK_PERF_EVENTS_DISABLE" => Ok(Self::TaskPerfEventsDisable),
            "PR_TASK_PERF_EVENTS_ENABLE" => Ok(Self::TaskPerfEventsEnable),
            _ => Err(format!("Unexpected Prctl: {s}")),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(i32)]
pub enum AddressFamily {
    Unix = libc::AF_UNIX,
    Inet = libc::AF_INET,
    Inet6 = libc::AF_INET6,
    Netlink = libc::AF_NETLINK,
    Packet = libc::AF_PACKET,
    Bluetooth = libc::AF_BLUETOOTH,
}
impl FromStr for AddressFamily {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "AF_UNIX" | "unix" => Ok(Self::Unix),
            "AF_INET" | "inet" => Ok(Self::Inet),
            "AF_INET6" | "inet6" => Ok(Self::Inet6),
            "AF_NETLINK" | "netlink" => Ok(Self::Netlink),
            "AF_PACKET" | "packet" => Ok(Self::Packet),
            "AF_BLUETOOTH" | "bluetooth" => Ok(Self::Bluetooth),
            _ => Err(format!("Unknown AddressFamily: {s}")),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(i32)]
pub enum NetlinkFamily {
    Route = libc::NETLINK_ROUTE,
    //W1 = libc::NETLINK_W1,
    Usersock = libc::NETLINK_USERSOCK,
    Firewall = libc::NETLINK_FIREWALL,
    SockDiag = libc::NETLINK_SOCK_DIAG,
    //InetDiag = libc::NETLINK_INET_DIAG,
    Nflog = libc::NETLINK_NFLOG,
    Xfrm = libc::NETLINK_XFRM,
    Selinux = libc::NETLINK_SELINUX,
    Iscsi = libc::NETLINK_ISCSI,
    Audit = libc::NETLINK_AUDIT,
    FibLookup = libc::NETLINK_FIB_LOOKUP,
    Connector = libc::NETLINK_CONNECTOR,
    Netfilter = libc::NETLINK_NETFILTER,
    Scsitransport = libc::NETLINK_SCSITRANSPORT,
    Rdma = libc::NETLINK_RDMA,
    //Ipv6Fw = libc::NETLINK_IP6_FW,
    Dnrtmsg = libc::NETLINK_DNRTMSG,
    KobjectUevent = libc::NETLINK_KOBJECT_UEVENT,
    Generic = libc::NETLINK_GENERIC,
    Crypto = libc::NETLINK_CRYPTO,
}
impl FromStr for NetlinkFamily {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "NETLINK_ROUTE" | "route" => Ok(Self::Route),
            "NETLINK_USERSOCK" | "usersock" => Ok(Self::Usersock),
            "NETLINK_FIREWALL" | "firewall" => Ok(Self::Firewall),
            "NETLINK_SOCK_DIAG" | "sock-diag" | "NETLINK_INET_DIAG" | "inet-diag" => {
                Ok(Self::SockDiag)
            }
            "NETLINK_NFLOG" | "nflog" => Ok(Self::Nflog),
            "NETLINK_XFRM" | "xfrm" => Ok(Self::Xfrm),
            "NETLINK_SELINUX" | "selinux" => Ok(Self::Selinux),
            "NETLINK_ISCSI" | "iscsi" => Ok(Self::Iscsi),
            "NETLINK_AUDIT" | "audit" => Ok(Self::Audit),
            "NETLINK_FIB_LOOKUP" | "fib-lookup" => Ok(Self::FibLookup),
            "NETLINK_CONNECTOR" | "connector" => Ok(Self::Connector),
            "NETLINK_NETFILTER" | "netfilter" => Ok(Self::Netfilter),
            "NETLINK_SCSITRANSPORT" | "scsitransport" => Ok(Self::Scsitransport),
            "NETLINK_RDMA" | "rdma" => Ok(Self::Rdma),
            "NETLINK_DNRTMSG" | "dnrtmsg" => Ok(Self::Dnrtmsg),
            "NETLINK_KOBJECT_UEVENT" | "kobject-uevent" => Ok(Self::KobjectUevent),
            "NETLINK_GENERIC" | "generic" => Ok(Self::Generic),
            "NETLINK_CRYPTO" | "crypto" => Ok(Self::Crypto),
            _ => Err(format!("Unknown NetlinkFamily: {s}")),
        }
    }
}
