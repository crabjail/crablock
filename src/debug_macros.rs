/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#![allow(unused_macros)]

//! This module contains macro for debugging.
//!
//! The most resemble shell commands but work w/o fork+execve which is not
//! always possible when debugging around namespaces.
//!
//! - `sleep!($secs)`: sleep for `$secs` seconds.
//! - `ls_l!($path)`: imitates `ls -l $path`.
//! - `cat!($file)`: imitates `cat $file`.
//! - `grep!($pat, $file)`: imitates `grep $pat $file`. No regex and case sensitive.

macro_rules! sleep {
    ($secs:literal) => {{
        std::thread::sleep(std::time::Duration::from_secs($secs));
    }};
}

macro_rules! ls_l {
    ($path:expr) => {{
        println!("Content of {}:", std::path::Path::new($path).display());
        for entry in std::fs::read_dir($path).unwrap() {
            use std::os::unix::prelude::*;

            const ESCAPE_BLUE: &str = "\u{001B}[1;34m";
            const ESCAPE_CYAN: &str = "\u{001B}[1;36m";
            const ESCAPE_RESET: &str = "\u{001B}[0m";

            let entry = entry.unwrap();

            let path = entry.path();
            let path = path.display();
            let file_type = match entry.file_type().unwrap() {
                ft if ft.is_dir() => "dir",
                ft if ft.is_file() => "reg",
                ft if ft.is_symlink() => "sym",
                ft if ft.is_block_device() => "blk",
                ft if ft.is_char_device() => "chr",
                ft if ft.is_fifo() => "fifo",
                ft if ft.is_socket() => "sock",
                _ => unreachable!(),
            };
            let metadata = entry.metadata().unwrap();
            let mode = metadata.mode() & !libc::S_IFMT;
            let uid = metadata.uid();
            let gid = metadata.gid();

            if let Ok(link_path) = std::fs::read_link(entry.path()) {
                let link_path = link_path.display();
                println!("{file_type:<4} {mode:>3o} {uid:<3} {gid:<3} {ESCAPE_CYAN}{path}{ESCAPE_RESET} -> {link_path}");
            } else if file_type == "dir" {
                println!("{file_type:<4} {mode:>3o} {uid:<3} {gid:<3} {ESCAPE_BLUE}{path}{ESCAPE_RESET}");
            } else {
                println!("{file_type:<4} {mode:>3o} {uid:<3} {gid:<3} {path}");
            }
        }
    }};
}

macro_rules! cat {
    ($file:expr) => {{
        const ESCAPE_RED: &str = "\u{001B}[1;31m";
        const ESCAPE_RESET: &str = "\u{001B}[0m";

        println!(
            "Content of {}:\n{}{ESCAPE_RED}EOF{ESCAPE_RESET}",
            std::path::Path::new($file).display(),
            std::fs::read_to_string($file).unwrap(),
        );
    }};
}

macro_rules! grep {
    ($pat:expr, $file:expr) => {{
        const ESCAPE_RED: &str = "\u{001B}[1;31m";
        const ESCAPE_RESET: &str = "\u{001B}[0m";

        println!(
            "Occurrences of '{}' in {}:",
            $pat,
            std::path::Path::new($file).display()
        );
        std::fs::read_to_string($file)
            .unwrap()
            .lines()
            .filter(|line| line.contains($pat))
            .for_each(|line| println!("{line}"));
        println!("{ESCAPE_RED}END{ESCAPE_RESET}");
    }};
}
