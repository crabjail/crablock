/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs;
use std::os::unix::net::UnixDatagram;

use lnix::Never;
use lnix::process::Program;
use rustix::fs::{CWD, Mode, OFlags, openat};

use crate::utils::ResultExt;
use crate::{ClConfig, START_TIME};

/// `child::main`
///
/// ## Ordering
///
/// - `uid_map`/`gid_map` must/should come first to "finish" `CLONE_NEWUSER`.
/// - "block on xdg-dbus-proxy" must come before mounting in order to mount its socket.
///   Everything we do before it reduces waiting time.
/// - `NamespacesLimits` must come
///   - before mount-proc because subset=pid will hide `/proc/sys`.
///   - before landlock because we do not grant write to `/proc/sys/user/*`.
///   - after mounting because `max_mnt_namespaces` can break `open_tree`/`fsmount`.
///   - after pasta is initialized.
/// - Resources must come before mount-proc because it needs writeable procfs for `oom_score_adj`.
/// - `MountApifs` should come before Mnt.
/// - `Mnt` must come before landlock because landlock disables mounting.
/// - `Cwd` should come after `Mnt`.
/// - `umask` must come after `Mnt` because it affects us.
/// - capabilities should come before seccomp if no-new-privs is set.
/// - seccomp should come as late as possible to not interfere.
///
/// setid is ugly!
pub fn main(config: ClConfig, host_op_sock: UnixDatagram) -> Result<Never, String> {
    // FIXME: This is racey and does not work when --setuid is used.
    capctl::prctl::set_pdeathsig(Some(libc::SIGKILL)).err_msg("set PDEATHSIG")?;

    if let Some(uid_map) = &config.uid_map {
        uid_map.write(&host_op_sock)?;
    }

    if let Some(gid_map) = &config.gid_map {
        debug!("Writing 'deny' to /proc/self/setgroups.");
        fs::write("/proc/self/setgroups", "deny").err_msg("write /proc/self/setgroups")?;

        gid_map.write(&host_op_sock)?;
    }

    let crabreaper_fd = if !config.custom_init {
        let crapreaper_fd = openat(
            CWD,
            crate::CRABREAPER,
            OFlags::CLOEXEC | OFlags::PATH,
            Mode::empty(),
        )
        .err_msg("open crabreaper")?;
        Some(crapreaper_fd)
    } else {
        None
    };

    config.no_new_privs.apply()?;

    config.mdwe.apply()?;

    config.mitigations.apply()?;

    crate::request_host_op(&host_op_sock, b"block on xdg-dbus-proxy;")?;

    config.namespaces_limits.open_proc_sys_user()?;

    config.resources.apply()?;

    config.mount_apifs.apply()?;

    config.fsm.apply()?;

    config.mnt.apply(&config.settings)?;

    config.cwd.apply();

    crate::request_host_op(&host_op_sock, b"block on pasta;")?;

    config.namespaces_limits.apply()?;

    config.landlock.apply(&crabreaper_fd)?;

    config.new_session.apply()?;

    config.hostname.apply()?;

    config.umask.apply();

    config.environment.apply();

    config.setid.apply()?;

    config.capabilities.apply().err_msg("capabilities.apply")?;

    config.seccomp.apply(&host_op_sock)?;

    /* Prepare for and do execve */

    let argv = if crabreaper_fd.is_none() {
        &config.argv[1..]
    } else {
        &config.argv
    };

    // Duration::as_millis_f64 is unstable.
    debug!(
        "Starting child after {:.2} milliseconds",
        START_TIME
            .get()
            .unwrap()
            .elapsed()
            .mul_f64(1000.0)
            .as_secs_f64()
    );
    debug!("execve {:?}", argv);

    let err = if let Some(crabreaper_fd) = crabreaper_fd {
        Program::with_dirfd(crabreaper_fd, c"")
            .unwrap()
            .argv(argv)
            .err_msg("argv")?
            .envp_inherit()
            .at_empty_path()
            .exec()
    } else {
        Program::new(&argv[0])
            .err_msg("Program::new")?
            .argv(argv)
            .err_msg("argv")?
            .envp_inherit()
            .search_path()
            .exec()
    };
    Err(format!("execve failed: {err}"))
}
