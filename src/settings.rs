/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::fmt::Debug;
use std::path::{Path, PathBuf};
use std::sync::OnceLock;

use crate::dirs::{xdg_data_home, xdg_runtime_dir};

#[derive(Debug, Default)]
pub struct Settings {
    _sandbox_name: Option<String>,
    _crablock_data_home: Option<PathBuf>,
    _crablock_runtime_dir: Option<PathBuf>,
}
impl Settings {
    const APP_ID: &'static str = "page.codeberg.crabjail.Crablock";

    pub fn set_sandbox_name<S: Into<String>>(&mut self, name: S) -> Result<(), String> {
        let name = name.into();
        if name
            .chars()
            .all(|c| c.is_ascii_alphanumeric() || ['+', '-', ':', '_'].contains(&c))
        {
            self._sandbox_name = Some(name);
            Ok(())
        } else {
            Err(format!(
                "'{name}' is an invalid sandbox-name. Only ASCII alphanumeric characters and '+-:_' are allowed."
            ))
        }
    }

    pub fn sandbox_name(&self) -> &str {
        self._sandbox_name.as_ref().expect("No sandbox-name set")
    }

    pub fn set_crablock_data_home<P: Into<PathBuf>>(&mut self, path: P) {
        self._crablock_data_home = Some(path.into());
    }

    fn crablock_data_home(&self) -> &'static Path {
        static PATH: OnceLock<PathBuf> = OnceLock::new();
        PATH.get_or_init(|| {
            if let Some(crablock_data_home) = self._crablock_data_home.clone() {
                crablock_data_home
            } else {
                xdg_data_home().join(Self::APP_ID)
            }
        })
    }

    pub fn set_crablock_runtime_dir<P: Into<PathBuf>>(&mut self, path: P) {
        self._crablock_runtime_dir = Some(path.into());
    }

    fn crablock_runtime_dir(&self) -> &'static Path {
        static PATH: OnceLock<PathBuf> = OnceLock::new();
        PATH.get_or_init(|| {
            if let Some(crablock_runtime_dir) = self._crablock_runtime_dir.clone() {
                crablock_runtime_dir
            } else {
                xdg_runtime_dir().join(Self::APP_ID)
            }
        })
    }

    pub fn get_mnt_private_path(&self) -> Cow<'static, Path> {
        static PATH: OnceLock<PathBuf> = OnceLock::new();
        PATH.get_or_init(|| {
            self.crablock_data_home()
                .join(self.sandbox_name())
                .join("private")
        })
        .into()
    }

    pub fn get_mnt_deny_path(&self) -> Cow<'static, Path> {
        static PATH: OnceLock<PathBuf> = OnceLock::new();
        PATH.get_or_init(|| self.crablock_runtime_dir().join("deny"))
            .into()
    }
}
