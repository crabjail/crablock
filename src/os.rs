/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::os::raw::*;
use std::{io, ptr};

use bitflags::bitflags;
use lnix::process::Pid;
use rustix::process::{Signal, getpid, kill_process};

fn cvt(rv: i32) -> io::Result<i32> {
    if rv == -1 {
        Err(io::Error::last_os_error())
    } else {
        Ok(rv)
    }
}

// process

pub unsafe fn fork_with_pdeathsig(pdeathsig: Signal) -> Result<Option<Pid>, lnix::error::Errno> {
    let parent_pid = Pid::current();

    match unsafe { lnix::process::fork()? } {
        // Parent
        Some(pid) => Ok(Some(pid)),
        // Child
        None => {
            capctl::prctl::set_pdeathsig(Some(pdeathsig.as_raw()))
                .expect("fork_with_pdeathsig->set_pdeathsig");

            if Pid::parent().is_none_or(|ppid| ppid != parent_pid) {
                kill_process(getpid(), pdeathsig).expect("fork_with_pdeathsig->kill_process");
            }

            Ok(None)
        }
    }
}

bitflags! {
    #[derive(Debug, Clone, Copy)]
    pub struct CloneFlags: i32 {
        const CLONE_NEWUSER = libc::CLONE_NEWUSER;
        const CLONE_NEWPID = libc::CLONE_NEWPID;
        const CLONE_NEWIPC = libc::CLONE_NEWIPC;
        const CLONE_NEWNS = libc::CLONE_NEWNS;
        const CLONE_NEWNET = libc::CLONE_NEWNET;
        const CLONE_NEWUTS = libc::CLONE_NEWUTS;
        const CLONE_NEWCGROUP = libc::CLONE_NEWCGROUP;
    }
}

#[cfg(target_arch = "x86_64")]
pub unsafe fn clone(flags: CloneFlags) -> io::Result<u32> {
    let flags = flags.bits() | libc::SIGCHLD;
    let pid = cvt(unsafe {
        libc::syscall(
            libc::SYS_clone,
            flags as c_ulong,
            ptr::null_mut::<c_void>(), // stack
            ptr::null_mut::<c_int>(),  // parent_tid
            ptr::null_mut::<c_int>(),  // child_tid
            0,                         // tls
        ) as libc::pid_t
    })?;
    Ok(pid as u32)
}

#[cfg(target_arch = "aarch64")]
pub unsafe fn clone(flags: CloneFlags) -> io::Result<u32> {
    let flags = flags.bits() | libc::SIGCHLD;
    let pid = cvt(unsafe {
        libc::syscall(
            libc::SYS_clone,
            flags as c_ulong,
            ptr::null_mut::<c_void>(), // stack
            ptr::null_mut::<c_int>(),  // parent_tid
            0,                         // tls
            ptr::null_mut::<c_int>(),  // child_tid
        ) as libc::pid_t
    })?;
    Ok(pid as u32)
}
