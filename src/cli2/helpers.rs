/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::num::ParseIntError;

use landlock::AccessFs;
use lnix::ugid::{Gid, RawGid, RawUid, Uid};

/// Helper function because `AccessFs` does not implement `FromStr`.
///
/// `s` can either be `"ALL"` or a `|` (pipe) separated list of `AccessFs` flags.
pub(super) fn access_fs_from_str(s: &str) -> Result<AccessFs, String> {
    match s {
        "ALL" => return Ok(AccessFs::all_available()),
        "ALL_v1" => return Ok(AccessFs::ALL_v1),
        "ALL_v2" => return Ok(AccessFs::ALL_v2),
        "ALL_v3" => return Ok(AccessFs::ALL_v3),
        "ALL_v5" => return Ok(AccessFs::ALL_v5),
        _ => (),
    }

    let mut access_fs = AccessFs::empty();
    for flag in s.split('|') {
        access_fs |= match flag {
            "EXECUTE" => AccessFs::EXECUTE,
            "WRITE_FILE" => AccessFs::WRITE_FILE,
            "READ_FILE" => AccessFs::READ_FILE,
            "READ_DIR" => AccessFs::READ_DIR,
            "REMOVE_DIR" => AccessFs::REMOVE_DIR,
            "REMOVE_FILE" => AccessFs::REMOVE_FILE,
            "MAKE_CHAR" => AccessFs::MAKE_CHAR,
            "MAKE_DIR" => AccessFs::MAKE_DIR,
            "MAKE_REG" => AccessFs::MAKE_REG,
            "MAKE_SOCK" => AccessFs::MAKE_SOCK,
            "MAKE_FIFO" => AccessFs::MAKE_FIFO,
            "MAKE_BLOCK" => AccessFs::MAKE_BLOCK,
            "MAKE_SYM" => AccessFs::MAKE_SYM,
            "REFER" => AccessFs::REFER,
            "REFER?" => AccessFs::REFER.remove_unavailable(),
            "TRUNCATE" => AccessFs::TRUNCATE,
            "TRUNCATE?" => AccessFs::TRUNCATE.remove_unavailable(),
            "IOCTL_DEV" => AccessFs::IOCTL_DEV,
            "IOCTL_DEV?" => AccessFs::IOCTL_DEV.remove_unavailable(),
            _ => return Err(format!("Invalid AccessFs flag: {flag}")),
        };
    }
    Ok(access_fs)
}

macro_rules! setids_from_str {
    ($Id:ty, $RawId:ty, $ids:expr) => {
        if let Some((id, eid)) = $ids.split_once(',') {
            Ok((
                if id == "-" {
                    None
                } else {
                    Some(<$Id>::from_raw(id.parse::<$RawId>()?))
                },
                Some(<$Id>::from_raw(eid.parse::<$RawId>()?)),
            ))
        } else {
            Ok((Some(<$Id>::from_raw($ids.parse::<$RawId>()?)), None))
        }
    };
}

pub(super) fn setuids_from_str(ids: &str) -> Result<(Option<Uid>, Option<Uid>), ParseIntError> {
    setids_from_str!(Uid, RawUid, ids)
}

pub(super) fn setgids_from_str(ids: &str) -> Result<(Option<Gid>, Option<Gid>), ParseIntError> {
    setids_from_str!(Gid, RawGid, ids)
}
