/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::str::FromStr;

pub(super) fn parse_collection_from_str<S, T, U, E>(s: S) -> Result<T, E>
where
    S: AsRef<str>,
    T: FromIterator<U>,
    U: FromStr<Err = E>,
{
    s.as_ref()
        .split(',')
        .filter(|x| !x.is_empty())
        .map(|x| x.parse())
        .collect()
}

#[expect(dead_code, reason = "Possible future use")]
pub(super) fn parse_collection_from_into<S, T, U>(s: S) -> T
where
    S: AsRef<str>,
    T: FromIterator<U>,
    U: for<'a> From<&'a str>,
{
    s.as_ref()
        .split(',')
        .filter(|x| !x.is_empty())
        .map(|x| x.into())
        .collect()
}

pub(super) fn parse_collection_with<S, T, U, E, F>(s: S, f: F) -> Result<T, E>
where
    S: AsRef<str>,
    T: FromIterator<U>,
    F: for<'a> FnMut(&'a str) -> Result<U, E>,
{
    s.as_ref()
        .split(',')
        .filter(|x| !x.is_empty())
        .map(f)
        .collect()
}

pub(super) fn parse_super_collection_with<S, T, U, E, F>(s: S, f: F) -> Result<T, E>
where
    S: AsRef<str>,
    T: FromIterator<U>,
    F: for<'a> FnMut(&'a str) -> Result<U, E>,
{
    s.as_ref()
        .split(';')
        .filter(|x| !x.is_empty())
        .map(f)
        .collect()
}

pub(super) fn parse_some_from_str<S, T, E>(s: S) -> Result<Option<T>, String>
where
    S: AsRef<str>,
    T: FromStr<Err = E>,
    E: ToString,
{
    Ok(Some(s.as_ref().parse::<T>().map_err(|e| e.to_string())?))
}
