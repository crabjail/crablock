/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

pub(super) const CRABLOCK_VERSION: &str = env!("CARGO_PKG_VERSION");
pub(super) const SHORT_USAGE: &str = "USAGE: crablock [OPTIONS] -- <PROGRAM> [PROGRAM ARGUMENTS]";
pub(super) const USAGE: &str = "
USAGE:

    crablock [OPTIONS] -- <PROGRAM> [PROGRAM ARGUMENTS]

OPTIONS:
    --help                   Show this help and exit.
    --version                Show version and exit.
    --args-lf <FD|PATH>      Parse LF-separated args from FD or PATH.
    --args-nul <FD>          Parse NUL-separated args from FD.
    --sandbox-name <NAME>    Set a sandbox-name.

    --unshare <NAMESPACES>   Unshare all listed namespaces.

    --uid <UID>              Map the current user to UID in the new user-namespace.
    --gid <GID>              Map the current group to GID in the new user-namespace.
    --map-uids <UID>:<LOWER_UID>:<COUNT>
                             Map the given range of UIDs in the new user-namespace.
    --map-gids <GID>:<LOWER_GID>:<COUNT>
                             Map the given range of GIDs in the new user-namespace.
    --setuid <UID>[,<EUID>]  Set the uid and optionally the euid.
    --setgid <GID>[,<EGID>]  Set the gid and optionally the egid.

    --capabilities <CAPABILITIES>
                             Add the listed capabilities to the sandbox and remove all other.
    --securebits <SECBITS>   Set securebits.

    --no-new-privs           Set the no_new_privs prctl.

    --mount-devpts           Mount new devpts at /dev/pts.
    --mount-mqueue           Mount new mqueue at /dev/mqueue.
    --mount-proc             Mount new proc at /proc.
    --mount-proc-subset <SUBSET>
                             Mount proc with given subset.
    --mount-proc-read-only   Mount proc with read-only suberblock.
    --mount-proc-read-write  Mount proc with read-write superblock.
    --fsm-mount <FILESYSTEM> <TARGET> <SUPER-OPTIONS> <MOUNT-OPTIONS>
                                         Manipulate by mounting a new filesystem.
    --fsm-umount <TARGET>                Manipulate with umount2(2).
    --fsm-bind <SOURCE> <TARGET>         Manipulate with a non-recursive bind-mount.
    --fsm-rbind <SOURCE> <TARGET>        Manipulate with a recursive bind-mount.
    --fsm-move-mount <FROM> <TO>         Manipulate by moving a mount.
    --fsm-pivot-root <NEW-ROOT> <PUT-OLD>
                                         Manipulate with pivot_root(2).
    --fsm-mount-setattr <PATH> <ATTRS>   Manipulate with mount_setattr(2).
    --fsm-chdir <PATH>                   Manipulate with chdir(2).
    --fsm-chroot <PATH>                  Manipulate with chroot(2).
    --mnt-mask [+MODIFIERS] <PATH>       Masks PATH.
    --mnt-mask-dev [+MODIFIERS] <PATH>   Masks PATH and setsup a minimal /dev.
    --mnt-private-volatile [+MODIFIERS] <PATH>
                                         Makes PATH private.
    --mnt-private [+MODIFIERS] <PATH>    Makes PATH private.
    --mnt-allow-volatile-from [+MODIFIERS] <PATH> <FROM>
                                         Allows PATH from FROM.
    --mnt-allow-volatile [+MODIFIERS] <PATH>
                                         Allows PATH.
    --mnt-allow-from [+MODIFIERS] <PATH> <FROM>
                                         Allows PATH from FROM.
    --mnt-allow [+MODIFIERS] <PATH>      Allows PATH.
    --mnt-deny [+MODIFIERS] <PATH>       Makes PATH inaccessible.
    --mnt-ro [+MODIFIERS] <PATH>         Makes PATH read-only.
    --mnt-rw [+MODIFIERS] <PATH>         Makes PATH read-write.
    --mnt-nosuid [+MODIFIERS] <PATH>     Makes PATH nosuid.
    --mnt-suid [+MODIFIERS] <PATH>       Makes PATH suid.
    --mnt-nodev [+MODIFIERS] <PATH>      Makes PATH nodev.
    --mnt-dev [+MODIFIERS] <PATH>        Makes PATH dev.
    --mnt-noexec [+MODIFIERS] <PATH>     Makes PATH noexec.
    --mnt-exec [+MODIFIERS] <PATH>       Makes PATH exec.
    --cwd <PATH>             Set the working directory.

    --hostname <HOSTNAME>    Set the hostname in the new uts-namespace to HOSTNAME.
    --pasta                  Use pasta(1) for user-mode network connectivity.

    --landlock-fs-executable-path <PATH>
                             Allow to execute files beneath PATH.
    --landlock-fs-readable-path <PATH>
                             Allow to read files beneath PATH.
    --landlock-fs-writable-path <PATH>
                             Allow to write and truncate files beneath PATH.
    --landlock-fs-listable-path <PATH>
                             Allow to list directory contents beneath PATH.
    --landlock-fs-manageable-path <PATH>
                             Allow to manage (CRUD) beneath PATH.
    --landlock-fs-device-useable-path <PATH>
                             Allow to use device files beneath PATH.
    --landlock-fs-unrestricted-path <PATH>
                             Allow everything beneath PATH.
    --landlock-fs-custom <ACCESS_FS1> <PATH>
                             Allow the listed access rights beneath PATH.
    --landlock-restrict-bind-tcp <PORTS>
                             Restrict bind() for TCP to the listed ports.
    --landlock-restrict-connect-tcp <PORTS>
                             Restrict connect() for TCP to the listed ports.
    --landlock-scope-abstract-unix-socket
                             Scope abstract UNIX sockets
    --landlock-scope-signal  Scope signals

    --add-seccomp-fd <FD>    Load seccomp filter from FD.
    --seccomp-syscall-filter *:<DEFAULT_ACTION>,<SYSCALL:ACTION,...>
                             Load a seccomp filter which filters all syscalls.
    --seccomp-restrict-iotl <IOCTLS>
                             Load a seccomp filter which allows only the listed ioctls.
    --seccomp-restrict-prctl <PRCTLS>
                             Load a seccomp filter which allows only the listed prctls.
    --seccomp-restrict-socket <ARG0,ARG1,ARG2;...>
                             Load a seccomp filter which allows only the listed socket calls.
    --seccomp-flatpak        Load a seccomp filter similar to the one used by flatpak.
    --seccomp-memfd-noexec   Allow memfd_create only for NX usage.
    --seccomp-deny-clone-newuser
                             Load a seccomp filer which disallows CLONE_NEWUSER.
    --seccomp-deny-execve-null
                             Load a seccomp filer which disallows execve with argv/envp==NULL.
    --seccomp-deny-memory-write-execute
                             Load a seccomp filter which enforces W^X for mmap family.

    --max-cgroup-namespaces <COUNT>   Per user limit of cgroup namespaces.
    --max-ipc-namespaces <COUNT>      Per user limit of ipc namespaces.
    --max-mnt-namespaces <COUNT>      Per user limit of mnt namespaces.
    --max-net-namespaces <COUNT>      Per user limit of net namespaces.
    --max-pid-namespaces <COUNT>      Per user limit of pid namespaces.
    --max-time-namespaces <COUNT>     Per user limit of time namespaces.
    --max-user-namespaces <COUNT>     Per user limit of user namespaces.
    --max-uts-namespaces <COUNT>      Per user limit of uts namespaces.

    --choom <SCORE>          Set oom_score_adj value.
    --nice <INC>             Adjust niceness.

    --clearenv               Remove all env-vars.
    --keepenv <NAME>         Keep env-var NAME when using --clearenv.
    --unsetenv <NAME>        Remove env-var NAME.
    --setenv <NAME>=<VALUE>  Set env-var NAME to VALUE.

    --mdwe-refuse-exec-gain  Set the PR_MDWE_REFUSE_EXEC_GAIN prctl flag.
    --new-session            Create a new session.
    --strict-mitigations     Enabled stricter mitigations.
    --umask <UMASK>          Set the umask to UMASK.

    --custom-init            Do not use crabreaper and start program as pid-1.
    --die-with-parent        SIGKILL everything if our parent dies.
    --crablock-data-home <PATH>     Overwrite crablock's data home directory.
    --crablock-runtime-dir <PATH>   Overwrite crablock's runtime directory.

    --dbus-proxy <BUS> <ADDR> <PATH>     Add new bus to dbus-proxy.
    --dbus-proxy <BUS> <PROXY OPTION>    Add PROXY OPTION to existing bus.
    --dbus-proxy-capture-output <FILE>   Capture output from xdg-dbus-proxy to FILE.
";
