/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! crablock - Low-level unprivileged Linux sandboxing tool
//!
//! **SAFETY NOTE**: All syscalls/ffi-calls (where it matters) assume that we
//! are a single-threaded program at the time we clone (fork) and in the child.

use std::os::unix::net::UnixDatagram;
use std::process::ExitCode;
use std::sync::OnceLock;
use std::time::{Duration, Instant};
use std::{env, str};

use rustix::process::{Pid, RawPid, Signal, WaitOptions, waitpid};

use crate::idmap::IdMap;
use crate::os::{clone, fork_with_pdeathsig};
use crate::utils::{LogLevel, ResultExt};

#[cfg(feature = "debug")]
#[macro_use]
mod debug_macros;

#[macro_use]
mod utils;
mod os;

mod cli2;
mod dirs;
mod path;
mod settings;

mod child;
mod host_op;
mod idmap;
mod modules;
mod unshare;

const CRABREAPER: &str = env!("CRABREAPER");
const SET_MEMFD_NOEXEC: &str = env!("SET_MEMFD_NOEXEC");

static START_TIME: OnceLock<Instant> = OnceLock::new();
static LOG_LEVEL: OnceLock<LogLevel> = OnceLock::new();

fn main() -> ExitCode {
    let _ = START_TIME.set(Instant::now());
    let _ = LOG_LEVEL.set(LogLevel::get("CRABLOCK_LOG", LogLevel::Info));

    match do_crablock() {
        Ok(exit_code) => exit_code,
        Err(err) => {
            eprintln!("crablock: {err}");
            ExitCode::FAILURE
        }
    }
}

#[derive(Debug, Default)]
struct ClConfig {
    //
    // ## Settings ##
    //
    settings: settings::Settings,

    //
    // ## Core ##
    //
    unshare: unshare::Unshare,
    uid_map: Option<IdMap>,
    gid_map: Option<IdMap>,

    custom_init: bool,
    die_with_parent: bool,

    //
    // ## Modules ##
    //
    setid: modules::setid::Setid,

    capabilities: modules::capabilities::Capabilities,

    no_new_privs: modules::no_new_privs::NoNewPrivs,

    mount_apifs: modules::mount_apifs::MountApifs,
    fsm: modules::fsm::Fsm,
    mnt: modules::mnt::Mnt,
    cwd: modules::cwd::Cwd,

    landlock: modules::landlock::Landlock,

    hostname: modules::hostname::Hostname,

    seccomp: modules::seccomp::Seccomp,

    namespaces_limits: modules::namespaceslimits::NamespacesLimits,

    resources: modules::resources::Resources,

    environment: modules::environment::Environment,

    mdwe: modules::mdwe::Mdwe,
    new_session: modules::new_session::NewSession,
    mitigations: modules::mitigations::Mitigations,
    umask: modules::umask::UMask,

    //
    // ## External ##
    //
    dbus_proxy: Option<host_op::dbus_proxy::DBusProxy>,
    pasta: Option<host_op::pasta::Pasta>,

    //
    // ## Program ##
    //
    argv: Vec<String>,
}

fn do_crablock() -> Result<ExitCode, String> {
    let config = ClConfig::from_args()?;

    if config.die_with_parent {
        capctl::prctl::set_pdeathsig(Some(libc::SIGKILL)).err_msg("set PDEATHSIG")?;
    }

    let host_op_sock = UnixDatagram::pair().err_msg("UnixDatagram::pair")?;
    for sock in [&host_op_sock.0, &host_op_sock.1] {
        sock.set_read_timeout(Some(Duration::from_secs(1)))
            .err_msg("set_read_timeout")?;
        sock.set_write_timeout(Some(Duration::from_secs(1)))
            .err_msg("set_write_timeout")?;
    }

    let clone_flags = config.unshare.into_clone_flags();
    debug!("clone({:?})", clone_flags);
    let child_pid = unsafe { clone(clone_flags) }.err_msg("clone")?;
    if child_pid == 0 {
        drop(host_op_sock.1);
        child::main(config, host_op_sock.0)?;
        unreachable!();
    }

    if unsafe { fork_with_pdeathsig(Signal::TERM) }
        .err_msg("fork")?
        .is_none()
    {
        drop(host_op_sock.0);
        // We are the child.
        return host_op::main(&config, host_op_sock.1, child_pid);
    }

    drop(host_op_sock.1);
    do_parent(config, child_pid)
}

fn do_parent(_config: ClConfig, child_pid: u32) -> Result<ExitCode, String> {
    /* Wait until child stops */
    let (_, wait_status) = waitpid(
        Some(Pid::from_raw(child_pid as RawPid).unwrap()),
        WaitOptions::empty(),
    )
    .err_msg("waitpid")?
    .unwrap();

    Ok(if let Some(code) = wait_status.exit_status() {
        ExitCode::from(u8::try_from(code).unwrap_or(u8::MAX))
    } else if let Some(signal) = wait_status.terminating_signal() {
        ExitCode::from(u8::try_from(128 + signal).unwrap_or(u8::MAX))
    } else {
        ExitCode::FAILURE
    })
}

fn request_host_op(host_op_sock: &UnixDatagram, request: &[u8]) -> Result<(), String> {
    debug!(
        "Requesting host_op: b\"{}\"",
        String::from_utf8_lossy(request),
    );

    assert_eq!(host_op_sock.send(request).err_msg("send")?, request.len());

    let mut buf: [u8; 8] = [0; 8];
    host_op_sock.recv(&mut buf).err_msg("recv")?;
    if &buf != b"success;" {
        unimplemented!();
    }

    Ok(())
}
