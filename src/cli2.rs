/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;
use std::cell::RefCell;
use std::collections::HashSet;
use std::ffi::{CString, OsString};
use std::fs::{File, create_dir_all};
use std::io::BufReader;
use std::io::prelude::*;
use std::iter::Peekable;
use std::num::ParseIntError;
use std::os::unix::prelude::*;
use std::path::PathBuf;
use std::str::FromStr;
use std::{env, io, iter, process};

use libseccomp::{ScmpAction, ScmpSyscall};
use rustix::fs::{Mode, RawMode};

use self::constants::*;
use self::generic_helpers::*;
use self::helpers::*;
use crate::ClConfig;
use crate::host_op::pasta::Pasta;
use crate::idmap::{IdMap, IdMapping};
use crate::modules::hostname::Hostname;
use crate::modules::mitigations::Mitigations;
use crate::modules::mnt::MntFlags;
use crate::modules::new_session::NewSession;
use crate::modules::no_new_privs::NoNewPrivs;
use crate::modules::seccomp::SeccompSyscallFilter;
use crate::modules::umask::UMask;
use crate::path::expand_path;
use crate::unshare::Unshare;
use crate::utils::{ResultExt, TriBool};

mod constants;
mod generic_helpers;
mod helpers;

struct Args<I>
where
    I: Iterator<Item = String>,
{
    args: RefCell<Peekable<I>>,
}
impl<I: Iterator<Item = String>> Args<I> {
    fn next(&self, kind: ArgKind) -> String {
        self.try_next(kind)
            .expect("Unexpected end of arguments list")
    }

    fn try_next(&self, kind: ArgKind) -> Option<String> {
        if let Some(arg) = self.args.borrow_mut().next() {
            match kind {
                ArgKind::Option if !arg.starts_with("--") => {
                    warn!("Parsing '{}' as an option rather than a value.", arg);
                }
                ArgKind::Value | ArgKind::Path if arg.starts_with("--") => {
                    warn!("Parsing '{}' as a value rather than an option.", arg);
                }
                ArgKind::Any | ArgKind::Option | ArgKind::Value | ArgKind::Path => (),
            }
            Some(arg)
        } else {
            None
        }
    }

    #[expect(dead_code, reason = "Future use")]
    fn next_if_value(&self) -> Option<String> {
        self.args.borrow_mut().next_if(|arg| !arg.starts_with("--"))
    }

    fn into_inner(self) -> Peekable<I> {
        self.args.into_inner()
    }
}
impl<I> From<I> for Args<I>
where
    I: Iterator<Item = String>,
{
    fn from(iterator: I) -> Self {
        Self {
            args: RefCell::new(iterator.peekable()),
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum ArgKind {
    Any,
    /// Warn if arg does not start with `--`.
    Option,
    /// Warn if arg does start with `--`.
    Value,
    /// Alias for `Value`.
    Path,
}

impl ClConfig {
    pub fn from_args() -> Result<Self, Cow<'static, str>> {
        let mut config = Self::default();
        let args = Args::from(env::args());

        // Drop argv[0]
        // Hey, we can implement multi call.
        drop(args.next(ArgKind::Any));

        while let Some(arg) = args.try_next(ArgKind::Option) {
            if arg == "--" {
                break;
            }
            match_arg(&mut config, &args, &arg)
                .with_err_msg(|| format!("failed to parse '{arg}'"))?;
        }

        config.argv = iter::once(crate::CRABREAPER.to_string())
            .chain(args.into_inner())
            .collect();
        if config.argv.len() == 1 {
            println!("{SHORT_USAGE}");
            process::exit(1);
        }

        config.dwim();

        debug!("{:#?}", config);
        Ok(config)
    }

    /// Do what I mean
    fn dwim(&mut self) {
        if self.hostname.needs_unshare_uts() {
            self.unshare.insert(Unshare::UTS);
        }

        if self.seccomp.memfd_noexec
            || self.mount_apifs.needs_unshare_pid()
            || self.fsm.needs_unshare_pid()
        {
            self.unshare.insert(Unshare::PID);
        }

        if self.pasta.is_some()
            || self.mount_apifs.needs_unshare_net()
            || self.fsm.needs_unshare_net()
        {
            self.unshare.insert(Unshare::NET);
        }

        if self.mount_apifs.needs_unshare_ipc() || self.fsm.needs_unshare_ipc() {
            self.unshare.insert(Unshare::IPC);
        }

        if self.mount_apifs.needs_unshare_cgroup() {
            self.unshare.insert(Unshare::CGROUP);
        }

        if self.unshare.contains(Unshare::PID) && !self.mount_apifs.proc {
            self.mount_apifs.proc = true;
            if self.mount_apifs.proc_subset_pid.is_unknown() {
                self.mount_apifs.proc_subset_pid = TriBool::False;
            }
            if self.mount_apifs.proc_ro.is_unknown() {
                self.mount_apifs.proc_ro = TriBool::False;
            }
        }

        if self.unshare.contains(Unshare::NET) && !self.mount_apifs.sysfs {
            self.mount_apifs.sysfs = true;
        }

        if self.unshare.contains(Unshare::CGROUP) && !self.mount_apifs.cgroup2 {
            self.mount_apifs.cgroup2 = true;
        }

        if self.mnt.needs_unshare_mnt()
            || self.mount_apifs.needs_unshare_mnt()
            || self.fsm.needs_unshare_mnt()
        {
            self.unshare.insert(Unshare::MNT);
        }

        if !self.unshare.is_empty()
            || self.uid_map.is_some()
            || self.gid_map.is_some()
            || self.capabilities.needs_unshare_user()
            || self.namespaces_limits.needs_unshare_user()
        {
            self.unshare.insert(Unshare::USER);
        }

        if self.unshare.user() && self.uid_map.is_none() {
            self.uid_map = Some(IdMap::uid_map());
        }

        if self.unshare.user() && self.gid_map.is_none() {
            self.gid_map = Some(IdMap::gid_map());
        }

        if !self.unshare.user()
            && (self.seccomp.wants_no_new_privs() || self.landlock.wants_no_new_privs())
        {
            self.no_new_privs = NoNewPrivs::Yes;
        }
    }
}

fn match_arg<I>(config: &mut ClConfig, args: &Args<I>, arg: &str) -> Result<(), Cow<'static, str>>
where
    I: Iterator<Item = String>,
{
    match arg {
        "--help" => {
            io::stdout().write_all(USAGE.as_bytes()).expect("show help");
            process::exit(0);
        }
        "--version" => {
            println!("crablock {CRABLOCK_VERSION}");
            process::exit(0);
        }
        "--args-lf" => {
            let path = args.next(ArgKind::Value);
            let args_lf = Args::from(Box::new(
                BufReader::new(if let Ok(fd) = path.parse() {
                    unsafe { File::from_raw_fd(fd) }
                } else {
                    File::open(&*path).with_err_msg(|| format!("failed to open `{path}'"))?
                })
                .lines()
                .map(|line| line.expect("successful read"))
                .filter(|line| !line.is_empty() && !line.starts_with('#')),
            ) as Box<dyn Iterator<Item = String>>);
            while let Some(arg_lf) = args_lf.try_next(ArgKind::Option) {
                match_arg(config, &args_lf, &arg_lf)
                    .with_err_msg(|| format!("failed to parse '{arg_lf}'"))?;
            }
        }
        "--args-nul" => {
            let args_nul = Args::from(Box::new(
                BufReader::new(unsafe {
                    File::from_raw_fd(args.next(ArgKind::Value).parse().expect("file-descriptor"))
                })
                .split(b'\0')
                .map(|arg_nul| arg_nul.expect("successful read"))
                .map(|arg_nul| String::from_utf8(arg_nul).expect("UTF-8")),
            ) as Box<dyn Iterator<Item = String>>);
            while let Some(arg_nul) = args_nul.try_next(ArgKind::Option) {
                match_arg(config, &args_nul, &arg_nul)
                    .with_err_msg(|| format!("failed to parse '{arg_nul}'"))?;
            }
        }
        "--sandbox-name" => config
            .settings
            .set_sandbox_name(args.next(ArgKind::Value))?,
        "--unshare" => {
            for namespace in args.next(ArgKind::Value).split(',') {
                match namespace {
                    "all" => config.unshare = Unshare::all(),
                    "-all" => config.unshare = Unshare::empty(),
                    "user" => config.unshare.insert(Unshare::USER),
                    "-user" => config.unshare.remove(Unshare::USER),
                    "pid" => config.unshare.insert(Unshare::PID),
                    "-pid" => config.unshare.remove(Unshare::PID),
                    "ipc" => config.unshare.insert(Unshare::IPC),
                    "-ipc" => config.unshare.remove(Unshare::IPC),
                    "mnt" => config.unshare.insert(Unshare::MNT),
                    "-mnt" => config.unshare.remove(Unshare::MNT),
                    "net" => config.unshare.insert(Unshare::NET),
                    "-net" => config.unshare.remove(Unshare::NET),
                    "uts" => config.unshare.insert(Unshare::UTS),
                    "-uts" => config.unshare.remove(Unshare::UTS),
                    "cgroup" => config.unshare.insert(Unshare::CGROUP),
                    "-cgroup" => config.unshare.remove(Unshare::CGROUP),
                    _ => bail!("bad namespace {}", namespace),
                }
            }
        }
        "--uid" => {
            config.uid_map.get_or_insert_with(IdMap::uid_map).our.id = args
                .next(ArgKind::Value)
                .parse::<u32>()
                .map_err(|e| e.to_string())?;
        }
        "--gid" => {
            config.uid_map.get_or_insert_with(IdMap::gid_map).our.id = args
                .next(ArgKind::Value)
                .parse::<u32>()
                .map_err(|e| e.to_string())?;
        }
        "--map-uids" => {
            config
                .uid_map
                .get_or_insert_with(IdMap::uid_map)
                .others
                .push(IdMapping::from_str(&args.next(ArgKind::Value))?);
        }
        "--map-gids" => {
            config
                .gid_map
                .get_or_insert_with(IdMap::gid_map)
                .others
                .push(IdMapping::from_str(&args.next(ArgKind::Value))?);
        }
        "--setuid" => {
            (config.setid.uid, config.setid.euid) =
                setuids_from_str(&args.next(ArgKind::Value)).map_err(|e| e.to_string())?;
        }
        "--setgid" => {
            (config.setid.gid, config.setid.egid) =
                setgids_from_str(&args.next(ArgKind::Value)).map_err(|e| e.to_string())?;
        }
        "--capabilities" => {
            let mut caps = capctl::CapSet::empty();
            for capability in args.next(ArgKind::Value).split(',') {
                if capability.eq_ignore_ascii_case("none") {
                    // do nothing
                } else if capability.eq_ignore_ascii_case("ALL") {
                    caps.add_all(capctl::caps::Cap::iter());
                } else {
                    caps.add(
                        capctl::caps::Cap::from_str(capability)
                            .map_err(|err| format!("Failed to parse capability: {err}"))?,
                    );
                }
            }
            config.capabilities.capabilities = Some(caps);
        }
        "--securebits" => {
            config.capabilities.securebits = Some(
                args.next(ArgKind::Value)
                    .split(',')
                    .map(|securebit| match securebit {
                        "no-keep-caps" => Ok(capctl::prctl::Secbits::KEEP_CAPS_LOCKED),
                        "no-setuid-fixup" => Ok(capctl::prctl::Secbits::NO_SETUID_FIXUP
                            | capctl::prctl::Secbits::NO_SETUID_FIXUP_LOCKED),
                        "no-root" => {
                            Ok(capctl::prctl::Secbits::NOROOT
                                | capctl::prctl::Secbits::NOROOT_LOCKED)
                        }
                        "no-cap-ambient-raise" => Ok(capctl::prctl::Secbits::NO_CAP_AMBIENT_RAISE
                            | capctl::prctl::Secbits::NO_CAP_AMBIENT_RAISE_LOCKED),
                        _ => Err(format!("Invalid securebit '{securebit}'.")),
                    })
                    .try_fold(
                        capctl::prctl::Secbits::empty(),
                        |securebits, securebit| -> Result<_, String> {
                            Ok(securebits | securebit?)
                        },
                    )?,
            );
        }
        "--no-new-privs" => {
            config.no_new_privs = NoNewPrivs::Yes;
        }
        "--mount-devpts" => {
            config.mount_apifs.devpts = true;
        }
        "--mount-mqueue" => {
            config.mount_apifs.mqueue = true;
        }
        "--mount-proc" => {
            config.mount_apifs.proc = true;
            if config.mount_apifs.proc_subset_pid.is_unknown() {
                config.mount_apifs.proc_subset_pid = TriBool::True;
            }
            if config.mount_apifs.proc_ro.is_unknown() {
                config.mount_apifs.proc_ro = TriBool::True;
            }
        }
        "--mount-proc-subset" => {
            config.mount_apifs.proc_subset_pid = match &*args.next(ArgKind::Value) {
                "none" => TriBool::False,
                "pid" => TriBool::True,
                _ => bail!("bad subset, neither 'none' nor 'pid'"),
            };
        }
        "--mount-proc-read-only" => {
            config.mount_apifs.proc_ro = TriBool::True;
        }
        "--mount-proc-read-write" => {
            config.mount_apifs.proc_ro = TriBool::False;
        }
        "--fsm-mount" => {
            let filesystem = args.next(ArgKind::Value);
            let target = args.next(ArgKind::Path);
            let super_options = args.next(ArgKind::Value);
            let mount_options = args.next(ArgKind::Value);
            config.fsm.add_mount(
                target,
                TriBool::Unknown,
                &filesystem,
                &super_options,
                &mount_options,
            )?;
        }
        "--fsm-umount" => config.fsm.add_umount(args.next(ArgKind::Path)),
        "--fsm-bind" => config.fsm.add_bind(
            args.next(ArgKind::Path),
            TriBool::Unknown,
            args.next(ArgKind::Path),
            TriBool::Unknown,
            TriBool::False,
        ),
        "--fsm-rbind" => config.fsm.add_bind(
            args.next(ArgKind::Path),
            TriBool::Unknown,
            args.next(ArgKind::Path),
            TriBool::Unknown,
            TriBool::True,
        ),
        "--fsm-move-mount" => config.fsm.add_move_mount(
            args.next(ArgKind::Path),
            TriBool::Unknown,
            args.next(ArgKind::Path),
            TriBool::Unknown,
        ),
        "--fsm-pivot-root" => config
            .fsm
            .add_pivot_root(args.next(ArgKind::Path), args.next(ArgKind::Path)),
        "--fsm-mount-setattr" => config.fsm.add_mount_setattr(
            args.next(ArgKind::Path),
            TriBool::Unknown,
            TriBool::Unknown,
            &args.next(ArgKind::Value),
        )?,
        "--fsm-chdir" => config.fsm.add_chdir(args.next(ArgKind::Path)),
        "--fsm-chroot" => config.fsm.add_chroot(args.next(ArgKind::Path)),
        _ if arg.starts_with("--mnt-") => {
            let value1 = args.next(ArgKind::Value);
            let (path, flags) = if let Some(modifiers) = value1.strip_prefix('+') {
                let mut flags = MntFlags::empty();
                for modifier in modifiers.chars() {
                    match modifier {
                        'f' => flags.insert(MntFlags::FOLLOW_SYMLINKS),
                        'n' => flags.insert(MntFlags::NORMALIZE),
                        'e' => flags.insert(MntFlags::EXISTING_ONLY),
                        'c' => flags.insert(MntFlags::CREATE_DIR),
                        'r' => flags.insert(MntFlags::CREATE_REG),
                        _ => bail!("bad modifier '{}'", modifier),
                    }
                }
                let path = args.next(ArgKind::Path);
                (path, flags)
            } else {
                (value1, MntFlags::empty())
            };

            match arg {
                "--mnt-mask" => config.mnt.add_mask_path(path, flags)?,
                "--mnt-mask-dev" => config.mnt.add_mask_dev_path(path, flags)?,
                "--mnt-private-volatile" => config.mnt.add_private_volatile_path(path, flags)?,
                "--mnt-private" => config.mnt.add_private_path(path, flags)?,
                "--mnt-allow-volatile-from" => config.mnt.add_allow_volatile_from_path(
                    path,
                    PathBuf::from(args.next(ArgKind::Path)),
                    flags,
                )?,
                "--mnt-allow-volatile" => config.mnt.add_allow_volatile_path(path, flags)?,
                "--mnt-allow-from" => config.mnt.add_allow_from_path(
                    path,
                    PathBuf::from(args.next(ArgKind::Path)),
                    flags,
                )?,
                "--mnt-allow" => config.mnt.add_allow_path(path, flags)?,
                "--mnt-deny" => config.mnt.add_deny_path(path, flags)?,
                "--mnt-ro" => config.mnt.add_read_only_path(path, flags)?,
                "--mnt-rw" => config.mnt.add_read_write_path(path, flags)?,
                "--mnt-nosuid" => config.mnt.add_nosuid_path(path, flags)?,
                "--mnt-suid" => config.mnt.add_suid_path(path, flags)?,
                "--mnt-nodev" => config.mnt.add_nodev_path(path, flags)?,
                "--mnt-dev" => config.mnt.add_dev_path(path, flags)?,
                "--mnt-noexec" => config.mnt.add_noexec_path(path, flags)?,
                "--mnt-exec" => config.mnt.add_exec_path(path, flags)?,
                _ => bail!("unknown option"),
            }

            // TODO: if let Some(access) = args.next_if_value() {}
        }
        "--cwd" => {
            config.cwd.path = PathBuf::from(args.next(ArgKind::Path));
        }
        "--hostname" => {
            config.hostname = Hostname::Custom(args.next(ArgKind::Value));
        }
        "--pasta" => {
            config.pasta = Some(Pasta::default());
        }
        _ if arg.starts_with("--pasta-") => {
            if let Some((arg, val)) = arg.split_once('=') {
                config.pasta.get_or_insert_default().set_option(arg, val)?;
            } else {
                config.pasta.get_or_insert_default().set_flag(arg)?;
            }
        }
        "--landlock-fs-executable-path" | "--ll-x" => {
            config
                .landlock
                .add_executable_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        "--landlock-fs-readable-path" | "--ll-r" => {
            config
                .landlock
                .add_readable_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        "--landlock-fs-writable-path" | "--ll-w" => {
            config
                .landlock
                .add_writable_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        "--landlock-fs-listable-path" | "--ll-l" => {
            config
                .landlock
                .add_listable_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        "--landlock-fs-manageable-path" | "--ll-m" => {
            config
                .landlock
                .add_manageable_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        "--landlock-fs-device-useable-path" | "--ll-d" => {
            config
                .landlock
                .add_device_usable_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        _ if arg.starts_with("--ll-") => {
            let path = PathBuf::from(args.next(ArgKind::Path));
            for perm in arg[5..].chars() {
                match perm {
                    'x' => config.landlock.add_executable_path(path.clone()),
                    'r' => config.landlock.add_readable_path(path.clone()),
                    'w' => config.landlock.add_writable_path(path.clone()),
                    'l' => config.landlock.add_listable_path(path.clone()),
                    'm' => config.landlock.add_manageable_path(path.clone()),
                    'd' => config.landlock.add_device_usable_path(path.clone()),
                    _ => bail!("Invalid Landlock permission '{}'", perm),
                }
            }
        }
        "--landlock-fs-unrestricted-path" => {
            config
                .landlock
                .add_unrestricted_path(PathBuf::from(args.next(ArgKind::Path)));
        }
        "--landlock-fs-custom" => {
            config.landlock.add_custom_path(
                access_fs_from_str(&args.next(ArgKind::Value))?,
                PathBuf::from(args.next(ArgKind::Path)),
            );
        }
        "--landlock-restrict-bind-tcp" => {
            let ports = args.next(ArgKind::Value);
            config.landlock.restrict_bind_tcp = Some(if ports == "none" {
                HashSet::new()
            } else {
                parse_collection_from_str::<_, _, u16, _>(ports).map_err(|e| e.to_string())?
            });
        }
        "--landlock-restrict-connect-tcp" => {
            let ports = args.next(ArgKind::Value);
            config.landlock.restrict_connect_tcp = Some(if ports == "none" {
                HashSet::new()
            } else {
                parse_collection_from_str::<_, _, u16, _>(ports).map_err(|e| e.to_string())?
            });
        }
        "--landlock-scope-abstract-unix-socket" => {
            config.landlock.scope_abstract_unix_socket = true;
        }
        "--landlock-scope-signal" => {
            config.landlock.scope_signal = true;
        }
        "--add-seccomp-fd" => {
            config.seccomp.fds.push(
                args.next(ArgKind::Value)
                    .parse::<i32>()
                    .map_err(|e| e.to_string())?,
            );
        }
        "--seccomp-syscall-filter" => {
            let rules = args.next(ArgKind::Value);
            let Some((first_rule, rules)) = rules.split_once(',') else {
                bail!("Expected --seccomp-syscall-filter *:ACTION,...");
            };
            let Some(("*", default_action)) = first_rule.split_once(':') else {
                bail!("Expected --seccomp-syscall-filter *:ACTION,...");
            };
            let default_action = match default_action {
                "kill" => ScmpAction::KillProcess,
                "EPERM" => ScmpAction::Errno(libc::EPERM),
                "ENOSYS" => ScmpAction::Errno(libc::ENOSYS),
                "log" => ScmpAction::Log,
                "allow" => ScmpAction::Allow,
                _ => bail!("bad action '{}'", default_action),
            };
            let rules = parse_collection_with(rules, |rule| -> Result<_, String> {
                let Some((syscall_name, action)) = rule.split_once(':') else {
                    bail!("Expected SYSCALL:ACTION");
                };
                let syscall = ScmpSyscall::from_name(syscall_name).map_err(|e| e.to_string())?;
                let action = match action {
                    "kill" => ScmpAction::KillProcess,
                    "EPERM" => ScmpAction::Errno(libc::EPERM),
                    "ENOSYS" => ScmpAction::Errno(libc::ENOSYS),
                    "log" => ScmpAction::Log,
                    "allow" => ScmpAction::Allow,
                    _ => bail!("bad action '{}'", action),
                };
                Ok((syscall, action))
            })?;
            config.seccomp.syscall_filter = Some(SeccompSyscallFilter {
                default_action,
                rules,
            });
        }
        "--seccomp-restrict-ioctl" => {
            config.seccomp.restrict_ioctl =
                Some(parse_collection_from_str(args.next(ArgKind::Value))?);
        }
        "--seccomp-restrict-prctl" => {
            config.seccomp.restrict_prctl =
                Some(parse_collection_from_str(args.next(ArgKind::Value))?);
        }
        "--seccomp-restrict-socket" => {
            config.seccomp.restrict_socket = Some(
                parse_super_collection_with(args.next(ArgKind::Value), |socket_call| {
                    let socket_args = socket_call.splitn(3, ',').collect::<Vec<_>>();
                    let arg0 = match socket_args[0] {
                        "" => None,
                        "AF_UNIX" => Some(linux_raw_sys::net::AF_UNIX as i32),
                        "AF_INET" => Some(linux_raw_sys::net::AF_INET as i32),
                        "AF_INET6" => Some(linux_raw_sys::net::AF_INET6 as i32),
                        "AF_NETLINK" => Some(linux_raw_sys::net::AF_NETLINK as i32),
                        domain => Some(domain.parse::<i32>()?),
                    };
                    let arg1 = match socket_args[1] {
                        "" => None,
                        "SOCK_STREAM" => Some(linux_raw_sys::net::SOCK_STREAM as i32),
                        "SOCK_DGRAM" => Some(linux_raw_sys::net::SOCK_DGRAM as i32),
                        "SOCK_RAW" => Some(linux_raw_sys::net::SOCK_RAW as i32),
                        "SOCK_SEQPACKET" => Some(linux_raw_sys::net::SOCK_SEQPACKET as i32),
                        domain => Some(domain.parse::<i32>()?),
                    };
                    let arg2 = match socket_args[2] {
                        "" => None,
                        "IPPROTO_IP" => Some(linux_raw_sys::net::IPPROTO_IP as i32),
                        "IPPROTO_TCP" => Some(linux_raw_sys::net::IPPROTO_TCP as i32),
                        "IPPROTO_UDP" => Some(linux_raw_sys::net::IPPROTO_UDP as i32),
                        "NETLINK_GENERIC" => Some(linux_raw_sys::netlink::NETLINK_GENERIC as i32),
                        "NETLINK_ROUTE" => Some(linux_raw_sys::netlink::NETLINK_ROUTE as i32),
                        "NETLINK_SOCK_DIAG" => {
                            Some(linux_raw_sys::netlink::NETLINK_SOCK_DIAG as i32)
                        }
                        domain => Some(domain.parse::<i32>()?),
                    };

                    Ok::<_, ParseIntError>((arg0, arg1, arg2))
                })
                .map_err(|e| e.to_string())?,
            );
        }
        "--seccomp-flatpak" => {
            config.seccomp.flatpak = true;
        }
        "--seccomp-memfd-noexec" => {
            config.seccomp.memfd_noexec = true;
        }
        "--seccomp-deny-clone-newuser" => {
            config.seccomp.deny_clone_newuser = true;
        }
        "--seccomp-deny-execve-null" => {
            config.seccomp.deny_execve_null = true;
        }
        "--seccomp-deny-memory-write-execute" => {
            config.seccomp.deny_memory_write_execute = true;
        }
        "--max-cgroup-namespaces" => {
            config.namespaces_limits.cgroup = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-ipc-namespaces" => {
            config.namespaces_limits.ipc = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-mnt-namespaces" => {
            config.namespaces_limits.mnt = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-net-namespaces" => {
            config.namespaces_limits.net = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-pid-namespaces" => {
            config.namespaces_limits.pid = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-time-namespaces" => {
            config.namespaces_limits.time = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-user-namespaces" => {
            config.namespaces_limits.user = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--max-uts-namespaces" => {
            config.namespaces_limits.uts = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--choom" => {
            config.resources.choom = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--nice" => {
            config.resources.nice = parse_some_from_str(args.next(ArgKind::Value))?;
        }
        "--clearenv" => {
            config.environment.clear = true;
        }
        "--keepenv" => {
            config
                .environment
                .keep
                .insert(OsString::from(args.next(ArgKind::Value)));
        }
        "--unsetenv" => {
            config
                .environment
                .unset
                .insert(OsString::from(args.next(ArgKind::Value)));
        }
        "--setenv" => {
            if let Some((var, val)) = args.next(ArgKind::Value).split_once('=') {
                config
                    .environment
                    .set
                    .insert(OsString::from(var), OsString::from(val));
            } else {
                bail!("Missing '='");
            }
        }
        "--mdwe-refuse-exec-gain" => {
            config.mdwe.refuse_exec_gain = true;
        }
        "--new-session" => {
            config.new_session = NewSession::Yes;
        }
        "--strict-mitigations" => {
            config.mitigations = Mitigations::Strict;
        }
        "--umask" => {
            config.umask = UMask {
                mask: Some(Mode::from_raw_mode(
                    RawMode::from_str_radix(&args.next(ArgKind::Value), 8)
                        .map_err(|e| e.to_string())?,
                )),
            }
        }
        "--custom-init" => {
            config.custom_init = true;
        }
        "--die-with-parent" => {
            config.die_with_parent = true;
        }
        "--crablock-data-home" => {
            config
                .settings
                .set_crablock_data_home(args.next(ArgKind::Path));
        }
        "--crablock-runtime-dir" => {
            config
                .settings
                .set_crablock_runtime_dir(args.next(ArgKind::Path));
        }
        "--dbus-proxy" => {
            let bus = args.next(ArgKind::Value);
            let proxy_opt = args.next(ArgKind::Value);
            if proxy_opt.starts_with('-') {
                config
                    .dbus_proxy
                    .as_mut()
                    .and_then(|dp| {
                        dp.buses
                            .get_mut(&bus)?
                            .push(CString::new(proxy_opt).unwrap());
                        Some(())
                    })
                    .ok_or("Unknown bus")?;
            } else {
                let addr = proxy_opt;
                let path = expand_path(PathBuf::from(args.next(ArgKind::Path)));
                create_dir_all(path.parent().ok_or("Invalid PATH")?)
                    .with_err_msg(|| format!("Failed to create parent of {}", path.display()))?;
                let address: Cow<'_, [u8]> = match &*addr {
                    "@{DBUS_SESSION_BUS_ADDRESS}" => env::var_os("DBUS_SESSION_BUS_ADDRESS")
                        .expect("$DBUS_SESSION_BUS_ADDRESS")
                        .into_vec()
                        .into(),
                    "@{DBUS_SYSTEM_BUS_ADDRESS}" => env::var_os("DBUS_SYSTEM_BUS_ADDRESS").map_or(
                        "unix:path=/run/dbus/system_bus_socket".as_bytes().into(),
                        |v| v.into_vec().into(),
                    ),
                    _ => addr.as_bytes().into(),
                };
                config.dbus_proxy.get_or_insert_default().buses.insert(
                    bus,
                    vec![
                        CString::new(address).unwrap(),
                        CString::new(path.into_owned().into_os_string().into_vec()).unwrap(),
                    ],
                );
            }
        }
        "--dbus-proxy-capture-output" => {
            config.dbus_proxy.get_or_insert_default().capture_output =
                Some(args.next(ArgKind::Value));
        }
        _ if arg.contains('=') => bail!(
            "unknown option\nNote that crablock expects the option value to be in the next argument, not in the current one after an equal sign."
        ),
        _ => bail!("unknown option"),
    }
    Ok(())
}
