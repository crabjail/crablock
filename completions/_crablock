#compdef crablock _crablock

UID=${UID:-$(id -u)}
GID=${GID:-$(id -g)}

_cl_unshare() {
    _namespaces=(
        '(user pid ipc mnt net uts cgroup)all'
        #'(-user -pid -ipc -mnt -net -uts -cgroup)-all'

        '(all -user)user'
        '(all -pid)pid'
        '(all -ipc)ipc'
        '(all -mnt)mnt'
        '(all -net)net'
        '(all -uts)uts'
        '(all -cgroup)cgroup'

        '(user)-user'
        '(pid)-pid'
        '(ipc)-ipc'
        '(mnt)-mnt'
        '(net)-net'
        '(uts)-uts'
        '(cgroup)-cgroup'
    )
    _values -s , namespaces $_namespaces
}

_cl_capabilities() {
    ALL_CAPS=(
        CAP_CHOWN CAP_DAC_OVERRIDE CAP_DAC_READ_SEARCH CAP_FOWNER CAP_FSETID CAP_KILL CAP_SETGID CAP_SETUID
        CAP_SETPCAP CAP_LINUX_IMMUTABLE CAP_NET_BIND_SERVICE CAP_NET_BROADCAST CAP_NET_ADMIN CAP_NET_RAW
        CAP_IPC_LOCK CAP_IPC_OWNER CAP_SYS_MODULE CAP_SYS_RAWIO CAP_SYS_CHROOT CAP_SYS_PTRACE CAP_SYS_PACCT
        CAP_SYS_ADMIN CAP_SYS_BOOT CAP_SYS_NICE CAP_SYS_RESOURCE CAP_SYS_TIME CAP_SYS_TTY_CONFIG CAP_MKNOD
        CAP_LEASE CAP_AUDIT_WRITE CAP_AUDIT_CONTROL CAP_SETFCAP CAP_MAC_OVERRIDE CAP_MAC_ADMIN CAP_SYSLOG
        CAP_WAKE_ALARM CAP_BLOCK_SUSPEND CAP_AUDIT_READ CAP_PERFMON CAP_BPF CAP_CHECKPOINT_RESTORE
    )

    _values -s , capabilities \
        '(ALL none)'$^ALL_CAPS \
        '(none '$ALL_CAPS')ALL' \
        '(ALL '$ALL_CAPS')none'
}

_cl_securebits() {
    _values -s , securebits no-keep-caps no-setuid-fixup no-root no-cap-ambient-raise
}

_cl_landlock_fs_custom() {
    _access_fs=(
        EXECUTE
        WRITE_FILE
        READ_FILE
        READ_DIR
        REMOVE_DIR
        REMOVE_FILE
        MAKE_CHAR
        MAKE_DIR
        MAKE_REG
        MAKE_SOCK
        MAKE_FIFO
        MAKE_BLOCK
        MAKE_SYM
        REFER
        'REFER?'
        TRUNCATE
        'TRUNCATE?'
        IOCTL_DEV
        'IOCTL_DEV?'
        ALL
        ALL_v1
        ALL_v2
        ALL_v3
        ALL_v5
    )
    _values -s "|" ACCESS_FS $_access_fs
}

_cl_seccomp_syscall_filter() {
    # _actions=(
    #     'kill[kill the process]'
    #     'EPERM[return EPERM]'
    #     'ENOSYS[return ENOSYS]'
    #     'log[log to audit]'
    #     'allow[allow this syscall]
    # )
    _actions=(kill EPERM ENOSYS log allow)
    _all_syscalls=(
        _llseek _newselect _sysctl accept accept4 access acct add_key adjtimex afs_syscall alarm arch_prctl
        arm_fadvise64_64 arm_sync_file_range bdflush bind bpf break breakpoint brk cachectl cacheflush
        cachestat capget capset chdir chmod chown chown32 chroot clock_adjtime clock_adjtime64 clock_getres
        clock_getres_time64 clock_gettime clock_gettime64 clock_nanosleep clock_nanosleep_time64
        clock_settime clock_settime64 clone clone3 close close_range connect copy_file_range creat
        create_module delete_module dup dup2 dup3 epoll_create epoll_create1 epoll_ctl epoll_ctl_old
        epoll_pwait epoll_pwait2 epoll_wait epoll_wait_old eventfd eventfd2 execve execveat exit exit_group
        faccessat faccessat2 fadvise64 fadvise64_64 fallocate fanotify_init fanotify_mark fchdir fchmod
        fchmodat fchmodat2 fchown fchown32 fchownat fcntl fcntl64 fdatasync fgetxattr finit_module flistxattr
        flock fork fremovexattr fsconfig fsetxattr fsmount fsopen fspick fstat fstat64 fstatat64 fstatfs
        fstatfs64 fsync ftime ftruncate ftruncate64 futex futex_requeue futex_time64 futex_wait futex_waitv
        futex_wake futimesat get_kernel_syms get_mempolicy get_robust_list get_thread_area get_tls getcpu
        getcwd getdents getdents64 getegid getegid32 geteuid geteuid32 getgid getgid32 getgroups getgroups32
        getitimer getpeername getpgid getpgrp getpid getpmsg getppid getpriority getrandom getresgid
        getresgid32 getresuid getresuid32 getrlimit getrusage getsid getsockname getsockopt gettid
        gettimeofday getuid getuid32 getxattr gtty idle init_module inotify_add_watch inotify_init
        inotify_init1 inotify_rm_watch io_cancel io_destroy io_getevents io_pgetevents io_pgetevents_time64
        io_setup io_submit io_uring_enter io_uring_register io_uring_setup ioctl ioperm iopl ioprio_get
        ioprio_set ipc kcmp kexec_file_load kexec_load keyctl kill landlock_add_rule landlock_create_ruleset
        landlock_restrict_self lchown lchown32 lgetxattr link linkat listen listxattr llistxattr lock
        lookup_dcookie lremovexattr lseek lsetxattr lstat lstat64 madvise map_shadow_stack mbind membarrier
        memfd_create memfd_secret migrate_pages mincore mkdir mkdirat mknod mknodat mlock mlock2 mlockall
        mmap mmap2 modify_ldt mount mount_setattr move_mount move_pages mprotect mpx mq_getsetattr mq_notify
        mq_open mq_timedreceive mq_timedreceive_time64 mq_timedsend mq_timedsend_time64 mq_unlink mremap
        msgctl msgget msgrcv msgsnd msync multiplexer munlock munlockall munmap name_to_handle_at nanosleep
        newfstatat nfsservctl nice oldfstat oldlstat oldolduname oldstat olduname open open_by_handle_at
        open_tree openat openat2 pause pciconfig_iobase pciconfig_read pciconfig_write perf_event_open
        personality pidfd_getfd pidfd_open pidfd_send_signal pipe pipe2 pivot_root pkey_alloc pkey_free
        pkey_mprotect poll ppoll ppoll_time64 prctl pread64 preadv preadv2 prlimit64 process_madvise
        process_mrelease process_vm_readv process_vm_writev prof profil pselect6 pselect6_time64 ptrace
        putpmsg pwrite64 pwritev pwritev2 query_module quotactl quotactl_fd read readahead readdir readlink
        readlinkat readv reboot recv recvfrom recvmmsg recvmmsg_time64 recvmsg remap_file_pages removexattr
        rename renameat renameat2 request_key restart_syscall riscv_flush_icache rmdir rseq rt_sigaction
        rt_sigpending rt_sigprocmask rt_sigqueueinfo rt_sigreturn rt_sigsuspend rt_sigtimedwait
        rt_sigtimedwait_time64 rt_tgsigqueueinfo rtas s390_guarded_storage s390_pci_mmio_read
        s390_pci_mmio_write s390_runtime_instr s390_sthyi sched_get_priority_max sched_get_priority_min
        sched_getaffinity sched_getattr sched_getparam sched_getscheduler sched_rr_get_interval
        sched_rr_get_interval_time64 sched_setaffinity sched_setattr sched_setparam sched_setscheduler
        sched_yield seccomp security select semctl semget semop semtimedop semtimedop_time64 send sendfile
        sendfile64 sendmmsg sendmsg sendto set_mempolicy set_mempolicy_home_node set_robust_list
        set_thread_area set_tid_address set_tls setdomainname setfsgid setfsgid32 setfsuid setfsuid32 setgid
        setgid32 setgroups setgroups32 sethostname setitimer setns setpgid setpriority setregid setregid32
        setresgid setresgid32 setresuid setresuid32 setreuid setreuid32 setrlimit setsid setsockopt
        settimeofday setuid setuid32 setxattr sgetmask shmat shmctl shmdt shmget shutdown sigaction
        sigaltstack signal signalfd signalfd4 sigpending sigprocmask sigreturn sigsuspend socket socketcall
        socketpair splice spu_create spu_run ssetmask stat stat64 statfs statfs64 statx stime stty
        subpage_prot swapcontext swapoff swapon switch_endian symlink symlinkat sync sync_file_range
        sync_file_range2 syncfs sys_debug_setcontext syscall sysfs sysinfo syslog sysmips tee tgkill time
        timer_create timer_delete timer_getoverrun timer_gettime timer_gettime64 timer_settime
        timer_settime64 timerfd timerfd_create timerfd_gettime timerfd_gettime64 timerfd_settime
        timerfd_settime64 times tkill truncate truncate64 tuxcall ugetrlimit ulimit umask umount umount2
        uname unlink unlinkat unshare uselib userfaultfd usr26 usr32 ustat utime utimensat utimensat_time64
        utimes vfork vhangup vm86 vm86old vmsplice vserver wait4 waitid waitpid write writev 
    )
    _values -s , -S : syscalls $^_all_syscalls": :($_actions)"
}

_crablock_args=(
    '--[]'

    '--help[Show help and exit]'
    '--version[Show version and exit]'

    '*--args-lf[Parse LF-separated args from FD or PATH]: :_files'
    '*--args-nul[Parse NUL-separated args from FD]: :'

    '--sandbox-name[Set a sandbox-name]: :'

    '--unshare[Unshare all listed namespaces]: :_cl_unshare'

    '--uid[Map the current user to UID in the new user-namespace]: :(0 '$UID')'
    '--gid[Map the current group to GID in the new user-namespace]: :(0 '$GID')'
    '*--map-uids[Map the given range of UIDs in the new user-namespace]: :'
    '*--map-gids[Map the given range of GIDs in the new user-namespace]: :'
    '--setuid[Set the uid and optionally the euid]: :'
    '--setgid[Set the gid and optionally the egid]: :'

    '--capabilities[Add the listed capabilities to the sandbox and remove all other]: :_cl_capabilities'
    '--securebits[Set securebits]: :_cl_securebits'

    '--no-new-privs[Set the no_new_privs prctl]'

    '--mount-devpts[Mount new devpts at /dev/pts]'
    '--mount-mqueue[Mount new mqueue at /dev/mqueue]'
    '--mount-proc[Mount new proc at /proc]'
    '--mount-proc-subset[Mount proc with given subset]: :(none pid)'
    '--mount-proc-read-only[Mount proc with read-only superblock]'
    '--mount-proc-read-write[Mount proc with read-write superblock]'
    '*--fsm-mount[Manipulate by mounting a new filesystem]: :(devpts mqueue overlay proc sysfs tmpfs): :_files -/: : : :'
    '*--fsm-umount[Manipulate with umount2(2)]: :_files'
    '*--fsm-bind[Manipulate with a non-recursive bind-mount]: :_files: :_files'
    '*--fsm-rbind[Manipulate with a recursive bind-mount]: :_files: :_files'
    '*--fsm-move-mount[Manipulate by moving a mount]: :_files: :_files'
    '*--fsm-pivot-root[Manipulate with pivot_root(2)]: :_files -/: :_files -/'
    '*--fsm-mount-setattr[Manipulate with mount_setattr(2)]: :_files: :'
    '*--fsm-chdir[Manipulate with chdir(2)]: :_files -/'
    '*--fsm-chroot[Manipulate with chroot(2)]: :_files -/'
    '*--mnt-mask[Masks PATH]: :_files'
    '*--mnt-mask-dev[Masks PATH and setsup a minimal /dev]: :_files -/'
    '*--mnt-private-volatile[Makes PATH private]: :_files'
    '*--mnt-private[Makes PATH private]: :_files'
    '*--mnt-allow-volatile-from[Allows PATH from FROM]: :_files: :_files'
    '*--mnt-allow-volatile[Allows PATH]: :_files'
    '*--mnt-allow-from[Allows PATH from FROM]: :_files: :_files'
    '*--mnt-allow[Allows PATH]: :_files'
    '*--mnt-deny[Makes PATH inaccessible]: :_files'
    '*--mnt-ro[Makes PATH read-only]: :_files'
    '*--mnt-rw[Makes PATH read-write]: :_files'
    '*--mnt-nosuid[Makes PATH nosuid]: :_files'
    '*--mnt-suid[Makes PATH suid]: :_files'
    '*--mnt-nodev[Makes PATH nodev]: :_files'
    '*--mnt-dev[Makes PATH dev]: :_files'
    '*--mnt-noexec[Makes PATH noexec]: :_files'
    '*--mnt-exec[Makes PATH exec]: :_files'
    '--cwd[Set the working directory]: :_files -/'

    '--hostname[Set the hostname in the new uts-namespace to HOSTNAME]: :'
    '--pasta[Use pasta(1) for user-mode network connectivity]'

    '*--ll-x: :_files'
    '*--ll-r: :_files'
    '*--ll-w: :_files'
    '*--ll-w: :_files'
    '*--ll-l: :_files'
    '*--ll-m: :_files'
    '*--ll-d: :_files'
    '*--ll-xr: :_files'
    '*--ll-xrl: :_files'
    '*--ll-rw: :_files'
    '*--ll-rwl: :_files'
    '*--ll-lm: :_files'
    '*--ll-xlm: :_files'
    '*--landlock-fs-executable-path[Allow to execute files beneath PATH]: :_files'
    '*--landlock-fs-readable-path[Allow to read files beneath PATH]: :_files'
    '*--landlock-fs-writable-path[Allow to write and truncate files beneath PATH]: :_files'
    '*--landlock-fs-listable-path[Allow to list directory contents beneath PATH]: :_files -/'
    '*--landlock-fs-manageable-path[Allow to manage (CRUD) beneath PATH]: :_files'
    '*--landlock-fs-device-useable-path[Allow to use device files beneath PATH]: :_files'
    '*--landlock-fs-unrestricted-path[Allow everything beneath PATH]: :_files'
    '*--landlock-fs-custom[Allow the listed access rights beneath PATH]: :_cl_landlock_fs_custom: :_files'
    '--landlock-restrict-bind-tcp[Restrict bind() for TCP to the listed ports]: :'
    '--landlock-restrict-connect-tcp[Restrict connect() for TCP to the listed ports]: :'
    '--landlock-scope-abstract-unix-socket[Scope abstract UNIX sockets]'
    '--landlock-scope-signal[Scope signals]'

    '*--add-seccomp-fd[Load seccomp filter from FD]: :'
    '--seccomp-syscall-filter[Load a seccomp filter which filters all syscalls]: :_cl_seccomp_syscall_filter'
    '--seccomp-restrict-ioctl[Load a seccomp filter which allows only the listed ioctls]: :'
    '--seccomp-restrict-prctl[Load a seccomp filter which allows only the listed prctls]: :'
    '--seccomp-restrict-socket[Load a seccomp filter which allows only the listed socket calls]: :'
    '--seccomp-flatpak[Load a seccomp filter similar to the one used by flatpak]'
    '--seccomp-memfd-noexec[Allow memfd_create only for NX usage]'
    '--seccomp-deny-clone-newuser[Load a seccomp filer which disallows CLONE_NEWUSER]'
    '--seccomp-deny-execve-null[Load a seccomp filer which disallows execve with argv/envp==NULL]'
    '--seccomp-deny-memory-write-execute[Load a seccomp filter which enforces W^X for mmap family]'

    '--max-cgroup-namespaces[Per user limit of cgroup namespaces]: :(0)'
    '--max-ipc-namespaces[Per user limit of ipc namespaces]: :(0)'
    '--max-mnt-namespaces[Per user limit of mnt namespaces]: :(0)'
    '--max-net-namespaces[Per user limit of net namespaces]: :(0)'
    '--max-pid-namespaces[Per user limit of pid namespaces]: :(0)'
    '--max-time-namespaces[Per user limit of time namespaces]: :(0)'
    '--max-user-namespaces[Per user limit of user namespaces]: :(0)'
    '--max-uts-namespaces[Per user limit of uts namespaces]: :(0)'

    '--choom[Set oom_score_adj value]: :'
    '--nice[Adjust niceness]: :({1..19})'

    '--clearenv[Remove all env-vars]'
    '*--keepenv[Keep env-var NAME when using --clearenv]: :_parameters'
    '*--unsetenv[Remove env-var NAME]: :_parameters'
    '*--setenv[Set env-var NAME to VALUE]: :'

    '--mdwe-refuse-exec-gain[Set the PR_MDWE_REFUSE_EXEC_GAIN prctl flag]'
    '--new-session[Create a new session]'
    '--strict-mitigations[Enabled stricter mitigations]'
    '--umask[Set the umask to UMASK]: :'

    '--custom-init[Do not use crabreaper and start program as pid-1]'
    '--die-with-parent[SIGKILL everything if our parent dies]'
    '--crablock-data-home[Overwrite crablock'"'"'s data home directory]: :_files -/'
    '--crablock-runtime-dir[Overwrite crablock'"'"'s runtime directory]: :_files -/'

    '--dbus-proxy[Filtered DBus]: : : :'
    '--dbus-proxy-capture-output[Capture output from xdg-dbus-proxy to FILE]: :_files'
)

_crablock() {
    _arguments -S "${_crablock_args[@]}"
}
