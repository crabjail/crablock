/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! Minimal `init` we place as pid-1 in a new pid-namespaces.
//!
//! `crabreaper <PROGRAM> [ARGUMENTS]...`

use std::os::unix::prelude::*;
use std::process::{Command, ExitCode};

use rustix::io::Errno as RustixIoErrno;
use rustix::process::{Pid, WaitOptions, wait};

fn main() -> ExitCode {
    // Mark us as "child subreaper".  While this is the default if
    // we are pid-1 in a new pid-namespaces an explicit call to prctl
    // makes testing (without a new pid-namespaces) easier.
    // We ignore error as this might not be allowed by the seccomp filter.
    let _ = capctl::prctl::set_subreaper(true);

    // If we were executed with fexecv /proc/$pid/comm will be the number of the
    // file-descriptor which looks ugly in process monitoring tools like ps.
    // Let's *try* to change it to "crabreaper".
    if let Ok(name) = capctl::prctl::get_name() {
        if name
            .as_os_str()
            .as_bytes()
            .iter()
            .copied()
            .all(|b| b.is_ascii_digit())
        {
            let _ = capctl::prctl::set_name(env!("CARGO_PKG_NAME"));
        }
    }

    let mut args = std::env::args().skip(1);
    let program = args.next().expect("argv[1] (PROGRAM)");
    #[expect(
        clippy::zombie_processes,
        reason = "false-positive, we wait on it manually with the libc/syscall interface"
    )]
    let child = Command::new(program).args(args).spawn().expect("spawn");
    let pid_of_first_child = Pid::from_child(&child);

    // Main loop.  Reap zombies and collect the exit of child.
    let mut wstatus_of_first_child = None;
    loop {
        match wait(WaitOptions::empty()) {
            Ok(Some((pid, wstatus))) if pid == pid_of_first_child => {
                wstatus_of_first_child = Some(wstatus);
            }
            Ok(Some(_)) => (),
            Ok(None) => unreachable!(),
            Err(err) if err == RustixIoErrno::CHILD => break,
            Err(err) if err == RustixIoErrno::INTR => (),
            Err(err) => {
                eprintln!("crabreaper: wait() returned a error: {err}");
                continue;
            }
        }
    }

    // Try to exit the same as the (first) program started by us.
    let Some(wstatus) = wstatus_of_first_child else {
        unreachable!();
    };
    if let Some(code) = wstatus.exit_status() {
        ExitCode::from(u8::try_from(code).unwrap_or(u8::MAX))
    } else if let Some(signal) = wstatus.terminating_signal() {
        ExitCode::from(u8::try_from(128 + signal).unwrap_or(u8::MAX))
    } else {
        ExitCode::FAILURE
    }
}
