#!/bin/bash

# Copyright © 2021,2023,2025 rusty-snake
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE

# Supported env variables:
#  - GIT_REF: Create distribution of the give git ref. (default: HEAD)
#  - MINISIGN: Sign artifacts using the this minisign key (by path).

set -euo pipefail

# cd into the project directory
cd -P -- "$(readlink -e "$(dirname "$0")")"

me="$(basename "$0")"

say() {
	printf '%s: %s\n' "$me" "$1"
}

#TODO: exec > >(tee "$me.log")

# Do not run if an old outdir exists
[ -d outdir ] && { say "Please delete 'outdir' first."; exit 1; }

# Check presents of non-standard programs (everything except coreutils and built-ins)
REQUIRED_PROGRAMS=(cargo git jq podman xz)
for program in "${REQUIRED_PROGRAMS[@]}"; do
	if ! command -v "$program" >&-; then
		say "Missing requirement: $program is not installed or could not be found."
		say "Please make sure $program is installed and in \$PATH."
		exit 1
	fi
done

## Check working tree
#if [[ "${ALLOW_DIRTY:-false}" != true && -n "$(git status --porcelain)" ]]; then
#	say "Working tree is not clean."
#	say "Please stash all changes: git stash --include-untracked"
#	exit 1
#fi

# Pull alpine image if necessary
if [[ -z "$(podman image list --noheading alpine:latest)" ]]; then
	podman pull docker.io/library/alpine:latest
fi

# Check if we are allowed to run podman
if [[ "$(podman run --rm alpine:latest echo "hello")" != "hello" ]]; then
	say "podman does not seem to work correctly."
	exit 1
fi

#
## mksrc
##

# Normalize GIT_REF
GIT_REF="$(git rev-parse --short "${GIT_REF:-HEAD}")"

TEMP_WORK_TREE=$(mktemp -d "${TMPDIR:-/tmp}/build-${PWD##*/}.XXXXXX")
# shellcheck disable=SC2064
trap "rm -r '$TEMP_WORK_TREE'" EXIT

say "Checkout $GIT_REF in temporary worktree at $TEMP_WORK_TREE"
git --git-dir=.git --work-tree="$TEMP_WORK_TREE" checkout "$GIT_REF" -- "*"

pushd "$TEMP_WORK_TREE" >/dev/null

# Get RELEASE_ID: <name>-v<version>[+<sha1>]
RELEASE_ID=$(
	cargo metadata --no-deps --format-version=1 \
	| jq -j '.packages | map(select(.manifest_path == "'"$PWD/Cargo.toml"'"))[0] | .name, "-v", .version'
)
if [[ "$GIT_REF" != v* ]]; then
	RELEASE_ID="$RELEASE_ID+$GIT_REF"
fi

# Vendor all dependencies
mkdir .cargo
cargo --color=never --locked vendor >.cargo/config.toml

popd >/dev/null
mkdir -v outdir

# Create the source archive
tar --numeric-owner --owner=root:0 --group=root:0 --transform="s;^\.;$RELEASE_ID;" -cJf "outdir/$RELEASE_ID.src.tar.xz" -C "$TEMP_WORK_TREE" .

rm -r "$TEMP_WORK_TREE"
trap - EXIT

#
## mkbin
#

# Build the project
SOURCEDIR="/sourcedir"
BUILDDIR="/builddir"
INSTALLDIR="/installdir"
podman run --rm --security-opt=no-new-privileges --cap-drop=all \
	-v ./outdir:/outdir:z --tmpfs "$SOURCEDIR" --tmpfs "$BUILDDIR" \
	--tmpfs "$INSTALLDIR:mode=0755" -w "$SOURCEDIR" alpine:latest sh -euo pipefail -c "
		apk update
		apk upgrade ||:
		apk add curl gcc libseccomp-static meson musl-dev pkgconf scdoc tar xz ||:
		curl --proto '=https' --tlsv1.3 -sSf 'https://sh.rustup.rs' | sh -s -- -y --profile minimal
		source ~/.cargo/env
		tar --no-same-owner --strip=1 -xf '/outdir/$RELEASE_ID.src.tar.xz'
		export CARGO_FROZEN=true
		meson setup --buildtype=release --prefix=/opt/page.codeberg.crabjail.Crablock -Duse-static-libseccomp=/usr/lib/libseccomp.a '$BUILDDIR' '$SOURCEDIR'
		meson compile -C '$BUILDDIR'
		meson install --no-rebuild --destdir='$INSTALLDIR' -C '$BUILDDIR'
		tar --numeric-owner --transform='s;^\.;$RELEASE_ID;' -cJf '/outdir/$RELEASE_ID-$(uname -m)-unknown-linux-musl.tar.xz' -C '$INSTALLDIR' .
	"

# Compute checksums
(cd outdir; sha256sum -- *.tar.xz) > outdir/SHA256SUMS
(cd outdir; sha512sum -- *.tar.xz) > outdir/SHA512SUMS

if [[ -n "${MINISIGN:-}" ]] && command -v minisign >&-; then
	minisign -S -s "$MINISIGN" -m outdir/*
fi

say "Success!"
