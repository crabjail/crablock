/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use crate::{SysnoX32, bpf, seccomp_bpf};

mod arg_rule;
mod rule;
mod syscall;

pub use arg_rule::ArgumentIndex::*;
pub use arg_rule::{ArgRules, Threshold};
pub use rule::{Action, Rule, SyscallNr};
pub use syscall::Syscall;

pub use crate::Errno;

#[cfg(target_arch = "x86_64")]
pub fn compile(rules: &[Rule], default_action: Action) -> bpf::Program<bpf::SeccompFilter> {
    let mut filter = bpf::Program::from_iter([
        /* If the architecture is != x86-64 or the abi is == x32,
         * we immediately kill the process. */
        seccomp_bpf::load(seccomp_bpf::SeccompDataFieldOffset::Arch),
        seccomp_bpf::jump_ne(seccomp_bpf::AuditArch::X86_64.as_(), 2, 0),
        seccomp_bpf::load(seccomp_bpf::SeccompDataFieldOffset::Nr),
        seccomp_bpf::jump_set(SysnoX32::X32_SYSCALL_BIT, 0, 1),
        seccomp_bpf::ret_kill_process(),
    ]);

    for rule in rules {
        rule.compile(&mut filter);
    }

    default_action.compile(&mut filter);

    filter
}
