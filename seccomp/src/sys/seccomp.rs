use core::arch::asm;
use core::ffi::{c_int, c_long, c_uint, c_void};

pub const SECCOMP_MODE_DISABLED: u32 = 0;
pub const SECCOMP_MODE_STRICT: u32 = 1;
pub const SECCOMP_MODE_FILTER: u32 = 2;

pub const SECCOMP_SET_MODE_STRICT: u32 = 0;
pub const SECCOMP_SET_MODE_FILTER: u32 = 1;
pub const SECCOMP_GET_ACTION_AVAIL: u32 = 2;
pub const SECCOMP_GET_NOTIF_SIZES: u32 = 3;

pub const SECCOMP_FILTER_FLAG_TSYNC: u32 = 1 << 0;
pub const SECCOMP_FILTER_FLAG_LOG: u32 = 1 << 1;
pub const SECCOMP_FILTER_FLAG_SPEC_ALLOW: u32 = 1 << 2;
pub const SECCOMP_FILTER_FLAG_NEW_LISTENER: u32 = 1 << 3;
pub const SECCOMP_FILTER_FLAG_TSYNC_ESRCH: u32 = 1 << 4;
pub const SECCOMP_FILTER_FLAG_WAIT_KILLABLE_RECV: u32 = 1 << 5;

pub const SECCOMP_RET_KILL_PROCESS: u32 = 0x80000000;
pub const SECCOMP_RET_KILL_THREAD: u32 = 0x00000000;
pub const SECCOMP_RET_KILL: u32 = SECCOMP_RET_KILL_THREAD;
pub const SECCOMP_RET_TRAP: u32 = 0x00030000;
pub const SECCOMP_RET_ERRNO: u32 = 0x00050000;
pub const SECCOMP_RET_USER_NOTIF: u32 = 0x7fc00000;
pub const SECCOMP_RET_TRACE: u32 = 0x7ff00000;
pub const SECCOMP_RET_LOG: u32 = 0x7ffc0000;
pub const SECCOMP_RET_ALLOW: u32 = 0x7fff0000;

pub const SECCOMP_RET_ACTION_FULL: u32 = 0xffff0000;
pub const SECCOMP_RET_ACTION: u32 = 0x7fff0000;
pub const SECCOMP_RET_DATA: u32 = 0x0000ffff;

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct seccomp_data {
    pub nr: c_int,
    pub arch: u32,
    pub instruction_pointer: u64,
    pub args: [u64; 6],
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct seccomp_notif_sizes {
    pub seccomp_notif: u16,
    pub seccomp_notif_resp: u16,
    pub seccomp_data: u16,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct seccomp_notif {
    pub id: u64,
    pub pid: u32,
    pub flags: u32,
    pub data: seccomp_data,
}

pub const SECCOMP_USER_NOTIF_FLAG_CONTINUE: u32 = 1;

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct seccomp_notif_resp {
    pub id: u64,
    pub val: i64,
    pub error: i32,
    pub flags: u32,
}

pub const SECCOMP_USER_NOTIF_FD_SYNC_WAKE_UP: u32 = 1;

pub const SECCOMP_ADDFD_FLAG_SETFD: u32 = 1 << 0;
pub const SECCOMP_ADDFD_FLAG_SEND: u32 = 1 << 1;

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct seccomp_notif_addfd {
    pub id: u64,
    pub flags: u32,
    pub srcfd: u32,
    pub newfd: u32,
    pub newfd_flags: u32,
}

// pub const SECCOMP_IOCTL_NOTIF_RECV: ... = ...;
// pub const SECCOMP_IOCTL_NOTIF_SEND: ... = ...;
// pub const SECCOMP_IOCTL_NOTIF_ID_VALID: ... = ...;
// pub const SECCOMP_IOCTL_NOTIF_ADDFD: ... = ...;
// pub const SECCOMP_IOCTL_NOTIF_SET_FLAGS: ... = ...;

#[cfg(target_arch = "x86_64")]
pub const SYS_seccomp: c_long = 317;
#[cfg(target_arch = "aarch64")]
pub const SYS_seccomp: c_long = 198;

pub unsafe fn seccomp(operation: c_uint, flags: c_uint, args: *mut c_void) -> Result<c_int, c_int> {
    match unsafe {
        syscall3(
            SYS_seccomp as usize,
            operation as usize,
            flags as usize,
            args as usize,
        )
    } {
        Ok(i) => Ok(i as c_int),
        Err(i) => Err(i as c_int),
    }
}

const MAX_ERRNO: isize = 4095;

#[cfg(target_arch = "x86_64")]
#[inline]
unsafe fn syscall3(nr: usize, arg1: usize, arg2: usize, arg3: usize) -> Result<usize, usize> {
    let mut ret: usize;

    unsafe {
        asm!(
            "syscall",
            inlateout("rax") nr => ret,
            in("rdi") arg1,
            in("rsi") arg2,
            in("rdx") arg3,
            lateout("rcx") _, // Clobbered
            lateout("r11") _, // Clobbered
            options(nostack, preserves_flags)
        );
    }

    if ret >= -MAX_ERRNO as usize {
        Err(-(ret as isize) as usize)
    } else {
        Ok(ret)
    }
}

#[cfg(target_arch = "aarch64")]
unsafe fn syscall3(nr: usize, arg1: usize, arg2: usize, arg3: usize) -> Result<usize, usize> {
    let _ = (nr, arg1, arg2, arg3);
    unsafe {
        asm!("nop");
    }
    unimplemented!()
}
