use core::ffi::c_ushort;
use core::fmt;

use crate::utils::HexInt;

#[derive(Clone, Copy)]
#[repr(C)]
pub struct sock_filter {
    pub code: u16,
    pub jt: u8,
    pub jf: u8,
    pub k: u32,
}
impl fmt::Debug for sock_filter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("sock_filter")
            .field("code", &HexInt(self.code))
            .field("jt", &self.jt)
            .field("jf", &self.jf)
            .field("k", &HexInt(self.k))
            .finish()
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct sock_fprog {
    pub len: c_ushort,
    pub filter: *mut sock_filter,
}

pub const fn BPF_RVAL(code: u16) -> u16 {
    code & 0x18
}
pub const BPF_A: u16 = 0x10;

pub const fn BPF_MISCOP(code: u16) -> u16 {
    code & 0xf8
}
pub const BPF_TAX: u16 = 0x00;
pub const BPF_TXA: u16 = 0x80;

pub const fn BPF_STMT(code: u16, k: u32) -> sock_filter {
    sock_filter {
        code,
        jt: 0,
        jf: 0,
        k,
    }
}

pub const fn BPF_JUMP(code: u16, k: u32, jt: u8, jf: u8) -> sock_filter {
    sock_filter { code, jt, jf, k }
}
