pub mod x32;
pub mod x86_64;

pub const __X32_SYSCALL_BIT: u32 = 0x40000000;
