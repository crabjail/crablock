/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! Generic bpf abstraction

use alloc::vec::Vec;
use core::ffi::c_ushort;
use core::fmt;
use core::marker::PhantomData;

use crate::sys::*;
use crate::utils::HexInt;

pub trait ProgramType {}
#[derive(Debug)]
pub struct SeccompFilter;
impl ProgramType for SeccompFilter {}

/// A BPF Program.
#[derive(Debug, Default, Clone)]
pub struct Program<T: ProgramType> {
    instructions: Vec<Instruction>,
    program_type: PhantomData<T>,
}
impl<T: ProgramType> Program<T> {
    pub const fn new() -> Self {
        Self {
            instructions: Vec::new(),
            program_type: PhantomData,
        }
    }

    pub fn instructions(&self) -> &[Instruction] {
        &self.instructions
    }

    pub fn instructions_mut(&mut self) -> &mut Vec<Instruction> {
        &mut self.instructions
    }

    #[expect(
        clippy::wrong_self_convention,
        reason = "Actually we do not modify the instructions buffer. However the kernel interface takes *mut rather than *const."
    )]
    pub fn to_sock_fprog(&mut self) -> SockFProg<'_> {
        // The kernel will reject unprivileged programs that are
        // longer than 4096 instructions anyway.
        assert!(self.instructions.len() <= c_ushort::MAX as usize);

        SockFProg(
            sock_fprog {
                len: self.instructions.len() as c_ushort,
                filter: self.instructions.as_mut_ptr().cast::<sock_filter>(),
            },
            PhantomData,
        )
    }

    pub fn as_bytes(&self) -> &[u8] {
        let (&[], bytes, &[]) = (unsafe { self.instructions.align_to::<u8>() }) else {
            unreachable!();
        };
        bytes
    }
}
impl fmt::Display for Program<SeccompFilter> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        disasm_seccomp(f, &self.instructions)?;

        Ok(())
    }
}

#[derive(Clone, Copy)]
#[repr(transparent)]
pub struct Instruction(sock_filter);
impl Instruction {
    pub const fn load_immediate(k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::Load.as_() | AddressingMode::Immediate.as_(),
            k,
        )
    }

    pub const fn load_absolute(size: Size, k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::Load.as_() | size.as_() | AddressingMode::Absolute.as_(),
            k,
        )
    }

    pub const fn load_indirect(size: Size, k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::Load.as_() | size.as_() | AddressingMode::Indirect.as_(),
            k,
        )
    }

    pub const fn load_memory(k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::Load.as_() | AddressingMode::Memory.as_(),
            k,
        )
    }

    pub const fn load_length() -> Self {
        Self::bpf_stmt(
            InstructionClass::Load.as_() | AddressingMode::Length.as_(),
            0,
        )
    }

    pub const fn loadx_immediate(k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::LoadX.as_() | Size::Word.as_() | AddressingMode::Immediate.as_(),
            k,
        )
    }

    pub const fn loadx_memory(k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::LoadX.as_() | Size::Word.as_() | AddressingMode::Memory.as_(),
            k,
        )
    }

    pub const fn loadx_length() -> Self {
        Self::bpf_stmt(
            InstructionClass::LoadX.as_() | Size::Word.as_() | AddressingMode::Length.as_(),
            0,
        )
    }

    pub const fn loadx_most_significant_half(k: u32) -> Self {
        Self::bpf_stmt(
            InstructionClass::LoadX.as_()
                | Size::Byte.as_()
                | AddressingMode::MostSignificantHalf.as_(),
            k,
        )
    }

    /// Store the accumulator in the scratch memory.
    ///
    /// - `M[k] = A`
    pub const fn store(k: u32) -> Self {
        Self::bpf_stmt(InstructionClass::Store.as_(), k)
    }

    /// Store the index register in the scratch memory.
    ///
    /// - `M[k] = X`
    pub const fn storex(k: u32) -> Self {
        Self::bpf_stmt(InstructionClass::StoreX.as_(), k)
    }

    /// Perform an arithmetic or logical operation.
    ///
    /// - `A = A + k`
    /// - `A = A - k`
    /// - `A = A * k`
    /// - `A = A / k`
    /// - `A = A | k`
    /// - `A = A & k`
    /// - `A = A + k`
    /// - `A = A << k`
    /// - `A = A >> k`
    /// - `A = A % k`
    /// - `A = A ^ k`
    /// - `A = A + X`
    /// - `A = A - X`
    /// - `A = A * X`
    /// - `A = A / X`
    /// - `A = A | X`
    /// - `A = A & X`
    /// - `A = A + X`
    /// - `A = A << X`
    /// - `A = A >> X`
    /// - `A = A % X`
    /// - `A = A ^ X`
    /// - `A = -A`
    pub const fn alu(op: AluOp, k: Option<u32>) -> Self {
        Self::bpf_stmt(
            InstructionClass::Alu.as_()
                | op.as_()
                // const op == AluOp::Neg is unstable
                | if matches!(op, AluOp::Neg) {
                    0
                } else if k.is_some() {
                    SourceOperant::K.as_()
                } else {
                    SourceOperant::X.as_()
                },
            // const unwrap_or is unstable, see https://github.com/rust-lang/rust/issues/67441
            if let Some(k) = k { k } else { 0 },
        )
    }

    pub const fn jump(when: Jump, k: Option<u32>, jump_true: u8, jump_false: u8) -> Self {
        Self::bpf_jump(
            InstructionClass::Jump.as_()
                | when.as_()
                // const when == Jump::Always is unstable
                | if matches!(when, Jump::Always) {
                    0
                } else if k.is_some() {
                    SourceOperant::K.as_()
                } else {
                    SourceOperant::X.as_()
                },
            // const unwrap_or is unstable, see https://github.com/rust-lang/rust/issues/67441
            if let Some(k) = k { k } else { 0 },
            jump_true,
            jump_false,
        )
    }

    pub const fn ret(k: Option<u32>) -> Self {
        Self::bpf_stmt(
            InstructionClass::Return.as_()
                | if k.is_some() {
                    ReturnValue::K.as_()
                } else {
                    ReturnValue::A.as_()
                },
            // const unwrap_or is unstable, see https://github.com/rust-lang/rust/issues/67441
            if let Some(k) = k { k } else { 0 },
        )
    }

    pub const fn tax() -> Self {
        Self::bpf_stmt(InstructionClass::Misc.as_() | Misc::Tax.as_(), 0)
    }

    pub const fn txa() -> Self {
        Self::bpf_stmt(InstructionClass::Misc.as_() | Misc::Txa.as_(), 0)
    }

    pub const fn bpf_stmt(code: u16, k: u32) -> Self {
        Self(sock_filter {
            code,
            jt: 0,
            jf: 0,
            k,
        })
    }

    pub const fn bpf_jump(code: u16, k: u32, jt: u8, jf: u8) -> Self {
        Self(sock_filter { code, jt, jf, k })
    }
}
impl<T: ProgramType> FromIterator<Instruction> for Program<T> {
    fn from_iter<I: IntoIterator<Item = Instruction>>(iter: I) -> Self {
        Self {
            instructions: iter.into_iter().collect(),
            program_type: PhantomData,
        }
    }
}
impl fmt::Debug for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SockFProg")
            .field("code", &HexInt(self.0.code))
            .field("jt", &self.0.jt)
            .field("jf", &self.0.jf)
            .field("k", &HexInt(self.0.k))
            .finish()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum InstructionClass {
    Load = BPF_LD,
    LoadX = BPF_LDX,
    Store = BPF_ST,
    StoreX = BPF_STX,
    Alu = BPF_ALU,
    Jump = BPF_JMP,
    Return = BPF_RET,
    Misc = BPF_MISC,
}
impl InstructionClass {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum SourceOperant {
    K = BPF_K,
    X = BPF_X,
}
impl SourceOperant {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum AluOp {
    Add = BPF_ADD,
    Sub = BPF_SUB,
    Mul = BPF_MUL,
    Div = BPF_DIV,
    Or = BPF_OR,
    And = BPF_AND,
    Lsh = BPF_LSH,
    Rsh = BPF_RSH,
    Neg = BPF_NEG,
    Mod = BPF_MOD,
    Xor = BPF_XOR,
}
impl AluOp {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum Jump {
    Always = BPF_JA,
    Equal = BPF_JEQ,
    GreaterThan = BPF_JGT,
    GreaterThanOrEqual = BPF_JGE,
    Set = BPF_JSET,
}
impl Jump {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum Size {
    Word = BPF_W,
    HalfWord = BPF_H,
    Byte = BPF_B,
}
impl Size {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum AddressingMode {
    Immediate = BPF_IMM,
    Absolute = BPF_ABS,
    Indirect = BPF_IND,
    Memory = BPF_MEM,
    Length = BPF_LEN,
    MostSignificantHalf = BPF_MSH,
}
impl AddressingMode {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum ReturnValue {
    A = BPF_A,
    K = BPF_K,
}
impl ReturnValue {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u16)]
pub enum Misc {
    Tax = BPF_TAX,
    Txa = BPF_TXA,
}
impl Misc {
    const fn as_(self) -> u16 {
        self as _
    }
}

#[repr(transparent)]
pub struct SockFProg<'a>(sock_fprog, PhantomData<&'a mut [sock_filter]>);
impl fmt::Debug for SockFProg<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("SockFProg")
            .field("len", &self.0.len)
            .field("filter", &self.0.filter)
            .finish()
    }
}

pub fn disasm_seccomp(f: &mut dyn fmt::Write, instructions: &[Instruction]) -> fmt::Result {
    use crate::errno::Errno;

    const LD: u16 = BPF_LD | BPF_W | BPF_ABS;
    const JA: u16 = BPF_JMP | BPF_JA;
    const JEQ: u16 = BPF_JMP | BPF_JEQ | BPF_K;
    const JGE: u16 = BPF_JMP | BPF_JGE | BPF_K;
    const JGT: u16 = BPF_JMP | BPF_JGT | BPF_K;
    const JSET: u16 = BPF_JMP | BPF_JSET | BPF_K;
    const AND: u16 = BPF_ALU | BPF_AND | BPF_K;
    const OR: u16 = BPF_ALU | BPF_OR | BPF_K;
    const RET: u16 = BPF_RET | BPF_K;

    for (i, instr) in instructions.iter().enumerate() {
        let instr = instr.0;

        write!(
            f,
            "{i:04}: {:#04x} {:#04x} {:#04x} {:#010x} | ",
            instr.code, instr.jt, instr.jf, instr.k
        )?;
        match instr.code {
            LD => writeln!(
                f,
                "ld [{}] ; {}",
                instr.k,
                match instr.k {
                    0 => "nr",
                    4 => "arch",
                    16 => "args[0].lo",
                    20 => "args[0].hi",
                    24 => "args[1].lo",
                    28 => "args[1].hi",
                    32 => "args[2].lo",
                    36 => "args[2].hi",
                    40 => "args[3].lo",
                    44 => "args[3].hi",
                    48 => "args[4].lo",
                    52 => "args[4].hi",
                    56 => "args[5].lo",
                    60 => "args[5].hi",
                    _ => "unknown",
                }
            )?,
            JA => writeln!(f, "ja +{}({:04})", instr.k, instr.k as usize + i + 1)?,
            JEQ | JGT | JGE | JSET => {
                match instr.code {
                    JEQ => write!(f, "jeq ")?,
                    JGT => write!(f, "jgt ")?,
                    JGE => write!(f, "jge ")?,
                    JSET => write!(f, "jset ")?,
                    _ => unreachable!(),
                }
                write!(f, "{}  ", instr.k)?;
                if instr.jt == 0 {
                    write!(f, "true:- ")?;
                } else {
                    write!(f, "true:+{}({:04}) ", instr.jt, instr.jt as usize + i + 1)?;
                }
                if instr.jf == 0 {
                    writeln!(f, "false:- ")?;
                } else {
                    writeln!(f, "false:+{}({:04}) ", instr.jf, instr.jf as usize + i + 1)?;
                }
            }
            AND => writeln!(f, "and {:#x}", instr.k)?,
            OR => writeln!(f, "or {:#x}", instr.k)?,
            RET => {
                write!(f, "ret ")?;

                let k = (
                    instr.k & SECCOMP_RET_ACTION_FULL,
                    instr.k & SECCOMP_RET_DATA,
                );
                match k {
                    (SECCOMP_RET_KILL_PROCESS, 0) => writeln!(f, "SECCOMP_RET_KILL_PROCESS")?,
                    (SECCOMP_RET_KILL_THREAD, 0) => writeln!(f, "SECCOMP_RET_KILL_THREAD")?,
                    (SECCOMP_RET_TRAP, data) => writeln!(f, "SECCOMP_RET_TRAP|{data:#x}")?,
                    (SECCOMP_RET_ERRNO, data) => writeln!(
                        f,
                        "SECCOMP_RET_ERRNO|{}",
                        Errno::from_u16(data as u16).name()
                    )?,
                    (SECCOMP_RET_USER_NOTIF, 0) => writeln!(f, "SECCOMP_RET_USER_NOTIF")?,
                    (SECCOMP_RET_TRACE, data) => writeln!(f, "SECCOMP_RET_TRACE|{data:#x}")?,
                    (SECCOMP_RET_LOG, 0) => writeln!(f, "SECCOMP_RET_LOG")?,
                    (SECCOMP_RET_ALLOW, 0) => writeln!(f, "SECCOMP_RET_ALLOW")?,
                    _ => writeln!(f, "unknown")?,
                };
            }
            _ => unimplemented!(),
        }
    }

    Ok(())
}
