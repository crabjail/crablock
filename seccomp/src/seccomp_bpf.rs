/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use crate::{bpf, sys};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u32)]
pub enum AuditArch {
    X86_64 = sys::AUDIT_ARCH_X86_64,
}
impl AuditArch {
    pub fn as_(self) -> u32 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u32)]
pub enum SeccompDataFieldOffset {
    Nr = 0,
    Arch = 4,
    Arg0Lo = 16,
    Arg0Hi = 20,
    Arg1Lo = 24,
    Arg1Hi = 28,
    Arg2Lo = 32,
    Arg2Hi = 36,
    Arg3Lo = 40,
    Arg3Hi = 44,
    Arg4Lo = 48,
    Arg4Hi = 52,
    Arg5Lo = 56,
    Arg5Hi = 60,
}
impl SeccompDataFieldOffset {
    pub const fn as_(self) -> u32 {
        self as _
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(u32)]
pub enum SeccompRet {
    KillProcess = sys::SECCOMP_RET_KILL_PROCESS,
    KillThread = sys::SECCOMP_RET_KILL_THREAD,
    Trap = sys::SECCOMP_RET_TRAP,
    Errno = sys::SECCOMP_RET_ERRNO,
    UserNotif = sys::SECCOMP_RET_USER_NOTIF,
    Trace = sys::SECCOMP_RET_TRACE,
    Log = sys::SECCOMP_RET_LOG,
    Allow = sys::SECCOMP_RET_ALLOW,
}
impl SeccompRet {
    pub const fn as_(self) -> u32 {
        self as _
    }
}

pub const fn load(offset: SeccompDataFieldOffset) -> bpf::Instruction {
    bpf::Instruction::load_absolute(bpf::Size::Word, offset.as_())
}

pub const fn jump_lt(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::GreaterThanOrEqual, Some(threshold), jf, jt)
}

pub const fn jump_le(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::GreaterThan, Some(threshold), jf, jt)
}

pub const fn jump_eq(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::Equal, Some(threshold), jt, jf)
}

pub const fn jump_ge(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::GreaterThanOrEqual, Some(threshold), jt, jf)
}

pub const fn jump_gt(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::GreaterThan, Some(threshold), jt, jf)
}

pub const fn jump_ne(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::Equal, Some(threshold), jf, jt)
}

pub const fn jump_set(threshold: u32, jt: u8, jf: u8) -> bpf::Instruction {
    bpf::Instruction::jump(bpf::Jump::Set, Some(threshold), jt, jf)
}

pub const fn ret_kill_process() -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::KillProcess.as_()))
}

pub const fn ret_kill_thread() -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::KillThread.as_()))
}

pub const fn ret_trap(data: u16) -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::Trap.as_() | data as u32))
}

pub const fn ret_errno(data: u16) -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::Errno.as_() | data as u32))
}

pub const fn ret_user_notif() -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::UserNotif.as_()))
}

pub const fn ret_trace(data: u16) -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::Trace.as_() | data as u32))
}

pub const fn ret_log() -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::Log.as_()))
}

pub const fn ret_allow() -> bpf::Instruction {
    bpf::Instruction::ret(Some(SeccompRet::Allow.as_()))
}
