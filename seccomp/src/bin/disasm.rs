/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::{env, fs};

use seccomp::bpf::{Instruction, disasm_seccomp};

fn main() {
    let file_path = env::args()
        .nth(1)
        .expect("cargo run --bin disasm -- <FILE>");
    let buf: Vec<u8> = fs::read(&file_path).expect("fs::read");
    assert!(buf.as_ptr() as usize % align_of::<Instruction>() == 0);
    assert!(buf.len() % size_of::<Instruction>() == 0);
    let slice: &[Instruction] = unsafe {
        core::slice::from_raw_parts(
            buf.as_ptr().cast::<Instruction>(),
            buf.len() / size_of::<Instruction>(),
        )
    };
    let mut buf = String::new();
    disasm_seccomp(&mut buf, slice).expect("disasm_seccomp");
    println!("{buf}");
}
