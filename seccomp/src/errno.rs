/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use core::fmt;

macro_rules! impl_errno {
    ( $( ($name:ident, $no:literal, $description:literal), )* ) => {
        impl Errno {
            pub const MAX_ERRNO: i32 = 4095;

            pub const fn name(self) -> &'static str {
                match self {
                    $(Self::$name => stringify!($name),)*
                    // TODO:
                    // - panic!()
                    // - Option<&'static str>
                    // - "{unknown errno}"
                    // - "0x123"
                    _ => todo!(),
                }
            }

            pub const fn description(self) -> &'static str {
                match self {
                    $(Self::$name => $description,)*
                    _ => "Unknown errno",
                }
            }

            /// Returns the underlying integer value.
            #[deprecated]
            #[must_use]
            pub const fn as_raw(self) -> i32 {
                self.no as i32
            }

            /// Converts from a raw os value to `Errno`.
            #[deprecated]
            pub const fn from_raw(no: i32) -> Self {
                Self { no: no as u16 }
            }

            /// Returns the underlying integer value as `i32`.
            #[must_use]
            pub const fn as_i32(self) -> i32 {
                self.no as i32
            }

            /// Converts from a raw os value to `Errno`.
            ///
            /// Behaviour for values outside of the range `0..=MAX_ERRNO` is not defined.
            pub const fn from_i32(no: i32) -> Self {
                debug_assert!(
                    0 <= no && no <= Self::MAX_ERRNO,
                    "`no' outside of range 0..=MAX_ERRNO",
                );

                Self { no: no as u16 }
            }

            /// Returns the underlying integer value as `u16`.
            #[must_use]
            pub const fn as_u16(self) -> u16 {
                self.no
            }

            /// Converts from a raw os value to `Errno`.
            ///
            /// Behaviour for values outside of the range `0..=MAX_ERRNO` is not defined.
            pub const fn from_u16(no: u16) -> Self {
                debug_assert!(
                    no <= Self::MAX_ERRNO as u16,
                    "`no' outside of range 0..=MAX_ERRNO",
                );

                Self { no }
            }
        }
        impl Errno {
            $(pub const $name: Self = Self { no: $no };)*
        }
    };
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Errno {
    no: u16,
}
// LC_ALL=C errno -l | grep -vE "(EWOULDBLOCK|EDEADLOCK|ENOTSUP)" | sed -E 's/([A-Z0-9]+) ([0-9]+) (.+)/    (\1, \2, "\3"),/' | wl-copy
impl_errno! {
    (EPERM, 1, "Operation not permitted"),
    (ENOENT, 2, "No such file or directory"),
    (ESRCH, 3, "No such process"),
    (EINTR, 4, "Interrupted system call"),
    (EIO, 5, "Input/output error"),
    (ENXIO, 6, "No such device or address"),
    (E2BIG, 7, "Argument list too long"),
    (ENOEXEC, 8, "Exec format error"),
    (EBADF, 9, "Bad file descriptor"),
    (ECHILD, 10, "No child processes"),
    (EAGAIN, 11, "Resource temporarily unavailable"),
    (ENOMEM, 12, "Cannot allocate memory"),
    (EACCES, 13, "Permission denied"),
    (EFAULT, 14, "Bad address"),
    (ENOTBLK, 15, "Block device required"),
    (EBUSY, 16, "Device or resource busy"),
    (EEXIST, 17, "File exists"),
    (EXDEV, 18, "Invalid cross-device link"),
    (ENODEV, 19, "No such device"),
    (ENOTDIR, 20, "Not a directory"),
    (EISDIR, 21, "Is a directory"),
    (EINVAL, 22, "Invalid argument"),
    (ENFILE, 23, "Too many open files in system"),
    (EMFILE, 24, "Too many open files"),
    (ENOTTY, 25, "Inappropriate ioctl for device"),
    (ETXTBSY, 26, "Text file busy"),
    (EFBIG, 27, "File too large"),
    (ENOSPC, 28, "No space left on device"),
    (ESPIPE, 29, "Illegal seek"),
    (EROFS, 30, "Read-only file system"),
    (EMLINK, 31, "Too many links"),
    (EPIPE, 32, "Broken pipe"),
    (EDOM, 33, "Numerical argument out of domain"),
    (ERANGE, 34, "Numerical result out of range"),
    (EDEADLK, 35, "Resource deadlock avoided"),
    (ENAMETOOLONG, 36, "File name too long"),
    (ENOLCK, 37, "No locks available"),
    (ENOSYS, 38, "Function not implemented"),
    (ENOTEMPTY, 39, "Directory not empty"),
    (ELOOP, 40, "Too many levels of symbolic links"),
    (ENOMSG, 42, "No message of desired type"),
    (EIDRM, 43, "Identifier removed"),
    (ECHRNG, 44, "Channel number out of range"),
    (EL2NSYNC, 45, "Level 2 not synchronized"),
    (EL3HLT, 46, "Level 3 halted"),
    (EL3RST, 47, "Level 3 reset"),
    (ELNRNG, 48, "Link number out of range"),
    (EUNATCH, 49, "Protocol driver not attached"),
    (ENOCSI, 50, "No CSI structure available"),
    (EL2HLT, 51, "Level 2 halted"),
    (EBADE, 52, "Invalid exchange"),
    (EBADR, 53, "Invalid request descriptor"),
    (EXFULL, 54, "Exchange full"),
    (ENOANO, 55, "No anode"),
    (EBADRQC, 56, "Invalid request code"),
    (EBADSLT, 57, "Invalid slot"),
    (EBFONT, 59, "Bad font file format"),
    (ENOSTR, 60, "Device not a stream"),
    (ENODATA, 61, "No data available"),
    (ETIME, 62, "Timer expired"),
    (ENOSR, 63, "Out of streams resources"),
    (ENONET, 64, "Machine is not on the network"),
    (ENOPKG, 65, "Package not installed"),
    (EREMOTE, 66, "Object is remote"),
    (ENOLINK, 67, "Link has been severed"),
    (EADV, 68, "Advertise error"),
    (ESRMNT, 69, "Srmount error"),
    (ECOMM, 70, "Communication error on send"),
    (EPROTO, 71, "Protocol error"),
    (EMULTIHOP, 72, "Multihop attempted"),
    (EDOTDOT, 73, "RFS specific error"),
    (EBADMSG, 74, "Bad message"),
    (EOVERFLOW, 75, "Value too large for defined data type"),
    (ENOTUNIQ, 76, "Name not unique on network"),
    (EBADFD, 77, "File descriptor in bad state"),
    (EREMCHG, 78, "Remote address changed"),
    (ELIBACC, 79, "Can not access a needed shared library"),
    (ELIBBAD, 80, "Accessing a corrupted shared library"),
    (ELIBSCN, 81, ".lib section in a.out corrupted"),
    (ELIBMAX, 82, "Attempting to link in too many shared libraries"),
    (ELIBEXEC, 83, "Cannot exec a shared library directly"),
    (EILSEQ, 84, "Invalid or incomplete multibyte or wide character"),
    (ERESTART, 85, "Interrupted system call should be restarted"),
    (ESTRPIPE, 86, "Streams pipe error"),
    (EUSERS, 87, "Too many users"),
    (ENOTSOCK, 88, "Socket operation on non-socket"),
    (EDESTADDRREQ, 89, "Destination address required"),
    (EMSGSIZE, 90, "Message too long"),
    (EPROTOTYPE, 91, "Protocol wrong type for socket"),
    (ENOPROTOOPT, 92, "Protocol not available"),
    (EPROTONOSUPPORT, 93, "Protocol not supported"),
    (ESOCKTNOSUPPORT, 94, "Socket type not supported"),
    (EOPNOTSUPP, 95, "Operation not supported"),
    (EPFNOSUPPORT, 96, "Protocol family not supported"),
    (EAFNOSUPPORT, 97, "Address family not supported by protocol"),
    (EADDRINUSE, 98, "Address already in use"),
    (EADDRNOTAVAIL, 99, "Cannot assign requested address"),
    (ENETDOWN, 100, "Network is down"),
    (ENETUNREACH, 101, "Network is unreachable"),
    (ENETRESET, 102, "Network dropped connection on reset"),
    (ECONNABORTED, 103, "Software caused connection abort"),
    (ECONNRESET, 104, "Connection reset by peer"),
    (ENOBUFS, 105, "No buffer space available"),
    (EISCONN, 106, "Transport endpoint is already connected"),
    (ENOTCONN, 107, "Transport endpoint is not connected"),
    (ESHUTDOWN, 108, "Cannot send after transport endpoint shutdown"),
    (ETOOMANYREFS, 109, "Too many references: cannot splice"),
    (ETIMEDOUT, 110, "Connection timed out"),
    (ECONNREFUSED, 111, "Connection refused"),
    (EHOSTDOWN, 112, "Host is down"),
    (EHOSTUNREACH, 113, "No route to host"),
    (EALREADY, 114, "Operation already in progress"),
    (EINPROGRESS, 115, "Operation now in progress"),
    (ESTALE, 116, "Stale file handle"),
    (EUCLEAN, 117, "Structure needs cleaning"),
    (ENOTNAM, 118, "Not a XENIX named type file"),
    (ENAVAIL, 119, "No XENIX semaphores available"),
    (EISNAM, 120, "Is a named type file"),
    (EREMOTEIO, 121, "Remote I/O error"),
    (EDQUOT, 122, "Disk quota exceeded"),
    (ENOMEDIUM, 123, "No medium found"),
    (EMEDIUMTYPE, 124, "Wrong medium type"),
    (ECANCELED, 125, "Operation canceled"),
    (ENOKEY, 126, "Required key not available"),
    (EKEYEXPIRED, 127, "Key has expired"),
    (EKEYREVOKED, 128, "Key has been revoked"),
    (EKEYREJECTED, 129, "Key was rejected by service"),
    (EOWNERDEAD, 130, "Owner died"),
    (ENOTRECOVERABLE, 131, "State not recoverable"),
    (ERFKILL, 132, "Operation not possible due to RF-kill"),
    (EHWPOISON, 133, "Memory page has hardware error"),

}
impl Errno {
    pub const EWOULDBLOCK: Self = Self::EAGAIN;
    pub const EDEADLOCK: Self = Self::EDEADLK;
    pub const ENOTSUP: Self = Self::EOPNOTSUPP;
}
impl From<Errno> for i32 {
    fn from(errno: Errno) -> Self {
        errno.as_i32()
    }
}
impl From<Errno> for u16 {
    fn from(errno: Errno) -> Self {
        errno.as_u16()
    }
}
impl fmt::Display for Errno {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(self.description())?;

        Ok(())
    }
}
#[cfg(feature = "std")]
impl std::error::Error for Errno {}
#[cfg(feature = "std")]
impl From<Errno> for std::io::Error {
    fn from(errno: Errno) -> Self {
        Self::from_raw_os_error(errno.as_i32())
    }
}
