/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! Seccomp - Secure Computing
//!
//! ## Useful documentation
//!
//! - <https://www.man7.org/linux/man-pages/man2/seccomp.2.html>
//! - <https://www.man7.org/training/download/splc_seccomp_slides.pdf>
//! - <https://www.kernel.org/doc/html/latest/bpf/classic_vs_extended.html>
//! - <https://www.kernel.org/doc/html/latest/networking/filter.html>
//! - <https://man.freebsd.org/cgi/man.cgi?query=bpf&sektion=4&manpath=FreeBSD>

#![cfg_attr(not(feature = "std"), no_std)]

extern crate alloc;

use core::ffi::{c_uint, c_void};

use bitflags::bitflags;

use self::bpf::{Program, SeccompFilter, SockFProg};
use self::sys::{SECCOMP_SET_MODE_FILTER, seccomp};

mod errno;
mod macros;
mod sys;
mod sysno;
mod utils;

pub mod bpf;
pub mod dsl;
pub mod seccomp_bpf;

pub use errno::Errno;
#[deprecated]
pub use seccomp_bpf::{AuditArch, SeccompRet};
pub use sysno::{SysnoX32, SysnoX86_64};

bitflags! {
    pub struct SeccompFilterFlags: u32 {
        // const TSYNC = SECCOMP_FILTER_FLAG_TSYNC;
        const LOG = sys::SECCOMP_FILTER_FLAG_LOG;
        const SPEC_ALLOW = sys::SECCOMP_FILTER_FLAG_SPEC_ALLOW;
        // const NEW_LISTENER = SECCOMP_FILTER_FLAG_NEW_LISTENER;
        // const TSYNC_ESRCH = SECCOMP_FILTER_FLAG_TSYNC_ESRCH;
        // const WAIT_KILLABLE_RECV = SECCOMP_FILTER_FLAG_WAIT_KILLABLE_RECV;
    }
}

impl Program<SeccompFilter> {
    pub fn load_seccomp_filter(&mut self, flags: SeccompFilterFlags) -> Result<(), Errno> {
        let flags: c_uint = SeccompFilterFlags::from_bits(flags.bits())
            .expect("Unexpected bits set")
            .bits();
        let sock_fprog: SockFProg<'_> = self.to_sock_fprog();

        let res = unsafe {
            seccomp(
                SECCOMP_SET_MODE_FILTER,
                flags,
                &raw const sock_fprog as *mut c_void,
            )
        };
        match res {
            Ok(_) => Ok(()),
            Err(errno) => Err(Errno::from_i32(errno)),
        }
    }
}
