/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

// Damn bug https://github.com/rust-lang/rust/issues/98291, everything is ugly now and
// there is no stable workaround/alternative.
//#[rustfmt::skip]
#[macro_export]
macro_rules! load {
    ("seccomp_data.nr") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 0)
    };
    ("seccomp_data.arch") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 4)
    };
    ("seccomp_data.args[0].lo") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 16)
    };
    ("seccomp_data.args[0].hi") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 20)
    };
    ("seccomp_data.args[1].lo") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 24)
    };
    ("seccomp_data.args[1].hi") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 28)
    };
    ("seccomp_data.args[2].lo") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 32)
    };
    ("seccomp_data.args[2].hi") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 36)
    };
    ("seccomp_data.args[3].lo") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 40)
    };
    ("seccomp_data.args[3].hi") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 44)
    };
    ("seccomp_data.args[4].lo") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 48)
    };
    ("seccomp_data.args[4].hi") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 52)
    };
    ("seccomp_data.args[5].lo") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 56)
    };
    ("seccomp_data.args[5].hi") => {
        $crate::bpf::Instruction::load_absolute($crate::bpf::Size::Word, 60)
    };
}

#[rustfmt::skip]
#[macro_export]
macro_rules! and {
    ($k:expr) => { $crate::bpf::Instruction::alu($crate::bpf::AluOp::And, Some($k)) };
}

#[rustfmt::skip]
#[macro_export]
macro_rules! or {
    ($k:expr) => { $crate::bpf::Instruction::alu($crate::bpf::AluOp::Or, Some($k)) };
}

#[macro_export]
macro_rules! jump {
    (+$k:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::Always, Some($k), 0, 0)
    };
}

#[macro_export]
macro_rules! jump_lt {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::GreaterThanOrEqual, Some($k), 0, $off)
    };
}

#[macro_export]
macro_rules! jump_le {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::GreaterThan, Some($k), 0, $off)
    };
}

#[macro_export]
macro_rules! jump_eq {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::Equal, Some($k), $off, 0)
    };
}

#[macro_export]
macro_rules! jump_ge {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::GreaterThanOrEqual, Some($k), $off, 0)
    };
}

#[macro_export]
macro_rules! jump_gt {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::GreaterThan, Some($k), $off, 0)
    };
}

#[macro_export]
macro_rules! jump_ne {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::Equal, Some($k), 0, $off)
    };
}

#[macro_export]
macro_rules! jump_set {
    ($k:expr, +$off:expr) => {
        $crate::bpf::Instruction::jump($crate::bpf::Jump::Set, Some($k), 0, $off)
    };
}

#[macro_export]
macro_rules! ret {
    (KILL_PROCESS) => {
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::KillProcess.as_()))
    };
    (KILL_THREAD) => {
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::KillThread.as_()))
    };
    (TRAP | $data:expr) => {{
        debug_assert!(u16::try_from($data).is_ok());
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::Trap.as_() | $data))
    }};
    (ERRNO | $data:expr) => {{
        debug_assert!(u16::try_from($data).is_ok());
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::Errno.as_() | $data))
    }};
    (USER_NOTIF) => {
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::UserNotif.as_()))
    };
    (TRACE | $data:expr) => {{
        debug_assert!(u16::try_from($data).is_ok());
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::Trace.as_() | $data))
    }};
    (LOG) => {
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::Log.as_()))
    };
    (ALLOW) => {
        $crate::bpf::Instruction::ret(Some($crate::SeccompRet::Allow.as_()))
    };
}

#[macro_export]
macro_rules! ret_errno {
    ($errno:ident) => {
        ret!(ERRNO | $crate::Errno::$errno.as_u16() as u32)
    };
}
