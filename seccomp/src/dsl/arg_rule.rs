/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use alloc::vec::Vec;

use crate::{bpf, seccomp_bpf};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ArgRules {
    arg_rules: Vec<ArgRule>,
}
impl ArgRules {
    #[expect(clippy::new_without_default, reason = "DSL")]
    pub fn new() -> Self {
        Self {
            arg_rules: Vec::new(),
        }
    }

    pub fn none() -> Self {
        Self {
            arg_rules: Vec::new(),
        }
    }

    pub fn or(mut self, arg_rule: impl Into<ArgRule>) -> Self {
        self.arg_rules.push(arg_rule.into());
        self
    }

    pub(super) fn compile(
        &self,
        filter: &mut bpf::Program<bpf::SeccompFilter>,
        mut jt: u8, // offset to Action
        mut jf: u8, // offset to next Rule
    ) {
        for arg_rule in &self.arg_rules {
            arg_rule.compile(filter, jt, jf);
            jt -= arg_rule.weight() as u8;
            jf -= arg_rule.weight() as u8;
        }
    }

    pub(super) fn weight(&self) -> usize {
        self.arg_rules
            .iter()
            .map(|arg_rule| arg_rule.weight())
            .sum()
    }
}
impl FromIterator<ArgRule> for ArgRules {
    fn from_iter<I: IntoIterator<Item = ArgRule>>(iter: I) -> Self {
        Self {
            arg_rules: Vec::from_iter(iter),
        }
    }
}
impl FromIterator<ArgCondition> for ArgRules {
    fn from_iter<I: IntoIterator<Item = ArgCondition>>(iter: I) -> Self {
        Self {
            arg_rules: Vec::from_iter(iter.into_iter().map(ArgRule::from)),
        }
    }
}
impl From<Vec<ArgRule>> for ArgRules {
    fn from(arg_rules: Vec<ArgRule>) -> Self {
        Self { arg_rules }
    }
}
impl From<Vec<ArgCondition>> for ArgRules {
    fn from(arg_rules: Vec<ArgCondition>) -> Self {
        Self {
            arg_rules: arg_rules.into_iter().map(ArgRule::from).collect(),
        }
    }
}
impl<const N: usize> From<[ArgRule; N]> for ArgRules {
    fn from(arg_rules: [ArgRule; N]) -> Self {
        Self {
            arg_rules: Vec::from(arg_rules),
        }
    }
}
impl From<[ArgCondition; 1]> for ArgRules {
    fn from(arg_rules: [ArgCondition; 1]) -> Self {
        let [condition] = arg_rules;
        Self {
            arg_rules: alloc::vec![ArgRule::from(condition)],
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ArgRule {
    conditions: Vec<ArgCondition>,
}
impl ArgRule {
    pub fn and(mut self, condition: ArgCondition) -> Self {
        self.conditions.push(condition);
        self
    }

    fn compile(
        &self,
        filter: &mut bpf::Program<bpf::SeccompFilter>,
        mut jt: u8, // offset to Action
        mut jf: u8, // offset to next ArgRule/Rule
    ) {
        let mut conditions = self.conditions.iter();
        for condition in conditions.by_ref().take(self.conditions.len() - 1) {
            condition.compile(filter, condition.weight() as u8, jf);
            jt -= condition.weight() as u8;
            jf -= condition.weight() as u8;
        }
        conditions.next().unwrap().compile(filter, jt, jf);
    }

    fn weight(&self) -> usize {
        self.conditions
            .iter()
            .map(|condition| condition.weight())
            .sum()
    }
}
impl From<ArgCondition> for ArgRule {
    fn from(condition: ArgCondition) -> Self {
        Self {
            conditions: alloc::vec![condition],
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ArgCondition {
    argument: ArgumentIndex,
    operator: ComparisonOperator,
    threshold: Threshold,
}
impl ArgCondition {
    pub fn and(self, condition: Self) -> ArgRule {
        ArgRule {
            conditions: alloc::vec![self, condition],
        }
    }

    fn compile(&self, filter: &mut bpf::Program<bpf::SeccompFilter>, jt: u8, jf: u8) {
        if self.argument.should_compare_low() {
            filter.instructions_mut().push(seccomp_bpf::load(
                self.argument.as_seccomp_data_field_offset_low(),
            ));
            let (jt, jf) = if self.argument.should_compare_high() {
                (self.operator.weight() as u8, jf - 1)
            } else {
                (jt - 1, jf - 1)
            };
            self.operator.compile(filter, self.threshold.low, jt, jf);
        }
        if self.argument.should_compare_high() {
            filter.instructions_mut().push(seccomp_bpf::load(
                self.argument.as_seccomp_data_field_offset_high(),
            ));
            let (jt, jf) = if self.argument.should_compare_low() {
                (
                    jt - 2 - self.operator.weight() as u8,
                    jf - 2 - self.operator.weight() as u8,
                )
            } else {
                (jt - 1, jf - 1)
            };
            self.operator.compile(filter, self.threshold.high, jt, jf);
        }
    }

    fn weight(&self) -> usize {
        match (
            self.argument.should_compare_high(),
            self.argument.should_compare_low(),
        ) {
            (true, true) => 2 + 2 * self.operator.weight(),
            (true, false) | (false, true) => 1 + self.operator.weight(),
            _ => unreachable!(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ArgumentIndex {
    Arg0,
    Arg1,
    Arg2,
    Arg3,
    Arg4,
    Arg5,
    Arg0Hi,
    Arg1Hi,
    Arg2Hi,
    Arg3Hi,
    Arg4Hi,
    Arg5Hi,
    Arg0Lo,
    Arg1Lo,
    Arg2Lo,
    Arg3Lo,
    Arg4Lo,
    Arg5Lo,
}
impl ArgumentIndex {
    fn as_seccomp_data_field_offset_high(self) -> seccomp_bpf::SeccompDataFieldOffset {
        match self {
            Self::Arg0 | Self::Arg0Hi => seccomp_bpf::SeccompDataFieldOffset::Arg0Hi,
            Self::Arg1 | Self::Arg1Hi => seccomp_bpf::SeccompDataFieldOffset::Arg1Hi,
            Self::Arg2 | Self::Arg2Hi => seccomp_bpf::SeccompDataFieldOffset::Arg2Hi,
            Self::Arg3 | Self::Arg3Hi => seccomp_bpf::SeccompDataFieldOffset::Arg3Hi,
            Self::Arg4 | Self::Arg4Hi => seccomp_bpf::SeccompDataFieldOffset::Arg4Hi,
            Self::Arg5 | Self::Arg5Hi => seccomp_bpf::SeccompDataFieldOffset::Arg5Hi,
            _ => panic!(),
        }
    }

    fn as_seccomp_data_field_offset_low(self) -> seccomp_bpf::SeccompDataFieldOffset {
        match self {
            Self::Arg0 | Self::Arg0Lo => seccomp_bpf::SeccompDataFieldOffset::Arg0Lo,
            Self::Arg1 | Self::Arg1Lo => seccomp_bpf::SeccompDataFieldOffset::Arg1Lo,
            Self::Arg2 | Self::Arg2Lo => seccomp_bpf::SeccompDataFieldOffset::Arg2Lo,
            Self::Arg3 | Self::Arg3Lo => seccomp_bpf::SeccompDataFieldOffset::Arg3Lo,
            Self::Arg4 | Self::Arg4Lo => seccomp_bpf::SeccompDataFieldOffset::Arg4Lo,
            Self::Arg5 | Self::Arg5Lo => seccomp_bpf::SeccompDataFieldOffset::Arg5Lo,
            _ => panic!(),
        }
    }

    fn should_compare_high(self) -> bool {
        matches!(
            self,
            Self::Arg0
                | Self::Arg1
                | Self::Arg2
                | Self::Arg3
                | Self::Arg4
                | Self::Arg5
                | Self::Arg0Hi
                | Self::Arg1Hi
                | Self::Arg2Hi
                | Self::Arg3Hi
                | Self::Arg4Hi
                | Self::Arg5Hi
        )
    }

    fn should_compare_low(self) -> bool {
        matches!(
            self,
            Self::Arg0
                | Self::Arg1
                | Self::Arg2
                | Self::Arg3
                | Self::Arg4
                | Self::Arg5
                | Self::Arg0Lo
                | Self::Arg1Lo
                | Self::Arg2Lo
                | Self::Arg3Lo
                | Self::Arg4Lo
                | Self::Arg5Lo
        )
    }

    pub fn lt<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::LessThan,
            threshold: threshold.into(),
        }
    }

    pub fn le<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::LessThanOrEqual,
            threshold: threshold.into(),
        }
    }

    pub fn eq<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::Equal,
            threshold: threshold.into(),
        }
    }

    pub fn ge<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::GreaterThanOrEqual,
            threshold: threshold.into(),
        }
    }

    pub fn gt<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::GreaterThan,
            threshold: threshold.into(),
        }
    }

    pub fn ne<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::NotEqual,
            threshold: threshold.into(),
        }
    }

    pub fn set<T: Into<Threshold>>(self, threshold: T) -> ArgCondition {
        ArgCondition {
            argument: self,
            operator: ComparisonOperator::Set,
            threshold: threshold.into(),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum ComparisonOperator {
    LessThan,
    LessThanOrEqual,
    Equal,
    GreaterThanOrEqual,
    GreaterThan,
    NotEqual,
    Set,
}
impl ComparisonOperator {
    fn compile(
        &self,
        filter: &mut bpf::Program<bpf::SeccompFilter>,
        threshold: u32,
        jt: u8,
        jf: u8,
    ) {
        match self {
            Self::LessThan => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_lt(threshold, jt - 1, jf - 1));
            }
            Self::LessThanOrEqual => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_le(threshold, jt - 1, jf - 1));
            }
            Self::Equal => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_eq(threshold, jt - 1, jf - 1));
            }
            Self::GreaterThanOrEqual => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_ge(threshold, jt - 1, jf - 1));
            }
            Self::GreaterThan => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_gt(threshold, jt - 1, jf - 1));
            }
            Self::NotEqual => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_ne(threshold, jt - 1, jf - 1));
            }
            Self::Set => {
                filter
                    .instructions_mut()
                    .push(seccomp_bpf::jump_set(threshold, jt - 1, jf - 1));
            }
        }
    }

    fn weight(&self) -> usize {
        1
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Threshold {
    high: u32,
    low: u32,
}
// FIXME: The introduction of Arg?Hi made the usage of implicit 32-bit thresolds very dangerous.
impl From<i32> for Threshold {
    fn from(val: i32) -> Self {
        Self {
            high: 0,
            low: val as u32,
        }
    }
}
impl From<u32> for Threshold {
    fn from(val: u32) -> Self {
        Self { high: 0, low: val }
    }
}
impl From<i64> for Threshold {
    fn from(val: i64) -> Self {
        Self {
            high: (val as u64 >> 32) as u32,
            low: val as u64 as u32,
        }
    }
}
impl From<u64> for Threshold {
    fn from(val: u64) -> Self {
        Self {
            high: (val >> 32) as u32,
            low: val as u32,
        }
    }
}
