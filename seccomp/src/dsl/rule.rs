/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use alloc::vec::Vec;

use super::{ArgRules, Syscall};
use crate::{Errno, bpf, seccomp_bpf};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Rule {
    syscall_nr: SyscallNr,
    arg_rules: ArgRules,
    action: Action,
}
impl Rule {
    pub fn new(syscall_nr: SyscallNr, arg_rules: impl Into<ArgRules>, action: Action) -> Self {
        Self {
            syscall_nr,
            arg_rules: arg_rules.into(),
            action,
        }
    }

    pub(super) fn compile(&self, filter: &mut bpf::Program<bpf::SeccompFilter>) {
        assert!(self.weight() < u8::MAX as usize, "Rule exceeds MAX_JUMP");

        self.syscall_nr
            .compile(filter, self.syscall_nr.weight() as u8, self.weight() as u8);
        self.arg_rules.compile(
            filter,
            self.arg_rules.weight() as u8,
            (self.arg_rules.weight() + self.action.weight()) as u8,
        );
        self.action.compile(filter);
    }

    pub(super) fn weight(&self) -> usize {
        self.syscall_nr.weight() + self.arg_rules.weight() + self.action.weight()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub enum SyscallNr {
    Equal(Syscall),
    NotEqual(Syscall),
    OneOf(Vec<Syscall>),
}
impl SyscallNr {
    pub(super) fn compile(
        &self,
        filter: &mut bpf::Program<bpf::SeccompFilter>,
        jt: u8, // offset to first ArgRule
        jf: u8, // offset to next Rule
    ) {
        match self {
            Self::Equal(syscall) => {
                filter.instructions_mut().extend([
                    seccomp_bpf::load(seccomp_bpf::SeccompDataFieldOffset::Nr),
                    seccomp_bpf::jump_eq(syscall.nr, jt - 2, jf - 2),
                ]);
            }
            Self::NotEqual(syscall) => {
                filter.instructions_mut().extend([
                    seccomp_bpf::load(seccomp_bpf::SeccompDataFieldOffset::Nr),
                    seccomp_bpf::jump_ne(syscall.nr, jt - 2, jf - 2),
                ]);
            }
            Self::OneOf(_) => unimplemented!(),
        }
    }

    pub(super) fn weight(&self) -> usize {
        match self {
            Self::Equal(_) | Self::NotEqual(_) => 2,
            Self::OneOf(v) => 1 + v.len(),
        }
    }
}

#[expect(clippy::enum_variant_names, reason = "Temporary!")] // FIXME
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Action {
    RetKillProcess,
    RetKillThread,
    // RetTrap(),
    RetErrno(Errno),
    RetUserNotif,
    // RetTrace(),
    RetLog,
    RetAllow,
    // Continue,
}
impl Action {
    pub(super) fn compile(&self, filter: &mut bpf::Program<bpf::SeccompFilter>) {
        filter.instructions_mut().push(match self {
            Self::RetKillProcess => seccomp_bpf::ret_kill_process(),
            Self::RetKillThread => seccomp_bpf::ret_kill_thread(),
            // Self::RetTrap() => seccomp_bpf::ret_trap(),
            Self::RetErrno(errno) => seccomp_bpf::ret_errno(errno.as_u16()),
            Self::RetUserNotif => seccomp_bpf::ret_user_notif(),
            // Self::RetTrace() => seccomp_bpf::ret_trace(),
            Self::RetLog => seccomp_bpf::ret_log(),
            Self::RetAllow => seccomp_bpf::ret_allow(),
        });
    }

    pub(super) fn weight(&self) -> usize {
        1
    }
}
