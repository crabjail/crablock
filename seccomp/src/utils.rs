/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use core::fmt;

/// Helpertype to format integers using a hex formatter.
///
/// This is necessary because `feature(debug_closure_helpers)` (i.e. `DebugStruct::filed_with`)
/// is unstable.
///
/// HexInt has two rather big where clauses for T because `feature(impl_trait_type_aliases)``
/// is unstable. Otherwise we could use
///
/// ```rust,ignore
/// type HexIntT = impl fmt::Debug + fmt::LowerHex + fmt::UpperHex;
/// ```
#[derive(Clone, Copy)]
pub struct HexInt<T>(pub T)
where
    T: fmt::Debug + fmt::LowerHex + fmt::UpperHex;
impl<T> fmt::Debug for HexInt<T>
where
    T: fmt::Debug + fmt::LowerHex + fmt::UpperHex,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:#x}", self.0)
    }
}
