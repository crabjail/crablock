# FAQ

## I get SElinux denials with `--pasta`.

Yes, our usage of `pasta` is not (yet) covered by passt-selinux. For now,
util crablock has SELinux integration and/or we have a crablock-pasta policy,
the best way is to make it permissive.

```bash
sudo semanage permissive -a pasta_t
```

## How to inspect failed syscalls with strace?

```bash
crablock --args-lf=Firefox.cla -- strace --seccomp-bpf --follow-forks --trace=!futex,read,pread64,recvmsg,write,pwrite64,sendmsg,lseek,close,%memory,%signal,sched_yield,poll,ppoll,epoll_wait,epoll_pwait,epoll_pwait2 --failed-only --decode-fds=all --decode-pids=comm --quiet=all --signal=none -o ~/firefox.strace /usr/bin/firefox --no-remote
```

#### Explanation of arguments:

- `--seccomp-bpf` `--trace=!futex,read,pread64,recvmsg,write,pwrite64,sendmsg,lseek,close,%memory,%signal,sched_yield,poll,ppoll,epoll_wait,epoll_pwait,epoll_pwait2`
  - Enable seccomp-bpf based tracing and disable tracing of hot syscalls that are
    uninteresting to us. Leading to a significant fast execution of the program.
- `--follow-forks`
  - Trace all processes.
- `--failed-only`
  - Output only failed syscalls.
- `--decode-fds=all`
  - Output various information associated with file descriptors.
- `--decode-pids=comm`
  - Output various information associated with process IDs.
- `--quiet=all`
  - Do not output various information messages.
- `--signal=none`
  - Do not trace signals.
- `-o ~/firefox.strace`
  - Save output at `~/firefox.strace` inside the sandbox.

#### Analysis of the output

We plan to provide a tool to analyse the output. However for now it is
a shell command with a log greps.

- Ignore errnos that are expectable, unlikely to be caused by us, not actable for us or irrelevant for other reasons.
- Ignore paths that are known to not exist (for various reasons).
- Ignore paths that are fully allowed.
- Ignore wanted errors (`EROFS` and `W_OK -> EACCES` on system paths).
- Ignore non UNIX-sockets.
- Ignore some syscalls.
- Ignore some paths.

```bash
cat ~/.local/share/page.codeberg.crabjail.Crablock/firefox/private/home/aurora/firefox.strace |
        grep -vE '= -1 (ENOTTY|EBADF|ENOTSOCK|ECHILD|EINVAL|EFAULT|EAGAIN|EINPROGRESS|ENOTCONN|ENETUNREACH|ETIMEDOUT|EEXIST)' |
        # grep -vE "= -1 (ENOENT|EACCES|EROFS)" |
        grep -vE '("/selinux|"/.flatpak-info|"/nix).*= -1 ENOENT' |
        grep -vE '("/run/opengl-driver|"/var/cache/fontconfig).*= -1 ENOENT' |
        grep -vE '("/tmp/Temp).*= -1 ENOENT' |
        grep -vE '("/usr|"/lib64|"/etc).*= -1 ENOENT' |
        grep -vE '("/usr|"/lib64|"/etc).*= -1 EROFS' |
        grep -vE '("/usr|"/lib64|"/etc).*, W_OK) = -1 EACCES' |
        grep -vE 'TCP|UDP' |
        grep -vE 'quotactl|epoll_ctl|setpriority' |
        grep -vE '"/home' |
        grep -vE '/dev|"/run' |
        bat -lstrace
```

## How to inspect successfully accessed paths with strace?

```bash
crablock --args-lf=Firefox.cla -- strace --seccomp-bpf --follow-forks --trace=%file --successful-only --decode-fds=all --decode-pids=comm --quiet=all --signal=none -o ~/firefox.strace /usr/bin/firefox --no-remote
```

#### Explanation of arguments:

- `--seccomp-bpf` `--trace=%file`
  - Enable seccomp-bpf based tracing and trace only filesystem related syscalls.
    Leading to a significant fast execution of the program.
- `--follow-forks`
  - Trace all processes.
- `--successful-only`
  - Output only successful syscalls.
- `--decode-fds=all`
  - Output various information associated with file descriptors.
- `--decode-pids=comm`
  - Output various information associated with process IDs.
- `--quiet=all`
  - Do not output various information messages.
- `--signal=none`
  - Do not trace signals.
- `-o ~/firefox.strace`
  - Save output at `~/firefox.strace` inside the sandbox.

#### Analysis of the output

We plan to provide a tool to analyse the output. However for now it is
a shell command with a log greps.

```bash
cat ~/.local/share/page.codeberg.crabjail.Crablock/firefox/private/home/aurora/firefox.strace |
        grep -oE '"[^"]+"' |
        cut -d'"' -f2 |
        sort -u |
        grep -E '^/' |
        grep -vE '^/home/rusty-snake/.mozilla/|^/home/rusty-snake/.cache/mozilla/|/mesa_shader_cache/|/fonts/|/fontconfig/|^/lib64/|^/proc/self/|^/proc/[0-9]+/|^/usr/|^/sys' |
        bat -lstrace
```
