githooks
========

Hooks
-----

- pre-commit:
  - Checks for typos
  - Checks formatting
  - Runs linter
  - Checks x86_64 and aarch64
  - Runs tests

Usage
-----

    cp <HOOK> ../.git/hooks
