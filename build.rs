/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::{env, fs};

/// Communicate to cargo
///
/// <https://doc.rust-lang.org/cargo/reference/build-scripts.html#outputs-of-the-build-script>
macro_rules! tell_cargo {
    (rerun-if-changed: $path:expr $(,)? ) => {
        println!("cargo:rerun-if-changed={}", $path)
    };
    (rerun-if-env-changed: $var:expr $(,)? ) => {
        println!("cargo:rerun-if-env-changed={}", $var)
    };
    (rustc-link-lib: $name:expr $(,)? ) => {
        println!("cargo:rustc-link-lib={}", $name)
    };
    (rustc-link-lib: $kind:expr, $name:expr $(,)? ) => {
        println!("cargo:rustc-link-lib={}={}", $kind, $name)
    };
    (rustc-env: $var:expr, $val:expr $(,)? ) => {
        println!("cargo:rustc-env={}={}", $var, $val)
    };
    (warning: $message:expr $(,)? ) => {
        println!("cargo:warning={}", $message)
    };
}

fn main() {
    tell_cargo!(rerun-if-changed: "build.rs");
    tell_cargo!(rerun-if-env-changed: "CRABREAPER");

    if env::var_os("CRABREAPER").is_none() {
        let crabreaper = fs::canonicalize(".")
            .expect("canonicalize(\".\")")
            .join("./target/debug/crabreaper");
        tell_cargo!(rustc-env: "CRABREAPER", crabreaper.to_str().unwrap());
    }

    if env::var_os("SET_MEMFD_NOEXEC").is_none() {
        tell_cargo!(rustc-env: "SET_MEMFD_NOEXEC", "/usr/local/libexec/set_memfd_noexec");
    }
}
