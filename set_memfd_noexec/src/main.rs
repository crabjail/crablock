/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::{env, fs};

use lnix::ioctl::ioctl_ns::{ioctl_ns_get_owner_uid, ioctl_ns_get_userns};
use lnix::process::{NsType, Uid, fork, seteuid, setns};
use rustix::fs::{CWD, Mode, OFlags, openat};
use rustix_0_38::process::{WaitOptions, waitpid};

fn main() {
    /* Drop effective privileges. */
    seteuid(Uid::real()).expect("seteuid");

    /* Respond to --help with a identifying message. */
    if env::args().any(|arg| arg == "--help") {
        println!("Privileged helper of crablock to set vm.memfd_noexec.");
        return;
    }

    /* target_pid is expected at argv[1]. */
    let Some(target_pid) = env::args().nth(1) else {
        panic!("Missing <target_pid>.");
    };

    /* Allow only numeric values for target_pid (it is a &str). */
    assert!(
        target_pid.bytes().all(|b| b.is_ascii_digit()),
        "Bad target_pid: only ascii digits are allowed."
    );

    /* Open pidns fd. */
    let pidns = openat(
        CWD,
        format!("/proc/{target_pid}/ns/pid"),
        OFlags::CLOEXEC | OFlags::RDONLY,
        Mode::empty(),
    )
    .unwrap_or_else(|e| panic!("Failed to open /proc/{target_pid}/ns/pid: {e}"));

    /* Get owner uid of owning userns of pidns. */
    let owning_userns =
        unsafe { ioctl_ns_get_userns(&pidns) }.expect("Failed to NS_GET_USERNS for pidns");
    let owner_uid = unsafe { ioctl_ns_get_owner_uid(&owning_userns) }
        .expect("Failed to NS_GET_OWNER_UID for owning_userns");

    /* Allow only pidns to be joined whose owning userns was created by the uid
     * that started us. */
    assert!(
        owner_uid == Uid::real(),
        "target_pid->pidns->userns->uid is not {}.",
        Uid::real().as_raw()
    );

    /* Regain effective privileges. */
    seteuid(Uid::from_raw(0)).expect("seteuid");

    /* Join target pidns, pid namespaces are special because you have to
     * fork to actually join them. */
    setns(pidns, NsType::PID).unwrap_or_else(|e| panic!("Failed to setns: {e}"));
    match unsafe { fork() }.unwrap_or_else(|e| panic!("Failed to fork: {e}")) {
        None => {
            /* We are the child. */
            /* Set vm.memfd_noexec = 1. */
            fs::write("/proc/sys/vm/memfd_noexec", "1")
                .unwrap_or_else(|e| panic!("Failed to write to /proc/sys/vm/memfd_noexec: {e}"));
        }
        Some(pid) => {
            /* We are the parent. */
            let wait_status = waitpid(Some(pid.into()), WaitOptions::empty())
                .unwrap_or_else(|e| panic!("Failed to waitpid: {e}"))
                .unwrap();
            assert!(
                wait_status.exit_status() == Some(0),
                "Child exited unhealthy"
            );
        }
    }
}
