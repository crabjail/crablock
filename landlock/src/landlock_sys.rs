#![allow(nonstandard_style, reason = "Mirroring style of C-header file")]
#![allow(
    clippy::upper_case_acronyms,
    reason = "Mirroring style of C-header file"
)]

//! Raw definitions of Landlock

use core::ffi::{c_int, c_long, c_void};

use libc::size_t as c_size_t;

#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
#[repr(C)]
pub struct landlock_ruleset_attr {
    pub handled_access_fs: u64,
    pub handled_access_net: u64,
    pub scoped: u64,
}

pub const LANDLOCK_CREATE_RULESET_VERSION: u32 = 1 << 0;

#[derive(Debug, Clone, Copy)]
#[non_exhaustive]
#[repr(C)]
pub enum landlock_rule_type {
    LANDLOCK_RULE_PATH_BENEATH = 1,
    LANDLOCK_RULE_NET_PORT,
}
pub use landlock_rule_type::*;

#[derive(Debug, Clone, Copy)]
#[repr(C, packed)]
pub struct landlock_path_beneath_attr {
    pub allowed_access: u64,
    pub parent_fd: i32,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct landlock_net_port_attr {
    pub allowed_access: u64,
    pub port: u64,
}

pub const LANDLOCK_ACCESS_FS_EXECUTE: u64 = 1 << 0;
pub const LANDLOCK_ACCESS_FS_WRITE_FILE: u64 = 1 << 1;
pub const LANDLOCK_ACCESS_FS_READ_FILE: u64 = 1 << 2;
pub const LANDLOCK_ACCESS_FS_READ_DIR: u64 = 1 << 3;
pub const LANDLOCK_ACCESS_FS_REMOVE_DIR: u64 = 1 << 4;
pub const LANDLOCK_ACCESS_FS_REMOVE_FILE: u64 = 1 << 5;
pub const LANDLOCK_ACCESS_FS_MAKE_CHAR: u64 = 1 << 6;
pub const LANDLOCK_ACCESS_FS_MAKE_DIR: u64 = 1 << 7;
pub const LANDLOCK_ACCESS_FS_MAKE_REG: u64 = 1 << 8;
pub const LANDLOCK_ACCESS_FS_MAKE_SOCK: u64 = 1 << 9;
pub const LANDLOCK_ACCESS_FS_MAKE_FIFO: u64 = 1 << 10;
pub const LANDLOCK_ACCESS_FS_MAKE_BLOCK: u64 = 1 << 11;
pub const LANDLOCK_ACCESS_FS_MAKE_SYM: u64 = 1 << 12;
pub const LANDLOCK_ACCESS_FS_REFER: u64 = 1 << 13;
pub const LANDLOCK_ACCESS_FS_TRUNCATE: u64 = 1 << 14;
pub const LANDLOCK_ACCESS_FS_IOCTL_DEV: u64 = 1 << 15;

pub const LANDLOCK_ACCESS_NET_BIND_TCP: u64 = 1 << 0;
pub const LANDLOCK_ACCESS_NET_CONNECT_TCP: u64 = 1 << 1;

pub const LANDLOCK_SCOPE_ABSTRACT_UNIX_SOCKET: u64 = 1 << 0;
pub const LANDLOCK_SCOPE_SIGNAL: u64 = 1 << 1;

pub const SYS_landlock_create_ruleset: c_long = 444;
pub const SYS_landlock_add_rule: c_long = 445;
pub const SYS_landlock_restrict_self: c_long = 446;

#[must_use]
pub unsafe fn landlock_create_ruleset(
    attr: *const landlock_ruleset_attr,
    size: c_size_t,
    flags: u32,
) -> c_int {
    unsafe { libc::syscall(SYS_landlock_create_ruleset, attr, size, flags) as c_int }
}

#[must_use]
pub unsafe fn landlock_add_rule(
    ruleset_fd: c_int,
    rule_type: landlock_rule_type,
    rule_attr: *const c_void,
    flags: u32,
) -> c_int {
    unsafe {
        libc::syscall(
            SYS_landlock_add_rule,
            ruleset_fd,
            rule_type,
            rule_attr,
            flags,
        ) as c_int
    }
}

#[must_use]
pub unsafe fn landlock_restrict_self(ruleset_fd: c_int, flags: u32) -> c_int {
    unsafe { libc::syscall(SYS_landlock_restrict_self, ruleset_fd, flags) as c_int }
}
