/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

//! Landlock - unprivileged access-control

use std::error::Error as StdError;
use std::ffi::c_void;
use std::io::Error as IoError;
use std::os::unix::prelude::*;
use std::path::Path;
use std::sync::atomic::{AtomicI32, Ordering};
use std::{fmt, ptr};

use bitflags::bitflags;
use capctl::{Cap, CapState};
use rustix::fs::{CWD, Mode, OFlags, openat};

use crate::landlock_sys::*;

mod landlock_sys;

#[derive(Debug, Clone, Copy)]
pub struct RulesetAttr {
    ruleset_attr: landlock_ruleset_attr,
}
impl RulesetAttr {
    pub fn new() -> Self {
        Self {
            ruleset_attr: landlock_ruleset_attr {
                handled_access_fs: 0,
                handled_access_net: 0,
                scoped: 0,
            },
        }
    }

    pub fn set_handled_access_fs(
        &mut self,
        handled_access_fs: AccessFs,
    ) -> Result<&mut Self, LandlockError> {
        handled_access_fs.ensure_available()?;
        self.ruleset_attr.handled_access_fs = handled_access_fs.bits();
        Ok(self)
    }

    pub fn set_handled_access_net(
        &mut self,
        handled_access_net: AccessNet,
    ) -> Result<&mut Self, LandlockError> {
        handled_access_net.ensure_available()?;
        self.ruleset_attr.handled_access_net = handled_access_net.bits();
        Ok(self)
    }

    pub fn set_scoped(&mut self, scoped: Scoped) -> Result<&mut Self, LandlockError> {
        scoped.ensure_available()?;
        self.ruleset_attr.scoped = scoped.bits();
        Ok(self)
    }

    pub fn create_ruleset(&self) -> Result<Ruleset, LandlockError> {
        let ruleset_fd = unsafe {
            landlock_create_ruleset(&self.ruleset_attr, size_of::<landlock_ruleset_attr>(), 0)
        };

        if ruleset_fd < 0 {
            let error_kind = match errno() {
                libc::ENOSYS => LandlockErrorKind::Unsupported,
                libc::EOPNOTSUPP => LandlockErrorKind::Disabled,
                libc::EINVAL | libc::E2BIG | libc::EFAULT | libc::ENOMSG => {
                    LandlockErrorKind::InternalFault
                }
                _ => LandlockErrorKind::UnexpectedErrno,
            };

            return Err(LandlockError {
                kind: error_kind,
                source: None,
                errno: Some(errno()),
            });
        }

        Ok(Ruleset {
            ruleset_fd: unsafe { OwnedFd::from_raw_fd(ruleset_fd) },
        })
    }
}
impl Default for RulesetAttr {
    fn default() -> Self {
        Self::new()
    }
}

/// A Landlock ruleset.
#[derive(Debug)]
pub struct Ruleset {
    ruleset_fd: OwnedFd,
}
impl Ruleset {
    /// Creates a `landlock_path_beneath` rule and adds it to the ruleset.
    ///
    /// # Examples
    ///
    /// ```
    /// # use landlock::*;
    /// let mut ruleset = RulesetAttr::new()
    ///     .set_handled_access_fs(AccessFs::all_available())?
    ///     .set_handled_access_net(AccessNet::all())?
    ///     .create_ruleset()?;
    /// ruleset.add_path_beneath_rule(
    ///     AccessFs::EXECUTE | AccessFs::READ_FILE | AccessFs::READ_DIR,
    ///     ParentFd::open("/usr")?,
    /// )?;
    /// ruleset.add_net_port_rule(
    ///     AccessNet::CONNECT_TCP,
    ///     443,
    /// )?;
    /// # Ok::<(), Box<dyn std::error::Error>>(())
    /// ```
    pub fn add_path_beneath_rule<FD: AsFd + AsRawFd>(
        &mut self,
        allowed_access: AccessFs,
        parent_fd: ParentFd<FD>,
    ) -> Result<&mut Self, LandlockError> {
        allowed_access.ensure_available()?;
        let path_beneath = landlock_path_beneath_attr {
            allowed_access: allowed_access.bits(),
            parent_fd: parent_fd.as_raw_fd(),
        };
        let rv = unsafe {
            landlock_add_rule(
                self.ruleset_fd.as_raw_fd(),
                LANDLOCK_RULE_PATH_BENEATH,
                (&raw const path_beneath).cast::<c_void>(),
                0,
            )
        };
        if rv != 0 {
            let error_kind = match errno() {
                libc::ENOSYS => LandlockErrorKind::Unsupported,
                libc::EOPNOTSUPP => LandlockErrorKind::Disabled,
                libc::EINVAL => LandlockErrorKind::InvalidAddPathBeneathRuleArgument,
                libc::ENOMSG | libc::EBADF | libc::EBADFD | libc::EPERM | libc::EFAULT => {
                    LandlockErrorKind::InternalFault
                }
                _ => LandlockErrorKind::UnexpectedErrno,
            };

            return Err(LandlockError {
                kind: error_kind,
                source: None,
                errno: Some(errno()),
            });
        }

        Ok(self)
    }

    /// Creates a `landlock_net_port` rule and adds it to the ruleset.
    ///
    /// # Examples
    ///
    /// ```
    /// # use landlock::*;
    /// if abi_version() >= 4 {
    ///     let mut ruleset = RulesetAttr::new()
    ///         .set_handled_access_net(AccessNet::CONNECT_TCP)?
    ///         .create_ruleset()?;
    ///     ruleset.add_net_port_rule(
    ///         AccessNet::CONNECT_TCP,
    ///         443,
    ///     )?;
    /// }
    /// # Ok::<(), Box<dyn std::error::Error>>(())
    /// ```
    pub fn add_net_port_rule(
        &mut self,
        allowed_access: AccessNet,
        port: u16,
    ) -> Result<&mut Self, LandlockError> {
        allowed_access.ensure_available()?;
        let net_port = landlock_net_port_attr {
            allowed_access: allowed_access.bits(),
            port: port as u64,
        };
        let rv = unsafe {
            landlock_add_rule(
                self.ruleset_fd.as_raw_fd(),
                LANDLOCK_RULE_NET_PORT,
                (&raw const net_port).cast::<c_void>(),
                0,
            )
        };
        if rv != 0 {
            let error_kind = match errno() {
                libc::ENOSYS => LandlockErrorKind::Unsupported,
                libc::EOPNOTSUPP => LandlockErrorKind::Disabled,
                libc::EINVAL
                | libc::ENOMSG
                | libc::EBADF
                | libc::EBADFD
                | libc::EPERM
                | libc::EFAULT => LandlockErrorKind::InternalFault,
                _ => LandlockErrorKind::UnexpectedErrno,
            };

            return Err(LandlockError {
                kind: error_kind,
                source: None,
                errno: Some(errno()),
            });
        }

        Ok(self)
    }

    /// Enforces this ruleset for the current thread.
    ///
    /// # Examples
    ///
    /// ```
    /// # use landlock::*;
    /// let mut ruleset = RulesetAttr::new()
    ///     .set_handled_access_fs(AccessFs::all_available())?
    ///     .create_ruleset()?;
    /// ruleset.add_path_beneath_rule(
    ///     AccessFs::EXECUTE | AccessFs::READ_FILE | AccessFs::READ_DIR,
    ///     ParentFd::open("/usr")?,
    /// )?;
    /// ruleset.enforce()?;
    /// # Ok::<(), Box<dyn std::error::Error>>(())
    /// ```
    pub fn enforce(self) -> Result<(), LandlockError> {
        if !CapState::get_current()?.permitted.has(Cap::SYS_ADMIN) {
            capctl::set_no_new_privs()?;
        }

        let rv = unsafe { landlock_restrict_self(self.ruleset_fd.as_raw_fd(), 0) };
        if rv != 0 {
            let error_kind = match errno() {
                libc::ENOSYS => LandlockErrorKind::Unsupported,
                libc::EOPNOTSUPP => LandlockErrorKind::Disabled,
                libc::E2BIG => unimplemented!(),
                libc::EINVAL | libc::EBADF | libc::EBADFD | libc::EPERM => {
                    LandlockErrorKind::InternalFault
                }
                _ => LandlockErrorKind::UnexpectedErrno,
            };

            return Err(LandlockError {
                kind: error_kind,
                source: None,
                errno: Some(errno()),
            });
        }

        Ok(())
    }
}

bitflags! {
    /// Filesystem actions
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct AccessFs: u64 {
        /// Execute a file.
        const EXECUTE = LANDLOCK_ACCESS_FS_EXECUTE;
        /// Open a file with write access.
        const WRITE_FILE = LANDLOCK_ACCESS_FS_WRITE_FILE;
        /// Open a file with read access.
        const READ_FILE = LANDLOCK_ACCESS_FS_READ_FILE;
        /// Open a directory or list its content.
        const READ_DIR = LANDLOCK_ACCESS_FS_READ_DIR;
        /// Remove an empty directory or rename one.
        const REMOVE_DIR = LANDLOCK_ACCESS_FS_REMOVE_DIR;
        /// Unlink (or rename) a file.
        const REMOVE_FILE = LANDLOCK_ACCESS_FS_REMOVE_FILE;
        /// Create (or rename or link) a character.
        const MAKE_CHAR = LANDLOCK_ACCESS_FS_MAKE_CHAR;
        /// Create (or rename) a directory.
        const MAKE_DIR = LANDLOCK_ACCESS_FS_MAKE_DIR;
        /// Create (or rename or link) a regular file.
        const MAKE_REG = LANDLOCK_ACCESS_FS_MAKE_REG;
        /// Create (or rename or link) a UNIX domain socket.
        const MAKE_SOCK = LANDLOCK_ACCESS_FS_MAKE_SOCK;
        /// Create (or rename or link) a named pipe.
        const MAKE_FIFO = LANDLOCK_ACCESS_FS_MAKE_FIFO;
        /// Create (or rename or link) a block device.
        const MAKE_BLOCK = LANDLOCK_ACCESS_FS_MAKE_BLOCK;
        /// Create (or rename or link) a symbolic link.
        const MAKE_SYM = LANDLOCK_ACCESS_FS_MAKE_SYM;
        /// Link or rename a file from or to a different directory. (ABIv2)
        const REFER = LANDLOCK_ACCESS_FS_REFER;
        /// Truncate a file with `truncate(2)`,`ftruncate(2)`, `creat(2)`, or `open(2)` with `O_TRUNC`. (ABIv3)
        const TRUNCATE = LANDLOCK_ACCESS_FS_TRUNCATE;
        /// Invoke `ioctl(2)` commands on an opened character or block device.
        const IOCTL_DEV = LANDLOCK_ACCESS_FS_IOCTL_DEV;

        const ALL_v1 = Self::EXECUTE.bits()
            | Self::WRITE_FILE.bits()
            | Self::READ_FILE.bits()
            | Self::READ_DIR.bits()
            | Self::REMOVE_DIR.bits()
            | Self::REMOVE_FILE.bits()
            | Self::MAKE_CHAR.bits()
            | Self::MAKE_DIR.bits()
            | Self::MAKE_REG.bits()
            | Self::MAKE_SOCK.bits()
            | Self::MAKE_FIFO.bits()
            | Self::MAKE_BLOCK.bits()
            | Self::MAKE_SYM.bits();
        const ALL_v2 = Self::ALL_v1.bits() | Self::REFER.bits();
        const ALL_v3 = Self::ALL_v2.bits() | Self::TRUNCATE.bits();
        const ALL_v5 = Self::ALL_v3.bits() | Self::IOCTL_DEV.bits();
    }
}
impl AccessFs {
    #[must_use]
    pub fn all_available() -> Self {
        match abi_version() {
            1 => Self::ALL_v1,
            2 => Self::ALL_v2,
            // ABIv4 did not any new AccessFs flags.
            3 | 4 => Self::ALL_v3,
            _ => Self::ALL_v5,
        }
    }

    #[must_use]
    pub fn remove_unavailable(self) -> Self {
        self & Self::all_available()
    }

    #[must_use]
    pub fn is_available(self) -> bool {
        Self::all_available().contains(self)
    }

    fn ensure_available(self) -> Result<(), LandlockError> {
        if self.is_available() {
            Ok(())
        } else {
            Err(LandlockError {
                kind: LandlockErrorKind::UnavailableAccessFs,
                source: None,
                errno: Some(libc::EINVAL),
            })
        }
    }
}

bitflags! {
    /// Network actions
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct AccessNet: u64 {
        /// Bind a TCP socket to a local port.
        const BIND_TCP = LANDLOCK_ACCESS_NET_BIND_TCP;
        /// Connect an active TCP socket to a remote port.
        const CONNECT_TCP = LANDLOCK_ACCESS_NET_CONNECT_TCP;
    }
}
impl AccessNet {
    #[must_use]
    pub fn is_available(self) -> bool {
        abi_version() >= 4
    }

    fn ensure_available(self) -> Result<(), LandlockError> {
        if self.is_empty() || self.is_available() {
            Ok(())
        } else {
            Err(LandlockError {
                kind: LandlockErrorKind::UnavailableAccessNet,
                source: None,
                errno: Some(libc::EINVAL),
            })
        }
    }
}

bitflags! {
    /// Scoped IPC actions
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct Scoped: u64 {
        /// Scope abstract UNIX sockets
        const ABSTRACT_UNIX_SOCKET = LANDLOCK_SCOPE_ABSTRACT_UNIX_SOCKET;
        /// Scope signals
        const SIGNAL = LANDLOCK_SCOPE_SIGNAL;
    }
}
impl Scoped {
    #[must_use]
    pub fn is_available(self) -> bool {
        abi_version() >= 6
    }

    fn ensure_available(self) -> Result<(), LandlockError> {
        if self.is_empty() || self.is_available() {
            Ok(())
        } else {
            Err(LandlockError {
                kind: LandlockErrorKind::UnavailableScoped,
                source: None,
                errno: Some(libc::EINVAL),
            })
        }
    }
}

/// File descriptor which identifies a file hierarchy
#[derive(Debug)]
pub struct ParentFd<FD: AsFd + AsRawFd>(FD);
impl ParentFd<OwnedFd> {
    /// Opens a path
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self, IoError> {
        Ok(Self(openat(
            CWD,
            path.as_ref(),
            OFlags::CLOEXEC | OFlags::PATH,
            Mode::empty(),
        )?))
    }

    /// Opens a path
    pub fn open_at<FD: AsFd, P: AsRef<Path>>(dirfd: FD, path: P) -> Result<Self, IoError> {
        Ok(Self(openat(
            dirfd.as_fd(),
            path.as_ref(),
            OFlags::CLOEXEC | OFlags::PATH,
            Mode::empty(),
        )?))
    }
}
impl From<OwnedFd> for ParentFd<OwnedFd> {
    fn from(fd: OwnedFd) -> Self {
        Self(fd)
    }
}
impl<'a> From<BorrowedFd<'a>> for ParentFd<BorrowedFd<'a>> {
    fn from(fd: BorrowedFd<'a>) -> Self {
        Self(fd)
    }
}
impl<FD: AsFd + AsRawFd> AsFd for ParentFd<FD> {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.0.as_fd()
    }
}
impl<FD: AsFd + AsRawFd> AsRawFd for ParentFd<FD> {
    fn as_raw_fd(&self) -> RawFd {
        self.0.as_raw_fd()
    }
}

/// Returns the highest landlock ABI version supported by the running kernel.
///
/// # Panics
///
/// This functions panics if the running kernel does not support Landlock.
pub fn abi_version() -> i32 {
    static CACHED_ABI_VERSION: AtomicI32 = AtomicI32::new(0);

    let cached_abi_version = CACHED_ABI_VERSION.load(Ordering::Relaxed);
    if cached_abi_version != 0 {
        return cached_abi_version;
    }

    let abi_version =
        unsafe { landlock_create_ruleset(ptr::null(), 0, LANDLOCK_CREATE_RULESET_VERSION) };
    if abi_version < 1 {
        match errno() {
            libc::EPERM => panic!("Permission denied"),
            libc::ENOSYS => panic!("Landlock is not supported by the kernel"),
            libc::EOPNOTSUPP => {
                panic!("Landlock is supported by the kernel but disabled at boot time")
            }
            _ => unreachable!(),
        }
    }

    CACHED_ABI_VERSION.store(abi_version, Ordering::Relaxed);

    abi_version
}

/// Landlock error type
#[derive(Debug)]
pub struct LandlockError {
    kind: LandlockErrorKind,
    source: Option<Box<dyn StdError + 'static>>,
    errno: Option<i32>,
}
impl LandlockError {
    #[must_use]
    pub fn kind(&self) -> &LandlockErrorKind {
        &self.kind
    }

    #[must_use]
    pub fn errno(&self) -> Option<i32> {
        self.errno
    }
}
impl StdError for LandlockError {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        self.source.as_deref()
    }
}
impl fmt::Display for LandlockError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.kind == LandlockErrorKind::Io {
            write!(f, "{}: {}", self.kind, self.source().unwrap())
        } else {
            write!(f, "{}", self.kind)
        }
    }
}
impl From<std::ffi::NulError> for LandlockError {
    fn from(err: std::ffi::NulError) -> Self {
        Self {
            kind: LandlockErrorKind::NulError,
            source: Some(Box::new(err)),
            errno: None,
        }
    }
}
impl From<std::io::Error> for LandlockError {
    fn from(err: std::io::Error) -> Self {
        Self {
            kind: LandlockErrorKind::Io,
            source: Some(Box::new(err)),
            errno: None,
        }
    }
}
impl From<capctl::Error> for LandlockError {
    fn from(err: capctl::Error) -> Self {
        Self {
            kind: LandlockErrorKind::CapCtl,
            source: Some(Box::new(err)),
            errno: None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum LandlockErrorKind {
    /// Landlock is not supported by the current kernel.
    Unsupported,
    /// Landlock is supported by the kernel but disabled at boot time.
    Disabled,
    /// Internal implementation fault.
    InternalFault,
    /// A syscall returned an error which wasn't expected.
    UnexpectedErrno,
    /// Unavailable `AccessFs` flag(s).
    UnavailableAccessFs,
    /// Unavailable `AccessNet` flag(s).
    UnavailableAccessNet,
    /// Unavailable `Scoped` flag(s).
    UnavailableScoped,
    /// An invalid argument was passed to `add_path_beneath_rule`.
    ///
    /// A common reason for this is that `allowed_access`
    ///  * is not a subset of `handled_access_fs`.
    ///  * contains a flag that is invalid for the given path such as
    ///    `MAKE_*` on a non-directory path.
    InvalidAddPathBeneathRuleArgument,
    /// I/O Error.
    ///
    /// To get the underlying error use
    /// ```ignore
    /// err.downcast_ref::<std::io::Error>().unwrap()
    /// ```
    Io,
    NulError,
    CapCtl,
}
impl fmt::Display for LandlockErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Unsupported => "Landlock is not supported by the current kernel.",
            Self::Disabled => "Landlock is supported by the kernel but disabled at boot time.",
            Self::InternalFault => "Internal implementation fault.",
            Self::UnexpectedErrno => "A syscall returned an error which wasn't expected.",
            Self::UnavailableAccessFs => "Unavailable AccessFs flag(s).",
            Self::UnavailableAccessNet => "Unavailable AccessNet flag(s).",
            Self::UnavailableScoped => "Unavailable Scoped flag(s).",
            Self::InvalidAddPathBeneathRuleArgument => concat!(
                "An invalid argument was passed to add_path_beneath_rule.\n",
                "A common reason for this is that allowed_access\n",
                " * is not a subset of handled_access_fs.\n",
                " * contains a flag that is invalid for the given path such as MAKE_* on a non-directory path."
            ),
            Self::Io => "I/O Error",
            Self::NulError => "NulError",
            Self::CapCtl => "capctl Error",
        };
        f.write_str(msg)
    }
}

fn errno() -> i32 {
    unsafe { *libc::__errno_location() }
}
