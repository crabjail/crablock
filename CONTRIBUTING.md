# Contributing

**Table Of Contents**

- [Building](#building)
- [Coding style](#coding-style)
  - [Python](#python)
  - [Rust](#rust)
- [Architecture](#architecture)

## Building

While we use meson to build crablock for production. However, the recommended
way to build crablock during development is to use cargo directly.

## Coding style

### Python

Run [black](https://pypi.org/project/black/) to format your code,
[isort](https://pypi.org/project/isort/) to format your imports
and pylint to catch some common mistakes before committing.

Also read [PEP8](https://www.python.org/dev/peps/pep-0008/),
you will be a better programmer.

### Rust

Run `cargo fmt` to format your code before committing. Keep in mind that this
will not format macros, you have to format them yourself.

If you use rust-analyzer you should configure the following options:

```
rust-analyzer.imports.granularity.group = "module"
rust-analyzer.imports.preferPrelude = true
```

Also run `cargo clippy` and fix all of the warnings/errors related to your code
or allow them if you believe that they are false positives. `cargo clippy`
should run without warnings/errors except for warnings/errors caused by
newer/older versions of clippy. Moreover you should look if
`cargo clippy -- --warn clippy::pedantic --warn clippy::nursery` reports
warnings/errors which seem to be correct.

<!-- Outdated, https://codeberg.org/crabjail/crablock/issues/51
## Architecture

Overview over processes/threads and the synchronization/events/IPC between them:

```mermaid
```
-->

## Adding command-line arguments

New command-line arguments must be added to
- the usage string.
- the copy of the usage string in the README.md.
- the manpage.
- the shell completion files.

## Tips

- Running `cargo run --features=debug -- ...` enables some useful debug macros.
