---

name: "Question"
about: "Do NOT ask questions here!"
title: "DO NOT ASK QUESTIONS HERE!"
labels:

- "Kind: Question"

---

# DO NOT ASK QUESTIONS HERE!

This is an issue tracker used to organize crablocks development.
If you have questions you can ask them [here](https://codeberg.org/crabjail/crablock-questions/issues/new).
