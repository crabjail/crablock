#![expect(
    clippy::needless_borrows_for_generic_args,
    reason = "https://github.com/rust-lang/rust-clippy/issues/13162"
)]

use std::env::current_dir;
use std::fs;
use std::fs::Permissions;
use std::os::unix::prelude::PermissionsExt;
use std::path::PathBuf;
use std::sync::LazyLock;
use std::time::Duration;

use assert_cmd::Command;
use assert_cmd::cargo::cargo_bin;
use assert_fs::TempDir;
use assert_fs::prelude::{FileTouch, FileWriteStr, PathAssert, PathChild, PathCreateDir};
use rustix::fs::{AtFlags, CWD, StatxFlags, statx};
use rustix::process::getsid;

mod utils;
use utils::*;

static CL_TEST_BIN: LazyLock<PathBuf> = LazyLock::new(|| cargo_bin("cl-test"));

static PWD: LazyString = LazyString::new(|| current_dir().unwrap().to_str().unwrap().to_string());

static CGROUP_NS: LazyString = LazyString::new(|| read_ns_link("cgroup"));
static IPC_NS: LazyString = LazyString::new(|| read_ns_link("ipc"));
static MNT_NS: LazyString = LazyString::new(|| read_ns_link("mnt"));
static NET_NS: LazyString = LazyString::new(|| read_ns_link("net"));
static PID_NS: LazyString = LazyString::new(|| read_ns_link("pid"));
static USER_NS: LazyString = LazyString::new(|| read_ns_link("user"));
static UTS_NS: LazyString = LazyString::new(|| read_ns_link("uts"));

static CGROUP2_MNT_ID: LazyString = LazyString::new(|| {
    statx(CWD, "/sys/fs/cgroup", AtFlags::empty(), StatxFlags::MNT_ID)
        .expect("statx /sys/fs/cgroup")
        .stx_mnt_id
        .to_string()
});
static DEVPTS_DEV_MINOR: LazyString = LazyString::new(|| {
    statx(CWD, "/dev/pts", AtFlags::empty(), StatxFlags::empty())
        .expect("statx /dev/pts")
        .stx_dev_minor
        .to_string()
});
static MQUEUE_DEV_MINOR: LazyString = LazyString::new(|| {
    statx(CWD, "/dev/mqueue", AtFlags::empty(), StatxFlags::empty())
        .expect("statx /dev/mqueue")
        .stx_dev_minor
        .to_string()
});
static PROC_DEV_MINOR: LazyString = LazyString::new(|| {
    statx(CWD, "/proc", AtFlags::empty(), StatxFlags::empty())
        .expect("statx /proc")
        .stx_dev_minor
        .to_string()
});
static SYSFS_DEV_MINOR: LazyString = LazyString::new(|| {
    statx(CWD, "/sys", AtFlags::empty(), StatxFlags::empty())
        .expect("statx /sys")
        .stx_dev_minor
        .to_string()
});

macro_rules! f {
    ($($arg:tt)*) => { &format!($($arg)*) };
}

/// Boilerplate for a single test case.
///
/// ```
/// #[test]
/// fn $name() -> Result<(), Box<dyn std::error::Error>> {
///     $body
///
///     Ok(())
/// }
/// ```
macro_rules! test {
    ($(#[$meta:meta])* $name:ident => $body:tt) => {
        #[allow(non_snake_case, reason = "Some tests for case have uppercase letters in their name")]
        #[test]
        $(#[$meta])*
        fn $name() -> Result<(), Box<dyn std::error::Error>> {
            $body

            Ok(())
        }
    };
}

/// Boilerplate for multiple test cases.
macro_rules! tests {
    ($($(#[$meta:meta])* $name:ident => $body:tt)*) => {
        $(test!($(#[$meta])* $name => $body);)*
    };
}

/// Boilerplate for running crablock with cl-test.
macro_rules! cl_test {
    ($name:ident, $crablock_args:expr, $cl_test_args:expr $(,)?) => {
        test! {
            $name => {
                cl_test!($crablock_args, $cl_test_args);
            }
        }
    };
    ($crablock_args:expr, $cl_test_args:expr $(,)?) => {
        Command::cargo_bin("crablock")?
            .args($crablock_args)
            .arg("--")
            .arg(&*CL_TEST_BIN)
            .args($cl_test_args)
            .timeout(Duration::from_secs(1))
            .assert()
            .success();
    };
}

tests! {
    simple_run => {
        Command::cargo_bin("crablock")?.args(&["--", "true"]).assert().success();
    }

    run_cl_test => {
        Command::cargo_bin("crablock")?.arg("--").arg(&*CL_TEST_BIN).assert().success();
    }
}

// TODO: --help, --version, --args-lf, --args-nul, --sandbox-name

cl_test!(
    unshare_cgroup,
    ["--unshare", "cgroup"],
    [f!("--assert-unshare-cgroup={CGROUP_NS}")],
);

cl_test!(
    unshare_ipc,
    ["--unshare", "ipc"],
    [f!("--assert-unshare-ipc={IPC_NS}")],
);

cl_test!(
    unshare_mnt,
    ["--unshare", "mnt"],
    [f!("--assert-unshare-mnt={MNT_NS}")],
);

cl_test!(
    unshare_net,
    ["--unshare", "net"],
    [f!("--assert-unshare-net={NET_NS}")],
);

cl_test!(
    unshare_pid,
    ["--unshare", "pid"],
    [f!("--assert-unshare-pid={PID_NS}")],
);

cl_test!(
    unshare_user,
    ["--unshare", "user"],
    [f!("--assert-unshare-user={USER_NS}")],
);

cl_test!(
    unshare_uts,
    ["--unshare", "uts"],
    [f!("--assert-unshare-uts={UTS_NS}")],
);

cl_test!(
    unshare_all_except_net,
    ["--unshare", "all,-net"],
    [
        f!("--assert-unshare-cgroup={CGROUP_NS}"),
        f!("--assert-unshare-ipc={IPC_NS}"),
        f!("--assert-unshare-mnt={MNT_NS}"),
        f!("--assert-unshare-pid={PID_NS}"),
        f!("--assert-unshare-user={USER_NS}"),
        f!("--assert-unshare-uts={UTS_NS}"),
        f!("--assert-share-net={NET_NS}"),
    ],
);

cl_test!(
    unshare_all_except_net_split,
    ["--unshare", "all", "--unshare", "-net"],
    [
        f!("--assert-unshare-cgroup={CGROUP_NS}"),
        f!("--assert-unshare-ipc={IPC_NS}"),
        f!("--assert-unshare-mnt={MNT_NS}"),
        f!("--assert-unshare-pid={PID_NS}"),
        f!("--assert-unshare-user={USER_NS}"),
        f!("--assert-unshare-uts={UTS_NS}"),
        f!("--assert-share-net={NET_NS}"),
    ],
);

cl_test!(
    unshare_cgroup_mount_cgroup2,
    ["--unshare", "cgroup"],
    [
        f!("--assert-unshare-cgroup={CGROUP_NS}"),
        f!("--assert-mount-cgroup2={CGROUP2_MNT_ID}"),
    ],
);

cl_test!(
    unshare_net_mount_sysfs,
    ["--unshare", "net"],
    [
        f!("--assert-unshare-net={NET_NS}"),
        f!("--assert-mount-sysfs={SYSFS_DEV_MINOR}"),
    ],
);

cl_test!(
    unshare_pid_mount_proc,
    ["--unshare", "pid"],
    [
        f!("--assert-unshare-pid={PID_NS}"),
        f!("--assert-mount-proc={PROC_DEV_MINOR}"),
        "--assert-procfs-no-subset",
        "--assert-procfs-rw",
    ],
);

// TODO: --uid, --gid, --map-uids, --map-gids, --setuid, --setgid

cl_test!(
    capabilities_none,
    ["--capabilities", "none"],
    [
        "--assert-capabilities-eff-empty",
        "--assert-capabilities-bnd-empty",
        "--assert-capabilities-amb-empty",
    ],
);

cl_test!(
    capabilities_NONE,
    ["--capabilities", "NONE"],
    [
        "--assert-capabilities-eff-empty",
        "--assert-capabilities-bnd-empty",
        "--assert-capabilities-amb-empty",
    ],
);

cl_test!(
    capabilities_ALL,
    ["--capabilities", "ALL"],
    [
        "--assert-capabilities-eff=CAP_AUDIT_CONTROL,CAP_AUDIT_READ,CAP_AUDIT_WRITE,CAP_BLOCK_SUSPEND,CAP_BPF,CAP_CHECKPOINT_RESTORE,CAP_CHOWN,CAP_DAC_OVERRIDE,CAP_DAC_READ_SEARCH,CAP_FOWNER,CAP_FSETID,CAP_IPC_LOCK,CAP_IPC_OWNER,CAP_KILL,CAP_LEASE,CAP_LINUX_IMMUTABLE,CAP_MAC_ADMIN,CAP_MAC_OVERRIDE,CAP_MKNOD,CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_BROADCAST,CAP_NET_RAW,CAP_PERFMON,CAP_SETFCAP,CAP_SETGID,CAP_SETPCAP,CAP_SETUID,CAP_SYS_ADMIN,CAP_SYS_BOOT,CAP_SYS_CHROOT,CAP_SYSLOG,CAP_SYS_MODULE,CAP_SYS_NICE,CAP_SYS_PACCT,CAP_SYS_PTRACE,CAP_SYS_RAWIO,CAP_SYS_RESOURCE,CAP_SYS_TIME,CAP_SYS_TTY_CONFIG,CAP_WAKE_ALARM",
        "--assert-capabilities-bnd=CAP_AUDIT_CONTROL,CAP_AUDIT_READ,CAP_AUDIT_WRITE,CAP_BLOCK_SUSPEND,CAP_BPF,CAP_CHECKPOINT_RESTORE,CAP_CHOWN,CAP_DAC_OVERRIDE,CAP_DAC_READ_SEARCH,CAP_FOWNER,CAP_FSETID,CAP_IPC_LOCK,CAP_IPC_OWNER,CAP_KILL,CAP_LEASE,CAP_LINUX_IMMUTABLE,CAP_MAC_ADMIN,CAP_MAC_OVERRIDE,CAP_MKNOD,CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_BROADCAST,CAP_NET_RAW,CAP_PERFMON,CAP_SETFCAP,CAP_SETGID,CAP_SETPCAP,CAP_SETUID,CAP_SYS_ADMIN,CAP_SYS_BOOT,CAP_SYS_CHROOT,CAP_SYSLOG,CAP_SYS_MODULE,CAP_SYS_NICE,CAP_SYS_PACCT,CAP_SYS_PTRACE,CAP_SYS_RAWIO,CAP_SYS_RESOURCE,CAP_SYS_TIME,CAP_SYS_TTY_CONFIG,CAP_WAKE_ALARM",
        "--assert-capabilities-amb=CAP_AUDIT_CONTROL,CAP_AUDIT_READ,CAP_AUDIT_WRITE,CAP_BLOCK_SUSPEND,CAP_BPF,CAP_CHECKPOINT_RESTORE,CAP_CHOWN,CAP_DAC_OVERRIDE,CAP_DAC_READ_SEARCH,CAP_FOWNER,CAP_FSETID,CAP_IPC_LOCK,CAP_IPC_OWNER,CAP_KILL,CAP_LEASE,CAP_LINUX_IMMUTABLE,CAP_MAC_ADMIN,CAP_MAC_OVERRIDE,CAP_MKNOD,CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_BROADCAST,CAP_NET_RAW,CAP_PERFMON,CAP_SETFCAP,CAP_SETGID,CAP_SETPCAP,CAP_SETUID,CAP_SYS_ADMIN,CAP_SYS_BOOT,CAP_SYS_CHROOT,CAP_SYSLOG,CAP_SYS_MODULE,CAP_SYS_NICE,CAP_SYS_PACCT,CAP_SYS_PTRACE,CAP_SYS_RAWIO,CAP_SYS_RESOURCE,CAP_SYS_TIME,CAP_SYS_TTY_CONFIG,CAP_WAKE_ALARM",
    ],
);

cl_test!(
    capabilities_caps,
    [
        "--capabilities",
        "CAP_NET_BIND_SERVICE,CAP_SETGID,CAP_SETUID"
    ],
    [
        "--assert-capabilities-eff=CAP_NET_BIND_SERVICE,CAP_SETGID,CAP_SETUID",
        "--assert-capabilities-bnd=CAP_NET_BIND_SERVICE,CAP_SETGID,CAP_SETUID",
        "--assert-capabilities-amb=CAP_NET_BIND_SERVICE,CAP_SETGID,CAP_SETUID",
    ],
);

// TODO: --securebits=

cl_test!(no_new_privs, ["--no-new-privs"], ["--assert-no-new-privs"]);

cl_test!(
    mount_devpts,
    ["--mount-devpts"],
    [f!("--assert-mount-devpts={DEVPTS_DEV_MINOR}")],
);

cl_test!(
    mount_mqueue,
    ["--mount-mqueue"],
    [f!("--assert-mount-mqueue={MQUEUE_DEV_MINOR}")],
);

cl_test!(
    mount_proc,
    ["--mount-proc"],
    [
        f!("--assert-mount-proc={PROC_DEV_MINOR}"),
        "--assert-procfs-subset=pid",
        "--assert-procfs-ro",
    ],
);

cl_test!(
    mount_proc_subset_none,
    ["--mount-proc", "--mount-proc-subset", "none"],
    [
        f!("--assert-mount-proc={PROC_DEV_MINOR}"),
        "--assert-procfs-no-subset",
        "--assert-procfs-ro",
    ],
);

cl_test!(
    mount_proc_read_write,
    ["--mount-proc", "--mount-proc-read-write"],
    [
        f!("--assert-mount-proc={PROC_DEV_MINOR}"),
        "--assert-procfs-subset=pid",
        "--assert-procfs-rw",
    ],
);

// TODO: --fsm-mount, --fsm-bind, --fsm-rbind, --fsm-move-mount, --fsm-pivot-root, --fsm-mount-setattr, --fsm-chdir, --fsm-chroot

tests! {
    mnt_mask => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();

        cl_test!(
            ["--mnt-mask", temp_path],
            [
                f!("--assert-tmpfs={temp_path}"),
                // f!("--assert-super-option= mode=750 {temp_path}"),
                // f!("--assert-mount-option-ro={temp_path}"), // or nr_inodes=
                f!("--assert-mount-option-nosuid={temp_path}"),
                f!("--assert-mount-option-nodev={temp_path}"),
                f!("--assert-mount-option-noexec={temp_path}"),
            ],
        );
    }

    // mnt_mask_root => {
    //     unimplemented!();
    // }

    // mnt_mask_dev => {
    //     unimplemented!();
    // }

    mnt_private_volatile => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();

        cl_test!(
            ["--mnt-private-volatile", temp_path],
            [
                f!("--assert-tmpfs={temp_path}"),
                // f!("--assert-super-option= mode=750 {temp_path}"),
                f!("--assert-mount-option-rw={temp_path}"),
                f!("--assert-mount-option-nosuid={temp_path}"),
                f!("--assert-mount-option-nodev={temp_path}"),
                f!("--assert-mount-option-noexec={temp_path}"),
            ],
        );
    }

    mnt_private_volatile_beneath_mask => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();
        let child = temp.child("a/b/c");
        let child_path = child.path().to_str().unwrap();
        child.create_dir_all()?;

        cl_test!(
            [
                "--mnt-mask",
                temp_path,
                "--mnt-private-volatile",
                child_path,
            ],
            [
                f!("--assert-tmpfs={temp_path}"),
                f!("--assert-tmpfs={child_path}"),
            ],
        );
    }

    // mnt_private_root => {
    //     unimplemented!();
    // }

    mnt_private => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();
        let crablock_data_home = temp.child("crablock_data_home");
        let crablock_data_home_path = crablock_data_home.path().to_str().unwrap();
        let make_me_private = temp.child("make_me_private");
        let make_me_private_path = make_me_private.path().to_str().unwrap();
        make_me_private.create_dir_all()?;

        cl_test!(
            [
                "--crablock-data-home",
                crablock_data_home_path,
                "--sandbox-name",
                "mnt_private",
                "--mnt-private",
                make_me_private_path,
            ],
            [
                f!("--assert-mountpoint={make_me_private_path}"),
                f!("--assert-mount-option-rw={make_me_private_path}"),
                f!("--assert-mount-option-nosuid={make_me_private_path}"),
                f!("--assert-mount-option-nodev={make_me_private_path}"),
                f!("--assert-mount-option-exec={make_me_private_path}"),
            ],
        );

        crablock_data_home
            .child(f!("mnt_private/private/{temp_path}"))
            .assert(predicates::path::is_dir());
    }

    // mnt_allow_volatile => {
    //     unimplemented!();
    // }

    // mnt_allow_volatile_from => {
    //     unimplemented!();
    // }

    // mnt_allow_from => {
    //     unimplemented!();
    // }

    // mnt_allow_from_root => {
    //     unimplemented!();
    // }

    // mnt_allow => {
    //     unimplemented!();
    // }

    mnt_deny => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();

        cl_test!(
            ["--mnt-deny", temp_path],
            [f!("--assert-mount-option-ro={temp_path}")],
        );
    }

    mnt_ro_rw => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();
        let child = temp.child("a/b");
        let child_path = child.path().to_str().unwrap();
        child.create_dir_all()?;

        cl_test!(
            [
                "--mnt-ro",
                temp_path,
                "--mnt-rw",
                child_path,
            ],
            [
                f!("--assert-mount-option-ro={temp_path}"),
                f!("--assert-mount-option-rw={child_path}"),
            ],
        );
    }

    mnt_ro_rw_root => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();
        let temp2 = TempDir::new()?;
        let temp2_path = temp2.path().to_str().unwrap();

        cl_test!(
            [
                "--mnt-ro",
                "/",
                "--mnt-rw",
                temp_path,
                "--mnt-private-volatile",
                temp2_path,
            ],
            [
                f!("--assert-mount-option-ro=/"),
                f!("--assert-mount-option-rw={temp_path}"),
                f!("--assert-mount-option-ro={temp2_path}"),
            ],
        );
    }

    // TempDir must be mounted suid.
    // mnt_nosuid_suid => {
    //     let temp = TempDir::new()?;
    //     let temp_path = temp.path().to_str().unwrap();
    //     let child = temp.child("a/b");
    //     let child_path = child.path().to_str().unwrap();
    //     child.create_dir_all()?;
    //
    //     cl_test!(
    //         [
    //             "--mnt-nosuid",
    //             temp_path,
    //             "--mnt-suid",
    //             child_path,
    //         ],
    //         [
    //             f!("--assert-mount-option-nosuid={temp_path}"),
    //             f!("--assert-mount-option-suid={child_path}"),
    //         ],
    //     );
    // }
    //
    // // mnt_nosuid_suid_root => {
    // //     unimplemented!();
    // // }

    // TempDir must be mounted dev.
    // mnt_nodev_dev => {
    //     let temp = TempDir::new()?;
    //     let temp_path = temp.path().to_str().unwrap();
    //     let child = temp.child("a/b");
    //     let child_path = child.path().to_str().unwrap();
    //     child.create_dir_all()?;
    //
    //     cl_test!(
    //         [
    //             "--mnt-nodev",
    //             temp_path,
    //             "--mnt-dev",
    //             child_path,
    //         ],
    //         [
    //             f!("--assert-mount-option-nodev={temp_path}"),
    //             f!("--assert-mount-option-dev={child_path}"),
    //         ],
    //     );
    // }
    //
    // // mnt_nodev_dev_root => {
    // //     unimplemented!();
    // // }

    mnt_noexec_exec => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();
        let child = temp.child("a/b");
        let child_path = child.path().to_str().unwrap();
        child.create_dir_all()?;

        cl_test!(
            [
                "--mnt-noexec",
                temp_path,
                "--mnt-exec",
                child_path,
            ],
            [
                f!("--assert-mount-option-noexec={temp_path}"),
                f!("--assert-mount-option-exec={child_path}"),
            ],
        );
    }

    // mnt_noexec_exec_root => {
    //     unimplemented!();
    // }
}

// TODO: --cwd

cl_test!(hostname, ["--hostname", "test"], ["--assert-hostname=test"]);

cl_test!(pasta, ["--pasta"], [] as [&str; 0]);
cl_test!(
    pasta_and_namespaces_limits,
    [
        "--pasta",
        "--max-cgroup-namespaces",
        "0",
        "--max-ipc-namespaces",
        "0",
        "--max-mnt-namespaces",
        "0",
        "--max-net-namespaces",
        "0",
        "--max-pid-namespaces",
        "0",
        "--max-time-namespaces",
        "0",
        "--max-user-namespaces",
        "0",
        "--max-uts-namespaces",
        "0",
    ],
    [] as [&str; 0],
);
// TODO: --pasta-*

tests! {
    landlock_not_allowed => {
        let temp = TempDir::new()?;
        let temp_path = temp.path().to_str().unwrap();
        let file = temp.child("file");
        let file_path = file.path().to_str().unwrap();
        file.touch()?;
        let dir = temp.child("dir");
        let dir_path = dir.path().to_str().unwrap();
        dir.create_dir_all()?;
        let exec = temp.child("exec");
        let exec_path = exec.path().to_str().unwrap();
        exec.write_str("#!/bin/bash\n")?;
        fs::set_permissions(&exec, Permissions::from_mode(0o755))?;

        cl_test!(
            [
                "--landlock-fs-unrestricted-path", &PWD,
                "--landlock-fs-unrestricted-path", "/usr",
                "--landlock-fs-unrestricted-path", "/proc",
            ],
            [
                f!("--assert-not-open-rdonly={file_path}"),
                f!("--assert-not-open-wronly={file_path}"),
                f!("--assert-not-open-wronly-trunc={file_path}"),
                f!("--assert-not-truncate={file_path}"),
                f!("--assert-not-open-rdwr={file_path}"),
                f!("--assert-not-execve={exec_path}"),
                f!("--assert-not-open-directory={dir_path}"),
                f!("--assert-not-rmdir={dir_path}"),
                f!("--assert-not-unlink={file_path}"),
                f!("--assert-not-mkdir={temp_path}/dir2"),
                f!("--assert-not-open-creat-excl={temp_path}/file2"),
                f!("--assert-not-symlink={temp_path}/symlink"),
            ],
        );
    }

    // TODO: --landlock-fs-*, --ll-*
}

cl_test!(
    landlock_restrict_bind_tcp_empty,
    [
        "--unshare",
        "net",
        "--capabilities",
        "CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_RAW",
        "--landlock-restrict-bind-tcp",
        "none"
    ],
    [
        "--assert-not-bind-tcp=127.0.0.1:8080",
        "--assert-connect-tcp=127.0.0.1:80",
    ]
);

cl_test!(
    landlock_restrict_bind_tcp_8080,
    [
        "--unshare",
        "net",
        "--capabilities",
        "CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_RAW",
        "--landlock-restrict-bind-tcp",
        "8080",
    ],
    [
        "--assert-bind-tcp=127.0.0.1:8080",
        "--assert-not-bind-tcp=127.0.0.1:8081",
        "--assert-connect-tcp=127.0.0.1:80",
    ]
);

cl_test!(
    landlock_restrict_connect_tcp_empty,
    [
        "--unshare",
        "net",
        "--capabilities",
        "CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_RAW",
        "--landlock-restrict-connect-tcp",
        "none",
    ],
    [
        "--assert-not-connect-tcp=127.0.0.1:80",
        "--assert-bind-tcp=127.0.0.1:8080",
    ]
);

cl_test!(
    landlock_restrict_connect_tcp_80,
    [
        "--unshare",
        "net",
        "--capabilities",
        "CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_RAW",
        "--landlock-restrict-connect-tcp",
        "80",
    ],
    [
        "--assert-connect-tcp=127.0.0.1:80",
        "--assert-not-connect-tcp=127.0.0.1:81",
        "--assert-bind-tcp=127.0.0.1:8080",
    ]
);

cl_test!(
    landlock_restrict_bind_connect_tcp,
    [
        "--unshare",
        "net",
        "--capabilities",
        "CAP_NET_ADMIN,CAP_NET_BIND_SERVICE,CAP_NET_RAW",
        "--landlock-restrict-bind-tcp",
        "8080",
        "--landlock-restrict-connect-tcp",
        "80",
    ],
    [
        "--assert-bind-tcp=127.0.0.1:8080",
        "--assert-not-bind-tcp=127.0.0.1:8081",
        "--assert-connect-tcp=127.0.0.1:80",
        "--assert-not-connect-tcp=127.0.0.1:81",
    ]
);

// TODO: --landlock-scope-abstract-unix-socket and --landlock-scope-signal

// TODO: seccomp default action
// TODO: seccomp action kill

cl_test!(
    seccomp_syscall_filter_action_eperm,
    ["--seccomp-syscall-filter", "*:allow,mkdirat:EPERM"],
    ["--assert-seccomp-action-eperm=mkdirat"],
);

cl_test!(
    seccomp_syscall_filter_action_enosys,
    ["--seccomp-syscall-filter", "*:allow,mkdirat:ENOSYS"],
    ["--assert-seccomp-action-enosys=mkdirat"],
);

// TODO: seccomp action log_allow

// TODO: seccomp-flatpak
// TODO: seccomp restrict ioctl,prctl,socket
// TODO: memfd_noexec, mdwe, deny-newuser, deny-execve-null

cl_test!(
    max_cgroup_namespaces,
    ["--max-cgroup-namespaces", "0"],
    ["--assert-sysctl=user.max_cgroup_namespaces=0"]
);
cl_test!(
    max_ipc_namespaces,
    ["--max-ipc-namespaces", "0"],
    ["--assert-sysctl=user.max_ipc_namespaces=0"]
);
cl_test!(
    max_mnt_namespaces,
    ["--max-mnt-namespaces", "0"],
    ["--assert-sysctl=user.max_mnt_namespaces=0"]
);
cl_test!(
    max_net_namespaces,
    ["--max-net-namespaces", "0"],
    ["--assert-sysctl=user.max_net_namespaces=0"]
);
cl_test!(
    max_pid_namespaces,
    ["--max-pid-namespaces", "0"],
    ["--assert-sysctl=user.max_pid_namespaces=0"]
);
cl_test!(
    max_time_namespaces,
    ["--max-time-namespaces", "0"],
    ["--assert-sysctl=user.max_time_namespaces=0"]
);
cl_test!(
    max_user_namespaces,
    ["--max-user-namespaces", "0"],
    ["--assert-sysctl=user.max_user_namespaces=0"]
);
cl_test!(
    max_uts_namespaces,
    ["--max-uts-namespaces", "0"],
    ["--assert-sysctl=user.max_uts_namespaces=0"]
);

cl_test!(
    choom_100,
    ["--choom", "100"],
    ["--assert-oom-score-adj=100"]
);
cl_test!(
    choom_1000,
    ["--choom", "1000"],
    ["--assert-oom-score-adj=1000"],
);

cl_test!(nice_0, ["--nice", "0"], ["--assert-nice=0"]);
cl_test!(nice_19, ["--nice", "19"], ["--assert-nice=19"]);
cl_test!(nice_20, ["--nice", "20"], ["--assert-nice=19"]);

tests! {
    clearenv => {
        Command::cargo_bin("crablock")?
            .envs([("KEEPENV", "keepenv"), ("UNSETENV", "unsetenv")])
            .args([
                "--clearenv",
                "--keepenv", "KEEPENV",
                "--setenv", "SETENV=setenv",
                // Necessary when built with '-C instrument-coverage'.
                "--keepenv", "LLVM_PROFILE_FILE",
            ])
            .arg("--")
            .arg(&*CL_TEST_BIN)
            .args([
                "--assert-env-set=KEEPENV=keepenv",
                "--assert-env-set=SETENV=setenv",
                "--assert-env-not-set=UNSETENV",
            ])
            .assert().success();
    }

    env => {
        Command::cargo_bin("crablock")?
            .envs([("KEEPENV", "keepenv"), ("UNSETENV", "unsetenv")])
            .args(["--setenv", "SETENV=setenv", "--unsetenv", "UNSETENV"])
            .arg("--")
            .arg(&*CL_TEST_BIN)
            .args([
                "--assert-env-set=KEEPENV=keepenv",
                "--assert-env-set=SETENV=setenv",
                "--assert-env-not-set=UNSETENV",
            ])
            .assert().success();
    }
}

cl_test!(
    mdwe_refuse_exec_gain,
    ["--mdwe-refuse-exec-gain"],
    ["--assert-mdwe-refuse-exec-gain"]
);
cl_test!(
    new_session,
    ["--new-session"],
    [f!(
        "--assert-new-session={}",
        getsid(None)?.as_raw_nonzero(),
    )]
);
// FIXME: Why does this fail?
// cl_test!(
//     strict_mitigations,
//     ["--strict-mitigations"],
//     ["--assert-strict-mitigations"]
// );
cl_test!(umask, ["--umask", "077"], ["--assert-umask=077"]);

cl_test!(
    custom_init,
    ["--unshare", "pid", "--custom-init"],
    ["--assert-is-pid1"],
);
cl_test!(
    no_custom_init,
    ["--unshare", "pid"],
    ["--assert-is-not-pid1"],
);

// TODO: --dbus-proxy

cl_test!(
    full,
    [
        "--unshare",
        "all",
        "--capabilities",
        "none",
        "--no-new-privs",
        "--mount-proc-subset",
        "none",
        "--mount-proc-read-only",
        "--landlock-restrict-bind-tcp",
        "8080",
        "--landlock-restrict-connect-tcp",
        "80,443",
        "--max-cgroup-namespaces",
        "0",
        "--max-ipc-namespaces",
        "0",
        "--max-mnt-namespaces",
        "0",
        "--max-net-namespaces",
        "0",
        "--max-pid-namespaces",
        "0",
        "--max-time-namespaces",
        "0",
        "--max-user-namespaces",
        "0",
        "--max-uts-namespaces",
        "0",
        "--choom",
        "1000",
        "--nice",
        "19",
        "--mdwe-refuse-exec-gain",
        "--new-session",
        "--strict-mitigations",
    ],
    [
        f!("--assert-unshare-cgroup={CGROUP_NS}"),
        f!("--assert-unshare-ipc={IPC_NS}"),
        f!("--assert-unshare-mnt={MNT_NS}"),
        f!("--assert-unshare-net={NET_NS}"),
        f!("--assert-unshare-pid={PID_NS}"),
        f!("--assert-unshare-user={USER_NS}"),
        f!("--assert-unshare-uts={UTS_NS}"),
        "--assert-capabilities-eff-empty",
        "--assert-capabilities-bnd-empty",
        "--assert-capabilities-amb-empty",
        "--assert-no-new-privs",
        f!("--assert-mount-proc={PROC_DEV_MINOR}"),
        "--assert-procfs-no-subset",
        "--assert-procfs-ro",
        f!("--assert-mount-sysfs={SYSFS_DEV_MINOR}"),
        "--assert-bind-tcp=127.0.0.1:8080",
        "--assert-not-bind-tcp=127.0.0.1:8081",
        "--assert-connect-tcp=127.0.0.1:80",
        "--assert-not-connect-tcp=127.0.0.1:81",
        "--assert-sysctl=user.max_cgroup_namespaces=0",
        "--assert-sysctl=user.max_ipc_namespaces=0",
        "--assert-sysctl=user.max_mnt_namespaces=0",
        "--assert-sysctl=user.max_net_namespaces=0",
        "--assert-sysctl=user.max_pid_namespaces=0",
        "--assert-sysctl=user.max_time_namespaces=0",
        "--assert-sysctl=user.max_user_namespaces=0",
        "--assert-sysctl=user.max_uts_namespaces=0",
        "--assert-oom-score-adj=1000",
        "--assert-nice=19",
        "--assert-mdwe-refuse-exec-gain",
        f!("--assert-new-session={}", getsid(None)?.as_raw_nonzero()),
        // FIXME: See above.
        // "--assert-strict-mitigations",
    ],
);
