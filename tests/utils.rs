use std::fmt;
use std::ops::Deref;
use std::sync::LazyLock;

use rustix::fs::readlink;

/// A `LazyLock<String>` that implements `Display`.
#[derive(Debug)]
pub struct LazyString(LazyLock<String>);
impl LazyString {
    pub const fn new(f: fn() -> String) -> Self {
        Self(LazyLock::new(f))
    }
}
impl fmt::Display for LazyString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}
impl Deref for LazyString {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.0.deref()
    }
}

pub fn read_ns_link(namespace: &str) -> String {
    readlink(format!("/proc/self/ns/{namespace}"), [])
        .unwrap()
        .into_string()
        .unwrap()
}
