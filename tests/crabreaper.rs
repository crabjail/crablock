#![expect(
    clippy::needless_borrows_for_generic_args,
    reason = "https://github.com/rust-lang/rust-clippy/issues/13162"
)]

use assert_cmd::Command;

#[test]
fn crabreaper() {
    Command::cargo_bin("crabreaper")
        .unwrap()
        .args(&["echo", "ok"])
        .assert()
        .stdout("ok\n");
}

#[test]
fn crabreaper_ec_signal() {
    Command::cargo_bin("crabreaper")
        .unwrap()
        .args(&["sh", "-c", "kill -TERM $$"])
        .assert()
        .code(128 + libc::SIGTERM);
}

#[test]
fn crabreaper_ec_signal_with_trap() {
    Command::cargo_bin("crabreaper")
        .unwrap()
        .args(&["sh", "-c", "trap 'exit 4' TERM && kill -TERM $$"])
        .assert()
        .code(4);
}

#[test]
fn crabreaper_ec_exit() {
    Command::cargo_bin("crabreaper")
        .unwrap()
        .args(&["sh", "-c", "exit 9"])
        .assert()
        .code(9);
}
