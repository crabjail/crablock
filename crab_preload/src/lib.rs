/*
 * Copyright © 2022-2025 The crablock Authors
 *
 * This file is part of crablock
 *
 * crablock is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * crablock is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

use std::fs::File;
use std::io::prelude::*;
use std::os::unix::prelude::*;
use std::{env, process, slice};

use lnix::seccomp::{SeccompFilterFlags, SockFProg, set_mode_filter};

const CRAB_PRELOAD_SKIP_IN_PID1: &str = "CRAB_PRELOAD_SKIP_IN_PID1";
const CRAB_PRELOAD_NOEXEC_FD: &str = "CRAB_PRELOAD_NOEXEC_FD";

#[used]
#[unsafe(link_section = ".init_array")]
static CRABLOADER_INIT_FN: extern "C" fn() = _init;
extern "C" fn _init() {
    if std::panic::catch_unwind(init).is_err() {
        std::process::abort();
    }
}

fn init() {
    if env::var_os(CRAB_PRELOAD_SKIP_IN_PID1).is_some() && process::id() == 1 {
        unsafe { env::remove_var(CRAB_PRELOAD_SKIP_IN_PID1) };
        return;
    }

    load_noexec_seccomp_filter();
}

fn load_noexec_seccomp_filter() {
    let Some(fd_no) = env::var_os(CRAB_PRELOAD_NOEXEC_FD) else {
        return;
    };
    unsafe { env::remove_var(CRAB_PRELOAD_NOEXEC_FD) };
    let fd_no = fd_no
        .to_str()
        .expect("fd_no.to_str")
        .parse::<i32>()
        .expect("fd_no.parse");

    let mut buf: Vec<u8> = Vec::new();
    unsafe { File::from_raw_fd(fd_no) }
        .read_to_end(&mut buf)
        .expect("FIXME");

    assert!(buf.as_ptr() as usize % align_of::<libc::sock_filter>() == 0);
    assert!(buf.len() % size_of::<libc::sock_filter>() == 0);

    #[expect(clippy::cast_ptr_alignment, reason = "Alignment was just asserted")]
    let ptr: *mut libc::sock_filter = buf.as_mut_ptr().cast::<libc::sock_filter>();
    let len: usize = buf.len() / size_of::<libc::sock_filter>();

    let filter: &mut [libc::sock_filter] = unsafe { slice::from_raw_parts_mut(ptr, len) };

    let fprog = SockFProg::new(filter);

    set_mode_filter(SeccompFilterFlags::LOG, &fprog).expect("set_mode_filter");
}
